<?php
require_once("Config.php");
require_once("Includes/index.php");

$_obj_database = new database($_host_db,$_user_db,$_password_db,$_db);

$_obj_interfaz = new Interfaz();

$_obj_mensajes = new Mensajes();

$_obj_interfaz->adicionarCSS("style.css");
$_obj_interfaz->adicionarCSS("font-awesome.min.css");

//Si no se definio un modulo toma por defecto usuarios
$datos['m'] = ( isset($datos['m']) ?  $datos['m'] : "Usuarios");

$datos['m'] = strtolower($datos['m']);

$datos['plantilla'] = 'principal';
$_obj_interfaz->asignarContenidoPrincipal($datos);
$_obj_interfaz->asignarContenidoTopBar($datos);

//si el parametro no es un directorio valido deja por defecto el de usuario
if( !is_dir($datos['m']) )
{
    $datos['m'] = "Usuarios";
}

require_once($datos['m']."/index.php");

print_r( $_obj_interfaz->crearInterfazGrafica($datos) );

if( $_SESSION['autenticado']!=1 )
{
    header('Location: ');
}

$_obj_database->desconectar();

?>
