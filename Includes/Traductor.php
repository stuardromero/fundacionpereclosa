<?php

class Traductor{



  function Traductor(){



	}

	/**
	 * Funcion que se encarga de traducir un texto a partir de los parametros de idioma
	 * de entrada y salida. La funcion se carga desde el api de google para la version 2.0
	 * */
	function TraducirV2($original, $origen = "en", $destino = "es", $retraducir = "no", $debug = "")
    {
        global $_KEY_GOOGLE_TRADUCTOR;

        //DM - 2015-06-17 issue 6726
        $destino = str_replace('br','pt',$destino);

        $original = str_replace(" ","%20",$original);
        $url = "https://www.googleapis.com/language/translate/v2?key=".$_KEY_GOOGLE_TRADUCTOR;
        $url .= "&target=".$destino;
        $url .= "&q=".$original;
        $texto = @file($url);
                
        if(is_array($texto))
        {
	        $html      = implode('', $texto);
	        $html = str_replace("\u0026","&",$html);
	        $exp_info   = "/\"translatedText\": \"([^\"]+)\"/i";
	        //preg_match_all($exp_info, utf8_decode($html), $resultado);
            //DM - 2013-08-02
            //se retorna la traduccion en utf8
            preg_match_all($exp_info, $html, $resultado);
	        $resultado = $resultado[1][0];

	        if($resultado != ''){
	          if($debug == ""){
	            return array("original" => $original, "traduccion" => $resultado, "origen" => $origen, "destino" => $destino, "url" => $url);
	          } else {
	            return $resultado;
	          }//Fin de if($debug == "")
	        } else {
	          if($debug == ""){
	            return array("error" => "No se pudo traducir el texto <b>".$original."</b>. Compruebe que existe texto a traducir o que el sistema funciona aqui: http://translate.google.com/translate_t");
	          } else {
	                return false;
	          }//Fin de if($debug == "")
	        }//Fin de if($resultado != '')
        }//Fin de if(is_array($texto))

		return false;
    }//Fin de TraducirV2()

	/**
	 * Funcion que se encarga de traducir un texto a partir de los parametros de idioma
	 * de entrada y salida. La funcion se carga desde el api de google para la version 1.0
	 * */
    function Traducir($original, $origen = "en", $destino = "es", $retraducir = "no", $debug = "")
    {

        //Utiliza la version 2 para traducciones
        return $this->TraducirV2($original, $origen, $destino, $retraducir, $debug);

        $conversiones_validas = array(

      "de"    => array("fr" => "1","en" => "1"),
      "ar"    => array("en" => "1"),
      "zh"    => array("en" => "1"),
      "zh-CN" => array("zh-TW" => "1"),
      "zh-TW" => array("zh-CN" => "1"),
      "ko"    => array("en" => "1"),
      "es"    => array("en" => "1","fr"=>"1","de"=>"1"),
      "fr"    => array("en" => "1","de" => "1"),
      "en"=> array(
                "de" => "1",
                "ar" => "1",
                "zh-CN" => "1",
                "zh-TW" => "1",
                "ko" => "1",
                "fr" => "1",
                "it" => "1",
                "ja" => "1",
                "pt" => "1",
                "ru" => "1"
                ),
      "it"    => array("en" => "1"),
      "ja"    => array("en" => "1"),
      "pt"    => array("en" => "1"),
      "ru"    => array("en" => "1")
        );

    //if($conversiones_validas[$origen][$destino] == "1"){
        $url        = "http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=".urlencode(utf8_encode($original));
        //$url       .= '&langpair='.$origen.'|'.$destino.'&hl=es&ie=UTF8';
        $url       .= '&langpair=|'.$destino.'&hl=es&ie=UTF8';
        $texto    = @file($url);
        $html      = implode('', $texto);

        $html = str_replace("\u0026","&",$html);

        $exp_info   = "/{\"translatedText\":\"([^\"]+)\"/i";
        preg_match_all($exp_info, utf8_decode($html), $resultado);
        $resultado = $resultado[1][0];
        if($resultado != ''){
          if($debug == ""){
            return array("original" => $original, "traduccion" => $resultado, "origen" => $origen, "destino" => $destino, "url" => $url);
          } else {
            return $resultado;
          }//Fin de if($debug == "")
        } else {
          if($debug == ""){
            return array("error" => "No se pudo traducir el texto <b>".$original."</b>. Compruebe que existe texto a traducir o que el sistema funciona aqui: http://translate.google.com/translate_t");
          } else {
                return false;
          }//Fin de if($debug == "")
        }//Fin de if($resultado != '')

    /*} else {

        if($retraducir == "no"){

            return false;

        } else {

            if(strtolower($origen) == strtolower($destino)){

                if($debug == ""){

                    return array("error" => "El idioma de origen y destino son el mismo");

                } else {

                    return false;

                }//Fin de if($debug == "")

            } else {

                $texto = $this->Traducir($original, $origen, "en", $retraducir);

                $texto = $this->Traducir(strtolower($texto["traduccion"]), "en", $destino, $retraducir);

                $url  = "http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=".urlencode(utf8_encode($original));

                $url .= '&langpair='.$origen.'|'.$destino.'&hl=es&ie=UTF8';

                if($debug == ""){

                    return array("original" => $original, "traduccion" => $texto["traduccion"], "origen" => $origen, "destino" => $destino, "url" => $url);

                } else {

                    return $resultado;

                }//Fin de if($debug == "")

            }//Fin de if(strtolower($origen) == strtolower($destino))

        }//Fin de if($retraducir == "no")

    }//Fin de if($conversiones_validas[$origen][$destino] == "1")  */

  }//Fin de function traducir($original, $origen = "en", $destino = "es", $retraducir = "no", $debug = "")

}//Fin de clase Traductor

?>
