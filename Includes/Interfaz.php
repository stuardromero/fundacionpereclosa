<?php

class Interfaz {

    var $_obj_menu;
    var $_obj_mensajes;
    var $_l_js;
    var $_l_js_footer;
    var $_listado_js_externos;
    var $_listado_js_externos_footer;
    var $_l_css;
    var $_l_css_footer;
    var $_listado_css_externos;
    var $_listado_css_externos_footer;
    var $_plantillas;
    var $_migas;
    var $_contenido;
    var $_contenido_externo;
    var $_contenido_externo_general;
    var $_contenido_popup_automatico;
    var $_seccion;
    var $_link_ayuda;
    var $_titulo;
    var $_logo;
    var $_url_logo_establecimiento;
    var $_info;
    var $_propiedades;
    var $_campos;
    var $_calendario;
    var $_print_media;

    function __construct() {
        global $_PATH_SERVIDOR, $_opciones;

        $this->_plantillas = $_PATH_SERVIDOR . '/Includes/Plantillas';

        $this->_l_js = array();
        $this->_l_js_footer = array();
        $this->_listado_js_externos = array();
        $this->_listado_js_externos_footer = array();
        $this->_l_css = array();
        $this->_listado_css_externos = array();
        $this->_l_css_footer = array();
        $this->_listado_css_externos_footer = array();
        $this->_obj_menu = new Menu();
        $this->_migas = array();
        $this->_contenido = '';
        $this->_contenido_externo = '';
        $this->_contenido_externo_general = '';
        $this->_calendario = array();
        $this->_pasos_labels = "";
        $this->_seccion = '';
        $this->_url_logo_establecimiento = '';
        $this->_contenido_popup_automatico = '';
        $this->_selector_encuesta = array();

        $this->_campos = array();

        $this->adicionarCSS('general.css');
    }
    //Fin de Interfaz()

    /*
    * SA 01/12/2020
    * Funcion que carga la vista del footer general 
    */
    function asignarContenidoPrincipal($datos) {
        global $_PATH_PLANTILLAS;
    
        $contenido = Archivos::obtenerContenidoArchivo($_PATH_PLANTILLAS . "/footer.html");
        $this->_contenido_footer = $contenido;
    }

    function asignarContenidoTopBar($datos) {
        global $_PATH_PLANTILLAS;
    
        $contenido = Archivos::obtenerContenidoArchivo($_PATH_PLANTILLAS . "/top-bar.html");
        Interfaz::asignarToken('nombre', $_SESSION['usu_nombre'], $contenido);
        Interfaz::asignarToken('usu_id', $_SESSION['usu_id'], $contenido);
        $this->_contenido_top_bar = $contenido;
    }

    /**
     * Se encarga de agregar un campo a la lista de
     * tokens que se van a usar en la plantilla
     * de la interfaz
     * DM - 2016-09-22
     **/
    function agregarTokenInterfaz($datos)
    {
        if( is_array($datos) )
        {
            $this->_campos = array_merge( $this->_campos, $datos );
        }
    }//Fin de agregarTokenInterfaz

    function asignarCampos($campos) {
        $this->_campos = $campos;
    }

    function asignarClasePrintBody($tipo) {
        $this->_print_media = $tipo;
    }

//Fin de asignarCampos()

    /**
     * Función que se encarga de asignar el contenido de un mensaje y desplegar solo
     * ese contendio en la interfaz
     * DM - 2013-09-23
     * */
    function asignarContenidoMensaje($datos) {
        global $_PATH_IMAGENES, $idi_despliegue;

        $contenido = '<br /><br /><br />' . $datos['contenido_mensaje'];

        $datos['label_boton'] = $datos['label_boton'] == '' ? $idi_despliegue['enlace_continuar'] : $datos['label_boton'];

        $contenido .= '<br />
        <center>
        <a class="enlace" ' . $datos['accion_boton'] . ' >' . $datos['label_boton'] . '</a>
        </center>
        ';

        $this->_contenido = $contenido;
    }

//Fin de asignarContenidoMensaje

    function asignarLogo($logo) {
        $this->_logo = $logo;
    }

    /**
     * Asigna la ubicación web del logo del establecimieto
     * DM - 2015-09-24
     * */
    function asignarURLLogoEstablecimiento($url_logo) {
        $this->_url_logo_establecimiento = $url_logo;
    }

//Fin de asignarURLLogoEstablecimiento

    function asignarTitulo($titulo) {
        $this->_titulo = $titulo;
    }

//Fin de asignarCampos()

    function asignarInfo($info) {
        $this->_info = $info;
    }

    function asignarPropiedades($propiedades) {
        $this->_propiedades = $propiedades;
    }

    //amp - 12/8/15 - migas de pan interfaz
    function asignarMigas($migas) {
        $this->_migas = $migas;
    }

    //amp - 19/10/15 - calendario general
    function asignarCalendario($cal) {
        $this->_calendario = $cal;
    }

    //amp - 19/10/15 - agrega selector de encuesta
    function asignarSelectorEncuesta($sel_encuesta) {
        $this->_selector_encuesta = $sel_encuesta;
    }
     //amp - 11/07/16 - calendario general
    function asignarLabelPasos($cal) {
        $this->_pasos_labels = $cal;
    }

    function adicionarJS($script, $usar_directorio_parametro = false) {
        //DM  - 2013-09-12 issue 2971
        if ($usar_directorio_parametro)
        {
            $this->_listado_js_externos[] = $script;
        } else {
            $this->_l_js[] = $script;
        }
    }

//Fin de adicionarJS()
    /*
     * Adicionar JS en el footer
     */

    function adicionarFooterJS($script, $usar_directorio_parametro = false) {
        //DM  - 2013-09-12 issue 2971
        if ($usar_directorio_parametro) {
            $this->_listado_js_externos_footer[] = $script;
        } else {
            $this->_l_js_footer[] = $script;
        }
    }

//Fin de adicionarFooterJS()

    /**
     * Se encarga se inicializar en ceros los arreglos de js
     * para que se vuelvan a cargar de acuerdo a los nuevos
     * parámetros
     * DM - 2014-06-16
     * */
    function resetJS() {
        $this->_listado_js_externos = array();
        $this->_l_js = array();
        $this->_l_js_footer = array();
        $this->_listado_js_externos_footer = array();
    }

//Fin de resetCSS

    /**
     * Se encarga se inicializar en ceros los arreglos de css
     * para que se vuelvan a cargar de acuerdo a los nuevos
     * parámetros
     * DM - 2014-06-16
     * */
    function resetCSS() {
        $this->_listado_css_externos = array();
        $this->_l_css = array();
        $this->_l_css_footer = array();
        $this->_listado_css_externos_footer = array();
    }

//Fin de resetCSS

    function adicionarCSS($estilo, $propiedades = null, $usar_directorio_parametro = false) {
        if ($usar_directorio_parametro) {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_listado_css_externos[] = $propiedades;
            } else {
                $this->_listado_css_externos[] = $estilo;
            }
        }//Fin de if( $usar_directorio_parametro )
        else {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_l_css[] = $propiedades;
            } else {
                $this->_l_css[] = $estilo;
            }
        }//Fin de else de if( $usar_directorio_parametro )
    }

//Fin de adicionarCSS()


    

    /*
     * AMP - 5/8/15 - Adecuar CSS en eñ footer merjorar despliegue de info
     */

    function adicionarFooterCSS($estilo, $propiedades = null, $usar_directorio_parametro = false) {
        if ($usar_directorio_parametro) {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_listado_css_externos_footer[] = $propiedades;
            } else {
                $this->_listado_css_externos_footer[] = $estilo;
            }
        }//Fin de if( $usar_directorio_parametro )
        else {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_l_css_footer[] = $propiedades;
            } else {
                $this->_l_css_footer[] = $estilo;
            }
        }//Fin de else de if( $usar_directorio_parametro )
    }

    function asignarContenido($contenido) {
        $this->_contenido = $contenido;
    }

    function asignarContenidoExterno($contenido) {
        $this->_contenido_externo = $contenido;
    }
    function asignarContenidoExternoGeneral($contenido) {
        $this->_contenido_externo_general = $contenido;
    }

//Fin de asignarContenido()

    function asignarSeccion($seccion) {
        global $_PATH_IMAGENES;

        $this->_seccion = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td class='titulo'>" . $seccion . "</td>
			</tr>
			<tr>
				<td height='20' valign='middle'><img src='" . $_PATH_IMAGENES . "/linea_titulo.jpg' width='760'></td>
			</tr>
			</table>";
    }//Fin de asignarSeccion()

    function crearInterfazGrafica($datos = array()) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_despliegue, $idi_alertas,
        $_opciones, $_PATH_PAG_WEB, $id_encuesta_hotel, $_obj_database,
        $obj_usuarios, $_google_analytics_connect, $_google_analytics_club,
        $_google_analytics_cicerone, $_PATH_SERVIDOR, $_PATH_WEB_LOGOS,$_CONFIG;

        $idioma_alertas = "";
        if (is_array($idi_alertas)) {
            //MAOH - 18 Abr 2012 - Cambio a UTF 8
            $idioma_alertas = "<script languague=\"javascript\" charset=\"utf-8\">\nvar idioma = new Array();\n";
            //$idioma_alertas = "<script languague=\"javascript\">\nvar idioma = new Array();\n";
            foreach ($idi_alertas as $key => $texto) {
                $texto = str_replace("\n", "\\n", $texto);
                $texto = str_replace("\'", "'", $texto);
                $texto = str_replace("'", "\'", $texto);
                //DM - 2015-02-19
                $idioma_alertas .= "idioma['" . $key . "'] = '" . $texto . "' ;\n";
            }//Fin de foreach($idi_alertas as $key => $texto)
            $idioma_alertas .= "</script>";
        }//Fin de if( is_array($idi_alertas) )
        //DM - 2013-09-24
        $estilos = '';
        $contenido_estilos = '';
        $propiedades_contenido_estilos = array();

        if ($datos['include_css'] == 1) {
            //$contenido_estilos .= Archivos::obtenerContenidoArchivo($_PATH_SERVIDOR.'librerias/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css');
        }//Fin de if( $datos['include_css'] == 1 )
        else {
            //$estilos .= "<link href=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }

        foreach ($this->_listado_css_externos as $estilo) {
            //DM - 2013-09-23
            if (is_array($estilo)) {
                //DM - 2014-06-16 issue 4595
                //Si el css se debe incluir en la plantilla html carga el archivo para agregarlo
                if ($estilo['include'] == 1) {
                    //Elimina la url y el parametro de include
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    unset($estilo['include']);

                    $url_archivo = $url_estilo;
                    $contenido_css = Archivos::obtenerContenidoArchivo($url_archivo);

                    $contenido_css = str_replace("\r\n", "", $contenido_css);
                    //agregar el contenido del archivo
                    $contenido_estilos .= $contenido_css;

                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_contenido_estilos[$propiedad] = $val_propiedad;
                    }
                }//Fin de if( $estilo['include'] == 1 )
                else {
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    $propiedades_estilo = '';
                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                    }
                    $estilos .= "<link href=\"" . $url_estilo . "\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
                }//Fin de else if( $estilo['include'] == 1 )
            } else {
                $estilos .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)


        foreach ($this->_l_css as $estilo)
        {
            if ($datos['include_css'] == 1 && $estilo == 'general.css')
            {
                $estilo = array(
                    'url' => $estilo,
                    'include' => 1);
            }//Fin de if( $datos['include_css'] == 1 && $estilo == 'general.css' )
            
            //DM - 2013-09-23
            if (is_array($estilo))
            {
                //DM - 2014-06-16 issue 4595
                //Si el css se debe incluir en la plantilla html carga el archivo para agregarlo
                if ($estilo['include'] == 1)
                {
                    //Elimina la url y el parametro de include
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    unset($estilo['include']);

                    $url_archivo = $_PATH_SERVIDOR . 'Includes/CSS/' . $url_estilo;
                    $contenido_css = Archivos::obtenerContenidoArchivo($url_archivo);

                    $contenido_css = str_replace("\r\n", "", $contenido_css);
                    //agregar el contenido del archivo
                    $contenido_estilos .= $contenido_css;

                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_contenido_estilos[$propiedad] = $val_propiedad;
                    }
                }//Fin de if( $estilo['include'] == 1 )
                else 
                {
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    $propiedades_estilo = '';
                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                    }
                    
                    //DM - 2017-09-12 issue 10647
                    //Obtiene la ultima modificación del fichero para generar la versión
                    $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $url_estilo;
                    $info_archivo = stat($src_archivo);
                    $version_archivo = $info_archivo['mtime'];
                    $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $url_estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
                }//Fin de else if( $estilo['include'] == 1 )
            } else
            {
                //DM - 2017-09-12 issue 10647
                //Obtiene la ultima modificación del fichero para generar la versión
                $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
                $info_archivo = @stat($src_archivo);
                $version_archivo = $info_archivo['mtime'];
                $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)

        /* amp - 5/8/15 - armar el html de los esilos para el footer */
        $estilos_footer = "";
        foreach ($this->_listado_css_externos_footer as $estilo) {

            $estilos_footer .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)


        foreach ($this->_l_css_footer as $estilo)
        {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $estilos_footer .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)
        
        //si hay contenido para agregar lo hace
        if ($contenido_estilos != '')
        {
            $propiedades_estilo = '';
            foreach ($propiedades_contenido_estilos as $propiedad => $val_propiedad) {
                $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '" ';
            }

            $estilos .= '<style type="text/css" ' . $propiedades_estilo . '>' . $contenido_estilos . '</style>' . "\n";
        }//Fin de if( $contenido_estilos != '')
        
        //DM - 2019-09-09 redmine[12852]
        // $api_key_publica_notificaciones_push = Herramientas::obtenerValorConfiguracion( array('codigo' => 'api_key_publica_notificaciones_push') );
        
        // $firebase_config = Herramientas::obtenerValorConfiguracion( array('codigo' => 'firebase_config') );

        //DM - 2019-09-18 redmine[12852]
        $key_usuario = '65*.-493'.$_SESSION['usu_id'].'&%5$.*-|~'.$_SESSION['usu_usc_id'] ;
        $key_usuario = md5($key_usuario); 
        $js = "<script languague=\"javascript\" charset=\"utf-8\">\n";
        $js .= "var _user_id = '".$_SESSION['usu_id']."';\n";
        $js .= "var _user_key = '".( $_SESSION['usu_id'] > 0 ? $key_usuario : '')."';\n";
        $js .= "var _pwa = '".$_SESSION['pwa']."';\n";
        $js .= "var _facebook_app_id = '".$_CONFIG['facebook_app_id']."';\n";
        $js .= "var _firebase_api_key = '".$api_key_publica_notificaciones_push."';\n";
        $js .= $firebase_config;
        $js .= "\n";
        $js .= "</script>\n";
        
                
        $js .= $idioma_alertas;
        foreach ($this->_l_js as $script)
        {         
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $js .= "<script src=\"" .$_PATH_WEB . "/Includes/JS/" . $script. "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            //$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";
        }//Fin de foreach($this->_js as $script)
        

        /*
        * SA 01/12/2020
        * Se cargan los scripts por defecto de la plantilla principal 
        * y se deja por si se deben agregar otros scripts personalizados.
        */
        $js_footer .= '<script src="Includes/JS/feather-icons/feather.min.js"></script>';
        $js_footer .= '<script src="Includes/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>';
        $js_footer .= '<script src="Includes/JS/app.js"></script>';
        $js_footer .= '<script src="Includes/vendors/simple-datatables/simple-datatables.js"></script>';
        $js_footer .= '<script src="Includes/vendors/chartjs/Chart.min.js"></script>
                        <!-- Include Choices JavaScript -->
                        <script src="Includes/vendors/choices.js/choices.min.js"></script>
                        <script src="Includes/vendors/choices.js/choices.js"></script>';
        $js_footer .= '<script src="Includes/JS/main.js"></script>';

        $js_footer .= '<script src="Includes/vendors/apexcharts/apexcharts.min.js"></script>';
        $js_footer .= '<script src="Includes/JS/vendors.js"></script>';
        $js_footer .= '<script src="Includes/JS/pages/dashboard.js"></script>';
        
        $js_footer .= '<script src="Includes/JS/jquery-3.5.1.min.js"></script>';
        
        $js_footer .= '<script src="Includes/JS/bootstrap-select.min.js"></script>';
        $js_footer .= '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>';

        $js_footer .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>';
        $js_footer .= '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>';
        $js_footer .= '<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />';
        
        $js_footer .= '<script src="Includes/JS/formulario.js"></script> ';
        $js_footer .= '<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>';
        $js_footer .= '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">';
        $js_footer .= '<script src="Includes/JS/gestion_usuarios.js"></script>';
        $js_footer .= '<script src="Includes/JS/gestion_intervenciones.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.1/xlsx.full.min.js"></script>';

        foreach ($this->_listado_js_externos_footer as $script)
        {
            $js_footer .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }
        
        foreach ($this->_l_js_footer as $script)
        {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime']; 
            $js_footer .= "<script src=\"" . $_PATH_WEB . "/Includes/JS/" . $script . "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }//Fin de foreach($this->_js_foter as $script)
        
        //DM  - 2013-09-12 issue 2971
        foreach ($this->_listado_js_externos as $script)
        {
            if( is_array($script) )
            {
                $js .= "<script src=\"" . $script['url'] . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\" ".$script['propiedades']." ></script>\n";
            }
            else
            {
                $js .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            }
            
        }//Fin de foreach($this->_listado_js_externos as $script)
        //$js .= "<script src=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";

        $plantilla = $datos['plantilla'];
        if ($plantilla == "") {
            $plantilla = "principal";
        }
        //DM - 2020-01-02 redmine[14291]
        if( $datos['contenido_plantilla_interfaz'] != '' ){
            $contenido = $datos['contenido_plantilla_interfaz']; 
        }
        else{
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/" . $plantilla . ".html");
        }
        


        //JP - 2014-04-19 #issue 4167
        Interfaz::asignarToken('google_analytics_connect', $_google_analytics_connect, $contenido);
        Interfaz::asignarToken('google_analytics_club', $_google_analytics_club, $contenido);
        Interfaz::asignarToken('google_analytics_cicerone', $_google_analytics_cicerone, $contenido);

        // $contenido_politicas_cookies = Herramientas::generarContenidoPoliticasCookies($datos);

        Interfaz::asignarToken('contenido_politicas_cookies', $contenido_politicas_cookies, $contenido);

        Interfaz::asignarToken('contenido_popup_automatico', $this->_contenido_popup_automatico, $contenido);
        if ($datos['despedida'] == 1)
        {
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/principal_despedida.html");
            
            if($datos['include'] == 1 )
            {
                Interfaz::asignarToken('css_color_fondo_despedida_include', 'background-color:transparent !important;', $contenido);
                Interfaz::asignarToken('css_footer_despedida_include', 'display:none;', $contenido);
                Interfaz::asignarToken('enlace_pie_pagina', '', $contenido);
            }
            else
            {
                Interfaz::asignarToken('css_footer_despedida_include', '', $contenido);
                Interfaz::asignarToken('css_color_fondo_despedida_include', 'background-color:#FFF;', $contenido);
                
                //DM - 2020-07-07 redmine[14932]
                $enlace_pie_pagina = '<span style="font-family:Montserrat;font-size:11px;color:#434343;">Powered by <a target="_blank" href="'.$_PATH_PAG_WEB.'" >HQ</a></span>';
                //si es para tablet, entonces no agrega el enlace para no modificar la congifuración de los carrousel
                if ($datos['dispositivo_movil'] && $datos['tipo_dispositivo'] == 't') {
                    $enlace_pie_pagina = '<span style="font-family:Montserrat;font-size:11px;color::#434343;">Powered by HQ</span>';
                }                        
            }//Fin de if($datos['include'] == 1 )

            Interfaz::asignarToken('enlace_pie_pagina', $enlace_pie_pagina, $contenido);
        }

        $menu = $this->_obj_menu->crearMenu();
        Interfaz::asignarToken('menu', $menu, $contenido);

        //MAOH - 18 Abr 2012 - Cambio a UTF 8
        Interfaz::asignarToken("codificacion", "UTF-8", $contenido);

        //Interfaz::asignarToken('datos',$datos,$contenido);

        Interfaz::asignarTokenDatos("label_pie_desarrollo", $idi_despliegue, $contenido);
        Interfaz::asignarTokenDatos("label_pie", $idi_despliegue, $contenido);
        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);
        Interfaz::asignarToken("logo", $this->_logo, $contenido);
        Interfaz::asignarToken("url_logo_establecimiento", $this->_url_logo_establecimiento, $contenido);
        Interfaz::asignarToken("titulo", $this->_titulo, $contenido);
        Interfaz::asignarToken("info", $this->_info, $contenido);
        Interfaz::asignarToken("propiedades", $this->_propiedades, $contenido);

        if (is_array($this->_campos)) {
            foreach ($this->_campos as $clave => $valor) {
                Interfaz::asignarToken($clave, $valor, $contenido);
            }
        }//Fin de if( is_array($this->_campos) )
        
        
        //amp -12/8/15 - gestioar migas de pan
        $listado_migas = "";
        if (is_array($this->_migas)) {
            foreach ($this->_migas as $clave => $valor) {
                $listado_migas .= "<li";
                $listado_migas .= (array_key_exists('activo', $valor)) ? " class='active' style='font-weight:bold; color:#676A6C;'>" : ">";

                $listado_migas .= (array_key_exists('m', $valor)) ?
                        "<a style='text-decoration:underline;' href='index.php?m=" .
                        $valor['m'] . "&accion=" .
                        $valor['accion'] .
                        ($datos['extra'] != '' ? "&".$datos['extra'] : "").
                         "' >" .
                        $valor['titulo'] . "</a>" : $valor['titulo'];
                $listado_migas .= "</li>";
            }//fin de foreach
        }//Fin de if( is_array($this->_migas) )        
        Interfaz::asignarToken("migas_de_pan", $listado_migas, $contenido);

        //asignar a interfaz el calendario general
        $calendario_general = '';
        if (!empty($this->_calendario)) {
            if (!isset($this->_calendario['tipo_cal'])) {
                $this->_calendario['tipo_cal'] = 1;
            }
            $cal = $this->obtenerCalendarioGeneral($this->_calendario['tipo_cal'], $this->_calendario['fecha_i'], $this->_calendario['fecha_f']);
            
            //JMM-2017-18-08 - Desarrollo #10555
            $calendario_general = '<div style="display:inline-block;float:right"><div class="pull-right" style="z-index:999;max-width:600px;">'.$cal.'</div></div>';
        }//Fin de if (!empty($this->_calendario)) {
        Interfaz::asignarToken('calendario_general', $calendario_general, $contenido);


        if (!empty($this->_print_media)) {
            Interfaz::asignarToken("print", "print-" . $this->_print_media, $contenido);
        }

        $pasos_comunicacion = '';
        if ($this->_pasos_labels != ""){
        	//JMM-2017-08-18 - Desarrollo #10555
        	$pasos_comunicacion = '<div class="pasos-comunicacion">'.$this->mostrarPasosLabels($this->_pasos_labels).'</div>';
        }//Fin de if ($this->_pasos_labels != ""){
        Interfaz::asignarToken("pasos_comunicacion",$pasos_comunicacion, $contenido);

        $selector_encuestas = '';
        //pgs - 10/08/2012 - se pone el selector de encuestas, se trae las encuestas que tiene acceso el usuario
        if ($_SESSION['autenticado']){
            //$encuestas = $this->obtenerEncuestasAsociadas($datos);

            if ($encuestas){
              //asignar a interfaz el selector de encuesta
              if (!empty($this->_selector_encuesta)){
                  $listado_encuestas = array();
                  foreach ($encuestas as $enc_id => $encuesta) {
                      $listado_encuestas[$enc_id] = $encuesta['eni_nombre'];
                  }//Fin de foreach($encuestas as $enc)
                  //si no existe una encuesta seleccionada
                  if (empty($_SESSION ['enc_id'])) {
                      //si exite el id de la encuesta hotel se pone como predefinido, sino se pone la primera del array
                      if (array_key_exists($id_encuesta_hotel, $listado_encuestas)) {
                          $_SESSION ['enc_id'] = $id_encuesta_hotel;
                      } else {
                          $_SESSION ['enc_id'] = array_shift($listado_encuestas);
                      }//Fin de if(array_key_exists($id_encuesta_hotel, $listado_encuestas))
                  }//Fin de if(empty($_SESSION ['enc_id']))

                  $datos_sel_encuesta['array'] = $listado_encuestas;
                  
                  //OR 2020-04-02 - se hacen ajustes de diseño según rm#14579
                  $datos['enc_id'] = $_SESSION['enc_id'];                                    
                  $nuevo_selector_enc = $this->generarHtmlSelectorEncuesta($datos);
                  
                  //JMM-2017-08-18 - Desarrollo #10555
                  $selector_encuestas = '<div class="selector-encuestas">'.$nuevo_selector_enc.'</div>';                  
              }//Fin de if (!empty($this->_selector_encuesta)){
            }//Fin de if(sizeof($encuestas)>0)
        }//Fin de if($_SESSION['autenticado'])

        //BDL::03-02-2021
        //Si no hay contenido y no esté autenticado el usuario 
        //Redirige a iniciar sesion 
        if( $this->_contenido == '' && $_SESSION['autenticado'] != 1)
        {
            // $this->_contenido = "<center><br><br><br><h2>No tiene permisos para acceder a la 
            //             opci&oacute;n seleccionada.</h2><br><br><br><a href='index.php?m=usuarios'>Continuar</a></center>
            //             <br><br><br><br>";
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=Usuarios"; 
            </script> 
            <?php 
        }//Fin de if( $this->_contenido == '' && $_SESSION['autenticado'] != 1)

        Interfaz::asignarToken("selector_encuestas", $selector_encuestas, $contenido);
        //pgs - 10/08/2012 - fin de, se pone el selector de encuestas

        Interfaz::asignarToken("seccion", $this->_seccion, $contenido);

        if ($this->_contenido == "" && $this->_contenido_externo == "" && $this->_contenido_externo_general == "" ){
            $this->_contenido = "<center><br /><br /><br /><a href='" . $_PATH_PAG_WEB . "'>".$idi_despliegue['enlace_continuar']."</a><br /><br /></center>"; //MAOH - 13 Jun 2012
        }

        Interfaz::asignarToken("contenido", $this->_contenido, $contenido);
        Interfaz::asignarToken("footer", $this->_contenido_footer, $contenido);
        Interfaz::asignarToken("menu_entrada", $this->_contenido_menu_general, $contenido);
        Interfaz::asignarToken("top_bar", $this->_contenido_top_bar, $contenido);
        Interfaz::asignarToken("contenido_externo", $this->_contenido_externo, $contenido);
        Interfaz::asignarToken("contenido_externo_general", $this->_contenido_externo_general, $contenido);
        Interfaz::asignarToken("css", $estilos, $contenido);
        Interfaz::asignarToken("js", $js, $contenido);
        Interfaz::asignarToken("perdido_con_traduccion",$idi_despliegue['enlace_perdido_traduccion'], $contenido);
        Interfaz::asignarToken("soporte",$idi_despliegue['soporte'], $contenido);
        Interfaz::asignarToken("js_footer", $js_footer, $contenido);
        Interfaz::asignarToken("css_footer", $estilos_footer, $contenido);
        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);
        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);

        //AMP - DATOS PARA PRINCIPAL DESPEDIDA
        if (!is_null($info_est["est_logo"]) && $info_est["est_logo"] != "") {
            $logo = $_PATH_WEB_LOGOS . "/" . $info_est["est_logo"];
        } else {
            $logo = $_PATH_IMAGENES . "/hotels_quality.jpg";
        }
        if (!is_null($info_est["est_pagina_facebook"]) && $info_est["est_pagina_facebook"] != "")
            $url = $info_est["est_pagina_facebook"];


        Interfaz::asignarToken("url", $url, $contenido);
        Interfaz::asignarToken("compartido", $idi_despliegue['label_compartido_facebook_bien'], $contenido);
        Interfaz::asignarToken("no_compartido", $idi_despliegue['label_compartido_facebook_mal'], $contenido);
        Interfaz::asignarToken("logo", $logo, $contenido);
        Interfaz::asignarToken("url", $url, $contenido);
        Interfaz::asignarToken("descripcion", "", $contenido);
        Interfaz::asignarToken("es_preview", "", $contenido);

        return $contenido;
    }//Fin de crearInterfazGrafica()

    function crearPopUp($datos = array()) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_alertas,$_CONFIG;
        
        

        $idioma_alertas = "";
        if (is_array($idi_alertas)) {
            //MAOH - 18 Abr 2012 - Cambio a UTF 8
            $idioma_alertas = "<script languague=\"javascript\" charset=\"utf-8\">\nvar idioma = new Array();\n";
            //$idioma_alertas = "<script languague=\"javascript\">\nvar idioma = new Array();\n";

            foreach ($idi_alertas as $key => $texto) {
                $texto = str_replace("\n", "\\n", $texto);
                $texto = str_replace("\'", "'", $texto);
                $texto = str_replace("'", "\'", $texto);

                $idioma_alertas .= "idioma['" . $key . "'] = '" . $texto . "' ;\n";
            }//Fin de foreach($idi_alertas as $key => $texto)

            $idioma_alertas .= "</script>";
        }//Fin de if( is_array($idi_alertas) )

        $estilos = "";
        
        $js = "<script languague=\"javascript\" charset=\"utf-8\">\n";
        $js .= "var _facebook_app_id = '".$_CONFIG['facebook_app_id']."';\n";
        $js .= "</script>\n";
        $js .= $idioma_alertas;


        //DM - 2013-09-24
        $this->_listado_css_externos = array_unique($this->_listado_css_externos);
        foreach ($this->_listado_css_externos as $estilo) {
            //DM - 2013-09-23
            if (is_array($estilo)) {
                $url_estilo = $estilo['url'];
                unset($estilo['url']);
                $propiedades_estilo = '';
                foreach ($estilo as $propiedad => $val_propiedad) {
                    $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                }
                $estilos .= "<link href=\"" . $url_estilo . "\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
            } else {
                $estilos .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)


        $this->_l_css = array_unique($this->_l_css);
        foreach ($this->_l_css as $estilo) {
            //DM - 2013-09-23
            if (is_array($estilo))
            {
                $url_estilo = $estilo['url'];
                unset($estilo['url']);
                $propiedades_estilo = '';
                foreach ($estilo as $propiedad => $val_propiedad) {
                    $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                }
                
                //DM - 2017-09-12 issue 10647
                //Obtiene la ultima modificación del fichero para generar la versión
                $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $url_estilo;
                $info_archivo = @stat($src_archivo);
                $version_archivo = $info_archivo['mtime'];
                $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $url_estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
            } else
            {
                //DM - 2017-09-12 issue 10647
                //Obtiene la ultima modificación del fichero para generar la versión
                $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
                $info_archivo = @stat($src_archivo);
                $version_archivo = $info_archivo['mtime'];
                $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)
        //  $estilos .= "<link href=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n";

        /* amp - 5/8/15 - armar el html de los estilos para el footer */
        $estilos_footer = "";
        foreach ($this->_listado_css_externos_footer as $estilo) {

            $estilos_footer .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)


        foreach ($this->_l_css_footer as $estilo) {

            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $estilos_footer .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)
        //si hay contenido para agregar lo hace
        if ($contenido_estilos != '') {
            $propiedades_estilo = '';
            foreach ($propiedades_contenido_estilos as $propiedad => $val_propiedad) {
                $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '" ';
            }

            $estilos .= '<style type="text/css" ' . $propiedades_estilo . '>' . $contenido_estilos . '</style>' . "\n";
        }//Fin de if( $contenido_estilos != '')

        $this->_l_js = array_unique($this->_l_js);
        foreach ($this->_l_js as $script) {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $js .= "<script src=\"" . $_PATH_WEB . "/Includes/JS/" . $script . "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            //$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";
        }//Fin de foreach($this->_js as $script)
        //DM  - 2013-09-12 issue 2971
        $this->_listado_js_externos = array_unique($this->_listado_js_externos);
        foreach ($this->_listado_js_externos as $script) {
            $js .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }//Fin de foreach($this->_listado_js_externos as $script)
        //    $js .= "<script src=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        // amp - 5/8/15 - agrego array para mostrar links en el footer
        $js_footer = "";
        foreach ($this->_listado_js_externos_footer as $script) {
            $js_footer .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }//Fin de foreach($this->_listado_js_externos as $script)
        foreach ($this->_l_js_footer as $script) {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $js_footer .= "<script src=\"" . $_PATH_WEB . "/Includes/JS/" . $script . "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            //$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";
        }//Fin de foreach($this->_js_foter as $script)

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/popup.html");
        if ($datos['plantilla_html'] != '') {
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/" . $datos['plantilla_html']);
        }

        //MAOH - 18 Abr 2012 - Cambio a UTF 8
        Interfaz::asignarToken("codificacion", "UTF-8", $contenido);

        //Si el idioma seleccionado es Chino(13), Ruso(14) o Ucraniano(15) para que reconozca caracteres extraños
         if (($_SESSION['idi_id'] == 10) || ($_SESSION['idi_id'] == 13) || ($_SESSION['idi_id'] == 14) || ($_SESSION['idi_id'] == 15) || ($_SESSION['idi_id'] == 9) || ($_SESSION['idi_id'] == 26))
        {
          Interfaz::asignarToken("codificacion",'UTF-8',$contenido);
        }
        else //Los otros idiomas que no tienen caracteres extraños
        {
          Interfaz::asignarToken("codificacion",'ISO-8859-1',$contenido);
        } 

        Interfaz::asignarToken("propiedades", $this->_propiedades, $contenido);

        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);

        Interfaz::asignarToken("contenido", $this->_contenido, $contenido);

        Interfaz::asignarToken("css", $estilos, $contenido);
        Interfaz::asignarToken("js", $js, $contenido);
        Interfaz::asignarToken("js_footer", $js_footer, $contenido);
        Interfaz::asignarToken("css_footer", $estilos_footer, $contenido);

        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);


        if (is_array($this->_campos)) {
            foreach ($this->_campos as $clave => $valor) {
                Interfaz::asignarToken($clave, $valor, $contenido);
            }
        }//Fin de if( is_array($this->_campos) )

        Interfaz::asignarToken("metas", "", $contenido);

        return $contenido;
    }

//Fin de crearPopUp()

    /*
     * Utilidad: Crear Listado para e´vío de reportes por email
     * Entrada arreglo con configuracion de tabla, cabeceras, celdas y adicionales
     * Autor: AMP
     * Fecha: 18/4/16
     *
     */

    public function crearListadoTablasCorreos($arreglo_datos) {
        global $_PATH_WEB, $idi_despliegue, $_PATH_IMAGENES, $_PATH_SERVIDOR;

        $contenido = "";
        $contenido .= '<table border="0" class="' . $arreglo_datos['TABLA']['class'] . '" cellspacing="0" cellpadding="0" ' . $arreglo_datos['TABLA']['atributos'] . ' >';
        $header = "<thead><tr>";

        foreach ($arreglo_datos['CABECERAS_TABLA'] as $cabecera) {
            $header .= '<th class="' . $cabecera['class'] . '" '
                    . $cabecera['atributos'] . ">"
                    . $cabecera['titulo']
                    . "</th>";
        }
        $header .= "</tr></thead>";
        // enlaces seguros
        $datos_enlace = array();
            $est['opciones'] = array();
            $est['opciones']['tipo_consulta'] = "info_basica";
            $establecimiento = establecimientos::obtenerInfoEstablecimiento(
                            $arreglo_datos['TABLA_ADICIONALES']['est_id'], $est);
            $datos_enlace['codigo'] = $establecimiento['est_codigo'];
        $body = "<tbody>";
        foreach ($arreglo_datos['VALORES_TABLA'] as $cuerpos) {
            $body .= "<tr>";
            foreach ($cuerpos as $campo => $cuerpo) {
                if (isset($arreglo_datos['CELDAS_TABLA'][$campo])) {

                    if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['celda_indicador'])) {
                        $tipo = $arreglo_datos['CELDAS_TABLA'][$campo]['celda_tabla'];
                        $body .= '<td class="' . $arreglo_datos['CELDAS_TABLA'][$campo]['celda_indicador'][$cuerpos[$tipo]] . '" '
                                . $arreglo_datos['CELDAS_TABLA'][$campo]['atributos'] . ">";
                    } else {
                        $body .= '<td align="left" class="' . $arreglo_datos['CELDAS_TABLA'][$campo]['class'] . '" '
                                . $arreglo_datos['CELDAS_TABLA'][$campo]['atributos'] . ">";
                    }

                    //maximo caracteres
                    if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['maximo_caracter'])){
                        if(strlen($cuerpos[$campo]) > $arreglo_datos['CELDAS_TABLA'][$campo]['maximo_caracter']){
                            $cuerpos[$campo] = substr($cuerpos[$campo], 0, $arreglo_datos['CELDAS_TABLA'][$campo]['maximo_caracter']) . "...";
                        }
                    }

                    //si tiene link
                    if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['link'])) {
                        //concatena los parametros necesarios
                        $parametros = "";
                        if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['parametros'])) {
                            foreach ($arreglo_datos['CELDAS_TABLA'][$campo]['parametros'] as $parametro) {
                                $parametros .= "&$parametro=" . $cuerpos[$parametro];
                            }
                        }
                        //crea el enlace seguro
                        $datos_enlace['enlace'] = $arreglo_datos['CELDAS_TABLA'][$campo]['link'] . $parametros;
                        $enlace_acceso = Herramientas::generarEnlaceExterno($datos_enlace);

                        $body .= '<a href="' . $enlace_acceso . '" target="_blank">' . $cuerpos[$campo] . "</a>";
                    } else {//de lo contrario texto normal
                        $body .= $cuerpos[$campo];
                    }
                    $body .= "</td>";
                }
            }
            $body .="</tr>";
        }

        $body .= "</tbody></table>";

        $contenido .= $header . $body;

        $datos = array();
        $datos['ubicacion_archivo'] = $_PATH_SERVIDOR . 'Includes/CSS/estilos_informes_tabla_correo.css';
        $datos['contenido'] = $contenido;
        $html = Interfaz::asignarEstilosDesdeArchivo($datos);
        return $html;
    }

//Fin crearListado()

    /**
     * Función que se encarga de crear los enlaces para paginación
     * DM - 2013-11-18
     * */
    static function crearPaginacion($arreglo_datos) {
        global $_PATH_WEB, $idi_despliegue;
        $contenido = "<div class='row'>";
        $offset = $arreglo_datos['LISTADO']['offset'];
        $limit = $arreglo_datos['LISTADO']['limite'] != '' ? $arreglo_datos['LISTADO']['limite'] : $arreglo_datos['LISTADO']['limit'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        //se usa para pasar entre páginas los parametros de la paginación
        //agrega la acción para la paginación
        $enlace_datos = 'accion=' . $arreglo_datos['ACCION_LISTAR'];

        //pasa los datos de ordenamiento
        $enlace_datos .= '&ordena=' . $arreglo_datos['DATOS']['ordena'];
        $enlace_datos .= '&tipo_ordena=' . $arreglo_datos['DATOS']['tipo_ordena'];
        //agrega los valores de limit de la paginación
        $enlace_datos .= '&limit=' . $limit;

        //si es para ajax y tiene ' entonces debe cambiar para que no genere error en js
        $enlace_datos = $arreglo_datos['ajax'] == 1 ? str_replace('\'', '\\\'', $enlace_datos) : $enlace_datos;

        if ($total_registros > 0 && !isset($arreglo_datos['bloquear_paginacion_grupos'])) {
            //DMG 2014-10-22 Se agrega el texto de modificar por idiomas
            $mostrar = $idi_despliegue['mostrar'];
            $mostrando = $idi_despliegue['mostrando'];
            $a = $idi_despliegue['labela_a'];
            $de = $idi_despliegue['label_de'];
            $enlaceLimit = "index.php?ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&offset=0";
            $arr_limits = array(10, 25, 50, 100);

            // $select_mostrar .= " <label style='margin-left:8px;' id='select_mostrar' for='select_mostrar'>" . $mostrar .
            //         '&nbsp; <select onchange="opcionesTablaAjax(\'' . $enlaceLimit . '&limit=\'+this.value)" class="form-control input-sm" style="width:75px; display:inline-block;" id="select_mostrar">';
            foreach ($arr_limits as $limits) {
                $select_mostrar .= "<option value='$limits' ";
                $select_mostrar .= ($limit == $limits) ? "selected" : "";
                $select_mostrar .= "> $limits </option>";
            }
            $select_mostrar .= '</select></label>';

            $limite = (($offset + $limit) > $total_registros) ? $total_registros : ($offset + $limit);
            $showing = "<div class='col-md-6' style='text-align:left;'>$mostrando " . ($offset + 1) . " $a $limite $de $total_registros </div>";
        }//Fin de if( $total_registros > 0 )
        $conte = "<div class='col-md-6 right'><ul class='pagination-aux' >";

        //Crea el enlace para pasar a la página anterior
        if ($offset > 0) {
            if ($arreglo_datos['ajax'] == 1) {
                $enlace = $enlace_datos;
                $enlace .= '&offset=' . ($offset - $limit);
            }//Fin de if( $datos['ajax'] == 1)
            else {
                $enlace = 'index.php?' . $enlace_datos;
                $enlace .= '&offset=' . ($offset - $limit);
            }//Fin de if( $datos['ajax'] == 1)
            $conte .= "<li class='paginate_button previous' style='display:inline;'> <a href='$enlace'> <i class='fa fa-angle-double-left'></i> </a></li>";
        }//Fin de if( $offset > 0 )
        //Campos select con el numero de paginas

        if ($total_registros > $limit && $limit > 0) {
            if ($arreglo_datos['ajax'] == 1) {
                $enlace = $enlace_datos;
                $enlace .= '&offset=';
            }//Fin de if( $datos['ajax'] == 1)
            else {
                $enlace = 'index.php?' . $enlace_datos;
                $enlace .= '&offset=';
            }//Fin de if( $datos['ajax'] == 1)

            $size = ceil($total_registros / $limit);
            $pagina_actual = ceil($offset / $limit) + 1;

            //DM - 2016-02-24
            //Solo debe mostrar 10 registros de paginas
            $pagina_inicial = 1;
            $pagina_final = $size;
            //Si se necesita mas de 10 paginas
            if ($size > 10) {
                $pagina_inicial = $pagina_actual - 5;
                $pagina_final = $pagina_actual + 5;
                if ($pagina_inicial <= 0) {
                    $pagina_final += abs($pagina_inicial);
                    $pagina_inicial = 1;
                }

                if ($pagina_final > $size) {
                    $diferencia = $pagina_final - $size;
                    while ($diferencia > 0 && $pagina_inicial > 0) {
                        $pagina_inicial--;
                        $diferencia--;
                    }//Fin de while($diferencia>0 && $pagina_inicial>0)
                    $pagina_final = $size;
                }
            }//Fin de if( $size > 10 )

            for ($i = $pagina_inicial; $i <= $pagina_final; $i++) {
                $selected = ( $i == $pagina_actual ? 'active' : '');
                $conte .= "<li class='paginate_button $selected'><a href='$enlace" . (($i - 1) * $limit) . "'>$i</a></li>";
            }//Fin de for ($i = 1; $i <= $size; $i++)
            //$contenido .= '</select>';
        }//Fin de if( $total_registros > $limit)
        //Crea el enlace para pasar a la página siguiente
        if ($total_registros > ($offset + $limit)) {
            if ($arreglo_datos['ajax'] == 1) {
                $enlace = $enlace_datos;
                $enlace .= '&offset=' . ($offset + $limit);
            }//Fin de if( $datos['ajax'] == 1)
            else {
                $enlace = 'index.php?' . $enlace_datos;
                $enlace .= '&offset=' . ($offset + $limit);
            }//Fin de if( $datos['ajax'] == 1)

            $conte .= "<li class='paginate_button next'> <a href='$enlace'> <i class='fa fa-angle-double-right'></i> </a></li>";
        }//Fin deif( $total_registros > ($offset+$limit))

        $conte .= "</ul></div>";
        $contenido .= $showing . $conte . "</div>";

        if (isset($arreglo_datos['DATOS']['arreglo'])) {
            return array('footer' => $contenido, 'top' => $select_mostrar);
        } else {
            return $contenido;
        }
    }

//Fin de crearPaginacion()

    static function asignarToken($clave, $reemplazo, &$contenido) {
        $contenido = str_replace("<!--" . $clave . "-->", $reemplazo, $contenido);

        return $contenido;
    }

//Fin de asignarToken()

    static function asignarTokenDatos($clave, $datos, &$contenido) {
        //DM - 2019-11-18 redmine[14093]
        $valor = is_array($datos) ? $datos[$clave] : "";  
        $contenido = str_replace("<!--" . $clave . "-->", $valor, $contenido);

        return $contenido;
    }//Fin de asignarToken()

    function archivoExcel($archivo, $tabla) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_alertas;

        //Se cargan los estilos disponibles
        $estilos = "";
        $this->_l_css = is_array($this->_l_css) ? $this->_l_css : array();
        foreach ($this->_l_css as $estilo) {
            $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)

        $cabeceraInforme = $archivo;

        $archivo = Herramientas::eliminarCaracteresEspeciales($archivo);

        $archivo = str_replace('&aacute;', 'a', $archivo);
        $archivo = str_replace('&eacute;', 'e', $archivo);
        $archivo = str_replace('&iacute;', 'i', $archivo);
        $archivo = str_replace('&oacute;', 'o', $archivo);
        $archivo = str_replace('&oacute;', 'o', $archivo);
        $archivo = str_replace(' ', '_', $archivo);
        $archivo = $archivo . ".xls";
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $archivo);
        $documento = '	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml">
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                            <title>Archivo Excel</title>
                            </head>
                            <body>';
        $fin = "</body></html>";
        $cabeceraInforme = "<div style ='font-size: 20px;font-weight: bold;' align='center'>" . $cabeceraInforme . "</div>";
        echo $documento . $cabeceraInforme . $tabla . $fin;
    }

//Fin de archivoExcel()

    function crearPaginacionUsuarios($arreglo_datos, $campo, $tipo_ordena_base) {
        global $_PATH_WEB, $idi_despliegue;

        $contenido = "";

        $contenido .= "<table width='80%' border='0' style='display: flex;justify-content: flex-start;'>
        					<tr>
        						<td style='margin-right:5px;'>";

        $offset = ($arreglo_datos['DATOS']['offset'] == '') ? 0 : $arreglo_datos['DATOS']['offset']; //$arreglo_datos['LISTADO']['offset'];

        $limit = ($arreglo_datos['DATOS']['limit'] == '') ? 10 : $arreglo_datos['DATOS']['limit']; //$arreglo_datos['LISTADO']['limite'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        //Enlace de anterior
        if ($offset > 0) {
            $contenido .= "<a class='enlace' href='index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'] . "&offset=" . ($offset - $limit) . "&limit=" . $limit . "'>" . $idi_despliegue[paginacion_atras] . "</a>";
        }//Fin de if( $offset > 0 )
        //Campos select con el numero de paginas
        if ($total_registros > $limit) {
            $enlace = "index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'] . "&limit=" . $limit . "&offset=";

            $contenido .= "<select onchange=\"location.href='" . $enlace . "'+this.value\">";

            $size = ceil($total_registros / $limit);

            $pagina_actual = ceil($offset / $limit) + 1;

            for ($i = 1; $i <= $size; $i++) {
                $selected = ( $i == $pagina_actual ? "selected" : "");
                $contenido .= "<option " . $selected . " value='" . (($i - 1) * $limit) . "'>" . $idi_despliegue[paginacion_pag] . " " . $i . "</option>";
            }
            $contenido .= "</select>";
        }//Fin de if( $total_registros > $limit)
        //Enlace siguiente
        if ($total_registros > ($offset + $limit)) {
            $contenido .= "&nbsp;&nbsp;&nbsp;<a class='enlace' href='index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'] . "&offset=" . ($offset + $limit) . "&limit=" . $limit . "'>" . $idi_despliegue[paginacion_adelante] . "</a>";
            ;
        }//Fin deif( $total_registros > ($offset+$limit))

        $enlacePie = "index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $campo . "&tipo_ordena=" . $tipo_ordena_base . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'];

        $contenido .= "</td>"; //Se cierra la celda del select de las paginas

        if ($total_registros > $limit) {
            $contenido .= "<td>
                                        Mostrar:[ <a class='enlace' href='" . $enlacePie . "&limit=10'>10</a> -
                                                    <a class='enlace' href='" . $enlacePie . "&limit=25'>25</a> -
                                                    <a class='enlace' href='" . $enlacePie . "&limit=50'>50</a> -
                                                    <a class='enlace' href='" . $enlacePie . "&limit=100'>100</a> ]
                                </td>";
        }

        $contenido .= " </tr>
                        </table>";
        return $contenido;
    }

//Fin de crearPaginacionUsuarios()
    //pgs - 02/04/2012 - funcion que se encarga de crear la paginacion para la pagina de comentarios
    function crearPaginacionComenarios($arreglo_datos) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_despliegue;

        $offset = $arreglo_datos['LISTADO']['offset'];
        $limit = $arreglo_datos['LISTADO']['limite'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        if ($total_registros == 0) {
            return '';
        }//Fin de if($total_registros==0)

        $numero_de = ($total_registros - $offset < $limit) ? ($total_registros - $offset) + $offset : $offset + $limit;

        $contenido = "<table border='0' align='right'><tr><td valign='middle'><b>" . ($offset + 1) . " - " . $numero_de . "</b> " . $idi_despliegue['label_de'] . " <b>" . $total_registros . "</b></td><td>";

        $id_camp_offset = $arreglo_datos['LISTADO']['id_camp_offset'];
        $id_form = $arreglo_datos['LISTADO']['id_form'];

        //enlace para ir a la pagina anterior
        if ($offset > 0) {
            $contenido .= " <a href='#' onclick='$(\"#" . $id_camp_offset . "\").val(" . ($offset - $limit) . ");$(\"#" . $id_form . "\").submit();'><img src='" . $_PATH_IMAGENES . "/anterior.jpg' border='0'></a>";
        }//Fin de if( $offset > 0 )
        //Enlace siguiente
        if ($total_registros > ($offset + $limit)) {
            $contenido .= "<a href='#' onclick='$(\"#" . $id_camp_offset . "\").val(" . ($offset + $limit) . ");$(\"#" . $id_form . "\").submit();'><img src='" . $_PATH_IMAGENES . "/siguiente.jpg' border='0'></a>";
        }//Fin de if( $total_registros > ($offset+$limit))

        $contenido .= '</td></tr></table>';

        return $contenido;
    }

//Fin de crearPaginacionComenarios($arreglo_datos)

    /*
     * MAOH - 10 Sep 2012 - Funcion que se encarga de crear la paginacion de los comentarios usndo ajax
     */

    function crearPaginacionComentariosXML($arreglo_datos) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_despliegue;

        $offset = $arreglo_datos['LISTADO']['offset'];
        $limit = $arreglo_datos['LISTADO']['limite'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        //Si no hay registros
        if ($total_registros == 0)
            return '';

        $numero_de = ($total_registros - $offset < $limit) ? ($total_registros - $offset) + $offset : $offset + $limit;
        $contenido = "<div style='float: right;'><table border='0' align='right'><tr><td valign='middle' align='right'><b>" . ($offset + 1) . " - " . $numero_de . "</b> " . $idi_despliegue['label_de'] . " <b>" . $total_registros . "</b></td><td align='right'>";

        //Enlace para ir a la pagina anterior
        if ($offset > 0)
            $contenido .= " <a href='#' onclick='buscarComentariosXML(" . ($offset - $limit) . ");'><img src='" . $_PATH_IMAGENES . "/anterior.jpg' border='0'></a>";

        //Enlace siguiente
        if ($total_registros > ($offset + $limit))
            $contenido .= "<a href='#' onclick='buscarComentariosXML(" . ($offset + $limit) . ");'><img src='" . $_PATH_IMAGENES . "/siguiente.jpg' border='0'></a>";

        $contenido .= '</td></tr></table></div>';

        return $contenido;
    }

//Fin de crearPaginacionComentariosXML($arreglo_datos)

    /**
     * Función que se encarga de retornar el contenido de plantilla que se envia por
     * correo electronico para los mensajes que envia la aplicación
     * DM - 2013-09-23
     * */
    static function obtenerPlantillaCorreo($datos = array()) {
        global $_PATH_SERVIDOR;

        $plantillas = $_PATH_SERVIDOR . '/Includes/Plantillas';

        $contenido = Archivos::obtenerContenidoArchivo($plantillas . '/plantilla_correo.html');

        return $contenido;
    }

//Fin de obtenerPlantillaCorreo()

    /**
     * Función que se encarga de generar el html del botón que
     * utiliza en la interfaz
     * DM - 2013-09-24
     * */
    static function generarHtmlBoton($datos) {
        global $_PATH_IMAGENES;

        $datos['tipo_boton'] = $datos['tipo_boton'] == '' ? 'submit' : $datos['tipo_boton'];

        $html_boton = '
        <input ' . $datos['atributos_boton'] . ' ' . $datos['accion_boton'] . ' class="btn btn-sm btn-white" type="' . $datos['tipo_boton'] . '" value="' . $datos['label_boton'] . '">
        ';

        return $html_boton;
    }

//Fin de generarHtmlBoton

    /**
     * Función que se encarga de generar una ventana emergente de
     * manera automatica
     * DM - 2013-10-30
     * */
    function generarPopupAutomatico($datos) {
        global $_PATH_IMAGENES, $idi_despliegue;

        $this->_contenido_popup_automatico = '
          <div id="container">
           	<div id="light_popup" class="white_content">
          		<table border="0" width="100%">
          			<tr>
          				<td class="descripcion_funcionalidad_ligthbox">' . $idi_despliegue['label_informacion'] . '</td>
          				<td width="60%">&nbsp</td>
          				<td width="20"><img src="' . $_PATH_IMAGENES . '/cerrar_ligthbox.png" onclick = "document.getElementById(\'light_popup\').style.display=\'none\';document.getElementById(\'fade_popup\').style.display=\'none\'"></td>
          			</tr>
          			<tr>
          				<td align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="100%" height="1"></td>
          				<td colspan="2"></td>
          			</tr>
          			<tr>
          				<td colspan="3">' . $datos['contenido'] . '</td>
          			</tr>
          		</table>
          	</div>
          </div>
          <div id="fade_popup" class="black_overlay"></div>
          <script language="javascript" >
          var posicion_scroll = $(window).scrollTop() + 200;
          $(\'#light_popup\').show();
          $(\'#light_popup\').css(\'top\',posicion_scroll+\'px\');

          var ancho_ventana = $(window).width();
          var ancho_contenido = $(\'#light_popup\').width();
          var espacio_left = (ancho_ventana - ancho_contenido)/2;
          $(\'#light_popup\').css(\'left\',espacio_left+\'px\');
          $(\'#fade_popup\').show();
          </script>
        ';
    }

//Fin de generarPopupAutomatico()

    /**
     * Se encarga de asignar los estilos a una plantilla html
     * teniendo en cuenta un archivo CSS en donde se declararon los
     * estilos
     * DM - 2014-05-27
     * * */
    static function asignarEstilosDesdeArchivo($datos) {
        //asigna el contenido de la plantilla
        $contenido = $datos['contenido'];

        //obtiene el contenido del archivo css
        $contenido_css = Archivos::obtenerContenidoArchivo($datos['ubicacion_archivo']);

        //elimina los saltos de linea del css
        $contenido_css = str_replace("\r\n", "", $contenido_css);
        $contenido_css = str_replace("\n", "", $contenido_css);

        $contenido = str_replace('class ="', 'class="', $contenido);
        $contenido = str_replace('class = "', 'class="', $contenido);
        $contenido = str_replace('class= "', 'class="', $contenido);

        //Obtiene las clases declaradas en la plantilla para obtener del css, lo hace
        //a partir del texto class="nombre_clase"
        $datos_token = array('plantilla' => $contenido,
            'token_prefijo' => 'class="',
            'token_sufijo' => '"'
        );
        $lista_clases = obtenerTokensPlantilla($datos_token);

        //de acuerdo a los nombres de las clases, busca las declaraciones en el css
        //y extra el contenido declarado para cada clase
        foreach ($lista_clases as $clase) {

            $sublista_clases = preg_split("/,/", $clase);

            $contenido_clase = '';
            foreach ($sublista_clases as $subclase) {
                $subclase = trim($subclase);
                if ($subclase == "") {
                    continue;
                }//Fin de if( $subclase == "" )
                //Obtiene las clases declaradas en la plantilla para obtener del css
                $datos_token = array('plantilla' => $contenido_css,
                    'token_prefijo' => '.' . $subclase . '{',
                    'token_sufijo' => '}',
                    'omitir_validacion' => 1
                );
                $lista_contenido = obtenerTokensPlantillaCSS($datos_token);

                //Obtiene el contenido de la clase css
                $contenido_clase_tmp = $lista_contenido[0];

                $contenido_clase_tmp = trim($contenido_clase_tmp);
                $contenido_clase .= str_replace("\r\n", "", $contenido_clase_tmp);
            }//Fin de foreach($sublista_clases as $subclase)
            //reemplaza la declaración de la clase por el codigo css
            $contenido = str_replace('class="' . $clase . '"', 'style="' . $contenido_clase . '"', $contenido);
        }//Fin de foreach($lista_clases as $clase)

        return $contenido;
    }

//Fin de asignarEstilosDesdeArchivo

    /**
     * Se encarga de obtener un solo contenido css con la
     * lista de archivos que se envia mediante un arreglo
     * DM - 2014-10-10
     * */
    static function generarContenidoCSSExternos($datos) {

        $lista_archivos_css = $datos['lista_archivos_css'];
        $lista_texto_intercambio = $datos['lista_texto_intercambio_contenidos'];

        $lista_texto_intercambio = is_array($lista_texto_intercambio) ? $lista_texto_intercambio : array();

        $contenido_css_general = '<style type="text/css">';

        foreach ($lista_archivos_css as $archivo_css) {
            $contenido_css = Archivos::obtenerContenidoArchivo($archivo_css);
            $contenido_css = str_replace("\r\n", "", $contenido_css);
            $contenido_css = str_replace("\n", "", $contenido_css);

            $contenido_css = str_replace("}", "} ", $contenido_css);

            //Elimina todos los comentarios que tengan los css
            $pos_inicial = strpos($contenido_css, '/*');
            //Mientras existan los comentarios busca las posiciones para eliminarlos
            while ($pos_inicial !== false) {
                $pos_final = strpos($contenido_css, '*/');

                //Obtiene el contenido desde la posicion cero hasta donde se encuentra el primer comentario
                $contenido_inicial = substr($contenido_css, 0, $pos_inicial);

                //Obtiene el contenido final despues del primer comentario
                $contenido_final = substr($contenido_css, $pos_final + 2, strlen($contenido_css));

                //Genera un nuevo contenido sin los comentarios
                $contenido_css = $contenido_inicial . $contenido_final;

                //verifica si aun quedan comentarios
                $pos_inicial = strpos($contenido_css, '/*');
            }//Fin de while( $pos_inicial !== false)


            foreach ($lista_texto_intercambio as $valor_texto_intercambio) {
                $contenido_css = str_replace($valor_texto_intercambio['base'], $valor_texto_intercambio['destino'], $contenido_css);
            }//Fin de foreach($lista_texto_intercambio as $valor_texto_intercambio)

            $contenido_css_general .= $contenido_css;
        }//Fin de foreach($lista_css as $css)

        $contenido_css_general .= '</style>';

        return $contenido_css_general;
    }

//Fin generarContenidoCSSExternos


    function mostrarPasosLabels($seleccionado){
        global $idi_despliegue;
        if ($seleccionado == 1){
        //JMM-2017-08-18 - Desarrollo #10555
        $pasos = '<div>
                    <div class="col-md-12 center" >
                        <div class="btn-group" >
                            <span class="badge badge-primary labels-pasos" id="label_paso1" >';
        $pasos .= $idi_despliegue['label_seleccionar_contactos'] . '</span>
                        <span class="badge badge-white labels-pasos" id="label_paso2" >';
        $pasos .= $idi_despliegue['label_crear_envios'] . '</span>
                        <span class="badge badge-white labels-pasos" id="label_paso3" >';
        $pasos .= $idi_despliegue['label_selecciona_plantilla'] . '</span>
                        </div>
                    </div>
                </div>';
        }else if ($seleccionado == 3){
            $pasos = '<div class="row">
                    <div class="col-md-12 center">
                        <div class="btn-group" >
                            <span class="badge badge-white labels-pasos" id="label_paso1" >';
            $pasos .= $idi_despliegue['label_seleccionar_contactos'] . '</span>
                            <span class="badge badge-white labels-pasos" id="label_paso2" >';
            $pasos .= $idi_despliegue['label_crear_envios'] . '</span>
                            <span class="badge badge-primary labels-pasos" id="label_paso3" >';
            $pasos .= $idi_despliegue['label_selecciona_plantilla'] . '</span>
                        </div>
                    </div>
                </div>';
        }

        return $pasos;
    }

    /**
     * Llama las campanias según su genero para mostrarlas en HQ
     * OR 2015-12-29
     * */
    function mostrarCampania($datos) {
        global $_obj_database, $_obj_interfaz, $_obj_campanias, $_PATH_WEB;

        $idioma = isset($datos[idioma]) ? $datos[idioma] : $_SESSION[idi_id];

        //Si no llegó el idioma, se llama por defecto el que tiene definido en HQ
        if ($idioma == '') {
            $info_establecimiento = $_obj_database->obtenerRegistro("establecimiento", "est_id ='" . $datos['est_id'] . "' ");
            $idioma = $info_establecimiento['est_idi_id'];
        }

        //OR 2016-01-28 Si llegara el idioma portugués BR se toma como PT
        if ($idioma == 187) {
            $idioma = 7;
        }

        $idiomas_activos = array(1, 2, 7, 3);
        //Si no está el idioma en los idiomas disponibles, se asigna otro por defecto ingles
        if (!in_array($idioma, $idiomas_activos)) {
            $idioma = 2;
        }

        //DM - 2016-02-26
        //Valida si el usuario actual es root usando la cuenta del hotel
        $es_root_usando_hotel = isset($_SESSION['usuario_root_id']);

        $campo_base = '';
        $campo_nombre = '';
        $campo_activo = '';
        $origen_click = '';

        switch ($datos['tipo_campania']) {
            case 'inapp':
                //Si en la sesión ya se había mostrado una campaña, no se vuelve a mostrar el modal
                if ($_SESSION[mostrar_campania] == 1) {
                    return array();
                }

                $campo_base = 'cam_inapp_contenido';
                $campo_nombre = 'contenido_inapp';
                $campo_activo = 'ccam_inapp_activo';
                $origen_click = 'inapp';

                //DM - 2016-01-12
                //Consulta los parámetros de configuración de despliegue
                $sql_config = "SELECT *
                                FROM configuracion_general
                                WHERE cog_codigo IN ('latencia_inapp','latencia_campania_inapp')
                                ";
                $lista_configuracion = $_obj_database->obtenerRegistrosAsociativosPorClave($sql_config, "cog_codigo");

                //para el despliegue en la aplicación
                $latencia_inapp = $lista_configuracion['latencia_inapp']['cog_valor'];
                //para el despliegue de la campaña
                $latencia_campania_inapp = $lista_configuracion['latencia_campania_inapp']['cog_valor'];

                $registro_impacto = $_obj_database->obtenerRegistro("reporte_impacto_campania", "
                                                ric_usu_id = " . $_SESSION['usu_id'] . "
                                                AND ric_tipo_impacto = '" . $datos['tipo_campania'] . "'
                                                AND ric_cam_id > 0
                                                ORDER BY ric_fecha DESC
                                                ");

                //Obtiene el time de despliegue de la ultima alerta
                $time_registro = strtotime($registro_impacto['ric_fecha']);

                //agrega al tiempo la cantidad de dias en los cuales puede mostrar otra vez otra alerta
                $time_registro_prox_despliegue = strtotime('+' . $latencia_inapp . ' day', $time_registro);

                //Obtiene el time a partir de la primera hora del día
                $time_registro_prox_despliegue = strtotime(date('Y-m-d 00:00:00', $time_registro_prox_despliegue));

                //Si el time actual es menor al time en el que debe mostrar la prox alerta entonces
                //no muestra nada
                if (!$_SESSION['usuario_root_id'] && strtotime(date('Y-m-d 00:00:00')) < $time_registro_prox_despliegue) {
                    //OR 2016-01-28 indico que al menos se mostró una vez o que registro el impacto
                    $_SESSION[mostrar_campania] = 1;

                    $datos['campania_id'] = 0;
                    $this->registrarImpacto($datos);
                    return array();
                }//Fin if( time() < $time_registro_prox_despliegue )
                //Obtiene la fecha para cargar todas las campañas que se han mostrado en ese tiempo
                $time_mostrar_campanias = strtotime('-' . $latencia_campania_inapp . ' day');

                //Obtiene todas las campañas que a partir de los días de latencia
                //se han mostrado para ese usuario, esas campañas no se deben mostrar al usuario
                //hasta que no pasen los dias de latencia
                $sql_registros_campanias = "SELECT ric_cam_id
                                            FROM reporte_impacto_campania
                                            WHERE ric_usu_id = " . $_SESSION['usu_id'] . "
                                                AND ric_tipo_impacto = '" . $datos['tipo_campania'] . "'
                                                AND ric_fecha >= '" . date('Y-m-d 00:00:00', $time_mostrar_campanias) . "'
                                                AND ric_cam_id > 0
                                            ";

                $lista_campanias_id = $_obj_database->obtenerRegistrosAsociativosPorClave($sql_registros_campanias, "ric_cam_id");

                $condicion_campanias = count($lista_campanias_id) > 0 ? "AND ccam_id NOT IN ( " . implode(",", array_keys($lista_campanias_id)) . ")" : "";
                break;
            case 'header':
                $campo_base = 'cam_header_contenido';
                $campo_nombre = 'contenido_header';
                $campo_activo = 'ccam_header_activo';
                $origen_click = 'header';
                break;
            case 'landing':
                $campo_base = 'cam_landing_contenido';
                $campo_nombre = 'contenido_landing';
                $campo_activo = 'ccam_landing_activo';
                break;
        }//Fin de switch($datos['tipo_campania'])
        //OR 2016-01-28 indico que al menos se mostró una vez
        $_SESSION[mostrar_campania] = 1;

        if ($datos['camp_id'] != '') {
            $condicion_campanias .= "AND ccam_id = " . $datos['camp_id'];
        }

        $sql = "SELECT ccam_titulo_campania AS titulo,
                          ccam_id AS id_campania,
                         cam_idi_id AS idioma,
                         ccam_emails_notificacion_header AS emails_notificar,
                         " . $campo_base . "  AS " . $campo_nombre . "
                  FROM campania,campania_idioma
                  WHERE " . $campo_activo . " = 1
                  AND ccam_id = cam_ccam_id
                  AND cam_idi_id = " . $idioma . "
                  " . $condicion_campanias
        ;

        $contenido = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'id_campania');

        //Si no hay registros no continua
        if (count($contenido) == 0) {
            //DM - 2016-03-01
            //En caso de no tener disponible el contenido en el idioma del establecimiento
            //lo solicita en ingles, siempre y cuando el idioma no sea ingles
            if ($idioma != 2) {
                $sql = "SELECT ccam_titulo_campania AS titulo,
                                  ccam_id AS id_campania,
                                 cam_idi_id AS idioma,
                                 ccam_emails_notificacion_header AS emails_notificar,
                                 " . $campo_base . "  AS " . $campo_nombre . "
                          FROM campania,campania_idioma
                          WHERE " . $campo_activo . " = 1
                          AND ccam_id = cam_ccam_id
                          AND cam_idi_id = 2
                          " . $condicion_campanias;

                $contenido = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'id_campania');
            }//Fin deif( $idioma != 2 )
            //Si no hay contenido para el idioma del establecimiento o para ingles
            //consulta español que siempre sera obligatorio cuando se ingresan los contenidos
            if (count($contenido) == 0) {
                $sql = "SELECT ccam_titulo_campania AS titulo,
                                  ccam_id AS id_campania,
                                 cam_idi_id AS idioma,
                                 ccam_emails_notificacion_header AS emails_notificar,
                                 " . $campo_base . "  AS " . $campo_nombre . "
                          FROM campania,campania_idioma
                          WHERE " . $campo_activo . " = 1
                          AND ccam_id = cam_ccam_id
                          AND cam_idi_id = 1
                          " . $condicion_campanias
                ;

                $contenido = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'id_campania');
            }//Fin de if( $idioma != 1)
        }//Fin de if( count($contenido) == 0 )

        if (is_numeric($datos['camp_id'])) {
            //Si viene el id de una campaña por url pues se utiliza esta para luego mostrar su contenido
            $datos['campania_id'] = $datos['camp_id'];
        } else if (count($contenido) > 0) {
            //OR 2016-01-04 optiene una campaña aleatoriamente
            $pos_random = array_rand($contenido, 1);
            $datos['campania_id'] = $pos_random;
        }

        $url = $_PATH_WEB . '/index.php?m=campanias&accion=ver_campania&camp_id=' . $datos['campania_id'];

        $est_id = $datos[est_id] == '' ? $_SESSION[est_id] : $datos[est_id];

        if ($origen_click != '') {
            $url = $url . '&source=' . $origen_click . '&tipo_campania=' . $datos[tipo_campania] . '&est_id=' . $est_id;
        }
        $contenido[$datos['campania_id']][contenido_inapp] = $_obj_campanias->introducirURL($contenido[$datos['campania_id']][contenido_inapp], $url);
        $contenido[$datos['campania_id']][contenido_header] = $_obj_campanias->introducirURL($contenido[$datos['campania_id']][contenido_header], $url);

        //OR 2016-01-06 Registra el despliegue de la campaña en BD
        $this->registrarImpacto($datos);


        return $contenido[$datos['campania_id']];
    }

//fin mostrarCampania($datos);

    /**
     * Se encarga de registrar en BD el despliegue de una campaña
     * OR 2016-01-06
     * */
    function registrarImpacto($datos) {
        global $_obj_database;

        //Si es el usuario root o si el usuario root entró como usuario hotel o cadena o si es preview
        // entonces no registra impacto
        if ($_SESSION[tipo_usuario] == 'R' || isset($_SESSION['usuario_root_id']) || $datos[preview] == 1) {
            return 1;
        }

        $datos[est_id] = $_SESSION['est_id'] == '' ? $datos[est_id] : $_SESSION['est_id'];

        //OR 2016-02-19 llamo info del establecimiento para registrar el país en el impacto
        $datos['opciones']['tipo_consulta'] = 'info_establecimiento';
        $datos_est = establecimientos::obtenerInfoEstablecimiento($datos[est_id], $datos);

        $campos = array('ric_tipo_impacto', 'ric_cam_id',
            'ric_est_id', 'ric_usu_id', 'ric_procedencia', 'ric_pai_id', 'ric_correo_destinatario');


        $datos['tabla'] = "reporte_impacto_campania";
        $datos['ric_tipo_impacto'] = $datos['tipo_campania'];
        $datos['ric_cam_id'] = $datos['campania_id'];
        $datos['ric_est_id'] = $datos['est_id'];
        $datos['ric_procedencia'] = $datos['source'];
        $datos['ric_pai_id'] = $datos_est['esp_pai_id'];
        $datos['ric_correo_destinatario'] = '';
        //DM - 2016-02-16
        //Para los header no se guarda el usuario
        $datos['ric_usu_id'] = $_SESSION['usu_id'];

        if ($datos['tipo_campania'] == 'header') {
            $datos['ric_usu_id'] = 0;

            if (is_array($datos['correo_usuario']) && sizeof($datos['correo_usuario']) > 0) {
                foreach ($datos['correo_usuario'] as $key => $destinatario) {
                    if ($destinatario != '') {
                        $datos['ric_correo_destinatario'] = $destinatario;
                        $_obj_database->insertarDatosTabla($datos, $campos);
                    }
                }
            }
        }//Fin de if( $datos['tipo_campania'] == 'header' )
        else {
            $_obj_database->insertarDatosTabla($datos, $campos);
        }

        if ($_obj_database->obtenerNumeroFilasAfectadas() == 0) {
            return -1003;
        }
        return 1;
    }//fin function registrarImpacto($datos)

    /**
     * Se encarga de generar el contenido tipo json y hacer la impresión
     * en pantalla para que pueda ser enviado mediante ajax
     * DM - 2016-09-28
     **/
    static function asignarContenidoAjaxJson($datos)
    {
        global $_obj_database,$_db_obj;

        $datos_contenido = $datos;        
        $datos['log_nombre_proceso_actual'] = "asignarContenidoAjaxJson";
        $datos['log_nombre_subproceso_actual'] = "";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);
        $datos_log = $datos;
        
        header("Content-type: application/json; charset=utf-8");
        
        $datos_json = json_encode($datos_contenido);
        print($datos_json);

        if( is_array($_db_obj) && count($_db_obj) > 0 ){
            $_db_obj['see']->desconectar();
        }
        $_obj_database->desconectar();
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_nombre_proceso_actual'] = "asignarContenidoAjaxJson";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        
        $datos_log['log_nombre_proceso_actual'] = "Index";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        exit;
    }//Fin de asignarContenidoAjaxJson

    /**
     * Se encarga de hacer la impresión
     * en pantalla para que pueda ser enviado mediante ajax
     * DM - 2017-02-08
     **/
    function asignarContenidoAjax($contenido)
    {
        global $_obj_database,$_db_obj;
        
        $datos['log_nombre_proceso_actual'] = "asignarContenidoAjax";
        $datos['log_nombre_subproceso_actual'] = "";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);
        $datos_log = $datos;
        
        print($contenido);

        $_db_obj['see']->desconectar();
        $_obj_database->desconectar();
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_nombre_proceso_actual'] = "asignarContenidoAjax";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        
        $datos_log['log_nombre_proceso_actual'] = "Index";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        exit;
    }//Fin de asignarContenidoAjaxJson
    
    /**
     * Se encarga de crear la interfaz de despliegue a partir de una plantilla
     * desde la base de datos
     * DM - 2019-12-20
     **/
    function crearInterfazGraficaPlantilla($datos){
        global $_obj_database,$_db_obj,$datos_log;
                
        //Si existe un codigo de plantilla externa debe usarla
        if( $datos['codigo_plantilla_interfaz'] != ''){
            //busca la plantilla por idioma o en su defecto carga la generica
            $datos_plantilla_interfaz = array();
            $datos_plantilla_interfaz['idi_id'] = $datos['idi_id'];
            $datos_plantilla_interfaz['codigo'] = $datos['codigo_plantilla_interfaz'];
            $datos_plantilla_interfaz['no_tokens'] = 1;
            $contenido_plantilla_interfaz = PlantillasCorreo::obtenerPlantilla($datos_plantilla_interfaz);
            //Si no existe en el idioma solicitada entonces consulta generico
            if( $contenido_plantilla_interfaz['asunto'] == '' && $contenido_plantilla_interfaz['contenido'] == '' ){
                $datos_plantilla_interfaz['idi_id'] = '0';
                $contenido_plantilla_interfaz = PlantillasCorreo::obtenerPlantilla($datos_plantilla_interfaz);
                $datos['contenido_plantilla_interfaz'] = $contenido_plantilla_interfaz['contenido'];
                
                if( is_array( $datos['tokens_contenido_plantilla_interfaz'] ) ){
                    foreach($datos['tokens_contenido_plantilla_interfaz'] AS $token => $contenido_token){
                    
                        $datos['contenido_plantilla_interfaz'] = str_replace('<!--'.$token.'-->',$contenido_token,$datos['contenido_plantilla_interfaz']);
                        $datos['contenido_plantilla_interfaz'] = str_replace('[*'.$token.'*]',$contenido_token,$datos['contenido_plantilla_interfaz']);        
                    }//Fin de foreach($datos['tokens_contenido_plantilla_interfaz'] AS $token => $contenido_token){
                }//Fin de if( is_array( $datos['tokens_contenido_plantilla_interfaz'] ) ){
                
                //reemplaza los tokens que hayan quedado con [*token*] a <!--token-->
                $datos['contenido_plantilla_interfaz'] = str_replace('[*','<!--',$datos['contenido_plantilla_interfaz']);
                $datos['contenido_plantilla_interfaz'] = str_replace('*]','-->',$datos['contenido_plantilla_interfaz']);
                       
            }
        }//Fin de if( $datos['codigo_plantilla_interfaz'] != ''){
        else{
            $datos['plantilla_interfaz'] = $datos['plantilla_interfaz'] == '' ? 'principal_externa' : $datos['plantilla_interfaz'];
        }//Fin de else de if( $datos['codigo_plantilla_interfaz'] != ''){
        
        $datos_plantilla = array();
        $datos_plantilla['idi_id'] = $datos['idi_id'];
        $datos_plantilla['codigo'] = $datos['codigo_plantilla'];
        $contenido_plantilla = PlantillasCorreo::obtenerPlantilla($datos_plantilla);
        $contenido_plantilla['asunto'] = trim($contenido_plantilla['asunto']);
        $contenido_plantilla['contenido'] = trim($contenido_plantilla['contenido']);        
        //Si no existe en el idioma solicitada entonces consulta generico
        if( $contenido_plantilla['asunto'] == '' && $contenido_plantilla['contenido'] == '' ){
            $datos_plantilla['idi_id'] = '0';
            $contenido_plantilla = PlantillasCorreo::obtenerPlantilla($datos_plantilla);        
        }//Fin de if( !is_array($contenido_plantilla) ){    
        $this->asignarContenido( $contenido_plantilla['contenido'] );                
        $datos['plantilla'] = $datos['plantilla_interfaz'];
        print_r( $this->crearInterfazGrafica($datos) );
        
        $_obj_database->desconectar();
        $_db_obj[ 'see' ]->desconectar();
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_nombre_proceso_actual'] = "Index";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        exit;       
    }//Fin de crearInterfazGraficaPlantilla    

    /**
     * BDL::29-12-2020
     * Listar Alumnos
     */
    static function crearListadoAlumnos($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES,$_obj_interfaz;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped " id="table1">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th data-sortable>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='true'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        
        foreach ($resultado as $campo => $etiqueta) {
            $decrypt_name = $_obj_interfaz->encrypt_decrypt('decrypt', $etiqueta['est_nombre']);
            
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['est_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['est_idalu'] . "</td>";
            $contenido .= "<td>" . $decrypt_name . "</td>";
            switch($etiqueta['est_genero']){
                case 'M':
                    $contenido .= "<td> Home </td>";
                    break;
                case 'F':
                    $contenido .= "<td> Dona </td>";
                    break;
            }
            $corta =  Interfaz::cadena_limite($etiqueta['est_referente_servicio_social'],25);
            $contenido .= "<td>" . $etiqueta['edu_nombre'] . "</td>";
            $contenido .= "<td>" . $familia = ($etiqueta['est_fam_id'] > 0 )? $etiqueta['est_fam_id'] : '' . "</td>";
            $contenido .= "<td>" . $etiqueta['cur_nombre'] . "</td>";
            $contenido .= "<td>" . $corta . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
            if($etiqueta['est_desactivo'] == "on"){
                $contenido .= "<td><span class='badge bg-danger'>Inactiva</span></td>";
            }
            else{
                $contenido .= "<td><span class='badge bg-success'>Actiu</span></td>";
            }
            if(isset($opciones['actualizar'])){
                $enlace = "usc_usu_id_promotor=$usu_id&est_id=".$etiqueta['est_id']."&est_idalu=".$etiqueta['est_idalu']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=alumnos&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "usc_usu_id_promotor=$usu_id&est_id=".$etiqueta['est_id']."&est_idalu=".$etiqueta['est_idalu']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalEst".$etiqueta['est_id']."' tabindex='-1' aria-labelledby='DeleteModalEst".$etiqueta['est_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Alumne</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Vols eliminar el Alumne '".$decrypt_name." - ".$etiqueta['est_idalu']."'?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=alumnos&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalEst".$etiqueta['est_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL::09-02-2021
     * Listar Familias
     */

    static function crearListadofamiliaspereclosa($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped " id="table1">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th data-sortable>$etiqueta</th>";
            }
        }
        
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['fam_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['fam_nombre_padre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['fam_nombre_madre'] . "</td>";
            $contenido .= "<td>" . $numerosa = $etiqueta['fam_numerosa'] == 1 ? 'Sí' : 'No' . "</td>";
            $contenido .= "<td>" . $monoparental = $etiqueta['fam_monoparental'] == 1 ? 'Sí' : 'No' . "</td>";
            $contenido .= "<td>" . $etiqueta['fam_observaciones'] . "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "fam_id=".$etiqueta['fam_id'];
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=familias&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $fam_id = $etiqueta['fam_id'];
                $enlace = "fam_id=$fam_id";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalFam".$etiqueta['fam_id']."' tabindex='-1' aria-labelledby='DeleteModalFam".$etiqueta['fam_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Alumne</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Vols eliminar la Família '".$etiqueta['fam_id']."'?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=familias&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalFam".$etiqueta['fam_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL::29-12-2020
     * Listar Centros Escolares
     */
    static function crearListadocentros($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table2">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['ces_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ter_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['tce_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_maxcomplejidad'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_num_gitanos'] . "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "ces_usu_id_director=$usu_id&ces_id=".$etiqueta['ces_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=centros&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "ces_usu_id_director=$usu_id&ces_id=".$etiqueta['ces_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModal".$etiqueta['ces_id']."' tabindex='-1' aria-labelledby='DeleteModal".$etiqueta['ces_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Centre</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Voleu eliminar el Centre escolar '" . $etiqueta['ces_nombre'] . "'?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=centros&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModal".$etiqueta['ces_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL::29-12-2020
     * Listar Reportes Centro
     */
    static function crearListadoreporte($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table5">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['rce_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['periodo_lectivo'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_graduados_eso'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_haran_bachillerato'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_haran_cfgm'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_haran_pfi'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total__graduados_2n_bachillerato'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_universidad'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_ob_estudios'] . "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "usc_usu_id_promotor=$usu_id&rce_id=".$etiqueta['rce_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=reportecentro&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "usc_usu_id_promotor=$usu_id&rce_id=".$etiqueta['rce_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalRCE".$etiqueta['rce_id']."' tabindex='-1' aria-labelledby='DeleteModalRCE".$etiqueta['rce_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Centre</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Voleu eliminar el Informe de Centre escolar '" . $etiqueta['ces_nombre'] . "' Períodes Acadèmics '".$etiqueta['periodo_lectivo']."' ?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=reportecentro&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalRCE".$etiqueta['rce_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    //BDL:02-01-2021
    static function crearListadoInformeCentro($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                        <table class="table table-striped" id="">';
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            $contenido .= "<th data-sortable='false'></th>";
            $contenido .= "<th data-sortable='false'colspan='3'>Alumnes que graduen 4t d’ESO</th>";
            $contenido .= "<th data-sortable='false'colspan='3'>Alumnes que faran batxillerat</th>";
            $contenido .= "<th data-sortable='false'colspan='3'>Alumnes que faran un CFGM</th>";
            $contenido .= "<th data-sortable='false'colspan='3'>Alumnes que faran PFI</th>";
            $contenido .= "<th data-sortable='false'colspan='3'>Alumnes que graduen 2n Batxillerat</th>";
            $contenido .= "<th data-sortable='false'colspan='3'>Alumnes que ingressen en la Universitat</th>";
            $contenido .= "<th data-sortable='false'></th>";
            $contenido .= '</tr><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
            $contenido .= '</tr></thead>';
        }
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_graduados_eso_ninos'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_graduados_eso_ninas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_graduados_eso'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_haran_bachillerato_ninos'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_haran_bachillerato_ninas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_haran_bachillerato'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_haran_cfgm_ninos'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_haran_cfgm_ninas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_haran_cfgm'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_haran_pfi_ninos'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_haran_pfi_ninas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_haran_pfi'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_graduados_2n_bachillerato_ninos'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_graduados_2n_bachillerato_ninas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_graduados_bachillerato'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_ingresen_universidad_ninos'] . "</td>";
            $contenido .= "<td>" . $etiqueta['rce_ingresen_universidad_ninas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total_ingresen_universidad'] . "</td>";
            $contenido .= "<td>" . $etiqueta['periodo_lectivo'] . "</td>";
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL::24-12-2021
     * crearListadoInformeAbsentismo
     */

    static function crearListadoInformeAbsentismo($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <div style="padding:25px;">
                               </div><table class="table table-striped" id="">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
            $contenido .= '</tr></thead>';
        }
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['est_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_i'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_ii'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_iii'] . "</td>";
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';
        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL::24-12-2021
     * crearListadoInformeAbsentismo
     */

    static function crearListadoInformeMotivoIntervencion($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $servicio_territorial = $arreglo_datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $arreglo_datos['FORMATO_BUSQ']['territorio'];
        $anio_lectivo = $arreglo_datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $arreglo_datos['FORMATO_BUSQ']['centro_escolar'];

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                        <div style="padding:25px;">';
        if($servicio_territorial != null){
            $contenido .= '<h5>SERVEI TERRITORIAL: '.$servicio_territorial.'</h5>';
        }
        if($territorio != null){
            $contenido .= '<h5>TERRITORI: '.$territorio.'</h5>';
        }
        if($anio_lectivo != null){
            $contenido .= '<h5>CURS ESCOLAR: '.$anio_lectivo.'</h5>';
        }
        if($centro_escolar != null){
            $contenido .= '<h5>CENTRE EDUCATIU: '.$centro_escolar.'</h5>';
        }
                
                        $contenido .=   '   </div><table class="table table-striped" id="">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
            $contenido .= '</tr></thead>';
        }
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['min_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_i'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_ii'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_iii'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total'] . "</td>";
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';
        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL::29-12-2020
     * Listar Vista Horas Adicionaes
     */
    static function crearListadoHorasAdicionales($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table5">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['hap_id'] . "</td>";
            $contenido .= "<td>" . date("Y-m-d", strtotime($etiqueta['hap_fecha'])) . "</td>";
            $contenido .= "<td>" . $etiqueta['hap_horas_trabajadas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['hap_observaciones'] . "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "usc_usu_id_promotor=$usu_id&hap_id=".$etiqueta['hap_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=visitas&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "usc_usu_id_promotor=$usu_id&hap_id=".$etiqueta['hap_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalATC".$etiqueta['hap_id']."' tabindex='-1' aria-labelledby='DeleteModalATC".$etiqueta['hap_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Centre</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Voleu eliminar '" .date('Y-m-d', strtotime($etiqueta['hap_fecha'])) . " ?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=visitas&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalATC".$etiqueta['hap_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * Se realiza listado de usuarios
     * SA 13/12/2020
     **/
    static function crearListado($arreglo_datos)
    {
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";

        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                echo $etiqueta;
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['usu_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_login'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_apellido'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_correo'] . "</td>";
            switch($etiqueta['usu_per_id']){
                case 1:
                    $contenido .= "<td>Administrador</td>";
                    break;
                case 2:
                    $contenido .= "<td>Promotor</td>";
                    break;
                case 3:
                    $contenido .= "<td>Director</td>";
                    break;
                case 4:
                    $contenido .= "<td>Coordinador</td>";
                    break;
                default:
                $contenido .= "<td>Pendiente</td>";
                    break;
            }
            
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        echo '</table></div></div>';

        return $contenido;
    }

     /**
     * Se realiza listado de Visitas
     * SA 31/12/2020
     **/
    static function crearListadoVisitas($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0" id="table_visita">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];

        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['vis_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['vis_fecha'] . "</td>";
            $contenido .= "<td>" . $etiqueta['vis_hora_inicio'] . "</td>";
            $contenido .= "<td>" . $etiqueta['vis_hora_fin'] . "</td>";
            $contenido .= "<td>" . $etiqueta['vis_horas_trabajadas'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['vis_observaciones'] . "</td>";

            $enlace = "usu_id_admin=$usu_id&usu_id=".$etiqueta['usu_id']."";
            $contenido .= "
            <td align=\"center\" height='38' valign='middle'>
                <a role='button' class='btn btn-outline-info' href=\"index.php?m=visitas&accion=listar_intervencion&vis_id=".$etiqueta['vis_id']."\" >
                    Gestionar Intervencions
                </a>
            </td>";
            $contenido .= "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "vis_id=".$etiqueta['vis_id']."";
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=visitas&accion=editarVisitas&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "vis_id=".$etiqueta['vis_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalEst".$etiqueta['vis_id']."' tabindex='-1' aria-labelledby='DeleteModalEst".$etiqueta['vis_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Visita</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Vols eliminar de Visita?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=visitas&accion=eliminar_visita&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalEst".$etiqueta['vis_id']."' >
                        Eliminar 
                    </button>
                </td>";
            }


            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * Se realiza listado de Usuarios
     * SA 06/01/2021
     **/
    static function crearListadoUsuarios($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table3">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th data-sortable>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        
        
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['usu_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_login'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_apellido'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_correo'] . "</td>";
            switch($etiqueta['usu_per_id']){
                case '1':
                    $contenido .= "<td>Administrador</td>";
                    break;
                case '2':
                    $contenido .= "<td>Promotor</td>";
                    break;
                case '3':
                    $contenido .= "<td>Director</td>";
                    break;
                case '4':
                    $contenido .= "<td>Coordinador</td>";
                    break;
                default:
                    $contenido .= "<td>Por Definir</td>";
                    break;
            }
            if(isset($opciones['actualizar'])){
                $enlace = "usu_id_admin=$usu_id&usu_id=".$etiqueta['usu_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=usuarios&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "usu_id_admin=$usu_id&usu_id=".$etiqueta['usu_id']."";
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalUsu".$etiqueta['usu_id']."' tabindex='-1' aria-labelledby='DeleteModalUsu".$etiqueta['usu_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Usuari</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Vols eliminar el Usuari '".$etiqueta['usu_nombre']." - ".$etiqueta['usu_login']."'?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=usuarios&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalUsu".$etiqueta['usu_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }
    /**
     * Se realiza listado de periodos Escolares
     * SA 14/12/2020
     **/
    static function crearListadoPeriodosAcademicos($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped " id="table4">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th data-sortable>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['ADMINISTRADOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['pes_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['pes_ano_lectivo'] . "</td>";
            $contenido .= "<td>" . $etiqueta['pes_periodo'] . "</td>";
            $contenido .= "<td>" . $etiqueta['pes_fecha_inicio'] . "</td>";
            $contenido .= "<td>" . $etiqueta['pes_fecha_fin'] . "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "pes_id=".$etiqueta['pes_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=periodosescolares&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "usu_id_admin=$usu_id&pes_id=".$etiqueta['pes_id']."";
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalUsu".$etiqueta['pes_id']."' tabindex='-1' aria-labelledby='DeleteModalUsu".$etiqueta['pes_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Períodes Escolars</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Vols eliminar el Períodes Escolars '".$etiqueta['pes_ano_lectivo']." - ".$etiqueta['pes_periodo']."'?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=periodosescolares&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalUsu".$etiqueta['pes_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * Se realiza listado de Intervencion
     * SA 05/01/2021
     **/
    static function crearListadoIntervencion($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database,$_obj_interfaz;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0" id="table_intervencion">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];

        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['int_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['int_fecha'] . "</td>";

            $sql = "SELECT min_nombre FROM motivo_intervencion WHERE min_id = ".$etiqueta['int_min_id']."";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
           
            if($resultado){
                $contenido .= "<td>" . $resultado[0]['min_nombre'] . "</td>";
            }
            else{
                $contenido .= "<td>" . $etiqueta['int_min_id'] . "</td>";
            }

            $sql = "SELECT tab_nombre FROM tipo_absentismo WHERE tab_id = ".$etiqueta['int_tab_id']."";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
           
            if($resultado){
                $contenido .= "<td>" . $resultado[0]['tab_nombre'] . "</td>";
            }
            else{
                $contenido .= "<td class='text-center'>-</td>";
            }

            $sql = "SELECT pes_ano_lectivo, pes_periodo FROM periodos_escolares WHERE pes_id = ".$etiqueta['int_pes_id']."";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
           
            if($resultado){
                $contenido .= "<td>" . $resultado[0]['pes_ano_lectivo'] . " / " . $resultado[0]['pes_periodo'] . "</td>";
            }
            else{
                $contenido .= "<td>" . $etiqueta['int_pes_id'] . "</td>";
            }

            if($etiqueta['int_tin_id']){
                $sql = "SELECT tin_nombre FROM tipo_intervencion WHERE tin_id = ".$etiqueta['int_tin_id']."";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
                $contenido .= "<td>" . $resultado[0]['tin_nombre'] . "</td>";
            }
            else{
                $contenido .= "<td class='text-center'>-</td>";    
            }
            
            if($etiqueta['int_otro_motivo_intervencion']){
                $contenido .= "<td>" . $etiqueta['int_otro_motivo_intervencion'] . "</td>";
            }
            else{
                $contenido .= "<td class='text-center'>-</td>";    
            }

            if($etiqueta['int_otro_tipo_intervencion']){
                $contenido .= "<td>" . $etiqueta['int_otro_tipo_intervencion'] . "</td>";
            }
            else{
                $contenido .= "<td class='text-center'>-</td>";    
            }

            if($etiqueta['int_ces_id']){
                $sql = "SELECT ces_nombre FROM centro_escolar WHERE ces_id = ".$etiqueta['int_ces_id']."";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
                $contenido .= "<td>" . $resultado[0]['ces_nombre'] . "</td>";
            }

            $sql = "SELECT ies_est_id FROM intervencion_estudiante WHERE ies_int_id = ".$etiqueta['int_id']."";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

            $contenido .= "<td>";
            foreach($resultado as $key => $valor){
                $sql = "SELECT est_nombre FROM estudiante WHERE est_id = ".$valor['ies_est_id']."";
                $resultado_est = $_obj_database->obtenerRegistrosAsociativos($sql);
                $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $resultado_est[0]['est_nombre']);
                $contenido .= $alumn_name . "<br> ";
            }
            $contenido .= "
            <td align=\"center\" height='38' valign='middle'>
                <a role='button' class='btn btn-outline-info' href=\"index.php?m=visitas&accion=ver_intervencion&int_id=".$etiqueta['int_id']."\" >
                    Veure
                </a>
            </td>";
            $contenido .= "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "int_id=".$etiqueta['int_id']."";
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=visitas&accion=editarIntervenciones&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "int_id=".$etiqueta['int_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalEst".$etiqueta['int_id']."' tabindex='-1' aria-labelledby='DeleteModalEst".$etiqueta['int_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Intervencio</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Vols eliminar de Intervencio?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=visitas&accion=eliminar_intervencion&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalEst".$etiqueta['int_id']."' >
                        Eliminar 
                    </button>
                </td>";
            }
            
            
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL:: 04-02-2021
     * Descripcion: Cortar cadena de texto
     * Parametros: $value - Cadena, $limite - Limite de caracteres, $end - Sufijo al final de la cadena
     * retorna cadena corta
     */

    function cadena_limite($value, $limit = 100, $end = '...'){
		if (mb_strwidth($value, 'UTF-8') <= $limit) {
				return $value;
		}
		return rtrim(mb_strimwidth($value, 0, $limit, '', 'UTF-8')).$end;
    }
    
    /** crearListadoIntervencionAdmin
     * Se realiza listado de Intervencion por rangos de fecha y promotor
     * SA 09/02/2021
     **/
    static function crearListadoIntervencionAdmin($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database, $_obj_interfaz;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0" id="table_intervencion">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        $datos = $arreglo_datos['DATOS'];

        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['int_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['int_fecha'] . "</td>";
            $contenido .= "<td>" . $etiqueta['min_nombre'] . "</td>";

            $sql = "SELECT tab_nombre FROM tipo_absentismo WHERE tab_id = ".$etiqueta['int_tab_id']."";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
           
            if($resultado){
                $contenido .= "<td>" . $resultado[0]['tab_nombre'] . "</td>";
            }
            else{
                $contenido .= "<td class='text-center'>-</td>";
            }

            $contenido .= "<td>" . $etiqueta['pes_ano_lectivo'] . " / " . $etiqueta['pes_periodo'] . "</td>";
            $contenido .= "<td>" . $etiqueta['tin_nombre'] . "</td>";
            
            
            if($etiqueta['int_otro_motivo_intervencion']){
                $contenido .= "<td>" . $etiqueta['int_otro_motivo_intervencion'] . "</td>";
            }
            else{
                $contenido .= "<td class='text-center'>-</td>";    
            }

            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";

            $sql = "SELECT ies_est_id FROM intervencion_estudiante WHERE ies_int_id = ".$etiqueta['int_id']."";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

            $contenido .= "<td>";
            foreach($resultado as $key => $valor){
                $sql = "SELECT est_nombre FROM estudiante WHERE est_id = ".$valor['ies_est_id']."";
                $resultado_est = $_obj_database->obtenerRegistrosAsociativos($sql);
                $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $resultado_est[0]['est_nombre']);
                $contenido .= $alumn_name . "<br> ";
            }
            $contenido .= "
            <td align=\"center\" height='38' valign='middle'>
                <a role='button' class='btn btn-outline-info' href=\"index.php?m=visitas&accion=ver_intervencion&promotor=".$datos['promotor']."&fecha_inicio=".$datos['fecha_inicio']."&fecha_fin=".$datos['fecha_fin']."&vis_id=".$etiqueta['vis_id']."&int_id=".$etiqueta['int_id']."\" >
                    Veure
                </a>
            </td>";
            $contenido .= "</td>";
            
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoHorasTrabajadas
     * Se muestra la busqueda de los resultados de las horas trabajadas
     * SA 12/02/2021
     **/
    static function crearListadoHorasTrabajadas($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        foreach ($resultado as $campo => $etiqueta) {
            $horas_centro_escolar = [];
            $horas_fuera_centro_escolar = [];

            if($_SESSION['tipo_usuario'] == 2){
                $sql = "SELECT visita.vis_horas_trabajadas FROM visita 
                        JOIN centro_escolar ON centro_escolar.ces_id = visita.vis_ces_id 
                        JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor
                        WHERE vis_fecha BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."'
                        AND usuarios.usu_id = ".$etiqueta['usu_id'].";";
            }
            else{
                $sql = "SELECT visita.vis_horas_trabajadas FROM visita 
                        JOIN centro_escolar ON centro_escolar.ces_id = visita.vis_ces_id 
                        JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor
                        WHERE territorio.ter_id = ".$datos['territorio']." AND vis_fecha 
                        BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."'
                        AND usuarios.usu_id = ".$etiqueta['usu_id'].";";
            }
            $resultado_trabajos_centros_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);

            //Suma de trabajos en los centros escolares
            if($resultado_trabajos_centros_escolares){
                foreach($resultado_trabajos_centros_escolares as $key => $centros){
                    $parts = explode(":", $centros['vis_horas_trabajadas']);
                    if( count($parts) > 1 ){
                        $horas_centro_escolar[0] += $parts[0];
                        $horas_centro_escolar[1] += $parts[1];
                        $horas_centro_escolar[2] += $parts[2];
                    }
                    else{
                        $horas_centro_escolar[0] += 0;
                        $horas_centro_escolar[1] += 0;
                        $horas_centro_escolar[2] += 0;
                    }
                }

                if($horas_centro_escolar[1] >= 60){
                    $total_minutos = $horas_centro_escolar[1]/60;
                    $total_minutos = number_format($total_minutos, 2, '.', '');
                    $minutos = explode(".", $total_minutos);

                    $horas_centro_escolar[0] += $minutos[0];
                    $horas_centro_escolar[1] = $minutos[1];
                }
                $horas_centro_escolar = $horas_centro_escolar[0] . ':' . $horas_centro_escolar[1] . ':' . $horas_centro_escolar[2] . '0';
            }
            else{
                $horas_centro_escolar = 0;
            }
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['usu_nombre'] . " " . $etiqueta['usu_apellido'] . "</td>";
            $contenido .= "<td>" . $horas_centro_escolar . "</td>";

            $sql = "SELECT * FROM horas_adicionales_promotor 
                    WHERE hap_usu_id_promotor = ".$etiqueta['usu_id']." AND hap_fecha BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."';";
            $resultado_trabajos_fuera_centros_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);
            
            if($resultado_trabajos_fuera_centros_escolares){
                foreach($resultado_trabajos_fuera_centros_escolares as $key => $fuera_centros){
                    $parts = explode(":", $fuera_centros['hap_horas_trabajadas']);
                    $horas_fuera_centro_escolar[0] += $parts[0];
                    $horas_fuera_centro_escolar[1] += $parts[1];
                    $horas_fuera_centro_escolar[2] += $parts[2];
                }

                if($horas_fuera_centro_escolar[1] >= 60){
                    $total_minutos = $horas_fuera_centro_escolar[1]/60;
                    $total_minutos = number_format($total_minutos, 2, '.', '');
                    $minutos = explode(".", $total_minutos);

                    $horas_fuera_centro_escolar[0] += $minutos[0];
                    $horas_fuera_centro_escolar[1] = $minutos[1];
                }
                $horas_fuera_centro_escolar = $horas_fuera_centro_escolar[0] . ':' . $horas_fuera_centro_escolar[1] . ':' . $horas_fuera_centro_escolar[2] . '0';
            }
            else{
                $horas_fuera_centro_escolar = 0;
            }

            $contenido .= "<td>" . $horas_fuera_centro_escolar . "</td>";

            $centros = explode(":", $horas_centro_escolar);
            $fuera = explode(":", $horas_fuera_centro_escolar);

            $total = [];

            $total[0] = $centros[0] + $fuera[0];
            $total[1] = $centros[1] + $fuera[1];
            $total[2] = $centros[2] + $fuera[2];

            if($total[1] >= 60){
                $total_minutos = $total[1]/60;
                $total_minutos = number_format($total_minutos, 2, '.', '');
                $minutos = explode(".", $total_minutos);

                $total[0] += $minutos[0];
                $total[1] = $minutos[1];
            }
            $total = $total[0] . ':' . $total[1] . ':' . $total[2] . '0';

            $contenido .= "<td>" . $total . "</td>";
            
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoAlumnosAtendidos
     * Se muestra la busqueda de los resultados de los alumnos atendidos
     * SA 19/02/2021
     **/
    static function crearListadoAlumnosAtendidos($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0 id="ninos_atendidos">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            if($_SESSION['tipo_usuario'] != 2){
                $contenido .= "<th colspan='2'></th>";
            }
            else{
                $contenido .= "<th></th>";
            }
            $contenido .= "<th colspan='3'>Escola Bressol</th>";
            $contenido .= "<th colspan='3'>2n Cicle Infantil</th>";
            $contenido .= "<th colspan='3'>Educació Primària</th>";
            $contenido .= "<th colspan='3'>Educació Secundària</th>";
            $contenido .= "<th colspan='3'>Educació Postobligatòria</th>";
            $contenido .= '</tr><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        if($resultado){
            foreach ($resultado as $campo => $etiqueta) {
                $contenido .= "<tr>";
                $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
                if($_SESSION['tipo_usuario'] != 2){
                    $contenido .= "<td>" . $etiqueta['usu_nombre'] . " " . $etiqueta['usu_apellido'] . "</td>";
                }

                $sql = 'SELECT DISTINCT estudiante.est_id, etapa_educativa.edu_nombre, estudiante.est_genero, etapa_educativa.edu_id
                        FROM intervencion 
                        JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
                        JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id
                        JOIN etapa_educativa ON etapa_educativa.edu_id = estudiante.est_edu_id
                        JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id
                        JOIN visita ON visita.vis_id = intervencion.int_vis_id
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor
                        JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                        WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                        centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                        periodos_escolares.pes_ano_lectivo = "'.$datos['ano_lectivo'].'";';
                $resultado_informe = $_obj_database->obtenerRegistrosAsociativos($sql);

                if($resultado_informe){
                    $BRESSOL_fem = 0;
                    $BRESSOL_masc = 0;
                    $cicle_fem = 0;
                    $cicle_masc = 0;
                    $primaria_fem = 0;
                    $primaria_masc = 0;
                    $secundaria_fem = 0;
                    $secundaria_masc = 0;
                    $post_fem = 0;
                    $post_masc = 0;
                    foreach($resultado_informe as $key => $valor){
                        switch($valor['edu_id']){
                            case 1:
                                //'BRESSOL'
                                if($valor['est_genero'] == 'F'){
                                    $BRESSOL_fem++;
                                }
                                else{
                                    $BRESSOL_masc++;
                                }

                                break;
                            case 2:
                                //'2n CICLE INFANTIL'
                                if($valor['est_genero'] == 'F'){
                                    $cicle_fem++;
                                }
                                else{
                                    $cicle_masc++;
                                }

                                break;
                            case 3:
                                //'EDUCACIÓ PRIMÀRIA'
                                if($valor['est_genero'] == 'F'){
                                    $primaria_fem++;
                                }
                                else{
                                    $primaria_masc++;
                                }

                                break;
                            case 4:
                                //'EDUCACIÓ SECUNDÀRIA'
                                if($valor['est_genero'] == 'F'){
                                    $secundaria_fem++;
                                }
                                else{
                                    $secundaria_masc++;
                                }

                                break;
                            case 5:
                                //"POSTOBLIGATÒRIA"
                                if($valor['est_genero'] == 'F'){
                                    $post_fem++;
                                }
                                else{
                                    $post_masc++;
                                }

                                break;
                            default: 
                                break;
                        }
                    }
                    //Bressol
                    $total_bressol = $BRESSOL_masc + $BRESSOL_fem;
                    $contenido .= "<td>".$BRESSOL_masc."</td>";
                    $contenido .= "<td>".$BRESSOL_fem."</td>";
                    $contenido .= "<td>" . $total_bressol . "</td>";
                    //Cicle Infantil
                    $total_cicle = $cicle_masc + $cicle_fem;
                    $contenido .= "<td>".$cicle_masc."</td>";
                    $contenido .= "<td>".$cicle_fem."</td>";
                    $contenido .= "<td>" . $total_cicle . "</td>";
                    //Primaria
                    $total_primaria = $primaria_masc + $primaria_fem;
                    $contenido .= "<td>".$primaria_masc."</td>";
                    $contenido .= "<td>".$primaria_fem."</td>";
                    $contenido .= "<td>" . $total_primaria . "</td>";
                    //Secundaria
                    $total_secundaria = $secundaria_masc + $secundaria_fem;
                    $contenido .= "<td>".$secundaria_masc."</td>";
                    $contenido .= "<td>".$secundaria_fem."</td>";
                    $contenido .= "<td>" . $total_secundaria . "</td>";
                    //Postobligatoria
                    $total_post = $post_masc + $post_fem;
                    $contenido .= "<td>".$post_masc."</td>";
                    $contenido .= "<td>".$post_fem."</td>";
                    $contenido .= "<td>" . $total_post . "</td>";
                }
                else{
                    $contenido .= '<td colspan="15">No hi ha registres Associats Disponibles</td>';
                }
                $contenido .= "</tr>";
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoAlumnosAtendidosExportar
     * Se muestra la busqueda de los resultados de los alumnos atendidos para exportar
     * SA 24/03/2021
     **/
    static function crearListadoAlumnosAtendidosExportar($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            if($_SESSION['tipo_usuario'] != 2){
                $contenido .= "<th colspan='2'></th>";
            }
            else{
                $contenido .= "<th></th>";
            }
            $contenido .= "<th colspan='3'>Escola Bressol</th>";
            $contenido .= "<th colspan='3'>2n Cicle Infantil</th>";
            $contenido .= "<th colspan='3'>Educació Primària</th>";
            $contenido .= "<th colspan='3'>Educació Secundària</th>";
            $contenido .= "<th colspan='3'>Educació Postobligatòria</th>";
            $contenido .= '</tr><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        if($resultado){
            foreach ($resultado as $campo => $etiqueta) {
                $contenido .= "<tr>";
                $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
                if($_SESSION['tipo_usuario'] != 2){
                    $contenido .= "<td>" . $etiqueta['usu_nombre'] . " " . $etiqueta['usu_apellido'] . "</td>";
                }

                $sql = 'SELECT DISTINCT estudiante.est_id, etapa_educativa.edu_nombre, estudiante.est_genero, etapa_educativa.edu_id
                        FROM intervencion 
                        JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
                        JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id
                        JOIN etapa_educativa ON etapa_educativa.edu_id = estudiante.est_edu_id
                        JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id
                        JOIN visita ON visita.vis_id = intervencion.int_vis_id
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor
                        JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                        WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                        centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                        periodos_escolares.pes_ano_lectivo = "'.$datos['ano_lectivo'].'";';
                $resultado_informe = $_obj_database->obtenerRegistrosAsociativos($sql);

                if($resultado_informe){
                    $BRESSOL_fem = 0;
                    $BRESSOL_masc = 0;
                    $cicle_fem = 0;
                    $cicle_masc = 0;
                    $primaria_fem = 0;
                    $primaria_masc = 0;
                    $secundaria_fem = 0;
                    $secundaria_masc = 0;
                    $post_fem = 0;
                    $post_masc = 0;
                    foreach($resultado_informe as $key => $valor){
                        switch($valor['edu_id']){
                            case 1:
                                //'BRESSOL'
                                if($valor['est_genero'] == 'F'){
                                    $BRESSOL_fem++;
                                }
                                else{
                                    $BRESSOL_masc++;
                                }

                                break;
                            case 2:
                                //'2n CICLE INFANTIL'
                                if($valor['est_genero'] == 'F'){
                                    $cicle_fem++;
                                }
                                else{
                                    $cicle_masc++;
                                }

                                break;
                            case 3:
                                //'EDUCACIÓ PRIMÀRIA'
                                if($valor['est_genero'] == 'F'){
                                    $primaria_fem++;
                                }
                                else{
                                    $primaria_masc++;
                                }

                                break;
                            case 4:
                                //'EDUCACIÓ SECUNDÀRIA'
                                if($valor['est_genero'] == 'F'){
                                    $secundaria_fem++;
                                }
                                else{
                                    $secundaria_masc++;
                                }

                                break;
                            case 5:
                                //"POSTOBLIGATÒRIA"
                                if($valor['est_genero'] == 'F'){
                                    $post_fem++;
                                }
                                else{
                                    $post_masc++;
                                }

                                break;
                            default: 
                                break;
                        }
                    }
                    //Bressol
                    $total_bressol = $BRESSOL_masc + $BRESSOL_fem;
                    $contenido .= "<td>".$BRESSOL_masc."</td>";
                    $contenido .= "<td>".$BRESSOL_fem."</td>";
                    $contenido .= "<td>" . $total_bressol . "</td>";
                    //Cicle Infantil
                    $total_cicle = $cicle_masc + $cicle_fem;
                    $contenido .= "<td>".$cicle_masc."</td>";
                    $contenido .= "<td>".$cicle_fem."</td>";
                    $contenido .= "<td>" . $total_cicle . "</td>";
                    //Primaria
                    $total_primaria = $primaria_masc + $primaria_fem;
                    $contenido .= "<td>".$primaria_masc."</td>";
                    $contenido .= "<td>".$primaria_fem."</td>";
                    $contenido .= "<td>" . $total_primaria . "</td>";
                    //Secundaria
                    $total_secundaria = $secundaria_masc + $secundaria_fem;
                    $contenido .= "<td>".$secundaria_masc."</td>";
                    $contenido .= "<td>".$secundaria_fem."</td>";
                    $contenido .= "<td>" . $total_secundaria . "</td>";
                    //Postobligatoria
                    $total_post = $post_masc + $post_fem;
                    $contenido .= "<td>".$post_masc."</td>";
                    $contenido .= "<td>".$post_fem."</td>";
                    $contenido .= "<td>" . $total_post . "</td>";
                }
                else{
                    $contenido .= '<td colspan="15">No hi ha registres Associats Disponibles</td>';
                }
                $contenido .= "</tr>";
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoUltimaVisita
     * Se muestra el listado de las intervenciones en la ultima visita
     * SR 03/03/2021
     **/
    static function crearListadoUltimaVisita($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database, $_obj_interfaz;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0" id="ultima_visita">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        foreach ($resultado as $campo => $etiqueta) {

            if($etiqueta['int_tab_id'] != 0){
                $sql = "SELECT estudiante.est_nombre, motivo_intervencion.min_nombre, tipo_absentismo.tab_nombre, tipo_intervencion.tin_nombre,
                    intervencion_estudiante.ies_observacion
                    FROM intervencion 
                    JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
                    JOIN tipo_absentismo ON tipo_absentismo.tab_id = intervencion.int_tab_id 
                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id
                    WHERE intervencion.int_id = ".$etiqueta['int_id'].";";
            }
            else{
                $sql = "SELECT estudiante.est_nombre, motivo_intervencion.min_nombre, tipo_intervencion.tin_nombre,
                    intervencion_estudiante.ies_observacion
                    FROM intervencion 
                    JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id
                    WHERE intervencion.int_id = ".$etiqueta['int_id'].";";
            }
            $resultado_intervenciones = $_obj_database->obtenerRegistrosAsociativos($sql);

            foreach ($resultado_intervenciones as $campo => $intervenciones) {
                $decrypt_name = $_obj_interfaz->encrypt_decrypt('decrypt', $intervenciones['est_nombre']);
                $contenido .= "<tr>";
                $contenido .= "<td>" . $decrypt_name . "</td>";
                $contenido .= "<td>" . $intervenciones['min_nombre'] . "</td>";
                if($intervenciones['tab_nombre']){
                    $contenido .= "<td>" . $intervenciones['tab_nombre'] . "</td>"; 
                }
                else{
                    $contenido .= "<td>-</td>"; 
                }
                $contenido .= "<td>" . $intervenciones['tin_nombre'] . "</td>";
                $contenido .= "<td>" . $intervenciones['ies_observacion'] . "</td>";
                $contenido .= "</tr>";
            }
            
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoAlumnosAtendidosCentroEscolar
     * Se muestra la busqueda de los resultados de los alumnos atendidos por Centro Escolar
     * SA 09/03/2021
     **/
    static function crearListadoAlumnosAtendidosCentroEscolar($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0" id="ninos_centro_escolar">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            if($_SESSION['tipo_usuario'] != 2){
                $contenido .= "<th colspan='2'></th>";
            }
            else{
                $contenido .= "<th></th>";
            }
            $contenido .= "<th colspan='2'>Primer Trimestre</th>";
            $contenido .= "<th colspan='2'>Segon Trimestre</th>";
            $contenido .= "<th colspan='2'>Tercer Trimestre</th>";
            $contenido .= "<th colspan='3'>Curs Escolar</th>";
            $contenido .= '</tr><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        $sql = "SELECT * FROM periodos_escolares 
						WHERE pes_ano_lectivo = '".$datos['ano_lectivo']."';";
        $periodos_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultado){
            foreach ($resultado as $campo => $etiqueta) {
                $contenido .= "<tr>";
                $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
                if($_SESSION['tipo_usuario'] != 2){
                    $contenido .= "<td>" . $etiqueta['usu_nombre'] . " " . $etiqueta['usu_apellido'] . "</td>";
                }

                $primer_trimestre = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                    FROM intervencion 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                    JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                    centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                    periodos_escolares.pes_id = '.$periodos_escolares[0]['pes_id'].';';
                $primer_trimestre = $_obj_database->obtenerRegistrosAsociativos($primer_trimestre);
                $segundo_trimestre = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                    FROM intervencion 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                    JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                    centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                    periodos_escolares.pes_id = '.$periodos_escolares[1]['pes_id'].';';
                $segundo_trimestre = $_obj_database->obtenerRegistrosAsociativos($segundo_trimestre);
                $tercer_trimestre = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                    FROM intervencion 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                    JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                    centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                    periodos_escolares.pes_id = '.$periodos_escolares[2]['pes_id'].';';
                $tercer_trimestre = $_obj_database->obtenerRegistrosAsociativos($tercer_trimestre);

                $primer_fem = 0;
                $primer_masc = 0;
                $segundo_fem = 0;
                $segundo_masc = 0;
                $tercero_fem = 0;
                $tercero_masc = 0;
                $no_hay = 0;

                if($primer_trimestre){
                    foreach($primer_trimestre as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $primer_fem++;
                        }
                        else{
                            $primer_masc++;
                        }
                    }
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$primer_masc."</td>";
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$primer_fem."</td>";
                }
                else{
                    $contenido .= "<td style='text-align:center;'>0</td>";
                    $contenido .= "<td style='text-align:center;'>0</td>";
                }

                if($segundo_trimestre){
                    foreach($segundo_trimestre as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $segundo_fem++;
                        }
                        else{
                            $segundo_masc++;
                        }
                    }
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$segundo_masc."</td>";
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$segundo_fem."</td>";
                }
                else{
                    $contenido .= "<td style='text-align:center;'>0</td>";
                    $contenido .= "<td style='text-align:center;'>0</td>";
                }

                if($tercer_trimestre){
                    foreach($tercer_trimestre as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $tercero_fem++;
                        }
                        else{
                            $tercero_masc++;
                        }
                    }
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$tercero_masc."</td>";
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$tercero_fem."</td>";
                }
                else{
                    $contenido .= "<td style='text-align:center;'>0</td>";
                    $contenido .= "<td style='text-align:center;'>0</td>";
                }

                $total_fem = $primer_fem + $segundo_fem + $tercero_fem;
                $total_masc = $primer_masc + $segundo_masc + $tercero_masc;
                $total = $total_fem + $total_masc;

                $curso_escolar = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                FROM intervencion 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                periodos_escolares.pes_ano_lectivo = "'.$datos['ano_lectivo'].'";';
                $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($curso_escolar);

                $curso_fem = 0;
                $curso_masc = 0;

                if($curso_escolar){
                    foreach($curso_escolar as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $curso_fem++;
                        }
                        else{
                            $curso_masc++;
                        }
                    }
                }

                $curso_total = $curso_fem + $curso_masc;

                $contenido .= "<td style='text-align:center;color:#3775EA;'>".$curso_masc."</td>";
                $contenido .= "<td style='text-align:center;color:#3775EA;'>".$curso_fem."</td>";
                $contenido .= "<td style='text-align:center;color:#3775EA;'>".$curso_total."</td>";
                $contenido .= "</tr>";
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoAlumnosAtendidosCentroEscolarExportar
     * Se muestra la busqueda de los resultados de los alumnos atendidos por Centro Escolar para Exportar
     * SA 24/03/2021
     **/
    static function crearListadoAlumnosAtendidosCentroEscolarExportar($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            if($_SESSION['tipo_usuario'] != 2){
                $contenido .= "<th colspan='2'></th>";
            }
            else{
                $contenido .= "<th></th>";
            }
            $contenido .= "<th colspan='2'>Primer Trimestre</th>";
            $contenido .= "<th colspan='2'>Segon Trimestre</th>";
            $contenido .= "<th colspan='2'>Tercer Trimestre</th>";
            $contenido .= "<th colspan='3'>Curs Escolar</th>";
            $contenido .= '</tr><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        $sql = "SELECT * FROM periodos_escolares 
                        WHERE pes_ano_lectivo = '".$datos['ano_lectivo']."';";
        $periodos_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultado){
            foreach ($resultado as $campo => $etiqueta) {
                $contenido .= "<tr>";
                $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
                if($_SESSION['tipo_usuario'] != 2){
                    $contenido .= "<td>" . $etiqueta['usu_nombre'] . " " . $etiqueta['usu_apellido'] . "</td>";
                }

                $primer_trimestre = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                    FROM intervencion 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                    JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                    centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                    periodos_escolares.pes_id = '.$periodos_escolares[0]['pes_id'].';';
                $primer_trimestre = $_obj_database->obtenerRegistrosAsociativos($primer_trimestre);
                $segundo_trimestre = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                    FROM intervencion 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                    JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                    centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                    periodos_escolares.pes_id = '.$periodos_escolares[1]['pes_id'].';';
                $segundo_trimestre = $_obj_database->obtenerRegistrosAsociativos($segundo_trimestre);
                $tercer_trimestre = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                    FROM intervencion 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                    JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                    centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                    periodos_escolares.pes_id = '.$periodos_escolares[2]['pes_id'].';';
                $tercer_trimestre = $_obj_database->obtenerRegistrosAsociativos($tercer_trimestre);

                $primer_fem = 0;
                $primer_masc = 0;
                $segundo_fem = 0;
                $segundo_masc = 0;
                $tercero_fem = 0;
                $tercero_masc = 0;
                $no_hay = 0;

                if($primer_trimestre){
                    foreach($primer_trimestre as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $primer_fem++;
                        }
                        else{
                            $primer_masc++;
                        }
                    }
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$primer_masc."</td>";
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$primer_fem."</td>";
                }
                else{
                    $contenido .= "<td style='text-align:center;'>0</td>";
                    $contenido .= "<td style='text-align:center;'>0</td>";
                }

                if($segundo_trimestre){
                    foreach($segundo_trimestre as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $segundo_fem++;
                        }
                        else{
                            $segundo_masc++;
                        }
                    }
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$segundo_masc."</td>";
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$segundo_fem."</td>";
                }
                else{
                    $contenido .= "<td style='text-align:center;'>0</td>";
                    $contenido .= "<td style='text-align:center;'>0</td>";
                }

                if($tercer_trimestre){
                    foreach($tercer_trimestre as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $tercero_fem++;
                        }
                        else{
                            $tercero_masc++;
                        }
                    }
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$tercero_masc."</td>";
                    $contenido .= "<td style='text-align:center;color:#3775EA;'>".$tercero_fem."</td>";
                }
                else{
                    $contenido .= "<td style='text-align:center;'>0</td>";
                    $contenido .= "<td style='text-align:center;'>0</td>";
                }

                $total_fem = $primer_fem + $segundo_fem + $tercero_fem;
                $total_masc = $primer_masc + $segundo_masc + $tercero_masc;
                $total = $total_fem + $total_masc;

                $curso_escolar = 'SELECT DISTINCT estudiante.est_id, estudiante.est_genero 
                                FROM intervencion 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
                                JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                WHERE visita.vis_usu_id_promotor = '.$etiqueta['usu_id'].' AND 
                                centro_escolar.ces_nombre = "'.$etiqueta['ces_nombre'].'" AND 
                                periodos_escolares.pes_ano_lectivo = "'.$datos['ano_lectivo'].'";';
                $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($curso_escolar);

                $curso_fem = 0;
                $curso_masc = 0;

                if($curso_escolar){
                    foreach($curso_escolar as $key => $value){
                        if($value['est_genero'] == 'F'){
                            $curso_fem++;
                        }
                        else{
                            $curso_masc++;
                        }
                    }
                }

                $curso_total = $curso_fem + $curso_masc;

                $contenido .= "<td style='text-align:center;color:#3775EA;'>".$curso_masc."</td>";
                $contenido .= "<td style='text-align:center;color:#3775EA;'>".$curso_fem."</td>";
                $contenido .= "<td style='text-align:center;color:#3775EA;'>".$curso_total."</td>";
                $contenido .= "</tr>";
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /**
     * BDL::29-12-2020
     * Listar Centros Escolares
     */
    static function crearListadocentrosExportar($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        //Exportar listado
        $contenido .= "";
        $atributos .= array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped" id="listadoCentros">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['ces_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ste_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ter_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['tce_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_maxcomplejidad'] . "</td>";
            $contenido .= "<td>" . $etiqueta['usu_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_num_gitanos'] . "</td>";
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }
    /** crearListadoUltimaVisitaExportar
     * Se muestra el listado de las intervenciones en la ultima visita para exportar
     * SA 23/03/2021
     **/
    static function crearListadoUltimaVisitaExportar($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        foreach ($resultado as $campo => $etiqueta) {

            if($etiqueta['int_tab_id'] == 1){
                $sql = "SELECT estudiante.est_nombre, motivo_intervencion.min_nombre, tipo_absentismo.tab_nombre, tipo_intervencion.tin_nombre,
                    intervencion_estudiante.ies_observacion
                    FROM intervencion 
                    JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
                    JOIN tipo_absentismo ON tipo_absentismo.tab_id = intervencion.int_tab_id 
                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id
                    WHERE intervencion.int_id = ".$etiqueta['int_id'].";";
            }
            else{
                $sql = "SELECT estudiante.est_nombre, motivo_intervencion.min_nombre, tipo_intervencion.tin_nombre,
                    intervencion_estudiante.ies_observacion
                    FROM intervencion 
                    JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id
                    WHERE intervencion.int_id = ".$etiqueta['int_id'].";";
            }
            $resultado_intervenciones = $_obj_database->obtenerRegistrosAsociativos($sql);

            
            foreach ($resultado_intervenciones as $campo => $intervenciones) {
                $decrypt_name = $_obj_interfaz->encrypt_decrypt('decrypt', $intervenciones['est_nombre']);
                $contenido .= "<tr>";
                $contenido .= "<td>" . $decrypt_name . "</td>";
                $contenido .= "<td>" . $intervenciones['min_nombre'] . "</td>";
                if($intervenciones['tab_nombre']){
                    $contenido .= "<td>" . $intervenciones['tab_nombre'] . "</td>"; 
                }
                else{
                    $contenido .= "<td>-</td>"; 
                }
                $contenido .= "<td>" . $intervenciones['tin_nombre'] . "</td>";
                $contenido .= "<td>" . $intervenciones['ies_observacion'] . "</td>";
                $contenido .= "</tr>";
            }
            
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }
    /**
     * BDL::24-12-2021
     * crearListadoInformeTipoIntervencion
     */
    static function crearListadoInformeTipoIntervencion($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $servicio_territorial = $arreglo_datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $arreglo_datos['FORMATO_BUSQ']['territorio'];
        $anio_lectivo = $arreglo_datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $arreglo_datos['FORMATO_BUSQ']['centro_escolar'];

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                        <div style="padding:25px;">';
        if($servicio_territorial != null){
            $contenido .= '<h5>SERVEI TERRITORIAL: '.$servicio_territorial.'</h5>';
        }
        if($territorio != null){
            $contenido .= '<h5>TERRITORI: '.$territorio.'</h5>';
        }
        if($anio_lectivo != null){
            $contenido .= '<h5>CURS ESCOLAR: '.$anio_lectivo.'</h5>';
        }
        if($centro_escolar != null){
            $contenido .= '<h5>CENTRE EDUCATIU: '.$centro_escolar.'</h5>';
        }
                
                        $contenido .=   '   </div><table class="table table-striped" id="">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
            $contenido .= '</tr></thead>';
        }
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['tin_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_i'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_ii'] . "</td>";
            $contenido .= "<td>" . $etiqueta['per_iii'] . "</td>";
            $contenido .= "<td>" . $etiqueta['total'] . "</td>";
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';
        $contenido .= '</table></div></div>';

        return $contenido;
    }

    static function crearListadoTerritorios($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table5">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= "<th data-sortable='false'></th>";
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['ter_id'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ter_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ste_nombre'] . "</td>";
            if(isset($opciones['actualizar'])){
                $enlace = "ter_id=".$etiqueta['ter_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                    <a role='button' class='btn btn-outline-success' href=\"index.php?m=territorios&accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
                        Edita
                    </a>
                </td>";
            }
            if(isset($opciones['eliminar'])){
                $enlace = "ter_id=".$etiqueta['ter_id']."";
                //Fin de for($i=0; $i<count($atributos); $i++)
                $contenido .= "
                <td align=\"center\" height='38' valign='middle'>
                <!-- Modal -->
                <div class='modal fade' id='DeleteModalTer".$etiqueta['ter_id']."' tabindex='-1' aria-labelledby='DeleteModalTer".$etiqueta['ter_id']."Label' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header bg-danger'>
                        <h5 class='modal-title white' id='exampleModalLabel'>Eliminar Centre</h5>
                        <button type='button' class='close rounded-pill' data-dismiss='modal' aria-label='Close'>x</button>
                      </div>
                      <div class='modal-body'>
                        Voleu eliminar el Territori '" . $etiqueta['ter_nombre'] . "'?
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancel</button>
                        <a role='button' class='btn btn-danger' href=\"index.php?m=territorios&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\">Eliminar</a>
                      </div>
                    </div>
                  </div>
                </div>
                    <button type='button' class='btn btn-outline-danger' data-toggle='modal' data-target='#DeleteModalTer".$etiqueta['ter_id']."' >
                        Eliminar
                    </button>
                </td>";
            }
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';


        //Exportar listado
        /* $contenido .= "";
        $atributos .= array_keys($arreglo_datos['CAMPOS_EXPORTAR']);
        $contenido .= '<div class="card-content" style="display:none;">
                        <div class="table-responsive">
                            <table class="table table-striped" id="listadoCentros">';
        
        if(count($arreglo_datos['CAMPOS_EXPORTAR']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS_EXPORTAR'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $usu_id = $arreglo_datos['PROMOTOR'];
        foreach ($resultado as $campo => $etiqueta) {
            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['ces_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ter_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['tce_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_maxcomplejidad'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ces_num_gitanos'] . "</td>";
            $contenido .= "</tr>";
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>'; */

        return $contenido;
    }

    /** crearListadoSeguimientoAlumno
     * Se muestra el listado para el seguimiento de un alumno
     * SA 01/05/2021
     **/
    static function crearListadoSeguimientoAlumno($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0" id="ultima_visita">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        foreach ($resultado as $campo => $etiqueta) {

            if($etiqueta['int_tab_id'] != 0){
                $sql = "SELECT tab_nombre
                    FROM tipo_absentismo 
                    WHERE tab_id = ".$etiqueta['int_tab_id'].";";
                $resultado_intervenciones = $_obj_database->obtenerRegistrosAsociativos($sql);
            }
            

            $etiqueta['int_tab_id'] = $resultado_intervenciones[0]['tab_nombre'];

            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['vis_fecha'] . "</td>";
            $contenido .= "<td>" . $etiqueta['min_nombre'] . "</td>";
            if($etiqueta['int_tab_id']){
                $contenido .= "<td>" . $etiqueta['int_tab_id'] . "</td>"; 
            }
            else{
                $contenido .= "<td>-</td>"; 
            }
            $contenido .= "<td>" . $etiqueta['tin_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ies_observacion'] . "</td>";
            $contenido .= "</tr>";
            
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoSeguimientoAlumnoExportar
     * Se muestra el listado para el seguimiento de un alumno
     * SA 01/05/2021
     **/
    static function crearListadoSeguimientoAlumnoExportar($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        foreach ($resultado as $campo => $etiqueta) {

            if($etiqueta['int_tab_id'] != 0){
                $sql = "SELECT tab_nombre
                    FROM tipo_absentismo 
                    WHERE tab_id = ".$etiqueta['int_tab_id'].";";
                $resultado_intervenciones = $_obj_database->obtenerRegistrosAsociativos($sql);
            }

            $etiqueta['int_tab_id'] = $resultado_intervenciones[0]['tab_nombre'];

            $contenido .= "<tr>";
            $contenido .= "<td>" . $etiqueta['vis_fecha'] . "</td>";
            $contenido .= "<td>" . $etiqueta['min_nombre'] . "</td>";
            if($etiqueta['int_tab_id']){
                $contenido .= "<td>" . $etiqueta['int_tab_id'] . "</td>"; 
            }
            else{
                $contenido .= "<td>-</td>"; 
            }
            $contenido .= "<td>" . $etiqueta['tin_nombre'] . "</td>";
            $contenido .= "<td>" . $etiqueta['ies_observacion'] . "</td>";
            $contenido .= "</tr>";
            
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        return $contenido;
    }

    /** crearListadoFamilia
     * Se muestra la busqueda de los resultados de las familias atendidas por curso escolar
     * SA 07/05/2021
     **/
    static function crearListadoFamilia($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";

        $contenido .= '<div class="list-group list-group-horizontal-sm mb-1 text-center" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="family-periodo-list" data-toggle="list" href="#family-periodo" role="tab" aria-selected="false">Informes Família General</a>
                        <a class="list-group-item list-group-item-action" id="family-centro-list" data-toggle="list" href="#family-centro" role="tab" aria-selected="true">Informe Família per Centre Escolar</a>
                    </div>';


        $contenido .= '<div class="tab-content text-justify">
                        <div class="tab-pane fade show active" id="family-periodo" role="tabpanel"
                            aria-labelledby="family-periodo">';
    
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        $sql = "SELECT * FROM periodos_escolares 
                        WHERE pes_ano_lectivo = '".$datos['ano_lectivo']."';";
        $periodos_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultado){
            $primero = 0;
            $segundo = 0;
            $tercero = 0;
            if($_SESSION['tipo_usuario'] != 2){
                $nombre_promotor = $resultado[0]['usu_nombre'] . ' ' . $resultado[0]['usu_apellido'];
                $id_promotor = $resultado[0]['usu_id'];
                foreach ($resultado as $campo => $etiqueta) {
                    $prueba_promotor = $etiqueta['usu_nombre'] . ' ' . $etiqueta['usu_apellido'];
                    if($nombre_promotor == $prueba_promotor){
                        if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id']." AND 
                                    territorio.ter_id = ".$etiqueta['ter_id'].";";
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $primero = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id']." AND 
                                    territorio.ter_id = ".$etiqueta['ter_id'].";";
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $segundo = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor
                                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id  
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id']." AND 
                                    territorio.ter_id = ".$etiqueta['ter_id'].";";
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $tercero = count($familias);
                        }
                    }
                    else{
                        $contenido .= "<tr>";
                        $contenido .= '<td>'.$nombre_promotor.'</td>';
                        $contenido .= '<td>'.$primero.'</td>';
                        $contenido .= '<td>'.$segundo.'</td>';
                        $contenido .= '<td>'.$tercero.'</td>';
                        $sql_curso = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE usuarios.usu_id = ".$id_promotor." AND 
                                    periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                                    territorio.ter_id = ".$etiqueta['ter_id'].";";
                        //Devuelve la cantidad del curso 
                        $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                        $contenido .= '<td>'.count($curso_escolar).'</td>';
                        $contenido .= "</tr>";

                        $primero = 0;
                        $segundo = 0;
                        $tercero = 0;

                        $nombre_promotor = $etiqueta['usu_nombre'] . ' ' . $etiqueta['usu_apellido'];
                        $id_promotor = $etiqueta['usu_id'];
                        if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id']." AND 
                                    territorio.ter_id = ".$etiqueta['ter_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $primero = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id']." AND 
                                    territorio.ter_id = ".$etiqueta['ter_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $segundo = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor
                                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id  
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id']." AND 
                                    territorio.ter_id = ".$etiqueta['ter_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $tercero = count($familias);
                        }
                    }
                }
                $contenido .= "<tr>";
                $contenido .= '<td>'.$nombre_promotor.'</td>';
                $contenido .= '<td>'.$primero.'</td>';
                $contenido .= '<td>'.$segundo.'</td>';
                $contenido .= '<td>'.$tercero.'</td>';
                $sql_curso = "SELECT DISTINCT familia.fam_id
                            FROM intervencion 
                            JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                            JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                            JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                            JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                            JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                            JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                            JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                            JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                            JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                            WHERE usuarios.usu_id = ".$id_promotor." AND 
						    periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                            territorio.ter_id = ".$etiqueta['ter_id'].";";
                //Devuelve la cantidad del curso 
                $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                $contenido .= '<td>'.count($curso_escolar).'</td>';
                $contenido .= "</tr>";
            }
            else{
                $contenido .= "<tr>";
                $etiqueta = $resultado[0];
                foreach($resultado as $campo => $etiqueta){
                    if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                        $sql = "SELECT DISTINCT familia.fam_id
                                FROM intervencion 
                                JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                                JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
                                JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
                                periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                                periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";
                        $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                        $primero = count($familias);
                    }

                    if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                        $sql = "SELECT DISTINCT familia.fam_id
                                FROM intervencion 
                                JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                                JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
                                JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
                                periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                                periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";
                        $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                        $segundo = count($familias);
                    }

                    if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                        $sql = "SELECT DISTINCT familia.fam_id
                                FROM intervencion 
                                JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                                JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
                                JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
                                periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                                periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";
                        $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                        $tercero = count($familias);
                    }
                }

                $contenido .= '<td>'.$primero.'</td>';
                $contenido .= '<td>'.$segundo.'</td>';
                $contenido .= '<td>'.$tercero.'</td>';
                    
                $sql_curso = "SELECT DISTINCT familia.fam_id
                    FROM intervencion 
                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                    WHERE usuarios.usu_id = ".$_SESSION['usu_id']." AND 
                    periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";

                //Devuelve la cantidad del curso 
                $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                $contenido .= '<td>'.count($curso_escolar).'</td>';
                $contenido .= "</tr>";
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';
        
        $contenido .= '</div>';

        //FAMILIA CENTROS 
        $contenido .= '<div class="tab-pane fade" id="family-centro" role="tabpanel"
                            aria-labelledby="family-centro-list">';


        $atributos = array_keys($arreglo_datos['CAMPOS_CENTRO']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS_CENTRO']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS_CENTRO'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $resultado_centros = $arreglo_datos['VALORES_CENTROS'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        $sql = "SELECT * FROM periodos_escolares 
                        WHERE pes_ano_lectivo = '".$datos['ano_lectivo']."';";
        $periodos_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultado_centros){
            if($_SESSION['tipo_usuario'] != 2){
                foreach($resultado_centros as $campos => $valores){
                    $primero = 0;
                    $segundo = 0;
                    $tercero = 0;
                    $centro_escolar = $valores['ces_nombre'];
                    $promotor = $valores['usu_id'];
                    foreach ($resultado as $campo => $etiqueta) {
                        if($promotor == $etiqueta['usu_id'] && $centro_escolar == $etiqueta['ces_nombre']){
                            if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                                $primero++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                                $segundo++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                                $tercero++;
                            }
                        }
                    }
                    $contenido .= "<tr>";
                    $contenido .= '<td>'.$centro_escolar.'</td>';
                    $contenido .= '<td>'. $valores['usu_nombre'] . ' ' . $valores['usu_apellido'].'</td>';
                    $contenido .= '<td>'.$primero.'</td>';
                    $contenido .= '<td>'.$segundo.'</td>';
                    $contenido .= '<td>'.$tercero.'</td>';
                    $sql_curso = "SELECT DISTINCT familia.fam_id
                        FROM intervencion 
                        JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                        JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                        JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                        JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                        JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                        JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                        JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                        JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                        WHERE territorio.ter_id = ".$datos['territorio']." AND 
                        centro_escolar.ces_id = ".$valores['ces_id']." AND
                        usuarios.usu_id = ".$valores['usu_id']." AND 
                        periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";

                    //Devuelve la cantidad del curso 
                    $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                    $contenido .= '<td>'.count($curso_escolar).'</td>';
                    $contenido .= "</tr>";
                }
            }
            else{
                foreach($resultado_centros as $campos => $valores){
                    $primero = 0;
                    $segundo = 0;
                    $tercero = 0;
                    $centro_escolar = $valores['ces_nombre'];
                    foreach ($resultado as $campo => $etiqueta) {
                        if($centro_escolar == $etiqueta['ces_nombre']){
                            if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                                $primero++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                                $segundo++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                                $tercero++;
                            }
                        }
                    }
                    $contenido .= "<tr>";
                    $contenido .= '<td>'.$centro_escolar.'</td>';
                    $contenido .= '<td>'.$primero.'</td>';
                    $contenido .= '<td>'.$segundo.'</td>';
                    $contenido .= '<td>'.$tercero.'</td>';
                    $sql_curso = "SELECT DISTINCT familia.fam_id
                        FROM intervencion 
                        JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                        JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                        JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                        JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                        JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                        JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                        JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                        JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                        WHERE usuarios.usu_id = ".$_SESSION['usu_id']." AND 
                        centro_escolar.ces_id = ".$valores['ces_id']." AND 
                        periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";

                    //Devuelve la cantidad del curso 
                    $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                    $contenido .= '<td>'.count($curso_escolar).'</td>';
                    $contenido .= "</tr>";
                }
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        $contenido .= '</div>';
        $contenido .= '</div>';

        return $contenido;
    }

    /** crearListadoFamiliaExportar
     * Se muestra la busqueda de los resultados de las familias atendidas por curso escolar
     * SA 07/05/2021
     **/
    static function crearListadoFamiliaExportar($arreglo_datos){
        global $_PATH_WEB, $_PATH_IMAGENES, $_obj_database;

        $contenido = "";

        $contenido .= '<div class="list-group list-group-horizontal-sm mb-1 text-center" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="family-periodo-list" data-toggle="list" href="#family-periodo" role="tab" aria-selected="false">Informes Família General</a>
                        <a class="list-group-item list-group-item-action" id="family-centro-list" data-toggle="list" href="#family-centro" role="tab" aria-selected="true">Informe Família per Centre Escolar</a>
                    </div>';


        $contenido .= '<div class="tab-content text-justify">
                        <div class="tab-pane fade show active" id="family-periodo" role="tabpanel"
                            aria-labelledby="family-periodo">';
    
        $atributos = array_keys($arreglo_datos['CAMPOS']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS']) > 0){
            $contenido .= '<thead><tr>';
            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        $sql = "SELECT * FROM periodos_escolares 
                        WHERE pes_ano_lectivo = '".$datos['ano_lectivo']."';";
        $periodos_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultado){
            $primero = 0;
            $segundo = 0;
            $tercero = 0;
            if($_SESSION['tipo_usuario'] != 2){
                $nombre_promotor = $resultado[0]['usu_nombre'] . ' ' . $resultado[0]['usu_apellido'];
                $id_promotor = $resultado[0]['usu_id'];
                foreach ($resultado as $campo => $etiqueta) {
                    $prueba_promotor = $etiqueta['usu_nombre'] . ' ' . $etiqueta['usu_apellido'];
                    if($nombre_promotor == $prueba_promotor){
                        if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $primero = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $segundo = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $tercero = count($familias);
                        }
                    }
                    else{
                        $contenido .= "<tr>";
                        $contenido .= '<td>'.$nombre_promotor.'</td>';
                        $contenido .= '<td>'.$primero.'</td>';
                        $contenido .= '<td>'.$segundo.'</td>';
                        $contenido .= '<td>'.$tercero.'</td>';
                        $sql_curso = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";

                        //Devuelve la cantidad del curso 
                        $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                        $contenido .= '<td>'.count($curso_escolar).'</td>';
                        $contenido .= "</tr>";

                        $primero = 0;
                        $segundo = 0;
                        $tercero = 0;

                        $nombre_promotor = $etiqueta['usu_nombre'] . ' ' . $etiqueta['usu_apellido'];
                        $id_promotor = $etiqueta['usu_id'];
                        if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $primero = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $segundo = count($familias);
                        }
        
                        if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                            $sql = "SELECT DISTINCT familia.fam_id
                                    FROM intervencion 
                                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id
                                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                    WHERE visita.vis_usu_id_promotor = ".$id_promotor." AND  
                                    periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";

                            
                            $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                            $tercero = count($familias);
                        }
                    }
                }
                $contenido .= "<tr>";
                $contenido .= '<td>'.$nombre_promotor.'</td>';
                $contenido .= '<td>'.$primero.'</td>';
                $contenido .= '<td>'.$segundo.'</td>';
                $contenido .= '<td>'.$tercero.'</td>';
                $sql_curso = "SELECT DISTINCT familia.fam_id
                            FROM intervencion 
                            JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                            JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                            JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                            JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                            JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                            JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                            JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                            JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                            JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                            WHERE usuarios.usu_id = ".$id_promotor." AND 
						    periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";

                //Devuelve la cantidad del curso 
                $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                $contenido .= '<td>'.count($curso_escolar).'</td>';
                $contenido .= "</tr>";
            }
            else{
                $contenido .= "<tr>";
                $etiqueta = $resultado[0];
                foreach($resultado as $campo => $etiqueta){
                    if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                        $sql = "SELECT DISTINCT familia.fam_id
                                FROM intervencion 
                                JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                                JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
                                JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
                                periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                                periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";
                        $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                        $primero = count($familias);
                    }

                    if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                        $sql = "SELECT DISTINCT familia.fam_id
                                FROM intervencion 
                                JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                                JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
                                JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
                                periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                                periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";
                        $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                        $segundo = count($familias);
                    }

                    if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                        $sql = "SELECT DISTINCT familia.fam_id
                                FROM intervencion 
                                JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                                JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                                JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                                JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                                JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                                JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                                JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                                JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
                                JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                                JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                                WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
                                periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' AND 
                                periodos_escolares.pes_id = ".$etiqueta['int_pes_id'].";";
                        $familias = $_obj_database->obtenerRegistrosAsociativos($sql);
                        $tercero = count($familias);
                    }
                }

                $contenido .= '<td>'.$primero.'</td>';
                $contenido .= '<td>'.$segundo.'</td>';
                $contenido .= '<td>'.$tercero.'</td>';
                    
                $sql_curso = "SELECT DISTINCT familia.fam_id
                    FROM intervencion 
                    JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                    JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                    JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                    JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                    JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                    JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                    JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                    JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                    JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                    WHERE usuarios.usu_id = ".$_SESSION['usu_id']." AND 
                    periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";

                //Devuelve la cantidad del curso 
                $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                $contenido .= '<td>'.count($curso_escolar).'</td>';
                $contenido .= "</tr>";
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';
        
        $contenido .= '</div>';

        //FAMILIA CENTROS 
        $contenido .= '<div class="tab-pane fade" id="family-centro" role="tabpanel"
                            aria-labelledby="family-centro-list">';


        $atributos = array_keys($arreglo_datos['CAMPOS_CENTRO']);
        $contenido .= '<div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">';
        
        if(count($arreglo_datos['CAMPOS_CENTRO']) > 0){
            $contenido .= '<thead><tr></tr></thead><thead><tr>';
            foreach ($arreglo_datos['CAMPOS_CENTRO'] as $campo => $etiqueta) {
                $contenido .= "<th>$etiqueta</th>";
            }
        }
        $contenido .= '</tr></thead>';
        $contenido .= '<tbody>';
        $resultado = $arreglo_datos['VALORES'];
        $resultado_centros = $arreglo_datos['VALORES_CENTROS'];
        $opciones = $arreglo_datos['OPCIONES'];
        $datos = $arreglo_datos['DATOS'];

        $sql = "SELECT * FROM periodos_escolares 
                        WHERE pes_ano_lectivo = '".$datos['ano_lectivo']."';";
        $periodos_escolares = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultado_centros){
            if($_SESSION['tipo_usuario'] != 2){
                foreach($resultado_centros as $campos => $valores){
                    $primero = 0;
                    $segundo = 0;
                    $tercero = 0;
                    $centro_escolar = $valores['ces_nombre'];
                    $promotor = $valores['usu_id'];
                    foreach ($resultado as $campo => $etiqueta) {
                        if($promotor == $etiqueta['usu_id'] && $centro_escolar == $etiqueta['ces_nombre']){
                            if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                                $primero++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                                $segundo++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                                $tercero++;
                            }
                        }
                    }
                    $contenido .= "<tr>";
                    $contenido .= '<td>'.$centro_escolar.'</td>';
                    $contenido .= '<td>'. $valores['usu_nombre'] . ' ' . $valores['usu_apellido'].'</td>';
                    $contenido .= '<td>'.$primero.'</td>';
                    $contenido .= '<td>'.$segundo.'</td>';
                    $contenido .= '<td>'.$tercero.'</td>';
                    $sql_curso = "SELECT DISTINCT familia.fam_id
                        FROM intervencion 
                        JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                        JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                        JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                        JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                        JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                        JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                        JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                        JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                        WHERE territorio.ter_id = ".$datos['territorio']." AND 
                        centro_escolar.ces_id = ".$valores['ces_id']." AND
                        usuarios.usu_id = ".$valores['usu_id']." AND 
                        periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";

                    //Devuelve la cantidad del curso 
                    $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                    $contenido .= '<td>'.count($curso_escolar).'</td>';
                    $contenido .= "</tr>";
                }
            }
            else{
                foreach($resultado_centros as $campos => $valores){
                    $primero = 0;
                    $segundo = 0;
                    $tercero = 0;
                    $centro_escolar = $valores['ces_nombre'];
                    foreach ($resultado as $campo => $etiqueta) {
                        if($centro_escolar == $etiqueta['ces_nombre']){
                            if($etiqueta['int_pes_id'] == $periodos_escolares[0]['pes_id']){
                                $primero++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[1]['pes_id']){
                                $segundo++;
                            }
                            if($etiqueta['int_pes_id'] == $periodos_escolares[2]['pes_id']){
                                $tercero++;
                            }
                        }
                    }
                    $contenido .= "<tr>";
                    $contenido .= '<td>'.$centro_escolar.'</td>';
                    $contenido .= '<td>'.$primero.'</td>';
                    $contenido .= '<td>'.$segundo.'</td>';
                    $contenido .= '<td>'.$tercero.'</td>';
                    $sql_curso = "SELECT DISTINCT familia.fam_id
                        FROM intervencion 
                        JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
                        JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
                        JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
                        JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
                        JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
                        JOIN familia ON estudiante.est_fam_id = familia.fam_id 
                        JOIN visita ON visita.vis_id = intervencion.int_vis_id 
                        JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
                        JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
                        WHERE usuarios.usu_id = ".$_SESSION['usu_id']." AND 
                        centro_escolar.ces_id = ".$valores['ces_id']." AND 
                        periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";

                    //Devuelve la cantidad del curso 
                    $curso_escolar = $_obj_database->obtenerRegistrosAsociativos($sql_curso);
                    $contenido .= '<td>'.count($curso_escolar).'</td>';
                    $contenido .= "</tr>";
                }
            }
        }
        else{
            $contenido .= '<p style="background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;text-align:center;">No hi ha resultats disponibles</p>';
        }
        $contenido .= '</tbody>';    

        $contenido .= '</table></div></div>';

        $contenido .= '</div>';
        $contenido .= '</div>';

        return $contenido;
    }

    /*
    Function encrypt_decrypt
    Params: ($action) encrypt/decrypt , ($string) string to encrypt/decrypt
    Return string with content encrypt/decrypt
    */
    public function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = '5a1s5fef';
        $secret_iv = '4as15e11';
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

}//class
?>