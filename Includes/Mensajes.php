<?php

class Mensajes
{

	var $_contenido_mensajes;
	
	function __construct()
	{
		$this->_contenido_mensajes = "";
	}

        //pgs - 13/01/2012 - se pone variable para cuando se desee mostrar el texto con o sin formato
	function crearMensaje($id_mensaje,$adicional=array(),$texto_sin_formato=false)
	{
		global $_PATH_IMAGENES,$_opciones,$_EMAIL_ADMIN;
		
		
		if( strlen($adicional['ACCION'])>1 )
		{
			$id_mensaje = 0;
		}
		
		switch($adicional['ACCION'])
		{
			case 0:
				$accion = "consultado";
				break;

			case 1:
				$accion = "ingresado";
				break;

			case 2:
				$accion = "modificado";
				break;

			case 3:
				$accion = "eliminado";
				break;
				
			case 4:
				$accion = "liberado";
				break;
				
			case 5:
				$accion = "enviado";
				break; 
				
			case 6:
				$accion = "anulado";
				break;
           
				
		}//Fin switch($tipo_accion_mensaje)
		
		switch($id_mensaje)
		{
            case 'RLET-1':
                $mensaje = 'La asoaciaci&oacute;n entre empresas de transporte y lineas se realiz� correctamente.';
				$tipo_mensaje = 1;
                break;
            case 'RLET-2':
                $mensaje = 'Se present� un error con el manejador de bases de datos al trata de asociar las empresas de transporte y las lineas.';
				$tipo_mensaje = 1;
                break;
            case 'RLET-3':
                $mensaje = 'No existen nuevos registros para actualizar.';
				$tipo_mensaje = 1;
                break;
                
            case 'RL-1':
                $mensaje = 'El n&uacute;mero de autorizaci&oacute;n ya se encuentra registrado. Para registrar el mismo n&uacute;mero
                        debe ingresar la observaci&oacute;n del duplicado.';
				$tipo_mensaje = 1;
                break;
                
            case 'M':
                $mensaje = $_SESSION[mensaje];
				$tipo_mensaje = 1;
                break;
			case 'TU-1':
                $mensaje = "La Aprobaci&oacute;n de los t&eacute;rminos de uso se registro correctamente.";
				$tipo_mensaje = 1;			
				break;
			case 'TU-2':
                $mensaje = "Se registro un error al registrar la Aprobaci&oacute;n de los t&eacute;rminos de uso.";
				$tipo_mensaje = 1;			
				break;
			case 'SL-1':
                $mensaje = "Debe ingresar el tipo de carga para solicitar la cita.";
				$tipo_mensaje = 0;			
				break;
			case 'SL-2':
                $mensaje = "Debe ingresar el n&uacute;mero de factura y serie para solicitar la cita.";
				$tipo_mensaje = 0;			
				break;
				
			case 'S3':
                $mensaje = "El archivo debe tener como m&aacute;ximo ".$_opciones[limite_pdf]." MB.";
				$tipo_mensaje = 0;			
				break;
			case 'S2':
                $mensaje = "La l&iacute;nea naviera ya est&aacute; relacionada con el Cliente.";
				$tipo_mensaje = 0;
				break;
			case 'AA2':
                $mensaje = "El cliente ya est&aacute; relacionado con el Agente aduanero.";
				$tipo_mensaje = 0;
				break;
            case 'CT-1':
                $mensaje = "La cita ya se encuentra anulada.";
				$tipo_mensaje = 0;
                break;
            case 'CT-2':
                $mensaje = "Para programar la cita debe cancelar todas las la factura asociadas al contenedor o debe tener asignado un cupo para cr&eacute;dito.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-3':
                $mensaje = "No se puede programar la cita en el d&iacute;a y hora seleccionados por que ya se cumplio el n&uacute;mero m&aacute;ximo de citas. Por favor seleccione otra fecha u hora.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-4':
                $mensaje = "No se puede programar la cita, el contenedor ya se encuentra en la terminal.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-5':
                $dias_cita = Herramientas::obtenerParametro("CITA_DIAS_DESPUES");
                $mensaje = "No se puede programar la cita, ya existe un registro para el ID de contendor, deben pasar $dias_cita d&iacute;as para solicitar una cita.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-6':
                $mensaje = "No se puede programar la cita, el contenedor no se encuentra en la terminal.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-7':
                $mensaje = "No se puede modificar la cita, el contenedor no se encuentra en la terminal.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-8':
                $mensaje = "No puede acceder al registro de la cita, se encuentra inactiva.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-9':
                $mensaje = "No se puede solicitar la cita, las dimensiones de la carga son superiores a las permitidas.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-10':
                $mensaje = "No se puede asignar mas contenedores a la cita.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-11':
                $mensaje = "No tiene los permisos para acceder al registro de la cita.";
				$tipo_mensaje = 0;
                break;
				
            case 'CT-12':
                $mensaje = "No se puede anular la cita, porque no tiene los permisos para acceder al registro.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-13':
                $mensaje = "No se puede modificar la cita, el contenedor ya se encuentra en la terminal.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-14':
                $mensaje = "El contenedor tiene retenciones por documentaci&oacute;n.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-15':
                $mensaje = "No se puede programar la cita. Ingreso de carga por fuera de los 9 d&iacute;as del ETA de la motonave.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-16':
                $mensaje = "La fecha ingresada para la cita no es v&aacute;lida. Fecha de operaci&oacute;n ingresada ".$_SESSION['CT-16-fecha']." - Fecha actual ".date("Y-m-d H:i:00").". La fecha de operaci&oacute;n debe ser mayor que la fecha actual.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-17':
                $mensaje = "No se puede solicitar la cita, no tiene una factura de puerta asignada.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-18':
                $mensaje = "No se puede solicitar la cita, la factura de puerta no esta pagada.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-19':
                $mensaje = "No se puede solicitar la cita, no tiene una factura de almacen asignada.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-19-1':
                $mensaje = "No se puede solicitar la cita, no tiene una factura asignada.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-20':
                $mensaje = "No se puede solicitar la cita, la factura de almacen no esta pagada.";
				$tipo_mensaje = 0;
                break;
                
            case 'CT-21':
                $mensaje = "La fecha l&iacute;mite para solicitar la cita ha expirado. ".$_SESSION['fecha_limite'];
                unset($_SESSION['fecha_limite']); 
				$tipo_mensaje = 0;
                break;
                
            case 'CT-22':
                $mensaje = "El c&oacute;digo ingresado para la cita es incorrecto, por favor verifique los datos enviados.";
				$tipo_mensaje = 0;
                break;
                
           case 'CT-23':
                $mensaje = "No se puede actualizar la cita. Contenedor con facturas pendientes de pago";
				$tipo_mensaje = 0;
                break;
                
           case 'CT-24':
                $mensaje = "Contenedor con factura de Puerta pendiente.";
				$tipo_mensaje = 0;
                break;
                
           case 'CT-25':
                $mensaje = "Contenedor  con facturas pendientes de pago.";
				$tipo_mensaje = 0;
                break;
                
           case 'CT-26':
                $mensaje = "Contenedor  con factura de almac�n pendiente.";
				$tipo_mensaje = 0;
                break;
                
           case 'CT-27':
                $mensaje = "Ha transcurrido bastante tiempo desde que inicio el proceso de Solicitar Cita. Por favor intente nuevamente.";
				$tipo_mensaje = 0;
                break;
                
           case 'CT-28':
                $mensaje = "Conductor no Enrolado.";
				$tipo_mensaje = 0;
                break; 
                
           //MAOH - 14 Ene 2012 - Se crea el mensaje para el codigo de registro incorrecto
           case 'CT-30':
                $mensaje = "El C&oacute;digo de Registro ingresado no es v&aacute;lido";
				$tipo_mensaje = 0;
                break;				
           //MAOH - 14 Ene 2012 - Fin
                      
           case 'CT-31P':
                //Carga el tiempo en horas para el cual no debe permitir ingresar un registro de cita
                //para el mismo numero de placa
                $HORAS_RANGO_CITA_PLACA_CONDUCTOR = Herramientas::obtenerParametro("HORAS_RANGO_CITA_PLACA_CONDUCTOR");
                
                //Se valida si el paramatro no existe entonces coloca un valor por defecto de cero
                if(!( $HORAS_RANGO_CITA_PLACA_CONDUCTOR >= 0))
                {
                    $HORAS_RANGO_CITA_PLACA_CONDUCTOR = 0;
                }
                
                $mensaje = "El n&uacute;mero de Placa no es v&aacute;lido porque ya tienen una cita asignada 
                    y deben pasar ".$HORAS_RANGO_CITA_PLACA_CONDUCTOR." hora(s) para pedir una nueva cita. ";
                    
				      $tipo_mensaje = 0;
                break;  
                
           case 'CT-31C':
                //Carga el tiempo en horas para el cual no debe permitir ingresar un registro de cita
                //para el mismo numero de conductor
                $HORAS_RANGO_CITA_PLACA_CONDUCTOR = Herramientas::obtenerParametro("HORAS_RANGO_CITA_PLACA_CONDUCTOR");
                
                //Se valida si el paramatro no existe entonces coloca un valor por defecto de cero
                if(!( $HORAS_RANGO_CITA_PLACA_CONDUCTOR >= 0))
                {
                    $HORAS_RANGO_CITA_PLACA_CONDUCTOR = 0;
                }
                
                $mensaje = "El n&uacute;mero de conductor no es v&aacute;lido porque ya tienen una cita asignada 
                    y deben pasar ".$HORAS_RANGO_CITA_PLACA_CONDUCTOR." hora(s) para pedir una nueva cita.";
                    
				      $tipo_mensaje = 0;
                break;    
                
           case 'CT-32':
                $mensaje = " Operaci&oacute;n de buque a&uacute;n no ha finalizado.";
				$tipo_mensaje = 0;
                break;	
           
		   //MAOH - 08 Mar 2012 - Mensaje de ejes de camion y trailer vacios     
           case 'CT-33':
                $mensaje = " El n&uacute;mero de ejes del cami&oacute;n y/o el n&uacute;mero de ejes del trailer es incorrecto.";
				$tipo_mensaje = 0;
                break;

            case 'CT-34':
                $mensaje = " La Placa no es v&aacute;lida.";
		$tipo_mensaje = 0;
                break;
                
			case 'CT-35':
                $mensaje = "La informaci&oacute;n para solicitar la cita no esta disponible debe reiniciar el proceso.";
				$tipo_mensaje = 1;			
				break;

            case 'CT-36':
                $mensaje = " La cita no se pudo ingresar debido a que la fecha de revisi&oacute;n tecno mec&aacute;nica de la placa ha caducado.";
		$tipo_mensaje = 0;
                break;

            case 'CT-37':
                $mensaje = " La cita no se pudo ingresar debido a que la fecha de revisi&oacute;n tecno mec&aacute;nica de la placa no es v&aacute;lida.";
		$tipo_mensaje = 0;
                break;

            case 'CT-38':
                $mensaje = "Conductor no habilitado en Argos.";
                $tipo_mensaje = 0;
                break;

            case 'CT-39':
                $mensaje = "Error al validar la fecha del pase comuniquese con el departamento de seguridad de Tcbuen.";
                $tipo_mensaje = 0;
                break;

            case 'CT-40':
                $mensaje = "Error al validar la fecha de la ARP comuniquese con el departamento de seguridad de Tcbuen.";
                $tipo_mensaje = 0;
                break;

            case 'CT-41':
                $mensaje = "Conductor con licencia vencida para el d&iacute;a ".$_SESSION['error_fecha_cita'].".";
                unset($_SESSION['error_fecha_cita']);
                $tipo_mensaje = 0;
                break;

            case 'CT-42':
                $mensaje = "Conductor sin cobertura de ARP hasta el d&iacute;a ".$_SESSION['error_fecha_cita'].".";
                unset($_SESSION['error_fecha_cita']);
                $tipo_mensaje = 0;
                break;

            case 'CT-43':
                $descripcion = $adicional['descripcion'] != "" ? $adicional['descripcion'] : $_SESSION['mensaje_retenciones']; 
                $mensaje = "No se puede crear la cita debido a que el contenedor tiene retenciones: ".$descripcion.".";
                unset($_SESSION['mensaje_retenciones']);
                $tipo_mensaje = 0;
                break;
                
            case 'CT-44':
                $mensaje = "No se puede solicitar citas en la fecha y hora ingresadas.";
                $tipo_mensaje = 0;
                break;
                
            case 'CT-45':
                $mensaje = "No se puede programar la cita, el anuncio no se encuentra activo en el sistema ARGOS.";
                $tipo_mensaje = 0;
                break;
                
            case 'CT-46':
                $mensaje = "No se encuentra el registro del viaje de la Motonave.";
                $tipo_mensaje = 0;
                break;
                
            case 'CT-47':
                $mensaje = "No existe una cita asociada a los datos ingresados para la b&uacute;squeda.";
                $tipo_mensaje = 0;
                break;
                
            case 'CT-48':
                $mensaje = 'Para contenedores DTA el campo N&uacute;mero de aceptaci&oacute;n es obligatorio.';
                $tipo_mensaje = 0;
                break;
                
            case 'CT-49':
                $descripcion = $adicional['descripcion'] != "" ? $adicional['descripcion'] : $_SESSION['mensaje_retenciones']; 
                $mensaje = "No se puede crear la cita debido a que la carga tiene las siguientes retenciones: <br> ".$descripcion.".";
                unset($_SESSION['mensaje_retenciones']);
                $tipo_mensaje = 0;
                break;

            //DMG - 06 jul 2012 - Se crea el mensaje para el codigo de autorizacion de la cita de urbaneo incorrecto
            case 'CU-1':
                $mensaje = "El C&oacute;digo de Autorizaci&oacute;n ingresado no es v&aacute;lido, verifique que sea el correcto.";
				$tipo_mensaje = 0;
                break;				
           //DMG - 6 jul 2012 - Fin

           //DMG - 10 jul 2012 - Se crea el mensaje para la cita de urbaneo 
            case 'CU-2':
                $mensaje = "El C&oacute;digo de Autorizaci&oacute;n ingresado no es v&aacute;lido, verifique que sea el correcto.";
				$tipo_mensaje = 0;
                break;				
           //DMG - 10 jul 2012 - Fin
           /*Se agregan los mensajes de la cita de urbaneo
               DMG 18 jul 2012*/		
			case 'CU-3':
			   $mensaje = "El Contenedor ingresado no existe en la lista de contenedores cargados";
			   	$tipo_mensaje = 0;
            break;	
			case 'CU-3-1':
			   $mensaje = "El Contenedor ingresado no esta anunciado en el sistema ARGOS.";
               if( $_SESSION['CU-3-1_contenedor'] != "" )
               {
                    $mensaje .= " Contenedor ".$_SESSION['CU-3-1_contenedor'];
                    unset($_SESSION['CU-3-1_contenedor']);
               }
			   	$tipo_mensaje = 0;
            break;
            case 'CU-4':
			    $mensaje = "El Conductor ingresado no existe en la lista de conductores ingresados anteriormente";
			   	$tipo_mensaje = 0;
            break;
            case 'CU-5':
			   	$mensaje = "La Placa ingresada no existe en la lista de placas ingresadas anteriormente";
			   	$tipo_mensaje = 0;
            break;
            
            case 'CU-6':
			   	$mensaje = "La fecha y hora de inicio deben ser inferiores a la fecha m&iacute;nima  de las citas creadas. Fecha de cita m&iacute;nima : ".$_SESSION['fecha_inicio'];
			   	$tipo_mensaje = 0;
            break;
            
            case 'CU-7':
			   	$mensaje = "La fecha y hora de fin deben ser superior a la fechas m&aacute;xima de las citas creadas. Fecha de cita m&aacute;xima :".$_SESSION['fecha_fin'];
			   	$tipo_mensaje = 0;
            break;
            
            case 'CU-8':
			   	$mensaje = "Existen citas creadas que sobrepasan la cantidad de contenedores por hora. Debe ingresar un valor superior.";
			   	$tipo_mensaje = 0;
            break;			

            case 'CU-9':
			   	$mensaje = "No se puede anular la autorizaci&oacute;n porque tiene citas asignadas y estan activas.";
			   	$tipo_mensaje = 0;
            break;
            case 'CU-10':
			   	$mensaje = "El acceso a las citas de urbaneo se encuetra bloqueado.";
			   	$tipo_mensaje = 0;
            break;
            case 'CU-11':           
			   	$mensaje = "Fecha no valida, la fecha ya se encuentra asignada a otra cita.";
			   	$tipo_mensaje = 0;
            break; 
            case 'CU-12':           
			   	$mensaje = "No hay datos registrados, debe cargar los datos inicialmente al solicitar la cita de urbaneo.";
			   	$tipo_mensaje = 0;
            break;
            case 'CU-13':           
			   	$mensaje = "Los datos de la cita de urbaneo han sido actualizados satisfactoriamente.";
			   	$tipo_mensaje = 0;
            break;
            case 'CU-14':           
			   	$mensaje = "Los datos ingresados de la cita de urbaneo son invalidos.";
			   	$tipo_mensaje = 0;
            break; 


            case 'O-1':
                $mensaje = "La Orden de Servicio ya se encuentra anulada.";
				$tipo_mensaje = 0;
                break;
                
            case 'E-1':
                $mensaje = "Se registro la orden correctamente pero no se logro enviar el mensaje EDI, quedara en proceso de espera para realizar el reenvio.";
				$tipo_mensaje = 0;
                break;
                
            case 'E-2':
                $mensaje = "Se registro la orden correctamente y se envio el mensaje EDI, pero se presento un error al actualizar el estado de la orden, quedara en proceso de espera para realizar el cambio.";
				$tipo_mensaje = 0;
                break;
                
            case 'E-3':
                $mensaje = "Se anulo la orden correctamente pero no se logro enviar el mensaje EDI, quedara en proceso de espera para realizar el reenvio.";
				$tipo_mensaje = 0;
                break;
                //dmg 8 ago 2012 se crea mensaje para no dejar crear el registro de et
             case 'E-4':
                $mensaje = "No tiene permisos para ingresar un registro de Empresa de Trasnporte .";
				$tipo_mensaje = 0;
                break;   

                        //pgs - 13/01/2012 - se crean los mensajes para el registro de ET
                        case 'RTCT-6':
                            $mensaje = "El contenedor no se encuentra en la terminal.";
                            $tipo_mensaje = 0;
                        break;

                        case 'RTCT-23':
                        $mensaje = "Contenedor con facturas pendientes de pago";
                                        $tipo_mensaje = 0;
                        break;

                        case 'RTCT-24':
                            $mensaje = "Contenedor con factura de Puerta pendiente.";
                            $tipo_mensaje = 0;
                        break;

                        case 'RTCT-25':
                            $mensaje = "Contenedor  con facturas pendientes de pago.";
                            $tipo_mensaje = 0;
                        break;

                        case 'RTCT-26':
                        $mensaje = "Contenedor  con factura de almac�n pendiente.";
                        $tipo_mensaje = 0;
                        break;

                        case 'RT-1':
                            $mensaje = "Los datos de Agente Aduanero y Cliente Final estan intercambiados.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-2':
                            $mensaje = "Se presento un error al registrar la empresa de transporte, por favor intentelo nuevamente.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-3':
                            $mensaje = "Se registr&oacute; correctamente la Empresa de Transporte.";
                            $tipo_mensaje = 1;
                        break;
                        case 'RT-4':
                            $mensaje = "Ha transcurrido bastante tiempo desde que inicio el proceso de Registrar Empresa de Transporte. Por favor intente nuevamente.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-5':
                            $mensaje = "El contenedor no tiene factura.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-6':
                            $mensaje = "Se actualiz&oacute; correctamente la Empresa de Transporte.";
                            $tipo_mensaje = 1;
                        break;
                         case 'RT-7':
                            $mensaje = "Ha transcurrido bastante tiempo desde que inicio el proceso de Actualizar la Empresa de Transporte. Por favor intente nuevamente.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-8':
                            $mensaje = "Se presento un error al actualizar la empresa de transporte, por favor intentelo nuevamente.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-9':
                            $mensaje = "No existen registros para el criterio de b�squeda seleccionado.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-10':
                            $mensaje = "Se presento un error al anular la empresa de transporte, por favor intentelo nuevamente.";
                            $tipo_mensaje = 0;
                        break;
                        case 'RT-11':
                            $mensaje = "Se anulo correctamente la Empresa de Transporte.";
                            $tipo_mensaje = 1;
                        break;
                        case 'RT-12':
                            $mensaje = "No hay datos sobre la Empresa de Transporte por que ya ha sido anulada.";
                            $tipo_mensaje = 1;
                        break;
                        case 'RT-13':
                            $mensaje = "Se ha enviado correctamente la notificaci&oacute;n de los c&oacute;digos al Agente Aduanero.";
                            $tipo_mensaje = 1;
                        break;
                        case 'RT-14':
                            $mensaje = "Se presento un error al enviar la notificaci&oacute;n de los c&oacute;digos al Agente Aduanero, por favor intentelo de nuevo.";
                            $tipo_mensaje = 0;
                        break;
                        //pgs - 13/01/2012 - fin de se crean los mensajes para el registro de ET
                                        
			case 'U-2':
				$mensaje = "La Contrasenya actual és incorrecta!";
				$tipo_mensaje = 0;
				break;
				
			case 'U-0':
				$mensaje = "L'Usuari no es troba registrat!";
				$tipo_mensaje = 0;
				break;
				
			case 'U-3':
				$mensaje = "L'usuari ja es troba registrat!";
				$tipo_mensaje = 0;
				break;
				
			case 'U-4':
				$mensaje = "El Correu ingressat no correspon a l'usuari!";
				$tipo_mensaje = 0;
				break;
				
			case 'U-5':
				$mensaje = "Vostè ha sol·licitat canvi de contrasenya si us plau revisi el seu correu i seguiu les instruccions.";
				$tipo_mensaje = 1;
				break;    
			
			//MAOH - 18 Nov 2011 - Mensaje cuando no envia el correo con la nueva clave	
			case 'U-6':
				$mensaje = "El correo con la nueva contrase&ntilde;a no se pudo enviar, por favor intentelo nuevamente.";
				$tipo_mensaje = 1;
				break;
				
			case 'U-7':
				$mensaje = "La cuenta de usuario ha sido activada correctamente";
				$tipo_mensaje = 0;
				break;
				
			case 'U-8':
				$mensaje = "La cuenta de usuario ya fue activada";
				$tipo_mensaje = 0;
				break;
				
			case 'U-9':
				$mensaje = "Sus datos fueron modificados satisfactoriamente";
				$tipo_mensaje = 0;
				break;
				
			case 'U-10':
				$mensaje = "Se presento un error al actualizar la contrase&ntilde;a por favor intentelo nuevamente";
				$tipo_mensaje = 0;
				break;  
				
			case 'U-11':
				$mensaje = "Los caracteres ingresados en el campo de verificaci&oacute;n no son v&aacute;lidos";
				$tipo_mensaje = 0;
				break;   
				
			case 'U-12':
				$mensaje = "La contrasenya no es va poder canviar. Si us plau, intenteu de nou.";
				$tipo_mensaje = 0;
				break;   
				
			case 'U-13':
				$mensaje = "La contrasenya s'ha canviat satisfactòriament.";
				$tipo_mensaje = 0;
				break;   
				
            case 'U-14':
                $mensaje = "El usuario no tiene informacion de cliente asociada.";
				$tipo_mensaje = 0;
                break;
				
			case 'U-15':
				$mensaje = "El usuario no tiene una Entidad asociada, por favor comun&iacute;quese con el adminstrador.";
				$tipo_mensaje = 0;
				break;
                
			case 'U-15-1':
				$mensaje = "El usuario no tiene una Entidad asociada.";
				$tipo_mensaje = 0;
				break;
                
			case 'U-16':
				$mensaje = "La cuenta de usuario fue bloqueada por inactividad.<br /> Por favor comun&iacute;quese con el administrador para solicitar la re activaci&oacute;n.";
				$tipo_mensaje = 0;
				break;
                
			case 'U-17':
                $dir_ip = Herramientas::obtenerIPConexion();
				$mensaje = 'El usuario no tiene permiso para acceder desde la IP '.$dir_ip.'. Por favor comun&iacute;quese con servicio al cliente TCBUEN, <a href="mailto:servicioalcliente@tcbuen.com">servicioalcliente@tcbuen.com</a>.';
				$tipo_mensaje = 0;
				break;
                
			case 'U-18':
				$mensaje = "L'usuari va ser registrat amb èxit.";
				$tipo_mensaje = 1;
                break;
            case 'U-19':
                $mensaje = "L'alumne es va registrar correctament";
                $tipo_mensaje = 1;
                break;
            case 'U-20':
                $mensaje = "Es va presentar un error a l'registrar l'alumne, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'U-21':
                $mensaje = "L'alumne es va actualitzar correctament";
                $tipo_mensaje = 1;
                break;
            case 'U-22':
                $mensaje = "L'alumne es va eliminar correctament";
                $tipo_mensaje = 1;
                break;
            case 'U-23':
                $mensaje = "Ha d'escollir un perfil per guardar el seu usuari";
                $tipo_mensaje = 1;
                break;
            case 'U-24':
                $mensaje = "Revisi les seves dades, ja que hi ha dades existents.";
                $tipo_mensaje = 1;
                break;
            case 'U-25':
                $mensaje = "La nova contrasenya no coincideix amb la confirmació. Introduïu la contrasenya nova una altra vegada.";
                $tipo_mensaje = 1;
                break;
            case 'U-26':
                $mensaje = "No es pot eliminar l'alumne ja que té registres associats.";
                $tipo_mensaje = 1;
                break;
            case 'CNT-01':
                $mensaje = "Es va registrar correctament el centre escolar";
                $tipo_mensaje = 0;
                break; 
            case 'CNT-02':
                $mensaje = "Es va actualitzar correctament el centre escolar";
                $tipo_mensaje = 0;
                break;
            case 'CNT-03':
                $mensaje = "Es va eliminar correctament el centre escolar";
                $tipo_mensaje = 0;
                break;  
            case 'CNT-04':
                $mensaje = "Es va presentar un error a l'registrar el centre escolar, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'CNT-05':
                $mensaje = "Es va presentar un error a l'actualitzar el centre escolar, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'RPCT-01':
                $mensaje = "Es va registrar correctament el informe";
                $tipo_mensaje = 0;
                break; 
            case 'RPCT-02':
                $mensaje = "Es va actualitzar correctament el informe";
                $tipo_mensaje = 0;
                break;
            case 'RPCT-03':
                $mensaje = "Es va eliminar correctament el informe";
                $tipo_mensaje = 0;
                break;  
            case 'RPCT-04':
                $mensaje = "Es va presentar un error a l'registrar el informe, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'RPCT-05':
                $mensaje = "Es va presentar un error a l'actualitzar el informe, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'VTAD-01':
                $mensaje = "Es va registrar correctament";
                $tipo_mensaje = 0;
                break; 
            case 'VTAD-02':
                $mensaje = "Es va actualitzar correctament";
                $tipo_mensaje = 0;
                break;
            case 'VTAD-03':
                $mensaje = "Es va eliminar correctament";
                $tipo_mensaje = 0;
                break;  
            case 'VTAD-04':
                $mensaje = "Es va presentar un error a l'registrar, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'VTAD-05':
                $mensaje = "Es va presentar un error a l'actualitzar, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
                case 'MFAM-01':
                $mensaje = "Es va registrar correctament";
                $tipo_mensaje = 0;
                break; 
            case 'MFAM-02':
                $mensaje = "Es va actualitzar correctament";
                $tipo_mensaje = 0;
                break;
            case 'MFAM-03':
                $mensaje = "Es va eliminar correctament";
                $tipo_mensaje = 0;
                break;  
            case 'MFAM-04':
                $mensaje = "Es va presentar un error a l'registrar, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'MFAM-05':
                $mensaje = "Es va presentar un error a l'actualitzar, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'TER-01':
                $mensaje = "Territori és va registrar correctament";
                $tipo_mensaje = 0;
                break; 
            case 'TER-02':
                $mensaje = "Territori és va actualitzar correctament";
                $tipo_mensaje = 0;
                break;
            case 'TER-03':
                $mensaje = "Territori és va eliminar correctament";
                $tipo_mensaje = 0;
                break;  
            case 'TER-04':
                $mensaje = "Es va presentar un error a l'registrar territori, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
            case 'TER-05':
                $mensaje = "Es va presentar un error a l'actualitzar territori, si us plau torni a provar.";
                $tipo_mensaje = 0;
                break;
			case 'S-0':
				$mensaje = "La Sesi&oacute;n se ha cerrado por inactividad. Debe volver a ingresar.";
				$tipo_mensaje = 0;
				break;
				
			case 'R-1':
				$mensaje = "S'ha generat el seu informe correctament.";
				$tipo_mensaje = 0;
				break;
            case 'R-2':
                $mensaje = "Los datos para cancelar la reserva son invalidos";
                $tipo_mensaje = 0;
                break;
			case 'US-3':
				$mensaje = "El Numero de Identificaci&oacute;n ya se encuentra registrado!";
				$tipo_mensaje = 0;
				break;
				
			case 'F-1':
				$mensaje = "El tama&ntilde;o del archivo excede el permitido.";
				$tipo_mensaje = 0;
				break;
				
			case 'F-2':
				$mensaje = "El tama&ntilde;o del archivo excede el valor especificado en el campo MAX_FILE_SIZE del formulario";
				$tipo_mensaje = 0;
				break;
				
			case 'F-3':
				$mensaje = "El archivo no fue cargado totalmente en el servidor";
				$tipo_mensaje = 0;
				break;
				
			case 'F-4':
				$mensaje = "No se seleccion&oacute; ning&uacute;n archivo";
				$tipo_mensaje = 0;
				break;
				
			case 'F-5':
				$mensaje = "El archivo que se va a copiar ya existe en el servidor";
				$tipo_mensaje = 0;
				break;
				
			case 'F-6':
				$mensaje = "El archivo no pudo ser copiado en el directorio seleccionado y con el nombre espec&iacute;fico";
				$tipo_mensaje = 0;
				break;
				
			case 'F-7':
				$mensaje = "El archivo no tiene uno de los Tipos de Archivo esperados.";
                if( $_SESSION['F-7-archivo'] != "" )
                {
                    $mensaje .= "Nombre archivo: ".$_SESSION['F-7-archivo'];
                    unset($_SESSION['F-7-archivo']);    
                }//Fin de if( $_SESSION['F-7-archivo'] != "" )
				$tipo_mensaje = 0;
                break;
                
            case 'V-0':
                $mensaje = "S'ha afegit la nova visita amb èxit.";
                $tipo_mensaje = 0;
                break;
            case 'V-1':
                $mensaje = "Hi ha una visita oberta, ha de tancar l'existent per registrar una nova visita.";
                $tipo_mensaje = 0;
                break;
            case 'V-2':
                $mensaje = "No hi ha alumnes associats a el centre escolar de la visita.";
                $tipo_mensaje = 0;
                break;
            case 'V-3':
                $mensaje = "Aquesta visita es troba oberta. Ha de tancar per poder editar.";
                $tipo_mensaje = 0;
                break;
            case 'V-4':
                $mensaje = "Es actualitzo la visita correctament.";
                $tipo_mensaje = 0;
                break;
            case 'V-5':
                $mensaje = "Es va eliminar la visita correctament.";
                $tipo_mensaje = 0;
                break;
            case 'V-6':
                $mensaje = "La visita no es pot eliminar ja que té registres d'intervencions associades.";
                $tipo_mensaje = 0;
                break;
            case 'V-7':
                $mensaje = "La visita no se li pot canviar el Centre Escolar ja que hi ha intervencions associats amb alumnes associats a aquest Centre, si us plau verifiqui el seu canvi.";
                $tipo_mensaje = 0;
                break;
            case 'V-8':
                $mensaje = "L'hora final de la visita ha de ser més gran que l'hora d'inici. Comproveu novament.";
                $tipo_mensaje = 0;
                break;
            case 'PE-1':
                $mensaje = "S'ha registrat el seu Període Escolar amb èxit.";
                $tipo_mensaje = 0;
                break;
            case 'PE-2':
                $mensaje = "El Període Escolar es va actualitzar correctament.";
                $tipo_mensaje = 1;
                break;
            case 'PE-3':
                $mensaje = "S'ha eliminat el Període Escolar Correctament";
                $tipo_mensaje = 1;
                break;
            case 'INT-1':
                $mensaje = "S'ha creat la intervenció amb èxit.";
                $tipo_mensaje = 1;
                break;
            case 'INT-2':
                $mensaje = "S'ha actualitzat la intervenció amb èxit.";
                $tipo_mensaje = 1;
                break;
            case 'INT-3':
                $mensaje = "S'ha eliminat la intervenció amb èxit";
                $tipo_mensaje = 1;
                break;
            case 'INT-ADMIN-1':
                $mensaje = "El promotor seleccionat no té registres associats a aquestes dates. Proveu novament!";
                $tipo_mensaje = 1;
                break;
            case 'INT-COOR-1':
                $mensaje = "No hi ha promotors en el seu territori";
                $tipo_mensaje = 1;
                break;
            case 'INT-EST-1':
                $mensaje = "Es van annexar la informació dels estudiants per intervenció";
                $tipo_mensaje = 1;
                break;
            case 'REP-1':
                $mensaje = "El centre escolar seleccionat no té visites associades.";
                $tipo_mensaje = 1;
                break;
            case 'REP-2':
                $mensaje = "El centre escolar seleccionat no té intervenciones associades.";
                $tipo_mensaje = 1;
                break;
            case 'REP-3':
                $mensaje = "El centre escolar Seleccionat no té intervencions associades en aquest rang de dates.";
                $tipo_mensaje = 1;
                break;
            case 'REP-4':
                $mensaje = "Introduïu les seves dades correctament.";
                $tipo_mensaje = 1;
                break;
				
			case 1:
				$imagen = "icon_exito.jpg";
				$tipo_mensaje = 1;
				$mensaje = $adicional['CADENA']." se ha ".$accion." satisfactoriamente.";
				break;

			case 2:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que ya existe un elemento igual en la Base de Datos.";
				break;

			case -1000:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a un error en la Conexi&oacute;n en la Base De Datos.";
				break;

			case -1001:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se ha asignado el nombre de la tabla.";
				break;

			case -1002:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se han asignado los atributos de la tabla.";
				break;

			case -1003:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". 
                        Debido a un error en el procesamiento de la solicitud con en el Gestor de Almacenamiento [Base de Datos]. 
                        Por favor intente nuevamente, si el error continua comun&iacute;quese con el administrador del 
                         sistema <a href='mailto:".$_EMAIL_ADMIN."'>".$_EMAIL_ADMIN."</a>";
                
                //DM - 2012-10-08
                //Si hay notificacion de errores entonces debe reportar los fallos en la bd por correo
                if( $_NOTIFICACION_ERRORES == 1 )
                {
                    $datos_error = array();
                    $datos_error['codigo_error'] = "BD";
                    registrarError($datos_error);
                }//Fin de if( $_NOTIFICACION_ERRORES == 1 )         
				break;
				
			case -1103:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". 
                Debido a un error en el  proceso de transacciones con el Gestor de Almacenamiento [Base de Datos]. 
                        Por favor intente nuevamente, si el error continua comun&iacute;quese con el administrador del 
                         sistema <a href='mailto:".$_EMAIL_ADMIN."'>".$_EMAIL_ADMIN."</a>
                         ";
                         
                //DM - 2012-10-08
                //Si hay notificacion de errores entonces debe reportar los fallos en la bd por correo
                if( $_NOTIFICACION_ERRORES == 1 )
                {
                    $datos_error = array();
                    $datos_error['codigo_error'] = "BD";
                    $datos_error['descripcion_error'] = "URI:".$_SERVER["REQUEST_URI"]." => USU_ID:".$_SESSION['acc_id'];
                    registrarError($datos_error);
                }//Fin de if( $_NOTIFICACION_ERRORES == 1 )
				break;
				
			case -1101:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". 
                Debido a un error en la solicitud enviada al Gestor de Almacenamiento [Base de Datos]. 
                        Por favor intente nuevamente, si el error continua comun&iacute;quese con el administrador del 
                         sistema <a href='mailto:".$_EMAIL_ADMIN."'>".$_EMAIL_ADMIN."</a>";
                         
                //DM - 2012-10-08
                //Si hay notificacion de errores entonces debe reportar los fallos en la bd por correo
                if( $_NOTIFICACION_ERRORES == 1 )
                {
                    $datos_error = array();
                    $datos_error['codigo_error'] = "BD";
                    registrarError($datos_error);
                }//Fin de if( $_NOTIFICACION_ERRORES == 1 )
				break;

			case -4:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = "La consulta no arroj&oacute; resultados";
				break;

			case -1005:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se pudo construir la instrucci�n SQL";
				break;
				
			case -1006:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a un error en el envio del correo";
				break; 
				
			case -1007:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion." debido a un error. Int&eacute;ntalo de nuevo!";
				break;
			
			case -1008:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = "Debes seleccionar un d&iacute;a y una hora para crear la cita.";
				break;

			case -6:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se indic&oacute; una condici&oacute;n";
				break;

			case -7:
				$imagen = "icon_advertencia.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA'];
				break;
				
			case 0:
				$imagen = "icon_error.jpg";
				$mensaje = $adicional['ACCION'];
				$tipo_mensaje = 0;
				break;
		
		}
		/*
		if($tipo_mensaje == 0)
		{
			$imagen_mensaje = "<img src='$_PATH_IMAGENES/alerta.jpg' alt='Alerta' border='0'>";
		}
		if($tipo_mensaje == 1)
		{
			$imagen_mensaje = "<img src='$_PATH_IMAGENES/exito.jpg' alt='&Eacute;xito' border='0'>";
		}*/

                if($_SESSION['error_msg']!=''){
                    $mensaje = $mensaje.'<br>'.$_SESSION['error_msg'];
                    unset($_SESSION['error_msg']);
                }
		
		$contenido_mensaje = "<center><table  cellpadding='4'>
            								<tr>
            									<td class='alert alert-primary mensaje'>$mensaje</td>
            								</tr>
                          </table></center><br>";
				
		if(!isset($tipo_mensaje))
		{
			$contenido_mensaje = "";
		}
		
		if( is_null($id_mensaje) )
		{
			$contenido_mensaje = "";
		}

                //pgs - 13/01/2012 - se pone validacion para cuando se desee mostrar el texto con o sin formato
                if($texto_sin_formato){
                    return $mensaje;
                }else{
                    return $contenido_mensaje;
                }//Fin de if($texto_sin_formato)
	}
	
	
	function crearMensajes($ids_mensajes)
	{
		if(is_array($ids_mensajes) && sizeof($ids_mensajes) > 0)
		{
			foreach($ids_mensajes as $id_mensaje)
			{
				$this->_contenido_mensajes .= $this->crearMensaje($id_mensaje);
			}
		}
		return $this->_contenido_mensajes;
	}
	
	function crearMensajeCustom($mensaje)
	{
    $contenido_mensaje = "<center><table  cellpadding='4'>
            								<tr>
            									<td class='mensaje'>$mensaje</td>
            								</tr>
                          </table></center><br>";
				
		return $contenido_mensaje;
  }
}
?>
