<?php
/*
*	Nombre: Menu.php
*	Modulo: Menu
*	DescripciÃ³n: Maneja todo lo referente al despliegue del menu de opciones
*/

class Menu
{
	var $_contenido_menu;

	function __construct()
    {
			$this->_contenido_menu = "";
	}//Fin de Menu()
	
	function crearMenu()
	{
		global $_PATH_WEB,$_PATH_IMAGENES;
		
		$this->contenido_menu = "";
          
        //si el usuario esta autentica y la opcion de no mostrar menu no esta activada
        //entonces despliega el menu de usuario
		//if( $_SESSION["autenticado"]==1 && $_SESSION[sin_menu] !=1 )
			//De acuerdo al tipo de usuario despliega las opciones del menu
			
		switch( intval($_SESSION["tipo_usuario"]) )
		{
			//Usuario Administrador   
			case 1:             
				$this->contenido_menu .= '<div class="sidebar-menu">
					<ul class="menu">
						<li class="sidebar-title">Menú Principal Administrador</li>
						<li class="sidebar-item">
							<a href="index.php?m=Usuarios&accion=dashboard" class="sidebar-link">
								<i data-feather="home" width="20"></i> 
								<span>Escriptori</span>
							</a>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="user" width="20"></i> 
								<span>Gestió d’Usuaris</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=usuarios&accion=listar_usuarios">Llistat Usuari</a>
								</li>
								
								<li>
									<a href="index.php?m=usuarios&accion=anadir_usuarios">Afegir Usuari</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="grid" width="20"></i> 
								<span>Gestió de Centres Escolars</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=centros&accion=listarcentros">Llistat Centres Escolars</a>
								</li>
								
								<li>
									<a href="index.php?m=centros&accion=fm_registrar">Afegir Centres Escolars</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="map" width="20"></i> 
								<span>Gestió de Territoris</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=territorios&accion=listarTerritorios">Llistat Territoris</a>
								</li>
								
								<li>
									<a href="index.php?m=territorios&accion=fm_registrar">Afegir Territoris</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="home" width="20"></i> 
								<span>Anys Lectius</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=periodosescolares&accion=listar_periodos_escolares">Llistat Anys Lectius</a>
								</li>
								<li>
									<a href="index.php?m=periodosescolares&accion=periodos_escolares">Crear Anys Lectius</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item">
							<a href="index.php?m=visitas&accion=visualizar_intervenciones" class="sidebar-link">
								<i data-feather="briefcase" width="20"></i> 
								<span>Intervencions</span>
							</a>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="file-text" width="20"></i> 
								<span>Informes</span>
							</a>
							<ul class="submenu informes">
								<li>
									<a href="index.php?m=reportes&accion=descripcion_intervencion">Seguiment alumnat (última visita)</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_ninos_atendidos_centro_escolar">Alumnat atés pel promotor/a als centres educatius durant el curs escolar</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_ninos_atendidos">Alumnat atès pel promotor/a escolar per gènere i per etapa educativa </a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=horas_trabajadas_centros_escolares">Hores treballades en Centres Escolars</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=informeCentroEscolarA">Dades de promoció escolar alumnat</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=evolucion_absentismo_a">Dades de l\'absentisme durant el curs</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=cnt_motivo_intervencion_a">Dades quantitatives per motiu d\'intervenció</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=tipointervencionA">Dades quantitatives per tipus d\'intervenció</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=descripcion_estudiantes">Seguiment de l\'alumne/a</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_familia">Famílies ateses pel promotor/a als centres educatius durant el curs escolar</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>';
			break;
			//Usuario Promotor   
			case 2:             
				$this->contenido_menu .= '<div class="sidebar-menu">
					<ul class="menu">
						<li class="sidebar-title">Menú Principal Promotor</li>
						<li class="sidebar-item">
							<a href="index.php?m=Usuarios&accion=dashboard" class="sidebar-link">
								<i data-feather="home" width="20"></i> 
								<span>Escriptori</span>
							</a>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="users" width="20"></i> 
								<span>Gestió de Famílies</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=familias&accion=listarfamilias">Llistat Famílies</a>
								</li>
								
								<li>
									<a href="index.php?m=familias&accion=fm_registrar">Afegir Família</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="user" width="20"></i> 
								<span>Gestió '. "d'Alumnes" .'</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=alumnos&accion=listaralumnos">Llistat Alumnes</a>
								</li>
								
								<li>
									<a href="index.php?m=alumnos&accion=fm_registrar">Afegir Alumne</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="grid" width="20"></i> 
								<span>Gestió de visites als centres</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=visitas&accion=listarVisitas">LListat de visites als centres</a>
								</li>
								
								<li>
									<a href="index.php?m=visitas&accion=agregar_visitas">Afegir Visites</a>
								</li>
								<li>
									<a href="index.php?m=visitas&accion=fm_horas_adicionales">Hores'." d'intervenció".' fora de centre</a>
								</li>
								<li>
									<a href="index.php?m=visitas&accion=listarHorasAdicionales">Llistat Hores'." d'intervenció".' fora de centre</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="file-minus" width="20"></i> 
								<span>Informe Centre</span>
							</a>
							<ul class="submenu ">
								<li>
									<a href="index.php?m=reportecentro&accion=listarreporte">Llistat Informe Centre</a>
								</li>
								
								<li>
									<a href="index.php?m=reportecentro&accion=fm_registrar">Afegir Informe Centre</a>
								</li>
							</ul>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="file-text" width="20"></i> 
								<span>Informes</span>
							</a>
							<ul class="submenu informes">
								<li>
									<a href="index.php?m=reportes&accion=descripcion_intervencion">Seguiment alumnat (última visita)</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_ninos_atendidos_centro_escolar">Alumnat atés pel promotor/a als centres educatius durant el curs escolar</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_ninos_atendidos">Alumnat atès pel promotor/a escolar per gènere i per etapa educativa </a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=horas_trabajadas_centros_escolares">Hores treballades en Centres Escolars</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=informeCentroEscolarP">Dades de promoció escolar alumnat</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=evolucion_absentismo_p">Dades de l\'absentisme durant el curs</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=cnt_motivo_intervencion_p">Dades quantitatives per motiu d\'intervenció</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=tipointervencionP">Dades quantitatives per tipus d\'intervenció</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=descripcion_estudiantes">Seguiment de l\'alumne/a</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_familia">Famílies ateses pel promotor/a als centres educatius durant el curs escolar</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>';
				break;
			//Director
			case 3:             
				$this->contenido_menu .= '<div class="sidebar-menu">
					<ul class="menu">
						<li class="sidebar-title">Menú Principal Director</li>
						<li class="sidebar-item">
							<a href="index.php?m=Usuarios&accion=dashboard" class="sidebar-link">
								<i data-feather="home" width="20"></i> 
								<span>Escriptori</span>
							</a>
						</li>
						<li class="sidebar-item">
							<a href="index.php?m=visitas&accion=visualizar_intervenciones" class="sidebar-link">
								<i data-feather="briefcase" width="20"></i> 
								<span>Intervencions</span>
							</a>
						</li>
					</ul>
				</div>';
				break;
			//Coordinador
			case 4:             
				$this->contenido_menu .= '<div class="sidebar-menu">
					<ul class="menu">
						<li class="sidebar-title">Menú Principal Coordinador</li>
						<li class="sidebar-item">
							<a href="index.php?m=Usuarios&accion=dashboard" class="sidebar-link">
								<i data-feather="home" width="20"></i> 
								<span>Escriptori</span>
							</a>
						</li>
						<li class="sidebar-item">
							<a href="index.php?m=visitas&accion=visualizar_intervenciones" class="sidebar-link">
								<i data-feather="briefcase" width="20"></i> 
								<span>Intervencions</span>
							</a>
						</li>
						<li class="sidebar-item has-sub">
							<a href="#" class="sidebar-link">
								<i data-feather="file-text" width="20"></i> 
								<span>Informes</span>
							</a>
							<ul class="submenu informes">
								<li>
									<a href="index.php?m=reportes&accion=descripcion_intervencion">Seguiment alumnat (última visita)</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_ninos_atendidos_centro_escolar">Alumnat atés pel promotor/a als centres educatius durant el curs escolar</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=informe_ninos_atendidos">Alumnat atès pel promotor/a escolar per gènere i per etapa educativa </a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=horas_trabajadas_centros_escolares">Hores treballades en Centres Escolars</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=informeCentroEscolarC">Dades de promoció escolar alumnat</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=evolucion_absentismo_c">Dades de l\'absentisme durant el curs</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=cnt_motivo_intervencion_c">Dades quantitatives per motiu d\'intervenció</a>
								</li>
								<li>
									<a href="index.php?m=basereportes&accion=tipointervencionC">Dades quantitatives per tipus d\'intervenció</a>
								</li>
								<li>
									<a href="index.php?m=reportes&accion=descripcion_estudiantes">Seguiment de l\'alumne/a</a>
								</li>	
								<li>
									<a href="index.php?m=reportes&accion=informe_familia">Famílies ateses pel promotor/a als centres educatius durant el curs escolar</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>';
				break;
			default:
				$this->contenido_menu = '<div class="barra_inicio"></div>';
			break;
		}

		if( $_SESSION["autenticado"]==1 ) {
			$this->contenido_menu .= '<ul class="menu">
			<li class="sidebar-item"><a class="dropdown-item" href="index.php?m=Usuarios&accion=cerrar_sesion">
			<i data-feather="log-out"></i> Tancar Sessió</a></li></ul>';
		}
		return $this->contenido_menu;
	}
}
?>
