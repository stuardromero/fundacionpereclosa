<?php
/**
 * Se encarga de verificar si la ejecución de los daemons
 * la esta realizando un crontab o una persona de HQ
 * DM - 2015-06-01
 ***/
function verificarEjecucionDaemon($datos)
{
    global $_PATH_SERVIDOR, $_CONFIG;
    include_once($_PATH_SERVIDOR.'/librerias/geoip/index.php');

    //Si hay un ip remota debe verificar desde que pais se esta ejecutando
    if( $_SERVER['REMOTE_ADDR'] != '' && $_SERVER['REMOTE_ADDR'] != '::1')
    {
        $pais_acceso = obtenerInfoPaisAcceso();

        //Si se esta ejcutando de un pais diferente a colombia y españa no hace nada
        if( ($pais_acceso['codigo'] != 'CO' && $pais_acceso['codigo'] != 'ES') && $_CONFIG['ACCESO_DAEMONS'] != 1)
        {
            exit;
        }//Fin de if( $pais_acceso['codigo'] != 'CO' && $pais_acceso['codigo'] != 'ES' )

    }//Fin de if( $_SERVER['REMOTE_ADDR'] != '' )

}//Fin de verificarEjecucionDaemon

/**
 * Se encarga de realizar la división de 2 numeros y verificar
 * que no se haga una división por cero
 * DM - 2015-04-23
 **/
function division($valor_1,$valor_2,$decimales=1)
{
    if( !is_numeric($valor_1) )
    {
        return '-';
    }
    if( $valor_2 == 0 || $valor_2 == "")
    {
        return '-';
    }
    return number_format($valor_1 / $valor_2, $decimales);
}//Fin de division

function i($valor,$var_dump= false)
{
	echo '<pre>
	';
	if( $var_dump )
	{
		var_dump($valor);
	}
	else
	{
		print_r($valor);
	}
	echo '</pre>
	';
}

function trimCampos($datos)
{
	foreach($datos as $key => $valor)
	{
		//si el campo no es un arreglo le aplica el trim
		if( !is_array($valor) )
		{
			$datos[$key] = trim($valor);
		}
		else
		{
			$datos[$key] = trimCampos($valor);
		}
	}//Fin de foreach($datos as $key => $valor)
	return $datos;
}//Fin de trimCampos

// function validarAcceso($perfiles_autorizados)
// {
// 	//captura el perfil del usuario
// 	$perfil = $_SESSION['tipo_usuario'];
		
// 	if(sizeof($perfiles_autorizados)>0)
// 	{
// 		//verifica si el perfil se encuentra en los autorizados
// 		$existe = array_search($perfil, $perfiles_autorizados);
		
// 		if(is_numeric($existe))
// 		{
// 			//Asigna el perfil correspondiente
// 			$perfil = $_SESSION['tipo_usuario'];
// 		}
// 		else
// 		{
// 			//elimina el acceso de todos los perfiles
// 			$perfil = "-";		
// 		}
// 	}//Fin de if( sizeof($perfiles_autorizados)>0 )
	
// 	return $perfil;
// }//Fin de validarAcceso()

function tienePermiso($tipo_usuario, $acceso)
{
	if( $_SESSION['tipo_usuario'] == $tipo_usuario && !$acceso )
	{
		header('Location: index.php');
	}

}//Fin de tienePermiso



function noCache() {

  header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
}

function exception_error_handler($errno, $errstr, $errfile, $errline )
{
	global $_EMAIL_ADMIN,$_host_db,$_user_db,$_password_db,$_db;

	switch($errno)
	{
		case 2:
		case 256:
		case 512:
		case 4096:
		$asunto = "Urgente - Error HQ";

		$errstr = str_replace("'","",$errstr);

		$cuerpo = 'Se genero un error en Hotels Quality :<br>
			Fecha: '.date("Y-m-d H:i:s").'<br>
			Codigo error: '.$errno.'<br>
			Archivo: '.$errfile.'<br>
			Linea: '.$errline.'<br>
			Error: '.$errstr.' <br>
			Navegador: '.$_SERVER["HTTP_USER_AGENT"].'<br>
			Host Cliente: '.$_SERVER["REMOTE_HOST"].'<br>
			IP Cliente: '.$_SERVER["REMOTE_ADDR"].'<br>
			Metodo Formulario: '.$_SERVER["REQUEST_METHOD"].'<br>
			URI Peticion : '.$_SERVER["REQUEST_URI"].'<br />
			Usuario id: '.$_SESSION['usu_id'].' <br />
            MYSQL:'.$_SESSION['sql_error_mysql'].'
			Datos en POST: '.var_export($_POST,true).' <br />
			Datos en GET: '.var_export($_GET,true).' <br />
			Datos en SESSION: '.var_export($_SESSION,true).' <br />
            Datos en SERVER: '.var_export($_SERVER,true).' <br />
			';

		$cuerpo = str_replace(".",". ",$cuerpo);

		//MAOH - 18 Abr 2012 - Cambio a UTF 8
		$contenido = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>HOTELS quality</title>
			</head>
			<body>'.$cuerpo.'</body></html>';

			$_obj_database = new database($_host_db,$_user_db,$_password_db,$_db);
			
			$obj_correo = new Correo();

			$fecha = date("Y-m-d");

			$sql = "select erp_id
					from errores_reportados
					where erp_codigo = '".$errno."'
							and erp_archivo =  '".$errfile."'
							and erp_linea = '".$errline."'
							and erp_fecha = '".$fecha."'
							and erp_mensaje = '".$errstr."' ";

			$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
						
			if( is_array($resultado) )
			{
				if( sizeof($resultado) > 0 )
				{
					//si ya lo envio no hace nada
				}//Fin de if( sizeof($resultado) > 0 )
				else
				{
					$campos = array("erp_codigo",
									"erp_archivo",
									"erp_linea",
									"erp_fecha",
									"erp_mensaje");

					$datos['tabla'] = "errores_reportados";
					$datos['erp_codigo'] = $errno;
					$datos['erp_archivo'] = $errfile;
					$datos['erp_linea'] = $errline;
					$datos['erp_fecha'] = $fecha;
					$datos['erp_mensaje'] = $errstr;

					$sql = $_obj_database->generarSQLInsertar($datos,$campos);
					$_obj_database->ejecutarSQL($sql);

					$opciones = array();
                    $obj_correo->enviarCorreo($_EMAIL_ADMIN,"david.males@querytek.com", $asunto, $contenido,NULL,$opciones);
					
				}//Fin de else de if( sizeof($resultado) > 0 )

			}//fin de if( is_array($resultado) )
			else
			{
				$opciones = array();                    
                $obj_correo->enviarCorreo($_EMAIL_ADMIN,"david.males@querytek.com", $asunto, $contenido,NULL,$opciones);
				
			}

			break;
	}//Fin de switch($errno)

	return false;
}//Fin de exception_error_handler

//Si esta activa la opcion entonces envia el reporte de errores
/*if( $_NOTIFICACION_ERRORES == 1 )
{
	set_error_handler("exception_error_handler");
}*/

/**
 * Función que se encarga de redireccionar la pagina a https
 * si el llamado se hace a http
 * DM - 2014-01-10
 **/
function redireccionarHttps($datos)
{
	global $_PATH_WEB;

	$url =  $_SERVER[SERVER_NAME];

	//si no existe el protocolo de seguridad entonces debe redireccionar la pagina
	$realiza_redireccion = !isset($_SERVER['HTTPS']) ;

	//si el llamado se hace desde la ruta principal de la aplicacion
	$realiza_redireccion &=  strpos($_PATH_WEB,'http://www.hotels-quality.com') !== false  || strpos($_PATH_WEB,'http://hotels-quality.com') !== false;

	if( $realiza_redireccion )
	{
		//elimina el campo de plantilla
		unset($datos['plantilla']);

		$enlace = Herramientas::enlaceCampos($datos, array_keys($datos) );
		$enlace = substr($enlace,1,strlen($enlace));

		if(  strpos( strtolower($url),"www") !==false )
		{
			header('Location: https://www.hotels-quality.com'.$_SERVER['SCRIPT_NAME'].'?'.$enlace);
			exit;
		}
		else
		{
			header('Location: https://hotels-quality.com'.$_SERVER['SCRIPT_NAME'].'?'.$enlace);
			exit;
		}
	}//Fin de if( strpos('https',$_PATH_WEB) === false )
}//Fin de redireccionarHttps


/**
 * Función que se encarga de interpretar y obtener la lista de tokens
 * que se han agregado en la plantilla
 * DM - 2014-01-22
 **/
function obtenerTokensPlantilla($datos)
{
	$plantilla = $datos['plantilla'];
	$token_prefijo = $datos['token_prefijo'];
	$token_sufijo = $datos['token_sufijo'];

	//obtiene la posición del primer token, si existe
	$pos_token = strpos($plantilla, $token_prefijo );

	$lista_tokens = array();

	//si existe al menos un token lo obtiene
	while( $pos_token !== false )
	{
		//obtiene la posicion del token final, si existe
		$pos_final_token = strpos($plantilla, $token_sufijo , $pos_token + strlen($token_prefijo) );


		if( $pos_final_token !== false)
		{
			//obtiene la cadena retirando el sufijo y pasando la longitud de la cadena desde el
			//inicio del texto, después del token prefijo hasta la posición inicial del token sufijo
			$token = substr( $plantilla , $pos_token+strlen($token_prefijo), $pos_final_token - $pos_token - strlen($token_prefijo) );

			//verifica que sea un token y no un contenido oculto de html como javascript o comentarios
			//verifica que no tenga espacios en blanco
			$sin_espacios = strpos($token,' ') === false;

			//si no cumple con las validaciones entonces lo agrega
			if( $sin_espacios )
			{
				$lista_tokens[] = $token;
			}

			//elimina el token de la plantilla para buscar el proximo, si existe
			$plantilla = str_replace($token_prefijo.$token.$token_sufijo,'',$plantilla);

			//obtiene la posición del primer token, si existe
			$pos_token = strpos($plantilla, $token_prefijo);

		}//Fin de if( $pos_final_token !== false)
		else
		{
			//si no existe un token final entonces no tiene en cuenta el inicial y termina
			$pos_token = false;
		}
	}//Fin de while( $pos_token !== false )

	return $lista_tokens;
}//Fin de obtenerTokensPlantilla


function obtenerTokensPlantillaCSS($datos)
{
	$plantilla = $datos['plantilla'];
	$token_prefijo = $datos['token_prefijo'];
	$token_sufijo = $datos['token_sufijo'];

	//obtiene la posición del primer token, si existe
	$pos_token = strpos($plantilla, $token_prefijo );

	$lista_tokens = array();

	//si existe al menos un token lo obtiene
	while( $pos_token !== false )
	{
		//obtiene la posicion del token final, si existe a partir del token prefijo
		//pues se debe buscar la cadena mas cercana despues del prefijo
		$pos_final_token = strpos($plantilla, $token_sufijo , $pos_token + strlen($token_prefijo) );


		if( $pos_final_token !== false)
		{
			//obtiene la cadena retirando el sufijo y pasando la longitud de la cadena desde el
			//inicio del texto, después del token prefijo hasta la posición inicial del token sufijo
			$token = substr( $plantilla , $pos_token+strlen($token_prefijo), $pos_final_token - $pos_token - strlen($token_prefijo) );

			$sin_espacios = true;

			//Indica si se valida o no que los token no tengas espacios
			if( $datos['omitir_validacion'] != 1  )
			{
				//verifica que sea un token y no un contenido oculto de html como javascript o comentarios
				//verifica que no tenga espacios en blanco
				$sin_espacios = strpos($token,' ') === false;
			}//Fin de if( $datos['omitir_validacion'] !=1  )


			//si no cumple con las validaciones entonces lo agrega
			if( $sin_espacios )
			{
				$lista_tokens[] = $token;
			}

			//elimina el token de la plantilla para buscar el proximo, si existe
			$plantilla = str_replace($token_prefijo.$token.$token_sufijo,'',$plantilla);

			//obtiene la posición del primer token, si existe
			$pos_token = strpos($plantilla, $token_prefijo);

		}//Fin de if( $pos_final_token !== false)
		else
		{
			//si no existe un token final entonces no tiene en cuenta el inicial y termina
			$pos_token = false;
		}
	}//Fin de while( $pos_token !== false )

	return $lista_tokens;
}//Fin de obtenerTokensPlantilla

/**
 * Función que se encarga de asignar una lista de token a una plantilla
 * de acuerdo al arreglo de idioma
 * DM - 2014-01-23
 **/
function asignarTokensIdiomas($plantilla, $lista_variables = array() )
{
	global $idi_despliegue, $_PATH_IMAGENES,$_PATH_WEB;

	Interfaz::asignarToken('path_imagenes',$_PATH_IMAGENES,$plantilla);
	Interfaz::asignarToken('path_web',$_PATH_WEB,$plantilla);

    //Si hay variables para asignar las coloca en la plantilla
    foreach($lista_variables as $variable => $valor)
    {
        if( strpos($plantilla,'<!--'.$variable.'-->') !== false )
        {
            Interfaz::asignarToken($variable,$valor,$plantilla);
        }
    }//Fin de foreach($lista_variables as $variable => $valor)

	$datos = array();
	$datos['plantilla'] = $plantilla;
	$datos['token_prefijo'] = '<!--';
	$datos['token_sufijo'] = '-->';
	$lista_tokens = obtenerTokensPlantilla($datos);

	foreach($lista_tokens as $token)
	{
		Interfaz::asignarTokenDatos($token,$idi_despliegue,$plantilla);
	}//Fin de foreach($lista_tokens as $token)


	return $plantilla;
}//Fin de asignarTokensIdiomas()


/**
 * Se encarga de validar que si el establecimiento de cadena que se pasa por parámetro
 * es vacío, entonces debe retornar uno de los establecimientos existentes de la cadena
 * DM - 2014-04-10
 **/
function obtenerIdEstablecimientoDefectoCadena($est_id)
{
	if( $est_id == '' && $_SESSION['tipo_usuario']=='C')
	{
		//obtiene los establecimientos almacenados en la cadena
		$establecimientos = $_SESSION['cadena_est'];
		//obtiene el primer establecimiento
		$establecimiento = array_shift($establecimientos);
		//retorna el id
		return $establecimiento['est_id'];

	}
	return $est_id;
}//Fin de obtenerIdEstablecimientoDefectoCadena

/**
 * Identifica si uno de los parámetros que se pasan en el formulario
 * tiene caracteres invalidos como <script>, <?php...
 * DM - 2014-06-19
 **/
function contieneCamposInvalidos($datos)
{
	$contiene_invalidos = false;

	//Verifica por cada campo si es correcto
	foreach($datos as $key => $valor)
	{
		//si el valor es un arreglo entonces revisa el arreglo
		//realizando un llamado recursivo
		if( is_array($valor) )
		{
			$contiene_invalidos = contieneCamposInvalidos($valor);
			//si contiene invalidos entonces debe retornar que no es valido
			if( $contiene_invalidos )
			{
				break;
			}
		}
		else
		{
			$contiene_invalidos = contieneCaracteresInvalidos($valor);
			//si contiene invalidos entonces debe retornar que no es valido
			if( $contiene_invalidos )
			{
				break;
			}
		}
	}//Fin de foreach($datos as $key => $valor)

	return $contiene_invalidos;

}//Fin de contieneCamposInvalidos


/**
 * Verifica si la cadena tiene caracteres invalidos que hagan parte de script
 * maliciosos
 * DM - 2014-06-19
 **/
function contieneCaracteresInvalidos($cadena)
{
	$contiene_invalidos = false;


	//Verifica si la cadena esta en base64
	$cadena_base64 = base64_decode( $cadena );

	//Convierte la cadena a minusculas para hacer la busqueda de cadenas
	$cadena = strtolower($cadena);
	$cadena_base64 = strtolower($cadena_base64);



	//Elimina los espacios en blanco
	$cadena = str_replace(" ","",$cadena);
	$cadena_base64 = str_replace(" ","",$cadena_base64);

	$lista_cadenas_invalidas = array(
						'<script',
						'<?php',
						'<?',
						'insertinto',
						'updatetable',
						'deletefrom',
						'droptable',
						'dropuser',
						'dropindex',
						'truncatetable',
						'<input',
						'<file'
						);

	foreach($lista_cadenas_invalidas as $val_cadena)
	{
		//verifica si una de las cadenas invalidas se encuentra en la cadena
		if( strpos($cadena,$val_cadena) !== false )
		{
			$contiene_invalidos = true;
			break;
		}

		//verifica si una de las cadenas invalidas se encuentra en la cadena
		if( strpos($cadena_base64,$val_cadena) !== false )
		{
			$contiene_invalidos = true;
			break;
		}

	}//Fin de foreach($lista_cadenas_invalidas as $val_cadena)

	return $contiene_invalidos;
}//Fin de contieneCaracteresInvalidos

/**
 * Se encarga de registrar el acceso a los reportes guardando
 * en un csv información del usuario y del reporte
 * DM - 2014-08-27
 **/
function registrarAccesoReportes($datos)
{
	global $_PATH_SERVIDOR;


	//Si no hay fecha inicial entonces registra el incio del reporte en otro log
	if( $datos['log_hora_inicial'] == '' )
	{
		$cabecera = "Id usuario,Usuario,Tipo usuario,Establecimiento,Cadena,Reporte,Inicia\r\n";

		$cadena .= $_SESSION['usu_id'];
		$cadena .= ','.$_SESSION['usu_nombre'];
		$cadena .= ','.$_SESSION['tipo_usuario'];
		$cadena .= ','.$_SESSION['est_nombre'];
		$cadena .= ','.$_SESSION['cad_nombre'];
		$cadena .= ','.$datos['log_nombre_reporte'];
		$cadena .= ','.date('H:i:s');
		$cadena .= "\r\n";

		$ruta = $_PATH_SERVIDOR.'log_reportes/'.date('Y_m_d').'_inicial.csv';

	}//Fin de if( $datos['log_hora_inicial'] == '' )
	else
	{
		$cabecera = "Id usuario,Usuario,Tipo usuario,Establecimiento,Cadena,Reporte,Inicia,Termina\r\n";

		$cadena .= $_SESSION['usu_id'];
		$cadena .= ','.$_SESSION['usu_nombre'];
		$cadena .= ','.$_SESSION['tipo_usuario'];
		$cadena .= ','.$_SESSION['est_nombre'];
		$cadena .= ','.$_SESSION['cad_nombre'];
		$cadena .= ','.$datos['log_nombre_reporte'];
		$cadena .= ','.$datos['log_hora_inicial'];
		$cadena .= ','.date('H:i:s');
		$cadena .= "\r\n";

		$ruta = $_PATH_SERVIDOR.'log_reportes/'.date('Y_m_d').'.csv';
	}



	//Si el archivo no existe, debe agregarle el encabezado
	if( !file_exists($ruta) )
	{
		$cadena = $cabecera.$cadena;
	}
	Archivos::crearArchivo($ruta,$cadena);
}//Fin de registrarAccesoReportes()

/* DMG 2014-09-25
Funcion que se encarga de validar si se estan realizando varios registros desde la misma IP
verifica las veces que se han registrado datos desde la misma IP,
teniendo en cuenta un periodo de tiempo 1 hora con mas de 100 registros.
cicerone: guardar la oferta
connet: encuesta, registrar establecimientos, autenticar usuario, olvidar clave
club: registrar oferta */
function validarRegistroIP($datos)
{
  global $_obj_database, $_EMAIL_ADMIN,$_PATH_SERVIDOR;

	$datos['ip']= Herramientas::obtenerIPConexion();
	//$datos['url']="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
	$datos['url'] =$_SERVER['HTTP_REFERER'];

	$datos['aua_datos'] = serialize(array_merge($_GET,$_POST));

	//rango de 1 hora
	$time_final = time();
	$fecha_final = date('Y-m-d H:i:s', $time_final);

	$time_inicial = strtotime('-1 hour');
	$fecha_inicial = date('Y-m-d H:i:s', $time_inicial);
    
    //DM - 2019-12-11 redmine[14093]
    $datos['url'] = rawurldecode( $datos['url'] );
    $datos['url'] = str_replace("\'","", $datos['url']);
    $datos['url'] = str_replace("'","", $datos['url']);
    $datos['url'] = str_replace("\\","", $datos['url']);

	//Se consulta si hay un registro con la misma ip, en un rango de una hora
	$tabla = 'auditoria_accesos';
	$condicion = "aua_ip = '".$datos['ip']."'
    and aua_codigo = '".$datos['codigo']."'
	and aua_est_id = '".$datos['est_id']."'
	and aua_url = '".$datos['url']."'
	and aua_fecha between '".$fecha_inicial."' and '".$fecha_final."'";

	$select = "count( aua_id) as contador";

	$info = $_obj_database->obtenerRegistro( $tabla, $condicion, $select );
	
   // i($info); exit;
   $limite = is_numeric($datos['limite']) ? $datos['limite'] : 100; 
	if( $info['contador'] >= $limite )
	{
        if( $datos['no_enviar_reporte'] != 1 )
        {
        	  $asunto ="Alerta envios masivos";
        
        	  $cuerpo ="Se presentaron mas de ".$limite." registros en la última hora para un establecimiento: <br><br>
        	   IP: = '".$datos['ip']."'<br>
        	   Codigo: = '".$datos['codigo']."'<br>
        	   Id establecimiento: = '".$datos['est_id']."'<br>
        	   URL: = '".$datos['url']."'<br>
        	   Fecha Inicial: '".$fecha_inicial."'<br>
        	   Fecha Final: '".$fecha_final."'
               SERVER : ".var_export($_SERVER,true)." <br>\r\n
               REQUEST : ".var_export($_REQUEST,true)." <br>\r\n
               ";
        
        
        		//MAOH - 18 Abr 2012 - Cambio a UTF 8
        		$contenido = '
        			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        			<html xmlns="http://www.w3.org/1999/xhtml">
        			<head>
        			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        			<title>HOTELS quality</title>
        			</head>
        			<body>'.$cuerpo.'</body></html>';
        
        	  $obj_correo = new Correo();
        	  
        	  //JMM-2017-01-30 - Validacion para datos SMTP
        	  //Se obtienen los datos de configuracion del hotel necesarios para envio de correos usando SMTP
        	  $opciones['opciones'] = establecimientos::obtenerConfiguracionGeneralEst($_SESSION['est_id']);
        	  //Se verifica la cantidad de emails enviados
        	  $cantidad_envios = establecimientos::verificarCantidadEmailsEnviados($opciones);
        	  if ($cantidad_envios === true) {
        	  	$obj_correo->enviarCorreo($_EMAIL_ADMIN,"david.males@querytek.com", $asunto, $contenido,NULL,$opciones);  
        	  }
        	  $obj_correo->enviarCorreo($_EMAIL_ADMIN,"david.males@querytek.com", $asunto, $contenido,NULL,$opciones);
             
        }//Fin de if( $datos['no_enviar_reporte'] != 1 )

      if( $datos['bloquear_pantalla'] == 1 )
      {
            $contenido = Archivos::obtenerContenidoArchivo($_PATH_SERVIDOR.'Includes/Plantillas/error_acceso_invalido.html');
            print_r($contenido);
            exit;
      }
	}//Fin de if( $info['contador'] >= 100 )

	$datos['est_id'] = $datos['est_id'] != "" ? $datos['est_id'] : $datos['sto_id'];


	$campos = array("aua_ip", "aua_codigo", "aua_url", "aua_datos");

	if($datos[est_id] != "")
	{
	  $campos[]="aua_est_id";
	  $datos['aua_est_id'] = $datos[est_id];
	}

	$datos['tabla'] = "auditoria_accesos";
	$datos['aua_ip'] = $datos['ip'];
	$datos['aua_codigo'] = $datos['codigo'];
	$datos['aua_url'] = $datos['url'];

	$sql = $_obj_database->generarSQLInsertar($datos,$campos);

	$_obj_database->ejecutarSQL($sql);

  //return false;
} //function validarRegistroIP($datos)


/**
 * Se encarga de registrar en un archivo por dia todas las peticiones que llegan
 * por POST
 * DM - 2014-10-06
 * */
function registrarLogPOST()
{
	global $_PATH_SERVIDOR;

	if( count($_POST) > 0 )
	{
		$cadena_post = var_export($_POST, true);

		$contenido = date('H:i:s').'::'.$_SERVER['REMOTE_ADDR'].'==>'.$cadena_post.'<==';
		$contenido .= "\r\n";

		$ruta = $_PATH_SERVIDOR.'/log_post/'.date('Y-m-d').'.txt';

		Archivos::crearArchivo($ruta, $contenido);
	}//Fin de if( count($_POST) > 0 )


}//Fin de registrarLogPOST


/**
 * Se encarga de buscar los tokens asignagos y reemplazar el valor entre los tokens
 * DM - 2018-04-09
 **/
function asignarValorEntreTokens($contenido,$valor,$token_prefijo,$token_sufijo='')
{
    $pos_token = strpos($contenido, $token_prefijo );
    
	//si no encuentra el token inicial entonces no retorna nada
	if( $pos_token === false)
	{
		return false;
	}//Fin de if( $pos_token === false)
    
	//Si no se pasa sufijo entonces retorna la posición final
	if( $token_sufijo == '')
	{
		$pos_final_token = strlen($contenido);
	}else
	{
		//obtiene la posicion del token final, si existe
		$pos_final_token = strpos($contenido, $token_sufijo,$pos_token );
	}
    
	//si no encuentra el token final  entonces no retorna nada
	if( $pos_final_token === false)
	{
		return false;
	}//Fin de if( $pos_final_token === false)
    
	//obtiene la cadena retirando el sufijo y pasando la longitud de la cadena desde el
	//inicio del texto, después del token prefijo hasta la posición inicial del token sufijo
	$rango_inicial = $pos_token+strlen($token_prefijo);
	$rango_final = $pos_final_token - $pos_token - strlen($token_prefijo);
	$cadena = substr($contenido,$rango_inicial,$rango_final );
    
    $cadena_inicial = substr($contenido,0,$pos_token);
    $pos_final_token += strlen($token_sufijo);
    $cadena_final = substr($contenido,$pos_final_token,strlen($contenido));
    
    $cadena = $cadena_inicial.$valor.$cadena_final; 
    
    return $cadena; 
    
}//Fin de asignarValorEntreTokens


/**
 * Funcion que se encarga de retornar el contenido que se encuentra
 * entre el token prefijo y sufijo
 * DM - 2014-03-20
 * */
function extraerValorEntreTokens($contenido,$token_prefijo,$token_sufijo='', $cantidad_apariciones_prefijo=0,$cantidad_apariciones_sufijo=0)
{
	//si debe tener en cuenta que el prefijo debe aparecer x veces para retornar el contenido
	if( $cantidad_apariciones_prefijo > 0)
	{
		$pos_token = strpos($contenido, $token_prefijo );

		$cantidad_apariones = 0;
		while($pos_token !== false && $cantidad_apariones < $cantidad_apariciones_prefijo)
		{
			$pos_token = strpos($contenido, $token_prefijo, $pos_token + strlen($token_prefijo) );
			$cantidad_apariones++;
		}

	}//Fin de if( $cantidad_apariciones_prefijo > 0)
	else
	{
		$pos_token = strpos($contenido, $token_prefijo );
	}

	//si no encuentra el token inicial entonces no retorna nada
	if( $pos_token === false)
	{
		return false;
	}//Fin de if( $pos_token === false)

	//si debe tener en cuenta que el sufijo debe aparecer x veces para retornar el contenido
	if( $cantidad_apariciones_sufijo > 0)
	{
		//obtiene la posicion del token final, si existe
		$pos_final_token = strpos($contenido, $token_sufijo,$pos_token );

		$cantidad_apariones = 0;
		while($pos_final_token !== false && $cantidad_apariones < $cantidad_apariciones_sufijo)
		{
			$pos_final_token = strpos($contenido, $token_sufijo, $pos_final_token + strlen($token_sufijo) );
			$cantidad_apariones++;
		}

	}//Fin de if( $cantidad_apariciones_prefijo > 0)
	else
	{
		//Si no se pasa sufijo entonces retorna la posición final
		if( $token_sufijo == '')
		{
			$pos_final_token = strlen($contenido);
		}else
		{
			//obtiene la posicion del token final, si existe
			$pos_final_token = strpos($contenido, $token_sufijo,$pos_token );
		}

	}

	//si no encuentra el token final  entonces no retorna nada
	if( $pos_final_token === false)
	{
		return false;
	}//Fin de if( $pos_final_token === false)

	//obtiene la cadena retirando el sufijo y pasando la longitud de la cadena desde el
	//inicio del texto, después del token prefijo hasta la posición inicial del token sufijo
	$rango_inicial = $pos_token+strlen($token_prefijo);
	$rango_final = $pos_final_token - $pos_token - strlen($token_prefijo);
	$cadena = substr($contenido,$rango_inicial,$rango_final );

	return trim($cadena);
}//Fin de  extraerValorEntreTokens

/**
 * Se encarga de verificar si un establecimiento o usuario no tienen
 * acceso a la aplicación, de ser así entonces debe redireccionarlo
 * a una página html con un mensaje explicando que no esta activa
 * DM - 2014-12-31
 *    */
function redireccionCuentaInactiva($datos)
{
	global $_PATH_WEB, $_CONFIG;

    //si no esta en la versión de pdn entonces no realiza validaciones
    if( $_CONFIG['general_version'] != 'pdn' )
    {
        return;
    }//Fin de if( $_CONFIG['general_version'] != 'pdn' )



	$lista_establecimientos_bloqueados = array();
	$lista_cadenas_bloqueados = array();

	if( time() > strtotime('2015-01-01 00:00:00') )
	{
		$lista_establecimientos_bloqueados = array(111,112,113,115,116,118,119,120,
									121,124,125,127,128,129,
									130,131,132,135,137,140,141,142,143,144,
									145,146,180,244,403
									);

		$lista_cadenas_bloqueados = array(4 //Vincci
										);
	}

	if( is_numeric($datos['est_id']) && $datos['est_id'] > 0 )
	{
		if( in_array( $datos['est_id'] , $lista_establecimientos_bloqueados ) )
		{
			header('Location: '.$_PATH_WEB.'/inactivo.html');
			exit;
		}
	}//Fin de if( is_numeric($datos['est_id']) && $datos['est_id'] > 0 )


	if( is_numeric($datos['cad_id']) && $datos['cad_id'] > 0 )
	{
		if( in_array( $datos['cad_id'] , $lista_cadenas_bloqueados ) )
		{
			header('Location: '.$_PATH_WEB.'/inactivo.html');
			exit;
		}
	}//Fin de if( is_numeric($datos['est_id']) && $datos['est_id'] > 0 )


}//Fin de redireccionCuentaInactiva

//Función para detectar el idioma del navegador del usuario para definir idioma de encuestas, ofertas y multiofertas
//OR 2015-01-08
function detectar_idioma_navegador()
{
	$idioma_navegador = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

	$idioma_navegador = substr($idioma_navegador, 0, 5);

	if ($idioma_navegador == "pt-BR")
	{
		$idioma_navegador = 'br';
	}
	else
	{
		$idioma_navegador = substr($idioma_navegador, 0, 2);
	}

	switch ($idioma_navegador)
    {
        case 'es':
            $idi_id = 1;
            break;
        case 'en':
            $idi_id = 2;
            break;
        case 'fr':
            $idi_id = 3;
            break;
        case 'de':
            $idi_id = 4;
            break;
        case 'it':
            $idi_id = 5;
            break;
        case 'pt':
            $idi_id = 7;
            break;
        case 'br':
            $idi_id = 187;
            break;
        case 'pl':
            $idi_id = 10;
            break;
        case 'ca':
            $idi_id = 12;
            break;
        case 'zh':
            $idi_id = 13;
            break;
        case 'ru':
            $idi_id = 14;
            break;
        case 'uk':
            $idi_id = 15;
            break;
        case 'az':
            $idi_id = 26;
            break;
        default:
            $idi_id = 2;
            break;
    }//fin de Switch
	return $idi_id;
}//Fin de detectar_idioma_navegador

/**
 * Se encarga de crear un fichero en donde se 
 * registra que el script se esta ejecutando
 * DM - 2017-04-07
 **/ 
function registrarControlEjecucion($datos)
{
    //Ubicacion del script actual
    $directorio_local = dirname($datos['archivo']);
    //Nombre del archivo del script actual incluida la extension
    $nombre_script = basename($datos['archivo']).".log";
    
    $ubicacion_fichero = $directorio_local."/".$nombre_script;
    
    //Si existe un archivo creado lo elimina
    if( file_exists($ubicacion_fichero) )
    {
        @unlink($ubicacion_fichero);
    }
    
    //Intenta abrir el fichero para escritura
    $file = @fopen($ubicacion_fichero, "a");
    
    if( $file === false )
    {
        print_r("El script no se puede ejecutar, al parecer existe otro en ejecucion o no se puede iniciar el control de verificacion.");
        exit;
    }//Fin de if( $fp === false )
    
    //Si se crea el fichero se agrega el tiempo actual y un valor aleatorio
    //para identificar el script actual 
    $codigo_daemon = ":".microtime().":".rand(1111,9999);
    $estado_escritura = @fwrite($file,$codigo_daemon);
    @fclose($file);
    
    if( $estado_escritura === false )
    {
        print_r("El script no se puede ejecutar, no se puede iniciar el control de verificacion.");
        exit;
    }//Fin de if( $fp === false )
        
    $datos['codigo_daemon'] = $codigo_daemon;
    
    //Guarda el tiempo en el que inicia el script
    $datos['time_inicial'] = time();
    
    
    //DM - 2017-08-07 issue 10533
    $_ubicacion_fichero = "/home/hotelsqu/data/auditoria/scripts/";
    $_nombre_fichero = date('Ymd').".csv";
    
    $ruta = $_ubicacion_fichero.$_nombre_fichero; 
    $file = @fopen($ruta,"a");
    if( $file )
    {
        $datos_consumo = array();
                        
        //Nombre del script que se esta ejecutando
        $datos_consumo['nombre'] = basename($datos['archivo']);
        //Código del script que se esta ejecutando para identificar cuando inicia y termina con el mismo 
        //código
        $datos_consumo['codigo_daemon'] = $datos['codigo_daemon'];
        //Indica que el script empezo a ejecutarse
        $datos_consumo['inicia'] = 1;
        //Indica que el script termino de ejecutarse
        $datos_consumo['termina'] = 0;
        //Diferencia time en segundos
        $datos_consumo['time'] = 0;
        
        //cpu
        $comando = "uptime";
        $resultado_comando = shell_exec($comando);
        $resultado_comando = str_replace("load","",$resultado_comando);
        $resultado_comando = str_replace("average","",$resultado_comando);
        $resultado_comando = str_replace(":","",$resultado_comando);
        $resultado_comando = str_replace("user","",$resultado_comando);
        $resultado_comando_partes = preg_split("/,/",$resultado_comando);
        
        $datos_consumo['cpu'] = trim($resultado_comando_partes[3]);
             
        //memoria
        $comando = "vmstat -s";
        $resultado_comando = shell_exec($comando);
        $resultado_comando = str_replace("total","",$resultado_comando);
        $resultado_comando = str_replace("memory","",$resultado_comando);
        $resultado_comando = str_replace("used","",$resultado_comando);
        $resultado_comando_partes = preg_split("/\\n/",$resultado_comando);
        
        $datos_consumo['memoria_usada'] = trim($resultado_comando_partes[1]);
         
        $contenido = date('Y;m;d;H;i;s;').time().";".implode(";",$datos_consumo);
        
        $contenido .= "\n";
        
        fwrite($file,$contenido);
        fclose($file);
    }//Fin de if( $file )
     
    return $datos; 
}//Fin de registrarControlEjecucion

/**
 * Se encarga de registrar el tiempo que ha tardado el script en procesarse
 * DM - 2017-08-07
 **/
function registrarControlEjecucionFinalizacion($datos)
{
    //DM - 2017-08-07 issue 10533
    $_ubicacion_fichero = "/home/hotelsqu/data/auditoria/scripts/";
    $_nombre_fichero = date('Ymd').".csv";
    
    $ruta = $_ubicacion_fichero.$_nombre_fichero; 
    $file = @fopen($ruta,"a");
    if( $file )
    {
        $datos_consumo = array();
        //Nombre del script que se esta ejecutando
        $datos_consumo['nombre'] = basename($datos['archivo']);
        //Código del script que se esta ejecutando para identificar cuando inicia y termina con el mismo 
        //código
        $datos_consumo['codigo_daemon'] = $datos['codigo_daemon'];
        //Indica que el script empezo a ejecutarse
        $datos_consumo['inicia'] = 0;
        //Indica que el script termino de ejecutarse
        $datos_consumo['termina'] = 1; 
        //Diferencia time en segundos 
        $datos_consumo['time'] = time() - $datos['time_inicial'];
        
        //cpu
        $comando = "uptime";
        $resultado_comando = shell_exec($comando);
        $resultado_comando = str_replace("load","",$resultado_comando);
        $resultado_comando = str_replace("average","",$resultado_comando);
        $resultado_comando = str_replace(":","",$resultado_comando);
        $resultado_comando = str_replace("user","",$resultado_comando);
        $resultado_comando_partes = preg_split("/,/",$resultado_comando);
        
        $datos_consumo['cpu'] = trim($resultado_comando_partes[3]);
             
        //memoria
        $comando = "vmstat -s";
        $resultado_comando = shell_exec($comando);
        $resultado_comando = str_replace("total","",$resultado_comando);
        $resultado_comando = str_replace("memory","",$resultado_comando);
        $resultado_comando = str_replace("used","",$resultado_comando);
        $resultado_comando_partes = preg_split("/\\n/",$resultado_comando);
        
        $datos_consumo['memoria_usada'] = trim($resultado_comando_partes[1]);
        
        $contenido = date('Y;m;d;H;i;s;').time().";".implode(";",$datos_consumo);
        
        $contenido .= "\n";
        
        fwrite($file,$contenido);
        fclose($file);
    }//Fin de if( $file ) 
    
}//Fin de registrarControlEjecucionFinalizacion

/**
 * Se encarga de verificar si el codigo asociado a un script
 * es el mismo que se esta ejecutando actualmente
 * DM - 2017-04-07
 **/
function verificarControlEjecucion($datos)
{
    //Ubicacion del script actual
    $directorio_local = dirname($datos['archivo']);
    //Nombre del archivo del script actual incluida la extension
    $nombre_script = basename($datos['archivo']).".log";
    
    $ubicacion_fichero = $directorio_local."/".$nombre_script;
     
    //Intenta abrir el fichero para leer su contenido     
    $file = @fopen($ubicacion_fichero,"r+");
    
    //Si genera error de lectura entonces retorna error en la verificacion
    if( $file === false )
    {
        print_r("El script no se puede ejecutar, al parecer existe otro en ejecucion o no se puede iniciar el control de verificacion.");
        exit;
    }//Fin de if( $file === false )
    
    $contenido = @fread($file, filesize($ubicacion_fichero));
    fclose($file);
    
    //Verifica que el contenido del fichero es el mismo que se asigno
    //al inicio de la ejecucion del mismo
    if( $contenido != $datos['codigo_daemon'] )
    {
        print_r("El script no se puede ejecutar, al parecer existe otro en ejecucion.");
        exit;
    }

    //Elimina el fichero creado
    @unlink($ubicacion_fichero);
    
}//Fin de verificarControlEjecucion


/**
 * Se encarga de registrar el nombre de un proceso 
 * y el estado del servidor al momento del proceso
 * DM - 2017-11-19
 **/
function logProceso($datos)
{
    global $_CONFIG,$_PATH_SERVIDOR;
    
    $datos_consumo = array();
    
    //Si el debug de procesos no es 1 entonces no hace nada
    if( $_CONFIG['debug_procesos'] != 1 )
    {
        return $datos;
    }//Fin de if( $_CONFIG['debug_procesos'] != 1 )
    
    //$datos_consumo['anho'] = date('Y');
    //$datos_consumo['mes'] = date('m');
    $datos_consumo['dia'] = date('d');
    $datos_consumo['Hora'] = date('H');
    $datos_consumo['minutos'] = date('i');
    $datos_consumo['segundos'] = date('s');
    //$datos_consumo['time'] = time();
    
    $datos_consumo['sesion_proceso'] = 'S'.$_SERVER['UNIQUE_ID']; 
    
    $ubicacion_archivo = $_SERVER['SCRIPT_FILENAME'];
    //Retira la ubicacion raiz de la aplicacion pues ya se conoce
    $ubicacion_archivo = str_replace($_PATH_SERVIDOR,"",$ubicacion_archivo);
    
    //$datos_consumo['script'] = $ubicacion_archivo;
    
    /*
    
    //Cuando el llamado es para terminar un proceso entonces no registra nuevamente el nombre del proceso
    if( $datos['log_termina_proceso'] ==  1)
    {
        //Asigna el nombre que se le dio al proceso
        $datos_consumo['proceso'] = trim($datos['log_nombre_proceso'])."/".trim($datos['log_nombre_subproceso_actual']);            
    }//Fin de if( $datos['log_termina_proceso'] ==  1)
    else if( $datos['log_nombre_subproceso_actual'] != "" )
    {
        //Asigna el nombre que se le dio al proceso
        $datos_consumo['proceso'] = trim($datos['log_nombre_proceso'])."/".trim($datos['log_nombre_subproceso_actual']);    
    }
    else
    {
        //Asigna el nombre que se le dio al proceso
        $datos_consumo['proceso'] = trim($datos['log_nombre_proceso'])."/".trim($datos['log_nombre_proceso_actual']);
        
        $datos['log_nombre_proceso'] = trim($datos['log_nombre_proceso'])."/".trim($datos['log_nombre_proceso_actual']);
    }//Fin de else de if( $datos['log_termina_proceso'] ==  1)
    
    //a todos los nombres les agrega el / al finla
    //$datos_consumo['proceso'] .= "/";
    
    */
    
    $datos_consumo['proceso'] = trim($datos['log_nombre_proceso_actual'])."/";
    //Reemplaza los doble //
    $datos_consumo['proceso'] =  str_replace("///", "/",$datos_consumo['proceso']);
    $datos_consumo['proceso'] =  str_replace("//", "/",$datos_consumo['proceso']);
    
    $datos_consumo['subproceso'] = $datos['log_nombre_subproceso_actual'] != "" ? trim($datos['log_nombre_subproceso_actual'])."/" : "";
    $datos_consumo['subproceso'] =  str_replace("///", "/",$datos_consumo['subproceso']);
    $datos_consumo['subproceso'] =  str_replace("//", "/",$datos_consumo['subproceso']);
    
    //Indica son 1 si es el fin del método y con 0 es cuando inicia
    $datos_consumo['estado_proceso'] = $datos['log_termina_proceso'];
        
    $datos_consumo['modulo'] = $datos['m']; 
    $datos_consumo['accion'] = $datos['accion'];
    
    $datos_consumo['usuario_id'] = $_SESSION['usu_id'];    
    
    $datos_consumo['tipo_usuario'] = $_SESSION['tipo_usuario'];  
    
    if( $_CONFIG['debug_estado_servidor'] == 1 )
    {
        $comando = "uptime";
        $resultado_comando = shell_exec($comando);
        $resultado_comando = str_replace("load","",$resultado_comando);
        $resultado_comando = str_replace("average","",$resultado_comando);
        $resultado_comando = str_replace(":","",$resultado_comando);
        $resultado_comando = str_replace("user","",$resultado_comando);
        $resultado_comando_partes = preg_split("/,/",$resultado_comando);
        
        $datos_consumo['usuarios'] = trim($resultado_comando_partes[2]);
        $datos_consumo['cpu'] = trim($resultado_comando_partes[3]);
        
        $comando = "vmstat -s";
        $resultado_comando = shell_exec($comando);
        $resultado_comando = str_replace("total","",$resultado_comando);
        $resultado_comando = str_replace("memory","",$resultado_comando);
        $resultado_comando = str_replace("used","",$resultado_comando);
        $resultado_comando_partes = preg_split("/\\n/",$resultado_comando);
        
        $datos_consumo['memoria_total'] = trim($resultado_comando_partes[0]);
        $datos_consumo['memoria_usada'] = trim($resultado_comando_partes[1]);            
    }//Fin de if( $_CONFIG['debug_estado_servidor'] == 1 )
    
    //Si se ha configurado la carpeta donde se van a ubicar los ficheros de los procesos
    if( $_CONFIG['debug_procesos_ubicacion'] != "" )
    {
        $contenido = implode(",",$datos_consumo);

        $contenido .= "\n";

        $ruta = $_CONFIG['debug_procesos_ubicacion'].DIRECTORY_SEPARATOR.date('Ymd').".csv";
        
        //Si el archivo no existe le agrega un encabezado 
        if( !file_exists($ruta) )
        {
            $campos = array_keys($datos_consumo);
            $contenido_campos = implode(",",$campos);
            
            $contenido_campos .= "\n";
            
            $contenido = $contenido_campos.$contenido;    
        }//Fin de if( !file_exists($ruta) )
        
        $file = fopen($ruta,"a");
        if( $file )
        {
            fwrite($file,$contenido);
            fclose($file);
        }//Fin de if( $file )
    }//Fin de if( $_CONFIG['debug_procesos_ubicacion'] != "" )
            
    return $datos;                
}//Fin de logProceso

?>