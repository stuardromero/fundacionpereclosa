$(document).ready(function () {  
    $('#contrasena').keyup(function () {  
        $('#seguridadContrasena').html(checkStrength($('#contrasena').val()))  
    })  
    function checkStrength(contrasena) {  
        var strength = 0  
        if (contrasena.length < 6) {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-muy-corta')  
            return 'Molt Insegura'  
        }  
        if (contrasena.length > 7) strength += 1  
        // If contrasena contains both lower and uppercase characters, increase strength value.  
        if (contrasena.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1  
        // If it has numbers and characters, increase strength value.  
        if (contrasena.match(/([a-zA-Z])/) && contrasena.match(/([0-9])/)) strength += 1  
        // If it has one special character, increase strength value.  
        if (contrasena.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1  
        // If it has two special characters, increase strength value.  
        if (contrasena.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1  
        // Calculated strength value, we can return messages  
        // If value is less than 2  
        if (strength < 2) {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-corta')  
            return 'Insegura'  
        } else if (strength == 2) {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-buena')  
            return 'Segura'  
        } else {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-excelente')  
            return 'Molt Segura'  
        }  
    }  
    
    //asignarCursos();

    $('#est_edu_id').change(function(){
        asignarCursos();
    });

    function asignarCursos(){
        var enlace = "m=alumnos&accion=obtenerCursosEtapaEducativa";
        $.ajax({
            type: "POST",
            url: "index.php",
            data: enlace + "&edu_id=" + $('#est_edu_id').val(),
            success: function(r){
                $('#selectCurso').html(r);
            }
        });
    }

    $('#ces_ste_id').change(function(){
        asignarTerritorios();
    });

    function asignarTerritorios(){
        var enlace = "m=centros&accion=obtenerSelectorTerritorios";
        $.ajax({
            type: "POST",
            url: "index.php",
            data: enlace + "&ste_id=" + $('#ces_ste_id').val(),
            success: function(r){
                $('#selectTerritorio').html(r);
            }
        });
    }

    $('#ste_id').change(function(){
        asignarTerritoriosServ();
    });

    function asignarTerritoriosServ(){
        var enlace = "m=basereportes&accion=obtenerTerritorios";
        $.ajax({
            type: "POST",
            url: "index.php",
            data: enlace + "&ste_id=" + $('#ste_id').val(),
            success: function(r){
                $('#selectTerritorio2').html(r);
            }
        });
    }

    $('#selectTerritorio2').change(function(){
        asignarCentoTer();
    });

    function asignarCentoTer(){
        var enlace = "m=basereportes&accion=obtenerCentrosPorTerritorio";
        $.ajax({
            type: "POST",
            url: "index.php",
            data: enlace + "&territorio_id=" + $('#territorio_id').val(),
            success: function(r){
                $('#selectCentroTer').html(r);
            }
        });
    }

    $("#timepicker").datetimepicker({
        format : "HH:mm",
        defaultDate:moment().hours(0).minutes(0).seconds(0).milliseconds(0),
    });
    
    $('.time-picker').timepicker({ 
        timeFormat: 'HH:mm ',
        showInputs: false, showMeridian: false,
        dropdown: true,
        scrollbar: true });
});
// $(document).ready(function () {  
    
// });