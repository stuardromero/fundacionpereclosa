// JavaScript Document
/**
 * Se encarga de crear una alerta tipo dinamica y agregarla
 * en el div de alertas, previamente creado, 
 * si no existe entonces indica que no se ha creado
 * el div de alertas
 * DM - 2015-09-15
 **/
function crearAlertaDinamica(tipo_alerta, codigo_mensaje)
{
    var mensaje = idioma[ codigo_mensaje ];    
    var contenido = '<div class="alert alert-'+tipo_alerta+ ' alertas_flotantes alert-dismissable" style="font-size:14px; text-align:center;">';
    contenido += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    contenido += mensaje;
    contenido += '</div>';
    
    if( $('#alertas').length > 0 )
    {
        $('#alertas').html(contenido);
    }
    else
    {
        alert(mensaje);
    }    
}//Fin de crearAlertaDinamica

/**
 * Se encarga de crear una alerta mostrando el mensaje
 * que se pasa por parámetro
 * DM - 2015-09-17
 **/
function crearAlerta(tipo_alerta, mensaje)
{
    if( typeof(swal) == "function" )
    {
        mensaje = mensaje.replace(/\n/g,"<br>");
        
        swal({
            title: "",
            text: mensaje,
            type: tipo_alerta,
            timer: 10000,
            html:true,
            showCancelButton: false,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        });    
    }//Fin de if( typeof($.toaster) == "function" )
    else if( $('#alertas').length > 0 )
    {
        var contenido = '<div class="alert alert-'+tipo_alerta+' alert-dismissable">';
        contenido += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
        contenido += mensaje;
        contenido += '&nbsp;&nbsp;<a class="alert-link" href="#" data-dismiss="alert">'+idioma['label_volver']+'</a></div>';
        
        $('#alertas').html(contenido);    
    }//Fin de else if( $('#alertas').length > 0 )
    else
    {
        alert(mensaje);
    }
}//Fin de crearAlerta


/**
 * Se encarga de mostrar una alerta tipo toaster
 * DM - 2016-02-03
 **/
function mostrarAlertaToaster(tipo_alerta, titulo_alerta, mensaje, tiempo_seg)
{
    if( typeof($.toaster) != "function" )
    {
        alert('Para usar Toaster se sebe agregar el js librerias/toaster/jquery.toaster.js');
        return;
    }
    
    if( tiempo_seg > 0)
    {
        //Por defecto lo muestra por 8 seg
        $.toaster({  settings:{ timeout : 6000},  
            priority : tipo_alerta, title : titulo_alerta, message : mensaje});    
    }
    else
    {
        tiempo_seg = tiempo_seg * 1000;
        $.toaster({ settings:{ timeout : tiempo_seg} , 
            priority : tipo_alerta, title : titulo_alerta, message : mensaje});    
    }    

}//Fin de mostrarAlertaToaster