$('#formintervenciones').on('change', '#int_min_id', function() {
    if($(this).val() == 1){
        let url = "index.php?m=visitas&accion=obtener_tipos_absentismo";
        $.ajax(
            {
                url: url,
                dataType: 'json',
                success: function(result) {
                    let $select = $('<select class="form-select" name="tab_id" id="tab_id" required>');
                    let $options = $('<option value="" selected>ESCOLLIR...</option>');
                    $select.append($options)

                    for(var i = 0; i < result.length; i++) {
                        var obj = result[i];

                        console.log(obj);
                    
                        let $options = $('<option value="'+obj.tab_id+'" id="'+obj.tab_id+'">');

                        $options.append(obj.tab_nombre);

                        $select.append($options)
                    }
                    $label = $("<label for='first-name-column'>TIPUS D'ABSENTISME</label>");
                    $('#formintervenciones .tab_id').empty();
                    $('#formintervenciones .tab_altres_obser').empty();
                    $('#formintervenciones .tab_id').append($label).append($select);
                }
        });
    }
    else{
        $('#formintervenciones .tab_id').empty();
        if($(this).val() == 12){
            let $input = $('<textarea required id="int_otro_motivo_intervencion" placeholder="Escrigui el Motiu de la intervenció" rows="10" cols="50" name="int_otro_motivo_intervencion" class="form-control"></textarea>');
            $label = $("<label for='first-name-column'>Un altre motiu d'intervenció</label>");
            $('#formintervenciones .tab_altres_obser').empty();
            $('#formintervenciones .tab_altres_obser').append($label).append($input);
        }
        else{
            $('#formintervenciones .tab_id').empty();
            $('#formintervenciones .tab_altres_obser').empty();
        }
    }
});

$('#formintervenciones').on('change', '#int_tin_id', function() {
    console.log("entro");
    var $valores = $('#int_tin_id').val();
    console.log($.inArray("20", $valores));
    if($.inArray("20", $valores) != -1){
        let $input = $('<textarea required id="int_otro_tipo_intervencion" placeholder="Escrigui el Tipus de la intervenció" rows="10" cols="50" name="int_otro_tipo_intervencion" class="form-control"></textarea>');
        $label = $("<label for='first-name-column'>Un altre tipus d'intervenció</label>");
        $('#formintervenciones .tin_altres_obser').empty();
        $('#formintervenciones .tin_altres_obser').append($label).append($input);
    }
    else{
        if($("#int_tin_id") == 20){
            let $input = $('<textarea required id="int_otro_tipo_intervencion" placeholder="Escrigui el Tipus de la intervenció" rows="10" cols="50" name="int_otro_tipo_intervencion" class="form-control"></textarea>');
            $label = $("<label for='first-name-column'>Un altre tipus d'intervenció</label>");
            $('#formintervenciones .tin_altres_obser').empty();
            $('#formintervenciones .tin_altres_obser').append($label).append($input);
        }

        if($.inArray("20", $valores) == -1){
            $('#formintervenciones .tin_altres_obser').empty();
        }
    }
});

$('#formeditarintervenciones').on('change', '#int_tin_id', function() {
    if($("#int_tin_id").val() == 20){
        let $input = $('<textarea required id="int_otro_tipo_intervencion" placeholder="Escrigui el Tipus de la intervenció" rows="10" cols="50" name="int_otro_tipo_intervencion" class="form-control"></textarea>');
        $label = $("<label for='first-name-column'>Un altre tipus d'intervenció</label>");
        $('#formeditarintervenciones .tin_altres_obser').empty();
        $('#formeditarintervenciones .tin_altres_obser').append($label).append($input);
    }
    else{
        $('#formeditarintervenciones .tin_altres_obser').empty();
    }

    if($("#int_min_id").val() == 12){
        let $input = $('<textarea required id="int_otro_motivo_intervencion" placeholder="Escrigui el Motiu de la intervenció" rows="10" cols="50" name="int_otro_motivo_intervencion" class="form-control"></textarea>');
        $label = $("<label for='first-name-column'>Un altre motiu d'intervenció</label>");
        $('#formeditarintervenciones .tab_altres_obser').empty();
        $('#formeditarintervenciones .tab_altres_obser').append($label).append($input);
    }
    else{
        $('#formeditarintervenciones .tab_id').empty();
        $('#formeditarintervenciones .tab_altres_obser').empty();
    }
});

$('#formeditarintervenciones').on('change', '#int_min_id', function() {
    if($(this).val() == 1){
        let url = "index.php?m=visitas&accion=obtener_tipos_absentismo";
        $.ajax(
            {
                url: url,
                dataType: 'json',
                success: function(result) {
                    let $select = $('<select class="form-select" name="tab_id" id="tab_id" required>');
                    let $options = $('<option value="" selected>ESCOLLIR...</option>');
                    $select.append($options)

                    for(var i = 0; i < result.length; i++) {
                        var obj = result[i];

                        console.log(obj);
                    
                        let $options = $('<option value="'+obj.tab_id+'" id="'+obj.tab_id+'">');

                        $options.append(obj.tab_nombre);

                        $select.append($options)
                    }
                    $label = $("<label for='first-name-column'>TIPUS D'ABSENTISME</label>");
                    $('#formeditarintervenciones .tab_id').empty();
                    $('#formeditarintervenciones .tab_id').append($label).append($select);
                }
        });
    }
    else{
        $('#formeditarintervenciones .tab_id').empty();
        if($(this).val() == 12){
            let $input = $('<textarea required id="int_otro_motivo_intervencion" placeholder="Escrigui el Motiu de la intervenció" rows="10" cols="50" name="int_otro_motivo_intervencion" class="form-control"></textarea>');
            $label = $("<label for='first-name-column'>Un altre motiu d'intervenció</label>");
            $('#formeditarintervenciones .tab_altres_obser').empty();
            $('#formeditarintervenciones .tab_altres_obser').append($label).append($input);
        }
        else{
            $('#formeditarintervenciones .tab_id').empty();
            $('#formeditarintervenciones .tab_altres_obser').empty();
        }
    }
});