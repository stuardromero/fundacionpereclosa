/*	********************	VALIDAR NULO	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar si el texto ingresado por el Usuario es vac�o o nulo
 *	P�rametros:
 *		email -> Texto digitado por el Usuario
 *	Valores de retorno:
 *		true, si el texto no es vac�o o nulo
 *		false, de lo contrario
 */

function trim(myString)
{
    if (myString != undefined)
    {
        myString = myString.toString();
        return myString.replace(/^\s+/g, '').replace(/\s+$/g, '');
    }
    return myString;
}


function validarNulo(valor)
{
    valor = trim(valor);
    if (valor == null || valor == '' || valor == 'null')
    {
        return false;
    }

    for (i = 0; i < valor.length; i++)
    {
        val = valor.charAt(i);
        if (!(val == null || val == ' '))
        {
            return true;
        }
    }
    return false;
}


/*	********************	VALIDAR MAIL	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar si el texto ingresado representa una Direcci�n de Correo Electr�nico
 *	P�rametros:
 *		email -> Texto que representa la Direcci�n de Correo Electr�nico digitada por el Usuario
 *	Valores de retorno:
 *		true, si el texto digitado por el Usuario representa una Direcci�n de Correo Electr�nico
 *		false, de lo contrario
 */

function validarMail(email)
{
    email = trim(email);
    //var token = /^[a-z][a-z-_0-9\.]+@[a-z-_=>0-9\.]+\.[a-z]{2,4}$/
    //MAOH - 28 Ago 2012 - Incidencia 1560: Chequear formato de email introducido
    var token = /^[a-zA-Z0-9]*[a-zA-Z-_0-9\.]*@[a-zA-Z-_=>0-9\.]+\.[a-zA-Z]{2,10}$/
    return token.test(email);
}


function validarDominio(dominio)
{
    dominio = trim(dominio);
    var token = /^[a-zA-Z-_=>0-9\.]+\.[a-zA-Z]{2,10}$/
    return token.test(dominio);
}

/*	********************	VALIDAR N�MERO	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar si el texto ingresado representa un N�mero
 *	P�rametros:
 *		valor -> Texto que representa el N�mero digitado por el Usuario
 *	Valores de retorno:
 *		true, si el texto digitado por el Usuario representa un N�mero
 *		false, de lo contrario
 */

function validarNumero(valor)
{
    valor = trim(valor);

    if (!validarNulo(valor))
    {
        return false;
    }

    if (isNaN(valor))
    {
        return false;
    }
    else
    {
        return true;
    }
}


/**
 * Se encarga de validar un número de teléfono
 * teniendo en cuenta que solo puede agregar números,
 * y los caracteres () y +
 * DM - 2014-02-19
 **/
function validarTelefono(valor)
{
    valor = trim(valor);
    var token = /^\+?\d+$/;
    return token.test(valor);

}

/*	********************	VALIDAR N�MERO ENTERO	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar si el texto ingresado representa un N�mero Entero
 *	P�rametros:
 *		valor -> Texto que representa el N�mero Entero digitado por el Usuario
 *	Valores de retorno:
 *		true, si el texto digitado por el Usuario representa un N�mero Entero
 *		false, de lo contrario
 */

function validarNumeroEntero(valor)
{
    valor = trim(valor);
    var token = /^\d+$/;
    return token.test(valor);
}


/*	*************************	VALIDAR TAMA�O	*********************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar que la entrada tenga cierto tama�o definido
 *	P�rametros:
 *		valor -> Valor de la Variable a la cual se le quiere verificar su Tama�o
 *		num_caracteres_minimo -> N�mero m�nimo de caracteres que debe contener la Variable
 *		num_caracteres_maximo -> N�mero m�ximo de caracteres que debe contener la Variable
 *	Valores de retorno:
 *		true, Si el valor de la Variable cumple con el Tama�o especificado
 *		false, de lo contrario
 */

function validarTamanho(valor, num_caracteres_minimo, num_caracteres_maximo)
{
    valor = trim(valor);
    var tamanho = valor.length;
    if (tamanho >= num_caracteres_minimo && (num_caracteres_maximo == null || tamanho <= num_caracteres_maximo))
    {
        return true;
    }
    return false;
}//Fin validarTamanho()


/*	********************	VALIDAR FECHA	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar si el texto ingresado representa una Fecha en el formato YYYY-MM-DD
 *	P�rametros:
 *		fecha -> Texto que representa la Fecha digitada por el Usuario
 *	Valores de retorno:
 *		true, si el texto digitado por el Usuario representa una Fecha en el formato YYYY-MM-DD
 *		false, de lo contrario
 */

function validarFecha(fecha)
{
    var token = /^\d{4}(-\d{1,2}){1,2}$/;
    return token.test(fecha);
}

/*	*****************************	VALIDAR FECHA ACTUAL	***********************************	*/
/*
 *	Utilidad:
 *		Se encarga de validar si la fecha ingresada (aaaa-dd-mm)
 *	P�rametros:
 *		fecha -> string con la fecha a validar
 *	Valores de retorno:
 *		true, la fecha ingresada es mayor a la actual
 *		false, de lo contrario
 */

function validarFechaActual(fecha)
{
    var fecha_actual = new Date();
    var fecha_actual_conversion = fecha_actual.getFullYear() + "-" + fecha_actual.getDate() + "-" + (fecha_actual.getMonth() + 1);
    return validarRangoFechas(fecha, fecha_actual_conversion);

}//!validarFechaActual(fecha)

/*	********************	VALIDAR TIPO ARCHIVO	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar si el Archivo seleccionado es del Tipo de Archivo esperado
 *	P�rametros:
 *		archivo -> Texto que representa la ruta del Archivo seleccionado
 *		tipo_archivo -> Extensi�n del Tipo de Archivo esperado
 *	Valores de retorno:
 *		true, si el Archivo seleccionado es del Tipo de Archivo esperado
 *		false, de lo contrario
 */

function validarTipoArchivo(archivo, tipo_archivo)
{
    var partes_nombre_archivo = archivo.split(".");
    var extension_archivo = partes_nombre_archivo[partes_nombre_archivo.length - 1].toLowerCase();
    if (extension_archivo == tipo_archivo)
    {
        return true;
    }
    else
    {
        return false;
    }
}


/*	********************	VALIDAR IMAGEN	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar si el Archivo seleccionado es una Imagen GIF, PNG, JPG o JPEG
 *	P�rametros:
 *		archivo -> Texto que representa la ruta del Archivo seleccionado
 *	Valores de retorno:
 *		true, si el Archivo seleccionado es una Imagen GIF, PNG, JPG o JPEG
 *		false, de lo contrario
 */

function validarImagen(archivo)
{
    if (validarTipoArchivo(archivo, "gif") || validarTipoArchivo(archivo, "png") || validarTipoArchivo(archivo, "jpg") || validarTipoArchivo(archivo, "jpeg"))
    {
        return true;
    }
    else
    {
        return false;
    }
}



/*	********************	VALIDAR RANGO FECHAS	************************	*/

/*
 *	Utilidad:
 *		Se encarga de validar el rango entre dos Fechas dadas
 *	P�rametros:
 *		fecha_inicial -> L�mite Inferior del Rango
 *		fecha_final -> L�mite Superior del Rango
 *	Valores de retorno:
 *		true, si el L�mite Superior del Rango es mayor o igual al L�mite Inferior
 *		false, de lo contrario
 */

function validarRangoFechas(fecha_inicial, fecha_final)
{
    var datos_fecha_inicial = fecha_inicial.split("-"); // Datos de la Fecha Inicial
    var datos_fecha_final = fecha_final.split("-"); // Datos de la Fecha Final

    var anho_fecha_inicial = parseInt(datos_fecha_inicial[2]); // A�o de la Fecha Inicial
    var mes_fecha_inicial = parseFloat(datos_fecha_inicial[1]); // Mes de la Fecha Inicial
    var dia_fecha_inicial = parseFloat(datos_fecha_inicial[0]); // D�a de la Fecha Inicial


    var anho_fecha_final = parseInt(datos_fecha_final[2]); // A�o de la Fecha Final
    var mes_fecha_final = parseFloat(datos_fecha_final[1]); // Mes de la Fecha Final
    var dia_fecha_final = parseFloat(datos_fecha_final[0]); // D�a de la Fecha Final

    if (anho_fecha_final == anho_fecha_inicial)
    {
        if (mes_fecha_final == mes_fecha_inicial)
        {
            if (dia_fecha_final >= dia_fecha_inicial)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if (mes_fecha_final > mes_fecha_inicial)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if (anho_fecha_final > anho_fecha_inicial)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Se encarga de validar el formulario de acuerdo a la lista de campos que se envían en la
 * lista
 * DM - 2014-01-28
 **/
function desplegarResultadoValidacionConArreglo(lista_campos, error_mensaje)
{
    var existe_error = false;
    var error = error_mensaje != undefined && error_mensaje != '' ? error_mensaje : '';
    
    for (var pos in lista_campos)
    {
        var id_campo = lista_campos[pos]['id'] == undefined ? '' : lista_campos[pos]['id'];
        var nombre_campo = lista_campos[pos]['nombre'] == undefined ? '' : lista_campos[pos]['nombre'];
        var nombre_label = 'label_'+lista_campos[pos]['nombre'];
        nombre_label = nombre_label.replace('[]','');
        
        var separador_campo = "\*- ";

        //obtiene el tipo de campo
        switch (lista_campos[pos]['tipo'])
        {
            case 'checked':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    $('#' + nombre_label).css('color', '#000000');
                    
                    var seleccionado = false;
                    var existe_campo = false;
                    $('input[type=checkbox]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            existe_campo = true;
                            if ($(this).is(':checked') )
                            {
                                seleccionado = true;
                            }//Fin de if( !$('input[name=acepta_terminos]').is(':checked') )   
                        }//Fin de if( this.name == nombre_campo )
                    });
                                        
                    if( existe_campo && !seleccionado )
                    {
                        existe_error = true;
                        $('#' + nombre_label).css('color', '#FF0000');
                    }
                }
                else
                {
                    $('#' + nombre_label).css('color', '#000000');
                    if (!$('input[name=' + nombre_campo + ']').is(':checked'))
                    {
                        existe_error = true;
                        $('#label_' + nombre_campo).css('color', '#FF0000');
                    }//Fin de if( !$('input[name=acepta_terminos]').is(':checked') )                
                }
                break;
            case 'telefono':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    var posicion = 0;
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            this.style.backgroundColor = '#FFFFFF';
                            if (!validarTelefono(this.value))
                            {
                                nombre_campo_modificado = nombre_campo.substring(0, nombre_campo.length - 2);
                                var label = $('#label_' + nombre_campo_modificado + '_' + posicion).html();
                                error += separador_campo + label + ': ' + this.value + '\n' + idioma['campo_telefono_invalido'] + '\n';
                                this.style.backgroundColor = '#F5A9A9';
                                existe_error = true;
                            }//Fin de if( !validarNulo(this.value) )

                            posicion++;

                        }//Fin de if( this.name == nombre_campo )
                    });
                }
                else if ($('input[name=' + nombre_campo + ']').length)
                {
                    $('input[name=' + nombre_campo + ']').css('background', '#FFFFFF');
                    if (!validarTelefono($('input[name=' + nombre_campo + ']').val()))
                    {
                        var label = $('#' + nombre_label).html();
                        error += separador_campo + label + ': ' + $('input[name=' + nombre_campo + ']').val() + '\n' + idioma['campo_telefono_invalido'] + '\n';
                        $('input[name=' + nombre_campo + ']').css('background', '#F5A9A9');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }//else de de if( nombre_campo.indexOf('[]') != -1 )
                break;
            case 'select_no_requerido':
                $('select[name=' + nombre_campo + ']').css('background', '#FFFFFF');
                break;
            case 'select':
                //DM - 2017-07-16
                if( $('select[name=' + nombre_campo + ']').length == 0)
                {
                    continue;
                }
                //DM - 2017-07-16
                if( $('select[name=' + nombre_campo + ']').is(':hidden') )
                {
                    continue;
                }            
                if( id_campo != '' )
                {
                    $('#' + id_campo).css('background', '#FFFFFF');
    
                    if (!validarNulo($('#' + id_campo).val()))
                    {
                        $('#' + id_campo).css('background', '#F5A9A9');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )                
                }//Fin de if( id_campo != undefined )
                else
                {
                    $('select[name=' + nombre_campo + ']').css('background', '#FFFFFF');
    
                    if (!validarNulo($('select[name=' + nombre_campo + ']').val()))
                    {
                        $('select[name=' + nombre_campo + ']').css('background', '#F5A9A9');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }//Fin de else de if( id_campo != undefined )
                break;
            case 'dominio':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    var posicion = 0;
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            this.style.backgroundColor = '#FFFFFF';
                            if (!validarDominio(this.value))
                            {
                                nombre_campo_modificado = nombre_campo.substring(0, nombre_campo.length - 2);
                                var label = $('#label_' + nombre_campo_modificado + '_' + posicion).html();
                                this.style.backgroundColor = '#F5A9A9';
                                error += separador_campo + label + ': ' + this.value + '\n'+idioma['mensaje_dominio_valido']+'\n';
                                existe_error = true;
                            }//Fin de if( !validarNulo(this.value) )

                            posicion++;

                        }//Fin de if( this.name == nombre_campo )
                    });
                }
                else if ($('input[name=' + nombre_campo + ']').length)
                {
                    $('input[name=' + nombre_campo + ']').css('background', '#FFFFFF');
                    if (!validarDominio($('input[name=' + nombre_campo + ']').val()))
                    {
                        var label = $('#' + nombre_label).length > 0 ?  $('#' + nombre_label).html()+'\n' : '';
                        $('input[name=' + nombre_campo + ']').css('background', '#F5A9A9');
                        error += separador_campo + label + $('input[name=' + nombre_campo + ']').val() + '\n'+idioma['mensaje_dominio_valido']+'\n';
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }
                break;
            case 'email':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    var posicion = 0;
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            this.style.backgroundColor = '#FFFFFF';
                            if ( !$(this).is(':hidden') && !validarMail(this.value))
                            {
                            
                                //DM - 2017-07-18
                                //Si tiene la clase no_validar entonces no hace nada
                                if( !$(this).hasClass('no_validar') )
                                {
                                    nombre_campo_modificado = nombre_campo.substring(0, nombre_campo.length - 2);
                                    var label = $('#label_' + nombre_campo_modificado + '_' + posicion).html(); 
                                    this.style.backgroundColor = '#F5A9A9';
                                    error += separador_campo + label + ': ' + this.value + '\n' + idioma['usuario_correovalido'] + '\n';
                                    existe_error = true;
                                }
                            }//Fin de if( !validarNulo(this.value) )

                            posicion++;

                        }//Fin de if( this.name == nombre_campo )
                    });
                }
                else if( id_campo != '' && $('#' + id_campo).length )
                {
                    $('#' + id_campo).css('background', '#FFFFFF');
                    if (!validarMail($('#' + id_campo).val()))
                    {
                        var label = $('#' + nombre_label).length > 0 ?  $('#' + nombre_label).html()+'\n' : '';
                        $('#' + id_campo).css('background', '#F5A9A9');
                        error += separador_campo + label + $('#' + id_campo).val() + '\n' + idioma['usuario_correovalido'] + '\n';
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )                
                }
                else if ($('input[name=' + nombre_campo + ']').length)
                {
                    $('input[name=' + nombre_campo + ']').css('background', '#FFFFFF');
                    if (!validarMail($('input[name=' + nombre_campo + ']').val()))
                    {
                        var label = $('#' + nombre_label).length > 0 ?  $('#' + nombre_label).html()+'\n' : '';
                        $('input[name=' + nombre_campo + ']').css('background', '#F5A9A9');
                        error += separador_campo + label + $('input[name=' + nombre_campo + ']').val() + '\n' + idioma['usuario_correovalido'] + '\n';
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }
                break;
            case 'email_comas':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    var posicion = 0;
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            this.style.backgroundColor = '#FFFFFF';
                            
                            var valor_emails = this.value.split(",");
                            var es_valido = true;    
                            for (i=0;i<valor_emails.length;i++)
                            { 
                                if(!validarMail(valor_emails[i]))
                            	{
                            		es_valido = false;
                            	}
                            }//Fin de for (i=0;i<correos_notificaciones.length;i++)  
                            
                            if (!es_valido)
                            {
                                nombre_campo_modificado = nombre_campo.substring(0, nombre_campo.length - 2);
                                var label = $('#label_' + nombre_campo_modificado + '_' + posicion).html();
                                this.style.backgroundColor = '#F5A9A9';
                                error += separador_campo + label + ': ' + this.value + '\n' + idioma['usuario_correo_valido_comas'] + '\n';
                                existe_error = true;
                            }//Fin de if( !validarNulo(this.value) )

                            posicion++;

                        }//Fin de if( this.name == nombre_campo )
                    });
                }
                else if( id_campo != '' && $('#' + id_campo).length )
                {
                    $('#' + id_campo).css('background', '#FFFFFF');
                    
                    var valor_emails = $('#' + id_campo).val().split(",");
                    var es_valido = true;    
                    for (i=0;i<valor_emails.length;i++)
                    { 
                        if(!validarMail(valor_emails[i]))
                    	{
                    		es_valido = false;
                    	}
                    }//Fin de for (i=0;i<correos_notificaciones.length;i++)  
                    
                    if (!es_valido)
                    {
                        var label = $('#' + nombre_label).length > 0 ?  $('#' + nombre_label).html()+'\n' : '';
                        $('#' + id_campo).css('background', '#F5A9A9');
                        error += separador_campo + label + $('#' + id_campo).val() + '\n' + idioma['usuario_correo_valido_comas'] + '\n';
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )                
                }
                else if ($('input[name=' + nombre_campo + ']').length)
                {
                    $('input[name=' + nombre_campo + ']').css('background', '#FFFFFF');
                    
                    
                    var valor_emails = $('input[name=' + nombre_campo + ']').val().split(",");
                    var es_valido = true;    
                    for (i=0;i<valor_emails.length;i++)
                    { 
                        if(!validarMail(valor_emails[i]))
                    	{
                    		es_valido = false;
                    	}
                    }//Fin de for (i=0;i<correos_notificaciones.length;i++) 
                    
                    if (!es_valido)
                    {
                        var label = $('#' + nombre_label).length > 0 ?  $('#' + nombre_label).html()+'\n' : '';
                        $('input[name=' + nombre_campo + ']').css('background', '#F5A9A9');
                        error += separador_campo + label + $('input[name=' + nombre_campo + ']').val() + '\n' + idioma['usuario_correo_valido_comas'] + '\n';
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }
                break;
            case 'ckeditor':
                var contenido = CKEDITOR.instances['' + nombre_campo ].getData();
                $('#cke_' + nombre_campo).css('border', 'solid 1px rgb(211, 211, 211)');
                if (!validarNulo(contenido))
                {
                    $('#cke_' + nombre_campo).css('border', 'solid 2px #F5A9A9');
                    existe_error = true;
                }
                else
                {
                    var modificar_descripcion = CKEDITOR.instances['' + nombre_campo ].getData();
                    modificar_descripcion = cambioPalabrasCkeditor(modificar_descripcion);
                    CKEDITOR.instances['' + nombre_campo ].setData(modificar_descripcion);
                }
                break;
            case 'ckeditor_validar':
                var contenido = CKEDITOR.instances['' + nombre_campo ].getData();
                $('#cke_' + nombre_campo).css('border', 'solid 1px rgb(211, 211, 211)');
                if (!validarNulo(contenido))
                {
                    $('#cke_' + nombre_campo).css('border', 'solid 2px #F5A9A9');
                    existe_error = true;
                }
                break;
            case 'text':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            this.style.backgroundColor = '#FFFFFF';
                            if ( !$(this).is(':hidden') &&  !validarNulo(this.value))
                            {
                                //DM - 2017-07-18
                                //Si tiene la clase no_validar entonces no hace nada
                                if( !$(this).hasClass('no_validar') )
                                {
                                    this.style.backgroundColor = '#F5A9A9';
                                    existe_error = true;
                                }
                            }//Fin de if( !validarNulo(this.value) )
                        }//Fin de if( this.name == nombre_campo )
                    });

                }//Fin de if( nombre_campo.indexOf('[]') != -1 )
                else if( id_campo != '' && $('#' + id_campo).length > 0 )
                {
                    $('#' + id_campo).css('background', '#FFFFFF');
                    if (!validarNulo($('#' + id_campo).val()))
                    {
                        $('#' + id_campo).css('background', '#F5A9A9');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )                
                }
                else if ($('input[name=' + nombre_campo + ']').length > 0)
                {
                    $('input[name=' + nombre_campo + ']').css('background', '#FFFFFF');
                    if (!validarNulo($('input[name=' + nombre_campo + ']').val()))
                    {
                        $('input[name=' + nombre_campo + ']').css('background', '#F5A9A9');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }//else de de if( nombre_campo.indexOf('[]') != -1 )
                break;
            case 'numero':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            this.style.backgroundColor = '#FFFFFF';
                            if (!validarNumero(this.value))
                            {
                                //DM - 2017-07-18
                                //Si tiene la clase no_validar entonces no hace nada
                                if( !$(this).hasClass('no_validar') )
                                {
                                    this.style.backgroundColor = '#F5A9A9';
                                    existe_error = true;
                                }
                            }//Fin de if( !validarNulo(this.value) )
                        }//Fin de if( this.name == nombre_campo )
                    });

                }//Fin de if( nombre_campo.indexOf('[]') != -1 )
                else if( id_campo != '' && $('#' + id_campo).length > 0 )
                {
                    $('#' + id_campo).css('background', '#FFFFFF');
                    if (!validarNumero($('#' + id_campo).val()))
                    {
                        $('#' + id_campo).css('background', '#F5A9A9');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )                
                }
                else if ($('input[name=' + nombre_campo + ']').length > 0  )
                {
                    $('input[name=' + nombre_campo + ']').css('background', '#FFFFFF');
                    if (!validarNumero($('input[name=' + nombre_campo + ']').val()))
                    {
                        $('input[name=' + nombre_campo + ']').css('background', '#F5A9A9');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }//else de de if( nombre_campo.indexOf('[]') != -1 )
                break;
            
            case 'time_no_requerido':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            $('#' + nombre_label).css('color', '#000000');
                        }//Fin de if( this.name == nombre_campo )
                    });

                }//Fin de if( nombre_campo.indexOf('[]') != -1 )
                else if ($('input[name=' + nombre_campo + ']').length)
                {
                    $('#' + nombre_label).css('color', '#000000');
                }//else de de if( nombre_campo.indexOf('[]') != -1 )
                break;    
            case 'time':
                if (nombre_campo.indexOf('[]') != -1)
                {
                    $('input[type=text]').each(function ()
                    {
                        if (this.name == nombre_campo)
                        {
                            $('#' + nombre_label).css('color', '#000000');
                            if (!validarNulo(this.value))
                            {
                                $('#' + nombre_label).css('color', '#FF0000');
                                existe_error = true;
                            }//Fin de if( !validarNulo(this.value) )
                        }//Fin de if( this.name == nombre_campo )
                    });

                }//Fin de if( nombre_campo.indexOf('[]') != -1 )
                else if ($('input[name=' + nombre_campo + ']').length)
                {
                    $('#' + nombre_label).css('color', '#000000');
                    if (!validarNulo($('input[name=' + nombre_campo + ']').val()))
                    {
                        $('#' + nombre_label).css('color', '#FF0000');
                        existe_error = true;
                    }//Fin de if( !validarNulo(this.value) )
                }//else de de if( nombre_campo.indexOf('[]') != -1 )
                break;

        }//Fin de switch( lista_campos[pos]['tipo'] )
        
    }//Fin de for(var pos in lista_campos)
    
    if (existe_error)
    {
        error += idioma['campos_obligatorios_rojo'];
        if( typeof(crearAlerta) == "function" )
        {
            crearAlerta('error',error);
        }
        else
        {
            alert(error);
        }
        return false;
    }
    else if (error != "" && error != undefined)
    {
        if( typeof(crearAlerta) == "function" )
        {
            crearAlerta('error',error);
        }
        else
        {
            alert(error);
        }
        return false;
    }
    return true;
}//Fin de desplegarResultadoValidacionConArreglo()


function transformarFecha($fecha)
{
    $arreglo_fecha = fecha.split('-');
    /*if($arreglo_fecha[1].length==1)
     {
     $arreglo_fecha[1]='0'+$arreglo_fecha[1];
     }
     if($arreglo_fecha[2].length==1)
     {
     $arreglo_fecha[2]='0'+$arreglo_fecha[2];
     }*/
    return new Date($arreglo_fecha[0], $arreglo_fecha[2] - 1, $arreglo_fecha[1]);

}//Fin transformarFecha()

/**
 * This array is used to remember mark status of rows in browse mode
 */

function obtenerCampoGrupo(forma, nombre, posicion)
{
    var campo = null;
    var size = forma.elements.length;
    var contador = 0;
    for (var i = 0; i < size; i++)
    {
        if (forma.elements[i].name == nombre)
        {
            if (contador == posicion)
            {
                campo = forma.elements[i];
                break;
            }
            contador++;

        }//fin de if( forma.elements[i].name==nombre)
    }//Fin de for(var i=0; i < size; i++)

    return campo;
}//Fin de obtenerCampoGrupo


String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}

/**
 * Función que se encarga de obtener la posición
 * de un elemento, en un arreglo asociativo
 * DM - 2013-11-25
 **/
function indexOfAsociativo(arreglo, valor)
{
    for (var i in arreglo)
    {
        //si encuentra el registro, retorna la posicion
        if (arreglo[i] == valor)
        {
            return i;
        }
    }
    return -1;
}//Fin de indexOfAsociativo

/**
 * Función para obtener los parametros de una url
 * DM - 2013-10-17
 * http://www.rodrigoasensio.com/2012/04/como-obtener-los-parametros-del-la-url-con-jquery/
 **/
(function ($) {
    $.getURL = function (key) {
        key = key.replace(/[\[]/, '\\[');
        key = key.replace(/[\]]/, '\\]');
        var pattern = "[\\?&]" + key + "=([^&#]*)";
        var regex = new RegExp(pattern);
        var url = unescape(location.href);
        var results = regex.exec(url);
        if (results === null) {
            return '';
        } else {
            return results[1];
        }
    }
})(jQuery);


/**
 * Se encarga de contar la cantidad de elementos de un arreglo
 * y retornar un valor entero con el total de elmentos
 * DM - 2014-06-17
 **/
function size_array(lista)
{
    var contador = 0;
    for (var key in lista)
    {
        contador++;
    }//Fin de for(var key in lista)
    return contador;
}//Fin de size_array



/**
 * Se encarga encontrar en el contenido de ckeditor las palabras selected y from
 * y retornar un valor boolean diciendo si estan o no estan
 * DM - 2014-12-23
 **/
function validarContenidoCkeditor(cadena) {
    if (/selected/.test(cadena) && /from/.test(cadena))
        return true;
    else
        return false;
}//Fin de validarContenidoCkeditor

/*AS
 * 2015-01-19
 *Función para codificar el contenido del ckeditor
 *
 */
function cambioPalabrasCkeditor(cadena_entrada)
{
    //OR 2015-06-04 #5241 - Codifica la cadena en Base64 para pasarlo a php
    //para que no haya conflicto a guardar palabras como delete y from
    var cadena = window.btoa(cadena_entrada) + '_b6A';

    return cadena;

}//Fin de cambioPalabrasCkeditor


/**
 * Se encarga de codificar a base 64 todos los campos de texto
 * del formulario
 * DM - 2015-06-11
 **/
function codificarBase64CamposTextoFormulario(id_formulario)
{
    var forma = document.getElementById(id_formulario);

    var size = forma.elements.length;

    for (var i = 0; i < size; i++)
    {
        //Si es un campo de modulo o accion no hace nada
        switch (forma.elements[i].name)
        {
            case 'm':
            case 'accion':
                continue;
                break;
        }//Fin de switch(forma.elements[i].name)

        //si es un valor numerico tampoco lo procesa
        if (!isNaN(forma.elements[i].value))
        {
            continue;
        }

        var valor = forma.elements[i].value;

        switch (forma.elements[i].type)
        {
            case 'textarea':
            case 'text':
            case 'hidden':
                //Convierte la cadena en utf8
                valor = utf8_encode(valor);
                //OR 2015-06-04 #5241 - Codifica la cadena en Base64 para pasarlo a php
                //para que no haya conflicto a guardar palabras como delete y from
                valor = window.btoa(valor);

                //Para saber que esta codificado en base 64 le agrega una cadena adicional
                valor += '_b6A';
                forma.elements[i].value = valor;
                break;
        }//Fin de switch(forma.elements[i].type)
    }//Fin de for(var i=0; i<size; i++)

    return forma;
}//Fin de codificarBase64CamposTextoFormulario
/*
 * AMP - 20/11/15 - Convierte texto plano a Base64 en cualquier idioma
 */
function codificarBase64Texto(texto) {
    var valor = unescape(encodeURIComponent(texto));
    return window.btoa(valor);
}

$(document).ready(function () {
    $('.icheck_todos').on('ifChanged', function () {

        // checkboxTodosIcheck();

    });


});



function utf8_encode(argString) {
    //  discuss at: http://phpjs.org/functions/utf8_encode/
    // original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: sowberry
    // improved by: Jack
    // improved by: Yves Sucaet
    // improved by: kirilloid
    // bugfixed by: Onno Marsman
    // bugfixed by: Onno Marsman
    // bugfixed by: Ulrich
    // bugfixed by: Rafal Kukawski
    // bugfixed by: kirilloid
    //   example 1: utf8_encode('Kevin van Zonneveld');
    //   returns 1: 'Kevin van Zonneveld'

    if (argString === null || typeof argString === 'undefined') {
        return '';
    }

    // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var string = (argString + '');
    var utftext = '',
            start, end, stringl = 0;

    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;

        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode(
                    (c1 >> 6) | 192, (c1 & 63) | 128
                    );
        } else if ((c1 & 0xF800) != 0xD800) {
            enc = String.fromCharCode(
                    (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
                    );
        } else {
            // surrogate pairs
            if ((c1 & 0xFC00) != 0xD800) {
                throw new RangeError('Unmatched trail surrogate at ' + n);
            }
            var c2 = string.charCodeAt(++n);
            if ((c2 & 0xFC00) != 0xDC00) {
                throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
            }
            c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
            enc = String.fromCharCode(
                    (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
                    );
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }

    if (end > start) {
        utftext += string.slice(start, stringl);
    }

    return utftext;
}

function utf8_decode(str_data) {
    //  discuss at: http://phpjs.org/functions/utf8_decode/
    // original by: Webtoolkit.info (http://www.webtoolkit.info/)
    //    input by: Aman Gupta
    //    input by: Brett Zamir (http://brett-zamir.me)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Norman "zEh" Fuchs
    // bugfixed by: hitwork
    // bugfixed by: Onno Marsman
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: kirilloid
    // bugfixed by: w35l3y (http://www.wesley.eti.br)
    //   example 1: utf8_decode('Kevin van Zonneveld');
    //   returns 1: 'Kevin van Zonneveld'

    var tmp_arr = [],
            i = 0,
            c1 = 0,
            seqlen = 0;

    str_data += '';

    while (i < str_data.length) {
        c1 = str_data.charCodeAt(i) & 0xFF;
        seqlen = 0;

        // http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
        if (c1 <= 0xBF) {
            c1 = (c1 & 0x7F);
            seqlen = 1;
        } else if (c1 <= 0xDF) {
            c1 = (c1 & 0x1F);
            seqlen = 2;
        } else if (c1 <= 0xEF) {
            c1 = (c1 & 0x0F);
            seqlen = 3;
        } else {
            c1 = (c1 & 0x07);
            seqlen = 4;
        }

        for (var ai = 1; ai < seqlen; ++ai) {
            c1 = ((c1 << 0x06) | (str_data.charCodeAt(ai + i) & 0x3F));
        }

        if (seqlen == 4) {
            c1 -= 0x10000;
            tmp_arr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)), String.fromCharCode(0xDC00 | (c1 & 0x3FF)));
        } else {
            tmp_arr.push(String.fromCharCode(c1));
        }

        i += seqlen;
    }

    return tmp_arr.join("");
}



/**
 * Se encarga de crear campos ocultos con contenidos en base 64
 * para los editores de texto, de tal manera que no se afecten los 
 * contenidos originales
 * DM - 2016-07-07
 **/
function camposEditoresHTML()
{
    $('.ckeditor').each( function(){
        
        var contenido_campo = CKEDITOR.instances[ this.id ].getData();
        
        contenido_campo = cambioPalabrasCkeditor( contenido_campo );
        
        var nombre = this.name+'_b64';
        
        if( $('#'+nombre).length > 0 )
        {
            $('#'+nombre).val(contenido_campo);
            
        }//Fin de if( $('#'+nombre).length > 0 )
        else
        {
            var campo_base64 = '<input type="hidden" id="'+nombre+'" name="'+nombre+'" value="'+contenido_campo+'">';
            $(this).after(campo_base64);
        }
                    
    });
}//Fin de camposEditoresHTML

 
 /**
 * Se encarga de generar el html de cargando para los casos
 * en los cuales se usan procesos con ajax
 * DM - 2016-09-28
 * 
 * */
function generarHtmlCargando()
{
    var contenido = '<div class="sk-spinner sk-spinner-wave">';
    contenido += '<div class="sk-rect1"></div>';
    contenido += '<div class="sk-rect2"></div>';
    contenido += '<div class="sk-rect3"></div>';
    contenido += '<div class="sk-rect4"></div>';
    contenido += '<div class="sk-rect5"></div>';
    contenido += '</div>';
    
    contenido = '<div class="ibox-content"><div class="row"><div class="col-md-12">'+contenido+'</div></div></div>';
    return contenido;
}//Fin de generarHtmlCargando
