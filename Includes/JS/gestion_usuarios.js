$( document ).ready(function() {
    $('#formregistrarusuarios').on('change', '#Perfil, #serviciosTerritoriales', function() {
        let url = "index.php?m=usuarios&accion=obtener_servicios_territoriales";
    
        if (parseInt(this.value) === 4) {
            $('.territorios').css("display", "block");
        } else {
            $('.territorios').css("display", "none");
            $("#territorios").val([]);
        }
    
        if (parseInt(this.value) === 2) { //Promotor
            $('.centros-escolares').css("display", "block");
        }
        else {
            $('.centros-escolares').css("display", "none");
            $("#centros-escolares").val([]);
        }
    });
    
    
    $('.row').on('change', '#fm_ver_ultima_intervencion select', function() {
        let url = '';
        let data = {};
        let id_select = $(this).attr('id');
        let usuario = $('#usuario').val();
        switch (id_select) {
            case 'servicio_territorial': 
                url = "index.php?m=usuarios&accion=obtener_territorios&usu=" + usuario;
                data = {id_servicio_territorial: $(this).children("option:selected").val()}
                break;
            case 'territorio': 
                url = "index.php?m=usuarios&accion=obtener_centros_escolares";
                data = {id_territorio: $(this).children("option:selected").val()}
                break;
            default:
                break;
        }
    
        $.ajax({
            url: url,
            dataType: 'json',
            data: data,
            success: function(result) {
                let $select = '';
                let $options = '';
                switch (id_select) {
                    case 'servicio_territorial': 
                        $select = $('<select class="form-select" name="territorio" id="territorio" required>');
                        
                        $options = $('<option selected value="">Escollir...</option>');
                        $select.append($options);
                        
                        for(var i = 0; i < result.length; i++) {
                            var obj = result[i];
                        
                            $options = $('<option value="'+obj.id_territorio+'" id="'+obj.id_territorio+'">');
        
                            $options.append(obj.nombre_territorio);
        
                            $select.append($options)
                        }
                        $label = $('<label for="territorio">TERRITORI:</label>');
        
                        $('#fm_ver_ultima_intervencion .territorio').empty();
                        $('#fm_ver_ultima_intervencion .centro-escolar').empty();
                        $('#fm_ver_ultima_intervencion .territorio')
                        .append($label)
                        .append($select);
                        
                        break;
    
                    case 'territorio': 
                        $select = $('<select class="form-select" name="centro_escolar" id="centro_escolar" required>');
                        
                        $options = $('<option selected value="">Escollir...</option>');
                        $select.append($options);
    
                        for(var i = 0; i < result.length; i++) {
                            var obj = result[i];
                        
                            $options = $('<option value="'+obj.id_centro_escolar+'" id="'+obj.id_centro_escolar+'">');
        
                            $options.append(obj.nombre_centro_escolar);
        
                            $select.append($options);
                        }
                        $label = $('<label for="centro-escolar">Centro Escolar:</label>');
        
                        $('#fm_ver_ultima_intervencion .centro-escolar').empty();
                        $('#fm_ver_ultima_intervencion .promotor').empty();
                        $('#fm_ver_ultima_intervencion .centro-escolar')
                        .append($label)
                        .append($select);
                        
                        break;
                }
            }
        });
    });

    $('.row').on('change', '#fm_ver_seguimiento_estudiante select', function() {
        let url = '';
        let data = {};
        let id_select = $(this).attr('id');
        let usuario = $('#usuario').val();
        switch (id_select) {
            case 'servicio_territorial': 
                url = "index.php?m=usuarios&accion=obtener_territorios&usu=" + usuario;
                data = {id_servicio_territorial: $(this).children("option:selected").val()}
                break;
            case 'territorio': 
                console.log($('#territorio').val());
                let territorio = $('#territorio').val();
                url = "index.php?m=usuarios&accion=obtener_promotores&territorio=" + territorio;
                data = {}
                break;
            case 'promotor': 
                console.log($('#promotor').val());
                let promotor = $('#promotor').val();
                url = "index.php?m=usuarios&accion=obtener_centros_escolares&promotor=" + promotor;
                data = {}
                break;
            case 'centro_escolar': 
                console.log($('#centro_escolar').val());
                let centro_escolar = $('#centro_escolar').val();
                url = "index.php?m=usuarios&accion=obtener_estudiantes&centro_escolar=" + centro_escolar;
                data = {}
                break;
            default:
                break;
        }
    
        $.ajax({
            url: url,
            dataType: 'json',
            data: data,
            success: function(result) {
                let $select = '';
                let $options = '';
                let $mensaje = '';
                switch (id_select) {
                    case 'servicio_territorial': 
                        $select = $('<select class="form-select" name="territorio" id="territorio" required>');
                        
                        $options = $('<option selected value="">Escollir...</option>');
                        $select.append($options);
                        
                        for(var i = 0; i < result.length; i++) {
                            var obj = result[i];
                        
                            $options = $('<option value="'+obj.id_territorio+'" id="'+obj.id_territorio+'">');
        
                            $options.append(obj.nombre_territorio);
        
                            $select.append($options)
                        }
                        $label = $('<label for="territorio">TERRITORI:</label>');
        
                        $('#fm_ver_seguimiento_estudiante .territorio').empty();
                        $('#fm_ver_seguimiento_estudiante .centro-escolar').empty();
                        $('#fm_ver_seguimiento_estudiante .promotor').empty();
                        $('#fm_ver_seguimiento_estudiante .estudiantes').empty();
                        $('#fm_ver_seguimiento_estudiante .territorio')
                        .append($label)
                        .append($select);
                        
                        break;
    
                    case 'territorio': 
                        $select = $('<select class="form-select" name="promotor" id="promotor" required>');
                        
                        $options = $('<option selected value="">Escollir...</option>');
                        $select.append($options);

                        for(var i = 0; i < result.length; i++) {
                            var obj = result[i];
                        
                            $options = $('<option value="'+obj.usu_id+'" id="'+obj.usu_id+'">');
        
                            $options.append(obj.usu_nombre + " " + obj.usu_apellido);
        
                            $select.append($options);
                        }
                        $label = $('<label for="promotor">NOM I COGNOMS PROMOTOR:</label>');
        
                        $('#fm_ver_seguimiento_estudiante .centro-escolar').empty();
                        $('#fm_ver_seguimiento_estudiante .promotor').empty();
                        $('#fm_ver_seguimiento_estudiante .estudiantes').empty();
                        $('#fm_ver_seguimiento_estudiante .promotor')
                        .append($label)
                        .append($select);
                        
                        break;
                    case 'promotor': 
                        $select = $('<select class="form-select" name="centro_escolar" id="centro_escolar" required>');
                        
                        $options = $('<option selected value="">Escollir...</option>');
                        $select.append($options);

                        console.log(result);

                        for(var i = 0; i < result.length; i++) {
                            var obj = result[i];
                        
                            $options = $('<option value="'+obj.ces_id+'" id="'+obj.ces_id+'">');
        
                            $options.append(obj.ces_nombre);
        
                            $select.append($options);
                        }
                        $label = $('<label for="centro-escolar">CENTRE EDUCATIU:</label>');
        
                        $('#fm_ver_seguimiento_estudiante .centro-escolar').empty();
                        $('#fm_ver_seguimiento_estudiante .estudiantes').empty();
                        $('#fm_ver_seguimiento_estudiante .centro-escolar')
                        .append($label)
                        .append($select);
                        
                        break;
                    case 'centro_escolar': 
                        $select = $('<select class="form-select" name="estudiantes" id="estudiantes" required>');
                        
                        $options = $('<option selected value="">Escollir...</option>');
                        $select.append($options);

                        console.log(result.length);

                        if(result.length != 0){
                            for(var i = 0; i < result.length; i++) {
                                var obj = result[i];
                            
                                $options = $('<option value="'+obj.est_id+'" id="'+obj.est_id+'">');
            
                                $options.append(obj.est_idalu + " - " + obj.est_nombre);
            
                                $select.append($options);
                            }
                        }
                        else{
                            console.log("entra en mensaje");
                            $mensaje = "<p style='font-size: 16px;'>No hi ha nens assignat al centre.</p>";
                        }
                        console.log($mensaje);
                        
                        $label = $('<label for="estudiantes">ALUMNE:</label>');
        
                        $('#fm_ver_seguimiento_estudiante .estudiantes').empty();
                        if($mensaje){
                            $('#fm_ver_seguimiento_estudiante .estudiantes').append($label).append($mensaje);
                        }
                        else{
                            $('#fm_ver_seguimiento_estudiante .estudiantes')
                            .append($label)
                            .append($select);
                        }
                        
                        
                        break;
                }
            }
        });
    });
});