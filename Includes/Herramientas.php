<?php

/*
  CARLOS H. CERON M. CARCERON@HOTMAIL.COM
  JULIO 01 2008

  FUNCIONES VARIAS QUE PUEDEN SER UTILIZADAS POR CUALQUIER CLASE
 */

class Herramientas {


    /**
     * Se encarga de verificar si se cumple el limite de memoria
     * establecido
     * DM - 2019-03-27
     **/
    static function verificarMemoria($datos)
    {
        global $_CONGIF;
        
        //obtiene el consumo de memoria en megas
        //ya que el valor se obtiene en bytes se divive por
        //100.000 para obtenerlo en mega bytes
        $memoria_usada = memory_get_usage()/100000;
        
        $limite_memoria = Herramientas::obtenerValorConfiguracion( array('codigo'=>'limite_memoria_php' ) );
        
        //Si no se ha definido el limite entonces retorna error
        if( !is_numeric($limite_memoria) )
        {
            return false;
        }//Fin de if( !is_numeric($limite_memoria) )
                        
        //Si el consumo de memoria hasta el momento supera el limite
        //entonces hace la redireccion
        if( $memoria_usada  >  $limite_memoria )
        {
            //Retorna que se supero el limite
            return false;                        
        }//Fin de if( $memoria_usada  >  $limite )
    
        return true;
    }//Fin de verificarMemoria

    /**
     * Se encarga de consultar los valores de configuracion
     * en la base de datos y responder con los valores por defecto
     * en caso de no existir
     * DM - 2019-02-22
     **/
    static function obtenerValorConfiguracion($datos)
    {
        global $_obj_database;
        
        $nombre = $datos['codigo'];
        
        $valor = $_obj_database->obtenerRegistro("configuracion_general",
                                        "cog_codigo='".$nombre."'");
                                                                                   
        //verifica si el valor ya se encuentra en la base de datos  
        if( !is_array($valor) )
        {
            $datos_registrar = array();
            $datos_registrar['tabla'] = "configuracion_general";
            $datos_registrar['campos'] = array("cog_codigo","cog_valor");
            $datos_registrar['cog_codigo'] = $nombre;
            
            //Si el valor no existe debe crearlo y asignarle un valor por defecto
            switch($nombre)
            {
                case 'dias_recordatorio_newsletter_finaliza':
                    $valor = 3;
                    $datos_registrar['cog_valor'] = $valor;
                    $_obj_database->insertar($datos_registrar);
                    break;
                case 'encuestas_sin_asunto_valoracion':
                    $valor = "";
                    break;
                case 'limite_memoria_php':
                    $valor = 2500;
                    $datos_registrar['cog_valor'] = $valor;
                    $_obj_database->insertar($datos_registrar);
                    break;
            }//Fin de switch($nombre)
        
        }//Fin de if( !is_array($valor) )
        else
        {
            $valor = $valor['cog_valor'];
        }                                                                         
        
        return $valor;
    }//Fin de obtenerValorConfiguracion

    /**
     * Se encarga de retornar las imagenes que se pueden usar en el
     * editor grapejs
     * DM - 2018-11-06
     **/
    static function obtenerImagenesEditorGrapeJs($datos)
    {
        global $_PATH_SERVIDOR,$_PATH_USUARIOS,$_PATH_WEB;
        
        $directorio_imagenes = $_PATH_SERVIDOR.'/userfiles/'.$_SESSION['usu_id'].'/Image/';
        $listado_imagenes = Archivos::listarArchivosImagenes($directorio_imagenes);
                
        foreach($listado_imagenes as $pos => $imagen)
        {
            $src_imagen = $directorio_imagenes.$imagen;
            
            $imagen = rawurlencode($imagen);
            //DM - 2019-10-17 redmine[14020]
            if( $datos['datos_imagen'] == 1 )
            {
                $info_imagen = getimagesize($src_imagen);
                $listado_imagenes[ $pos ] = array();
                $listado_imagenes[ $pos ]['ancho'] = $info_imagen[0];
                $listado_imagenes[ $pos ]['alto'] = $info_imagen[1];
                $listado_imagenes[ $pos ]['src'] = $_PATH_WEB.'/userfiles/'.$_SESSION['usu_id'].'/Image/'.$imagen;    
            }
            else
            {
                $listado_imagenes[ $pos ] = $_PATH_WEB.'/userfiles/'.$_SESSION['usu_id'].'/Image/'.$imagen;
            }             
        }
        return $listado_imagenes;
    }//Fin de obtenerImagenesEditorGrapeJs

    //devuelve la fecha y hora actual del sistema
    function obtenerFechaYHoraActual() {
        $dato = getDate();
        $hora = $dato["hours"];
        $minutos = $dato["minutes"];
        $dia = $dato["mday"];
        $mes = $dato["mon"];
        $anho = $dato["year"];

        if ($mes < 10)
            $mes = "0$mes";

        if ($dia < 10)
            $dia = "0$dia";

        $fecha_actual = "$anho-$mes-$dia";
        $hora_actual = "$hora:$minutos";

        $info["fecha"] = $fecha_actual;
        $info["hora"] = $hora_actual;

        return $info;
    }

    static function obtenerNombreMes($mes, $datos = array()) {
        global $_SESSION;

        $mes--;

        $idi_id = $_SESSION['idi_id'];
        if ($datos['idi_id'] != "") {
            $idi_id = $datos['idi_id'];
        }//Fin de if( $datos['idi_id'] != "" )

        if ($idi_id == 2) //ingles
            $meses = array("January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December");
        else if ($idi_id == 3) //frances
            $meses = array("Janvier", "F&eacute;vrier", "Mars", "Avril", "Mai", "Juin",
                "Juillet", "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre");
        else if ($idi_id == 4) //aleman
            $meses = array("Januar", "Februar", "M&auml;rz", "April", "Mai", "Juni",
                "Juli", "August", "September", "Oktober", "November", "Dezember");
        else if ($idi_id == 7) //portugues
            $meses = array("Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho",
                "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
        else if ($idi_id == 10) //Polaco
            $meses = array("Stycznia", "Lutego", "Marca", "Kwietnia", "Maja", "Czerwca",
                "Lipca", "Sierpnia", "WrzeÅ›nia", "PaÅºdziernika", "Listopada", "Grudnia");
        else if ($idi_id == 13) //Chino
            $meses = array("ä¸€æœˆ", "äºŒæœˆ", "ä¸‰æœˆ", "å››æœˆ", "äº”æœˆ", "å…­æœˆ",
                "ä¸ƒæœˆ", "å…«æœˆ", "ä¹æœˆ", "åæœˆ", "åä¸€æœˆ", "åäºŒæœˆ");
        else if ($idi_id == 14) //	Ruso
            $meses = array("Ð¯Ð½Ð²Ð°Ñ€ÑŒ", "Ð¤ÐµÐ²Ñ€Ð°Ð»ÑŒ", "ÐœÐ°Ñ€Ñ‚", "ÐÐ¿Ñ€ÐµÐ»ÑŒ", "ÐœÐ°Ð¹", "Ð˜ÑŽÐ½ÑŒ",
                "Ð˜ÑŽÐ»ÑŒ", "ÐÐ²Ð³ÑƒÑÑ‚", "Ð¡ÐµÐ½Ñ‚ÑÐ±Ñ€ÑŒ", "ÐžÐºÑ‚ÑÐ±Ñ€ÑŒ", "ÐÐ¾ÑÐ±Ñ€ÑŒ", "Ð”ÐµÐºÐ°Ð±Ñ€ÑŒ");
        else if ($idi_id == 15) //Ucraniano
            $meses = array("Ð¡Ñ–Ñ‡ÐµÐ½ÑŒ", "Ð›ÑŽÑ‚Ð¸Ð¹", "Ð‘ÐµÑ€ÐµÐ·ÐµÐ½ÑŒ", "ÐšÐ²Ñ–Ñ‚ÐµÐ½ÑŒ", "Ð¢Ñ€Ð°Ð²ÐµÐ½ÑŒ", "Ð§ÐµÑ€Ð²ÐµÐ½ÑŒ",
                "Ð›Ð¸Ð¿ÐµÐ½ÑŒ", "Ð¡ÐµÑ€Ð¿ÐµÐ½ÑŒ", "Ð’ÐµÑ€ÐµÑÐµÐ½ÑŒ", "Ð–Ð¾Ð²Ñ‚ÐµÐ½ÑŒ", "Ð›Ð¸ÑÑ‚Ð¾Ð¿Ð°Ð´", "Ð“Ñ€ÑƒÐ´ÐµÐ½ÑŒ");
        else
            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        return $meses[$mes];
    }

    //convierte una fecha en formato yyyy-mm-dd a dd-mm-yyyy
    function convertirFecha($fecha) {
        if ($fecha != "") {
            //si tiempo el campo de hora
            $partes = explode(" ", $fecha);
            $hora = $partes[1] != "" ? " " . $partes[1] : $partes[1];

            //obtiene el campo de fecha
            $partes = explode("-", $partes[0]);

            //verifica que el campo aÃ’o este al inicio y sea mayor que 4 digitos
            //si no es asi indica que esta en el formato solicitado
            if (is_numeric($partes[0]) && $partes[0] > 1000) {
                return "{$partes[2]}-{$partes[1]}-{$partes[0]}" . $hora;
            }//Fin de if( is_numeric($partes[0]) && $partes[0] > 1000 )
        }
        return $fecha;
    }

    static function convertirFechaFormatoDB($fecha, $tag = "/"){
        //verifica que el formato este en dd-mm-aaaa de lo contrario deja la cadena sin modificaciones
        $partes = explode($tag, $fecha); //dd-mm-aaaa
        if (is_numeric($partes[0]) && $partes[0] < 32) {
            return "{$partes[2]}-{$partes[1]}-{$partes[0]}";
        }
        return $fecha;
    }

    // Fecha en formato dd/mm/yyyy o dd-mm-yyyy retorna la diferencia en dias
    function restaFechas($dFecIni, $dFecFin) {


        $dFecIni = str_replace("-", "", $dFecIni);
        $dFecIni = str_replace("/", "", $dFecIni);
        $dFecFin = str_replace("-", "", $dFecFin);
        $dFecFin = str_replace("/", "", $dFecFin);

        ereg("([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
        ereg("([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);

        $date1 = mktime(0, 0, 0, $aFecIni[2], $aFecIni[1], $aFecIni[3]);
        $date2 = mktime(0, 0, 0, $aFecFin[2], $aFecFin[1], $aFecFin[3]);

        return round(($date2 - $date1) / (60 * 60 * 24));
    }

    /*     * ********************************  CREAR SELECT FORMA  ********************************* */
    /*
     * 	Utilidad:
     * 		Crea un select (html) de una consulta en la BD
     * 	Parametros de entrada:
     * 		$nombre_campo: nombre del select, es decir name=$nombre_campo
     * 		$nombre_tabla: Nombre de la tabla donde se va a consultar
     * 		$atributos: Arreglo con los atributos que se van a consultar
     * 		$condicion: Condicion de la consulta en la BD
     * 		$orden: Ordenar los datos de la consulta ORDER BY
     * 		$posicion: Identificador del valor seleccionado por defecto "si lo hay" (selected)
     * 		$opciones: opciones adicionales en el select
     * 	Valores de Retorno:
     * 		$select: el codigo html del select
     */

    static function crearSelectForma($datos, $desactivada = '') {
        global $_obj_database, $idi_despliegue;

        echo '<pre>';
        print_r($idi_despliegue);
        echo '</pre>';

        $texto_seleccionar = $idi_despliegue['seleccionar'];

        if ($texto_seleccionar == "") {
            $texto_seleccionar = $idi_despliegue['seleccionar'];
        }

        if ($datos['texto_defecto_seleccionar'] != "") {
            $texto_seleccionar = $datos['texto_defecto_seleccionar'];
        }//Fin de if( $datos['texto_defecto_seleccionar'] != "" )

        $codigo = $datos['atributos']['codigo']; // El identificador
        $mostrar = $datos['atributos']['mostrar']; // El nombre a mostrar en el select

        $resultado = $_obj_database->obtenerRegistrosAsociativosPorClave($datos['sql'], $codigo);

        //Esto es para reducir el tamaÃ±o de la letra al select cuando se despliega en pantallas pequeÃ±as
        if ($_GET['anchoPantalla'] != '') {
            if ($_GET['anchoPantalla'] <= 800) {
                $fuenteInicio = "<font size='1'>";
                $fuenteFinal = "</font>";
            } else {
                $fuenteInicio = "";
                $fuenteFinal = "";
            }
        } else {
            $fuenteInicio = "";
            $fuenteFinal = "";
        }//Fin Esto es para reducir el tamaÃ±o de la letra al select cuando se despliega en pantallas pequeÃ±as

        //JMM-2017-08-02 - Desarrollo #10532
        if ($datos['css'] != "") {
        	$estilo_css = $datos['css'];
        }//Fin de if ($datos['css'] != "")

        //JMM-2017-09-04 - Desarrollo #10532
        if ($datos['usa_flecha_chevron'] == 1) {
        	$select = $fuenteInicio . "<select id='id_select' onClick='ShowSelected();' ".$estilo_css." name=\"" . $datos['nombre_campo'] . "\" " . $datos['adicionales'] . " data-native-menu='false' " . $desactivada . ">";
        } else {
        	//Agosto 12 de 2011: El atributo data-native-menu='false' se agrego para cuando el cuestionario es para moviles y lo reconozca jQuery Mobile
        	$select = $fuenteInicio . "<select name=\"" . $datos['nombre_campo'] . "\" " . $datos['adicionales'] . " data-native-menu='false' " . $desactivada . ">";
        }
        
        //DM - 18-01-2012
        // Permite ocultar el valor por defecto
        if ($datos['ocultar_valor_por_defecto'] != 1) {
            //Valor por defecto
            $select .= "<option value='".$datos['valor_opcion_por_defecto']."' ".$datos['atributos_valor_por_defecto']." align=center>" . $texto_seleccionar . "</option>";
        }

        //Calcula el tamaño de resultado para llenar el select en el for.
        $numero = sizeof($resultado);

        if( is_array($datos['opciones_adicionales_inicial']) )
        {
            foreach ($datos['opciones_adicionales_inicial'] as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "")
                {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        }//Fin de if( is_array($datos['opciones_adicionales_inicial']) )
        

        if (is_array($resultado) && $numero > 0) {
            foreach ($resultado as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "") {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        if( is_array($datos['opciones_adicionales_final']) )
        {
            foreach ($datos['opciones_adicionales_final'] as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "")
                {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        }//Fin de if( is_array($datos['opciones_adicionales_final']) )

        //JMM-2017-09-04 - Desarrollo #10532
        if ($datos['usa_flecha_chevron'] == 1) {
        	$select .= "</select>
        		<i id='flecha_left' class='fa fa-chevron-left'></i>" . $fuenteFinal;
        } else {
        	$select .= "</select>" . $fuenteFinal;
        }

        return $select;
    }

// Fin crearSelectForma ()

    function crearListadoCheckForma($datos) {
        global $_obj_database, $idi_despliegue;

        $contenido = "";

        $codigo = $datos['codigo']; // El identificador
        $mostrar = $datos['mostrar']; // El nombre a mostrar en el select

        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $contenido = "";


        if ($datos['opcion_todos'] == 1) {
            $id_campo = str_replace("[]", "", $datos['nombre_campo']);

            $contenido = "<input type='checkbox' id='all_" . $id_campo . "' value='all_" . $id_campo . "' " . $datos['adicional'] . "
                            onClick=\"checkAll(this.form." . $id_campo . ", this.form.all_" . $id_campo . ",'" . $datos['nombre_campo'] . "');\" >";
            $contenido .= "&nbsp;" . $idi_despliegue['todas'] . "<br>\n";
        }//Fin de if( $datos['opcion_todos']==1 )
        //Calcula el tamaÃ’o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);

        if (is_array($resultado) && $numero > 0) {

            foreach ($resultado as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8
                //Si el idioma seleccionado es Polaco(10), Chino(13), Ruso(14) o Ucraniano(15) para que la consulta reconozca caracteres extraÃ±os
                /* if (($_SESSION['idi_id'] == 10) || ($_SESSION['idi_id'] == 13) || ($_SESSION['idi_id'] == 14) || ($_SESSION['idi_id'] == 15) || ($_SESSION['idi_id'] == 9) || ($_SESSION['idi_id'] == 26))
                  {
                  $mostrarFila = $res[$mostrar];
                  }
                  else
                  {
                  $mostrarFila = $res[$mostrar];
                  $mostrarFila = htmlentities($mostrarFila,ENT_QUOTES);
                  $mostrarFila = substr($mostrarFila, 0, 26);
                  $mostrarFila = strtolower($mostrarFila);
                  $mostrarFila = ucwords($mostrarFila);
                  } */

                $checked = "";
                if (is_array($datos['valor']) && in_array($codigoFila, $datos['valor'])) {
                    $checked = "checked";
                }
                $campo = "<input " . $checked . " type='checkbox' name='" . $datos['nombre_campo'] . "' value='" . $codigoFila . "' " . $datos['adicional'] . ">";

                $campo .= "&nbsp;" . $mostrarFila;

                $contenido .= $campo . "<br>\n";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)
        else {
            $contenido = $idi_despliegue['reportes_popup_sin_datos'];
        }

        return $contenido;
    }

//Fin de crearCheckForma()
    //MAOH - 22 Dic 2011 - Se agrega la imagen a mostrar para activar el calendario
    //MAOH - 20 Jun 2012 - Se agregan los offset (offX, offY) por si se desea mostrar el calendario en otra posicion
    function construirCalendario($objeto, $nombre_formulario, $nombre_campo, $retornar = 0, $imagen = "dynCalendar.gif", $offsetX = "", $offsetY = "") {
        global $_PATH_IMAGENES;

        if ($retornar == 0)
            echo "<script language='JavaScript' type='text/javascript'>
		   $objeto = new dynCalendar('$objeto', 'bajarFecha','$_PATH_IMAGENES/', '$imagen', '$offsetX', '$offsetY');
		   $objeto.nom_form = '$nombre_formulario';
		   $objeto.nom_campo = '$nombre_campo';
	    	   $objeto.setMonthCombo(true);
		   $objeto.setYearCombo(true);
		   </script>";
        else {
            $calendario = "&nbsp;<script language='JavaScript' type='text/javascript'>
		   $objeto = new dynCalendar('$objeto', 'bajarFecha','$_PATH_IMAGENES/', '$imagen', '$offsetX', '$offsetY');
		   $objeto.nom_form = '$nombre_formulario';
		   $objeto.nom_campo = '$nombre_campo';
	    	   $objeto.setMonthCombo(true);
		   $objeto.setYearCombo(true);
		   </script>";
            //  &nbsp;&nbsp;<input type='button' class='boton' value='Limpiar Fecha' onClick='document.$nombre_formulario.$nombre_campo.value=\"\";return false;'>";


            return $calendario;
        }
    }

    /**
     * Se encarga de validar si la cadena es o no una url valida
     * DM - 2015-05-20
     * */
    function validarURL($url) {
        $url = trim($url);

        $pattern = "/\b(?:(?:https?):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";

        if (!preg_match($pattern, $url)) {
            return false;
        }

        return true;
    }

//Fin de validarURL

    /**
     * Valida que un correo se encuentre bien formado
     * DM - 2015-03-25
     * */
    static function validarCorreo($email) {
        $email = trim($email);

        if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+\.+([a-zA-Z0-9\._-]+)+$/", $email)) {
            return false;
        }

        return true;
    }

//Fin de validarCorreo



    /***
     * Se encarga de revisar cuales variables no han sido asignada
     * en la variable $datos para poder asignarlas
     * DM - 2019-12-03
     **/
    static function verificarVariablesParametros($datos){
    
        $url_parametros = $_SERVER['QUERY_STRING'];
        //Retira los caracteres convertidos para url
        $url_parametros = rawurldecode( $url_parametros );
        $url_parametros = str_replace(" ","",$url_parametros);
        $url_parametros = str_replace("\r","",$url_parametros);
        $url_parametros = str_replace("\n","",$url_parametros);
        $url_parametros = str_replace("&amp;","&",$url_parametros);
        $parametros = preg_split("/&/", $url_parametros);
        foreach($parametros  as $valor){
        
            $partes_valor = preg_split("/=/", $valor);
                
            if( count($partes_valor ) == 2 ){
                $nombre_parametro = trim($partes_valor[0]);
                $valor_parametro = trim($partes_valor[1]);
                //Si la variable no esta asignada entonces lo hace
                if( !isset($datos[ $nombre_parametro ] ) )
                {
                    $datos[ $nombre_parametro ] = $valor_parametro; 
                }    
            }//Fin de if( count($partes_valor ) == 2 ){
            
        }//Fin de foreach($paremetros  as $valor){
        
        return $datos;
    }//Fin de verificarVariablesParametros
    
    
    /**
     * Se encarga de verificar los valores de las variables
     * necesarias para hacer el despliegue
     * DM - 2019-12-09
     **/
     static function verificarValoresParametros($datos){
        global $_obj_interfaz,$_obj_database,$_db_obj;
     
        //Si no existe el arreglo de variables a verificar no hace nada
        if( !is_array($datos['parametros_verificar']) ){
            return;
        }//Fin de if( is_array($datos['variables_verificar']) ){
                
        foreach($datos['parametros_verificar'] AS $nombre => $tipo){                
            //Si la variable no esta presente entre los parametros
            //debe generar mensaje de error
            if( !isset( $datos[$nombre] ) ){            
                $datos_plantilla = array();
                $datos_plantilla['idi_id'] = 1;
                $datos_plantilla['codigo_plantilla'] = 'contenido_mensaje_parametros_invalidos';
                $_obj_interfaz->crearInterfazGraficaPlantilla($datos_plantilla);
                exit;   
            }//Fin de if( !isset( $datos[$nombre] ) ){
            
            //si esta presente, debe verificar que sea el valor correcto
            switch($tipo){
                case 'entero':
                    $valor = $datos[ $nombre ];
                    if( !is_numeric($valor) ){
                        $datos_plantilla = array();
                        $datos_plantilla['idi_id'] = 1;
                        $datos_plantilla['codigo_plantilla'] = 'contenido_mensaje_parametros_invalidos';
                        $_obj_interfaz->crearInterfazGraficaPlantilla($datos_plantilla);
                        exit;                       
                    }//Fin de if( !is_numeric($valor) ){
                    break;
                case 'string':
                    $valor = $datos[ $nombre ];
                    if( !is_string($valor) ){
                        $datos_plantilla = array();
                        $datos_plantilla['idi_id'] = 1;
                        $datos_plantilla['codigo_plantilla'] = 'contenido_mensaje_parametros_invalidos';
                        $_obj_interfaz->crearInterfazGraficaPlantilla($datos_plantilla);
                        exit;                       
                    }//Fin de if( !is_numeric($valor) ){
                    break;
                default:
                    break;
            }//Fin de switch($tipo){        
        }//Fin de foreach($datos['variables_verificar'] AS $variable){
     }//Fin de verificarValoresParametros


    /**
     * Realiza la validacion de un tipo de variable
     * y ajusta el valor para que corresponda al tipo
     * DM - 2019-10-31 
     ***/
    static function validarTipoVariable($datos)
    {
        $cadena = $datos['valor'];
        $validar_limite = is_numeric($datos['limite_letras']) && $datos['limite_letras'] > 0;
        $limite_letras = $datos['limite_letras'];  
        switch($datos['tipo']){
            case 'entero':
                $valor = '';
                $cadena = str_replace(" ","",$cadena);
                if( is_numeric($cadena) )
                {
                    $valor = $cadena; 
                }
                else
                {
                    for( $i=0; $i<strlen($cadena); $i++)
                    {
                        $val = $cadena[$i];
                        if( is_numeric($val) )
                        {                 
                            $valor .=  $val;
                        }
                        else
                        {
                            break;
                        }
                    }                
                }
                break;
            case 'alfanumerico':
                $caracteres_revision = 'a,b,c,d,e,f,g,h,i,j,k,l,ll,m,n,l,o,p,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,9,0';
                $lista_revision = preg_split('/,/',$caracteres_revision);
                $valor = '';
                $cadena = str_replace(" ","",$cadena);
                $cadena = strtolower($cadena);
                for( $i=0; $i<strlen($cadena); $i++){
                    $val = $cadena[$i];
                    if(  in_array($val, $lista_revision) ){ 
                        if( $validar_limite ){
                            $cantidad_letras = strlen($valor) + 1;
                            if( $cantidad_letras > $limite_letras){
                                break;
                            }
                            $valor .=  $cadena[$i];                        
                        }//Fin de if( $validar_limite ){
                        else{
                            $valor .=  $cadena[$i];
                        }//Fin de else de if( $validar_limite ){                     
                    }//Fin de if(  in_array($val, $lista_revision) ){
                    else{
                        break;
                    }
                }//Fin de for( $i=0; $i<strlen($cadena); $i++){                
                break;
            case 'cadena':
                $caracteres_revision = 'a,b,c,d,e,f,g,h,i,j,k,l,ll,m,n,l,o,p,q,r,s,t,u,v,w,x,y,z,_,-';
                $lista_revision = preg_split('/,/',$caracteres_revision);
                $valor = '';
                $cadena = str_replace(" ","",$cadena);
                $cadena_comparacion = strtolower($cadena);
                for( $i=0; $i<strlen($cadena); $i++){
                    $val = $cadena_comparacion[$i];
                    if(  in_array($val, $lista_revision) ){
                        if( $validar_limite ){
                            $cantidad_letras = strlen($valor) + 1;
                            if( $cantidad_letras > $limite_letras){
                                break;
                            }
                            $valor .=  $cadena[$i];
                        }//Fin de if( $validar_limite ){
                        else{
                            $valor .=  $cadena[$i];
                        }//Fin de else de if( $validar_limite ){                 
                    }//Fin de if(  in_array($val, $lista_revision) ){
                    else{
                        break;
                    }
                }//Fin de for( $i=0; $i<strlen($cadena); $i++){                
                break;
        }//Fin de switch($datos['tipo']){
        return $valor;
    }//Fin de validarTipoVariable


    /**
     * Funcion que se encarga de eliminar los caracteres especiales de una cadena
     * MD - 18-01-2012
     * * */
    static function eliminarCaracteresEspeciales($cadena) {

        $cadena = html_entity_decode($cadena);

        $b = array("Ã¡", "Ã©", "Ã­", "Ã³", "Ãº", "Ã¤", "Ã«", "Ã¯", "Ã¶", "Ã¼", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "Ã±", " ", ",", ".", ";", ":", "Â¡", "!", "Â¿", "?", '"');
        $c = array("a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "n", "", "", "", "", "", "", "", "", "", '');

        $cadena = utf8_encode($cadena);
        $cadena = str_replace($b, $c, $cadena);

        //DM - 2015-09-17
        $cadena = Herramientas::quitarTildes($cadena);
        $cadena = Herramientas::eliminarTildes($cadena);

        return $cadena;
    }

//Fin de eliminarCaracteresEspeciales()

    /**
     * Funcion que se encarga de retornar el nombre de la columna en la tabla idiomas
     * a partir del id del idioma o del codigo de idioma
     * DM - 18-01-2012
     * * */
    function nombreColumnaIdiomaPorIdCodigo($datos) {
        global $_obj_database;

        if ($datos['idi_id'] != "") {
            $idioma = $_obj_database->obtenerRegistro("idiomas", "idi_id = '" . $datos['idi_id'] . "' ");
        }

        if ($datos['idi_codigo'] != "") {
            $datos['idi_codigo'] = strtolower($datos['idi_codigo']);
            $idioma = $_obj_database->obtenerRegistro("idiomas", "idi_codigo = '" . $datos['idi_codigo'] . "' ");
        }

        $nombre_columna = "";
        if (is_array($idioma)) {
            $nombre_columna = $idioma['idi_nombre'];
            $nombre_columna = utf8_decode($nombre_columna);
            $nombre_columna = str_replace(' ', '_', $nombre_columna);
            $nombre_columna = Herramientas::eliminarCaracteresEspeciales($nombre_columna);
        }//Fin de if( is_array($idioma) )

        return $nombre_columna;
    }

//Fin de nombreColumnaIdiomaPorIdCodigo()

    /**
     * Funcion que se encarga de obtener el nombre de campo de idioma de la base de datos
     * de acuerdo al idioma enviado en los parametros
     * DM - 2013-03-14
     *
     */
    static function obtenerCampoIdioma($datos) {

        $nombre_campo = $datos['nombre_campo'];

        $idi_id = $_SESSION['idi_id'];
        if ($datos['idi_id'] != "") {
            $idi_id = $datos['idi_id'];
        }//Fin de if( $datos['idi_id'] != "" )

        $descripcion = "";

        switch ($idi_id) {
            case 2:
                $descripcion = "_ingles";
                break;
            case 3:
                $descripcion = "_frances";
                break;
            case 4:
                $descripcion = "_aleman";
                break;
            case 5:
                $descripcion = "_italiano";
                break;
            case 6:
                $descripcion = "_holandes";
                break;
            case 7:
                $descripcion = "_portugues_pt";
                break;
            case 8:
                $descripcion = "_checo";
                break;
            case 9:
                $descripcion = "_turco";
                break;
            case 10:
                $descripcion = "_polaco";
                break;
            case 11:
                $descripcion = "_arabe";
                break;
            case 12:
                $descripcion = "_catalan";
                break;
            case 13:
                $descripcion = "_chino";
                break;
            case 14:
                $descripcion = "_ruso";
                break;
            case 15:
                $descripcion = "_ucraniano";
                break;
            case 26:
                $descripcion = "_azeri";
                break;
            case 187:
                $descripcion = "_portugues_br";
                break;
            case 82:
                $descripcion = "_japones";
                break;
        }//Fin de switch( $_SESSION['idi_id'] )

        return $nombre_campo . $descripcion;
    }

//Fin de obtenerCampoIdioma

    /**
     * Funcion que se encarga de crear un selector de idiomas
     * cada idioma aparece escrito en el idioma correspondiente
     * DM - 2013-04-01
     * * */
    function crearSelectIdiomas($datos) {
        global $_obj_database;

        $idiomas_ids = $datos['idiomas_ids'];

        //para cada idioma debe obtener el nombre de la columna para consultar
        //el texto asociado a cada idioma
        $campos_select = array();
        foreach ($idiomas_ids as $idioma_id) {
            $datos_idioma = array();
            $datos_idioma['idi_id'] = $idioma_id;
            $datos_idioma['nombre_campo'] = $datos['nombre_campo'];
            $campos_select[] = Herramientas::obtenerCampoIdioma($datos_idioma);
        }//Fin de foreach($idiomas_ids as $idioma_id)
        //consulta las columnas de los idiomas solicitados
        $sql = "select idi_id, " . implode(",", $campos_select) . "
                from idiomas
                where idi_id IN (" . implode(",", $idiomas_ids) . ") ";

        $resultado = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "idi_id");

        //Agosto 12 de 2011: El atributo data-native-menu='false' se agrego para cuando el cuestionario es para moviles y lo reconozca jQuery Mobile
        $select = "<select name='" . $datos['nombre_campo'] . "' " . $datos['adicionales'] . " data-native-menu='false' >";

        foreach ($idiomas_ids as $idioma_id) {
            $datos_idioma = array();
            $datos_idioma['idi_id'] = $idioma_id;
            $datos_idioma['nombre_campo'] = $datos['nombre_campo'];
            $nombre = Herramientas::obtenerCampoIdioma($datos_idioma);

            $mostrarFila = $resultado[$idioma_id][$nombre];

            $select .= "<option value='$idioma_id'";
            if ($datos['valor'] == $idioma_id) {
                $select .= " selected ";
            }
            $select .= "> $mostrarFila</option>";
        }//Fin de foreach($idiomas_ids as $idioma_id)

        $select .= "</select>";

        return $select;
    }

    static public function crearSelectIdiomasApp($datos) {
        global $_obj_database;

        $idiomas_ids = $datos['idiomas_ids'];
        //para cada idioma debe obtener el nombre de la columna para consultar
        //el texto asociado a cada idioma
        $campos_select = array();
        foreach ($idiomas_ids as $idioma_id) {
            $datos_idioma = array();
            $datos_idioma['idi_id'] = $idioma_id;
            $datos_idioma['nombre_campo'] = $datos['nombre_campo'];
            $campos_select[] = Herramientas::obtenerCampoIdioma($datos_idioma);
        }//Fin de foreach($idiomas_ids as $idioma_id)
        //consulta las columnas de los idiomas solicitados
        $sql = "SELECT idi_id, " . implode(",", $campos_select) . "
                FROM idiomas
                WHERE idi_id IN (" . implode(",", $idiomas_ids) . ") ";

        $resultado = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "idi_id");
        $lista = "";
        foreach ($idiomas_ids as $idioma_id) {
            $datos_idioma = array();
            $datos_idioma['idi_id'] = $idioma_id;
            $datos_idioma['nombre_campo'] = $datos['nombre_campo'];
            $nombre = Herramientas::obtenerCampoIdioma($datos_idioma);

            $mostrarFila = $resultado[$idioma_id][$nombre];
            if ($idioma_id != $_SESSION['idi_id']) {
                $lista .= '<li><a onclick="cambiarIdiomaAplicacion(' . $idioma_id . ', ' . $_SESSION['idi_id'] . '  )" href="#">' . $mostrarFila . '</a></li>';
            } else {
                $_SESSION['idi_nombre_tmp'] = $mostrarFila;
            }
        }//Fin de foreach($idiomas_ids as $idioma_id
        return $lista;
    }

//Fin de crearSelectIdiomas

    static function campoIdiomaPregunta($datos = array())
    {
        $pre_descripcion = "pre_descripcion";
        $pre_descripcion_alias = "pre_descripcion";

        $idi_id = $_SESSION['idi_id'];
        if ($datos['idi_id'] != "")
        {
            $idi_id = $datos['idi_id'];
        }//Fin de if( $datos['idi_id'] != "" )
        
        //DM - 2016-03-14
        //Si se va a cambiar el nombre del campo que se retorna
        if ($datos['nombre_campo'] != '')
        {
            $pre_descripcion_alias = $datos['nombre_campo'];
        }//Fin de if( $datos['nombre_campo'] != '' )

        switch ($idi_id) {
            case 2:
                $pre_descripcion = "pre_descripcion_ingles";
                break;
            case 3:
                $pre_descripcion = "pre_descripcion_frances";
                break;
            case 4:
                $pre_descripcion = "pre_descripcion_aleman";
                break;
            case 7:
                $pre_descripcion = "pre_descripcion_portugues_pt";
                break;
            case 9:
                $pre_descripcion = "pre_descripcion_turco";
                break;
            case 10:
                $pre_descripcion = "pre_descripcion_polaco";
                break;
            case 13:
                $pre_descripcion = "pre_descripcion_chino";
                break;
            case 14:
                $pre_descripcion = "pre_descripcion_ruso";
                break;
            case 15:
                $pre_descripcion = "pre_descripcion_ucraniano";
                break;
            case 26:
                $pre_descripcion = "pre_descripcion_azeri";
                break;
            case 187:
                $pre_descripcion = "pre_descripcion_portugues_br";
                break;
        }//Fin de switch( $_SESSION['idi_id'] )
        
        //DM - 2019-07-22 redmine[13580]
        if( $datos['retornar_nombre_base'] == 1)
        {
            return $pre_descripcion;
        }//Fin de if( $datos['retornar_nombre_base'] == 1)
        
        //Agrega el alias
        $pre_descripcion .= " AS " . $pre_descripcion_alias;

        return $pre_descripcion;
    }

    static function enlaceCampos($datos, $campos) {
        $enlace = "";
        foreach ($campos as $key) {
            if (is_array($datos[$key])) {
                foreach ($datos[$key] as $valor) {
                    $enlace .= "&" . $key . "[]=" . $valor;
                }
            } else if ($datos[$key] != '') {
                $enlace .= "&" . $key . "=" . $datos[$key];
            }
        }//Fin de foreach($campos as $key)
        return $enlace;
    }

//Fin de enlaceCampos

    /**
     * Funcion que se encarga de crear los campos ocultos
     * de los campos que se pasen por parametro
     * DM - 2013-09-16
     * */
    function generarHTMLCamposOcultos($datos, $campos) {
        $campos_html = '';
        foreach ($campos as $key) {
            if (is_array($datos[$key])) {
                foreach ($datos[$key] as $valor) {
                    $campos_html .= '<input type="hidden" name="' . $key . '[]" value="' . $valor . '" >';
                }
            } else if ($datos[$key] != '') {
                $campos_html .= '<input type="hidden" name="' . $key . '" value="' . $datos[$key] . '" >';
            }
        }//Fin de foreach($campos as $key)

        return $campos_html;
    }

//Fin de generarHTMLCamposOcultos

    static function crearSelectFormaPersonalizada($datos, $inicial, $desactivada = '', $valor_inicial = '') {
        global $_obj_database, $idi_despliegue;

        $inicial = $inicial == '' ? $idi_despliegue['seleccionar'] : $inicial;

        if ($inicial == '') {
            $inicial = 'Seleccionar';
        }

        $codigo = $datos['atributos']['codigo']; // El identificador
        $mostrar = $datos['atributos']['mostrar']; // El nombre a mostrar en el select

        /* if(is_array($datos['opciones'])){

          }
          if(!empty($datos['sql'])){
          $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);
          } */

        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $select = "<select name='" . $datos['nombre_campo'] . "' " . $datos['adicionales'] . " " . $desactivada . ">";

        if (!$datos['ultima_posicion_opcion_defecto']) {
            //Valor por defecto
            $select .= $datos['ocultar_opcion_defecto'] ? "" : "<option value='$valor_inicial' align=center> " . $inicial . " </option>";
        }
        //Calcula el tamaÃ’o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);

        if (is_array($resultado) && $numero > 0) {
            foreach ($resultado as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "") {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        if ($datos['ultima_posicion_opcion_defecto']) {
            //Valor por defecto
            $select .= $datos['ocultar_opcion_defecto'] ? "" : "<option " . ($datos['valor'] == $valor_inicial ? 'selected' : '') . " value='$valor_inicial' align=center> " . $inicial . " </option>";
        }

        $select .= "</select>";

        return $select;
    }

// Fin crearSelectFormaPersonalizada ()

    /*
     * pgs - 10/08/2012
     * Funcion que crea un select desde un array
     */

    static function crearSelectFormaDesdeArray($datos, $inicial = '') {
        global $_obj_database, $idi_despliegue;

        $resultado = $datos['array'];

        $inicial = $inicial == '' ? $idi_despliegue['seleccionar'] : $inicial;

        $select = "<select name=\"" . $datos['nombre_campo'] . "\" " . $datos['adicionales'] . ">";

        if ($datos['pordefecto']) {
            //Valor por defecto
            $select .= "<option value='" . $datos['valor_defecto'] . "' align=center>" . $inicial . "</option>";
        }

        //Calcula el tamaño de resultado para llenar el select en el for.
        $numero = sizeof($resultado);

        if (is_array($resultado) && $numero > 0) {

            foreach ($resultado as $key => $res) {
                $codigoFila = "$key";
                $mostrar = ( isset($datos['atributos']['mostrar']) ) ? $res[$datos['atributos']['mostrar']] : $res;

                $mostrarFila = substr($mostrar, 0, 45);

                //DM  - 2012-10-12
                //$mostrarFila = htmlentities($mostrarFila,ENT_QUOTES);
                if ($datos['mayusculas']) {
                    $mostrarFila = strtoupper($mostrarFila);
                } else {
                    if ($datos['minusculas']) {
                        $mostrarFila = strtolower($mostrarFila);
                    } else if ($datos['primera_mayus']) {
                        //MAOH - 09 Feb 2012 - Se agrega la validacion para que solo coloque la primera letra mayuscula
                        $mostrarFila = strtolower($mostrarFila);
                        $mostrarFila = ucfirst($mostrarFila);
                    } else if ($datos['normal']) {
                        //MAOH - 09 Feb 2012 - Si se envia este parametro, que no haga nada
                    } else {
                        //$mostrarFila = strtolower($mostrarFila);
                        $mostrarFila = ucwords($mostrarFila);
                    }
                }

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= ">$mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        $select .= "</select>";

        return $select;
    }

//Fin de crearSelectFormaDesdeArray ($datos)
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& FUNCIONES UTILIZADAS PARA AGREGAR DATOS ADICIONALES A DEMO &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

    function consultarCantidadHuespedesHotelSalidaRango($hotel, $fechaInicio, $fechaFin, $offset, $limite) {
        global $_obj_database;

        $sql = "SELECT *
				FROM
						(	SELECT 	*
							FROM 	huesped, respuesta, pregunta_establecimiento, pregunta
							WHERE	hue_id = res_huesped_id AND
									res_pee_id = pee_id AND
									pee_pregunta_id = pre_id AND
									(pre_tipo = '1' OR pre_tipo='2') AND
									hue_fecha_salida BETWEEN '" . $fechaInicio . "' AND '" . $fechaFin . "' AND
									hue_establecimiento_id = '" . $hotel . "' ORDER BY hue_fecha_salida
						) AS huespedescopiar
				GROUP BY hue_id LIMIT " . $limite . " OFFSET " . $offset;
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $cantidad = $_obj_database->obtenerNumeroFilas();

        $datos['huespedes'] = $resultado;
        $datos['cantidad'] = $cantidad;
        $datos['primera_fecha_salida'] = $resultado[0]['hue_fecha_salida'];

        return $datos;
    }

//Fin consultarCantidadHuespedesHotelSalidaRango ($hotel,$fechaInicio,$fechaFin)

    function consultarCantidadRespuestasHotelSalidaRango($hotel, $fechaInicio, $fechaFin) {
        global $_obj_database;

        $sql = "SELECT 	*
				FROM 	huesped, respuesta, pregunta_establecimiento, pregunta
				WHERE	hue_id = res_huesped_id AND
						res_pee_id = pee_id AND
						pee_pregunta_id = pre_id AND
						(pre_tipo = '1' OR pre_tipo='2') AND
						hue_fecha_salida BETWEEN '" . $fechaInicio . "' AND '" . $fechaFin . "' AND
						hue_establecimiento_id = '" . $hotel . "'";
        $_obj_database->ejecutarSql($sql);
        return $_obj_database->obtenerNumeroFilas();
    }

//Fin consultarCantidadHuespedesHotelSalidaRango ($hotel,$fechaInicio,$fechaFin)

    function consultarMaximos() {
        global $_obj_database;
        $sql = "SELECT 	MAX(hue_id) AS max_hue_id
				FROM 	huesped";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $datos['max_hue_id'] = $resultado[0]['max_hue_id'];

        $sql = "SELECT 	MAX(res_id) AS max_res_id
		FROM 	respuesta";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $datos['max_res_id'] = $resultado[0]['max_res_id'];
        return $datos;
    }

//Fin consultarMaximos ()

    function consultarRespuestasHotelSalidaRangoHuesped($hotel, $fechaInicio, $fechaFin, $hue_id) {
        global $_obj_database;

        $sql = "SELECT 	*
				FROM 	huesped, respuesta, pregunta_establecimiento, pregunta
				WHERE	hue_id = res_huesped_id AND
						res_pee_id = pee_id AND
						pee_pregunta_id = pre_id AND
						(pre_tipo = '1' OR pre_tipo='2') AND
						hue_fecha_salida BETWEEN '" . $fechaInicio . "' AND '" . $fechaFin . "' AND
						hue_establecimiento_id = '" . $hotel . "' AND
						hue_id = '" . $hue_id . "'";

        $respuestas = $_obj_database->obtenerRegistrosAsociativos($sql);
        $cantidad = $_obj_database->obtenerNumeroFilas();

        $datos['respuestas'] = $respuestas;
        $datos['cantidad'] = $cantidad;
        return $datos;
    }

//Fin consultarRespuestasHotelSalidaRangoHuesped ($hotel,$fechaInicio,$fechaFin,$hue_id)

    function reInsertarHuesped($datos, $nuevoHotel, $diasAdicionales) {
        global $_obj_database;

        //A las fechas del huesped le agregamos los dias adicionales
        list($year, $mon, $day) = explode('-', $datos['hue_fecha_llegada']);
        $datos['hue_fecha_llegada'] = date('Y-m-d', mktime(0, 0, 0, $mon, $day + $diasAdicionales, $year));

        list($year, $mon, $day) = explode('-', $datos['hue_fecha_salida']);
        $datos['hue_fecha_salida'] = date('Y-m-d', mktime(0, 0, 0, $mon, $day + $diasAdicionales, $year));

        list($year, $mon, $day) = explode('-', $datos['hue_fecha_respuesta']);
        $datos['hue_fecha_respuesta'] = date('Y-m-d', mktime(0, 0, 0, $mon, $day + $diasAdicionales, $year));

        $sql = "INSERT INTO huesped (hue_num_habitacion,hue_fecha_llegada,hue_fecha_salida,hue_fecha_respuesta,hue_publicidad,hue_lpd,hue_nombre,hue_apellidos,hue_telefono,hue_codigo_postal,
										hue_direccion,hue_email,hue_pais_id,hue_ciudad,hue_establecimiento_id,hue_mvi_id,hue_edad,hue_did,hue_respondio,hue_modo_respuesta,hue_enviar_correo,hue_response_id,hue_accedio)
							VALUES ('" . $datos['hue_num_habitacion'] . "','" . $datos['hue_fecha_llegada'] . "','" . $datos['hue_fecha_salida'] . "','" . $datos['hue_fecha_respuesta'] . "','" . $datos['hue_publicidad'] . "',
									'" . $datos['hue_lpd'] . "','" . $datos['hue_nombre'] . "','" . $datos['hue_apellidos'] . "','" . $datos['hue_telefono'] . "','" . $datos['hue_codigo_postal'] . "','" . $datos['hue_direccion'] . "',
									'" . $datos['hue_email'] . "','" . $datos['hue_pais_id'] . "','" . $datos['hue_ciudad'] . "','" . $nuevoHotel . "','" . $datos['hue_mvi_id'] . "','" . $datos['hue_edad'] . "',
									'" . $datos['hue_did'] . "','" . $datos['hue_respondio'] . "','" . $datos['hue_modo_respuesta'] . "','" . $datos['hue_enviar_correo'] . "','" . $datos['hue_response_id'] . "','" . $datos['hue_accedio'] . "')";

        $_obj_database->iniciarTransaccion();
        $_obj_database->ejecutarSql($sql);
        $newId = $_obj_database->obtenerUltimoIdGenerado();
        if ($_obj_database->obtenerNumeroFilasAfectadas() == 0) {
            $_obj_database->cancelarTransaccion();
        }
        $_obj_database->terminarTransaccion();

        $resultadoInsercion['newId'] = $newId;
        $resultadoInsercion['cadenaInsercion'] = 'Nuevo Huesped = ' . $newId . '<br>' . $sql . '<hr>';

        return $resultadoInsercion;
    }

    function reInsertarRespuesta($datos, $new_hue_id, $hotelBase, $hotelDestino) {
        global $_obj_database;

        //Ahora consultamos el res_pee_id para este hotel de esa misma pregunta
        $sql = "SELECT pee_pregunta_id FROM pregunta_establecimiento WHERE pee_id ='" . $datos['res_pee_id'] . "'";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $id_pregunta = $resultado[0]['pee_pregunta_id'];

        $sql = "SELECT pee_id FROM pregunta_establecimiento WHERE pee_pregunta_id = '" . $id_pregunta . "' AND pee_establecimiento_id = '" . $hotelDestino . "'";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $res_pee_id_correcta = $resultado[0]['pee_id'];

        $datos['res_texto'] = str_replace("'", "\"", $datos['res_texto']);

        $sql = "INSERT INTO respuesta (res_valor,res_texto,res_comentario_leido,res_huesped_id,res_pee_id)
							VALUES ('" . $datos['res_valor'] . "','" . $datos['res_texto'] . "','" . $datos['res_comentario_leido'] . "','" . $new_hue_id . "','" . $res_pee_id_correcta . "')";

        $_obj_database->iniciarTransaccion();
        $_obj_database->ejecutarSql($sql);
        if ($_obj_database->obtenerNumeroFilasAfectadas() == 0) {
            $_obj_database->cancelarTransaccion();
        }
        $_obj_database->terminarTransaccion();
        return 'Respuesta = ' . $new_hue_id . '<br>' . $sql . '<hr>';
    }

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& FIN FUNCIONES UTILIZADAS PARA AGREGAR DATOS ADICIONALES A DEMO &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    //FUNCION QUE CREA PARA LA CONFIGURACION DEL CUESTIONARIO EL SELECT DE LA LISTA DE LOS HOTELES Y ADICIONAL LA OPCION DE LAS PREGUNTAS ESTANDAR DE CADENA
    //Como parametro adicional se le debe pasar el nombre de la cadena y el ID de la cadena
    function crearSelectHotelesPreguntasCadena($datos, $cadena, $cad_id) {
        global $_obj_database, $idi_despliegue;


        $codigo = $datos['atributos']['codigo']; // El identificador
        $mostrar = $datos['atributos']['mostrar']; // El nombre a mostrar en el select

        $select = "<select name='" . $datos['nombre_campo'] . "' " . $datos['adicionales'] . ">";

        //Valor por defecto
        $select .= "<option value='' align=center>- {$idi_despliegue['seleccionar']} -</option>";

        //Esto es para que se mantenga el valor del select cuando se selecciona las preguntas estandar de la cadena
        $seleccionar = '';
        if ($datos['est_id'] == 'cad_id_' . $cad_id) {
            $seleccionar = ' selected ';
        }

        //SE AGREGA EN LOS ELEMENTOS DEL SELECT LA NUEVA OPCION DE LAS PREGUNTAS ESTANDAR DE CADENA CON EL NOMBRE DE LA CADENA
        $select .= "<option " . $seleccionar . "value='cad_id_" . $cad_id . "'>" . $idi_despliegue['preguntas_estandar_cadena'] . $cadena . "</option>";

        //CONSULTA PARA ENCONTRAR LOS HOTELES DE LA CADENA
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);
        //Calcula el tamaÃ±o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);
        if (is_array($resultado) && $numero > 0) {
            foreach ($resultado as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8
                //Si el idioma seleccionado es Polaco(10), Chino(13), Ruso(14) o Ucraniano(15) para que la consulta reconozca caracteres extraÃ±os
                /* if (($_SESSION['idi_id'] == 10) || ($_SESSION['idi_id'] == 13) || ($_SESSION['idi_id'] == 14) || ($_SESSION['idi_id'] == 15) || ($_SESSION['idi_id'] == 9) || ($_SESSION['idi_id'] == 26))
                  {
                  $mostrarFila = $res[$mostrar];
                  }
                  else
                  {
                  $mostrarFila = $res[$mostrar];
                  $mostrarFila = htmlentities($mostrarFila,ENT_QUOTES);
                  $mostrarFila = substr($mostrarFila, 0, 26);
                  $mostrarFila = strtolower($mostrarFila);
                  $mostrarFila = ucwords($mostrarFila);
                  } */

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        $select .= "</select>";

        return $select;
    }

// Fin crearSelectHotelesPreguntasCadena()
    //FUNCION QUE CREA PARA LA GESTION DE USUARIOS PARA EL USUARIO DE CADENA: EL SELECT DE LA LISTA DE LOS HOTELES Y ADICIONAL LA OPCION DE LOS USUARIOS DE CADENA
    //Como parametro adicional se le debe pasar el nombre de la cadena y el ID de la cadena
    function crearSelectHotelesGestionUsuarios($datos, $cadena, $cad_id) {
        global $_obj_database, $idi_despliegue;

        $codigo = $datos['atributos']['codigo']; // El identificador
        $mostrar = $datos['atributos']['mostrar']; // El nombre a mostrar en el select

        $select = "<select name='" . $datos['nombre_campo'] . "' " . $datos['adicionales'] . ">";

        //Valor por defecto
        $select .= "<option value='' align=center>- {$idi_despliegue['seleccionar']} -</option>";

        //Esto es para que se mantenga el valor del select cuando se selecciona las preguntas estandar de la cadena
        $seleccionar = '';
        if ($datos['est_id'] == 'cad_id_' . $cad_id) {
            $seleccionar = ' selected ';
        }

        //SE AGREGA EN LOS ELEMENTOS DEL SELECT LA OPCION DE LOS USUARIOS DE CADENA CON EL NOMBRE DE LA CADENA
        $select .= "<option " . $seleccionar . "value='cad_id_" . $cad_id . "'>" . $idi_despliegue['usuarios_cadena'] . $cadena . "</option>";

        //CONSULTA PARA ENCONTRAR LOS HOTELES DE LA CADENA
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);
        //Calcula el tamaÃ±o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);
        if (is_array($resultado) && $numero > 0) {
            $i = -1;
            foreach ($resultado as $res) {
                $i++;
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8
                //Si el idioma seleccionado es Polaco(10), Chino(13), Ruso(14) o Ucraniano(15) para que la consulta reconozca caracteres extraÃ±os
                /* if (($_SESSION['idi_id'] == 10) || ($_SESSION['idi_id'] == 13) || ($_SESSION['idi_id'] == 14) || ($_SESSION['idi_id'] == 15) || ($_SESSION['idi_id'] == 9) || ($_SESSION['idi_id'] == 26))
                  {
                  $mostrarFila = $res[$mostrar];
                  }
                  else
                  {
                  $mostrarFila = $res[$mostrar];
                  $mostrarFila = htmlentities($mostrarFila,ENT_QUOTES);
                  $mostrarFila = substr($mostrarFila, 0, 26);
                  $mostrarFila = strtolower($mostrarFila);
                  $mostrarFila = ucwords($mostrarFila);
                  } */

                $select .= "<option value='$codigoFila'";

                if ($datos['valor'] == '' && $i == 0) { // No se ha seleccionado ningun hotel
                    $select .= " selected ";
                }

                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        $select .= "</select>";

        return $select;
    }

// Fin crearSelectHotelesGestionUsuarios()
    //MAOH - 12 Abr 2012 - Funcion que devuelve el microtime actual
    function obtenerMicrotime() {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];

        return $mtime;
    }

//Fin de obtenerMicrotime()
    //MAOH - 12 Abr 2012 - Fin
    //MAOH - 27 Abr 2012 - Funcion que devuelve los dias del mes y aÃ±o indicados, por defecto toma el mes y/o aÃ±o actual
    static function obtenerDiasMes($mes = 0, $anho = 0) {
        //Si el mes es menos a 1 o mayor a 12, se toma el mes actual
        if ($mes < 1 || $mes > 12 || !is_numeric($mes))
            $mes = date("m");

        if ($anho < 1 || !is_numeric($anho))
            $anho = date("Y");

        //Verifico si el aÃ±o es bisiesto
        if (((fmod($anho, 4) == 0) && (fmod($anho, 100) != 0)) || (fmod($anho, 400) == 0))
            $dias_febrero = 29;
        else
            $dias_febrero = 28;

        //Cantidad de dias por mes
        $dias_mes = array(0, 31, $dias_febrero, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

        return $dias_mes[(int) $mes];
    }

//Fin de obtenerDiasMes($mes = date("m"), $anho = date("Y"))
    //MAOH - 27 Abr 2012 - Fin
    //MAOH - 02 Mayo 2012 - Evolucion 3.1
    //Funcion que crea el select para los informes de evolucion, agregandole las opciones de ver todos los hoteles o solo la cadena y marcas
    function crearSelectHotelesEvolucion($datos, $cadena, $cad_id) {
        global $_obj_database, $idi_despliegue;

        $codigo = $datos['atributos']['codigo']; // El identificador
        $mostrar = $datos['atributos']['mostrar']; // El nombre a mostrar en el select

        $select = "<select name='" . $datos['nombre_campo'] . "' " . $datos['adicionales'] . ">";

        //Valor por defecto
        $select .= "<option value='' align=center>- {$idi_despliegue['seleccionar']} -</option>";

        //Esto es para que se mantenga el valor del select cuando se seleccionan todos los hoteles o la cadena y las marcas
        $seleccionar_all = '';
        $seleccionar = '';

        if ($datos['valor'] == 'all_id_' . $cad_id) {
            $seleccionar_all = ' selected ';
        } else if ($datos['valor'] == 'cad_id_' . $cad_id) {
            $seleccionar = ' selected ';
        }

        $select .= "<option " . $seleccionar_all . "value='all_id_" . $cad_id . "'>" . $idi_despliegue['evolucion_todos_hoteles'] . " - " . $cadena . "</option>";
        $select .= "<option " . $seleccionar . "value='cad_id_" . $cad_id . "'>" . $idi_despliegue['evolucion_toda_cadena'] . " - " . $cadena . "</option>";

        //Consulta para encontrar los hoteles de la cadena
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        //Calcula el tamaÃ±o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);
        if (is_array($resultado) && $numero > 0) {
            foreach ($resultado as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar];

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin de if( is_array($resultado) && $numero>0)

        $select .= "</select>";

        return $select;
    }

// Fin crearSelectHotelesEvolucion($datos, $cadena, $cad_id)
    //MAOH - 02 Mayo 2012 - Fin
    //MAOH - 07 Mayo 2012 - Funcion que codifica todo tipo de caracteres de utf8 a entidades html
    static function UTF8toEntities($string) {
        /* 	Nota: Aplicar htmlspecialchars si se desea, antes de la aplicaciÃ³n de esta funciÃ³n
          SÃ³lo se usa la conversion lenta si hay caracteres de 8 bits
          Evitar el uso de 0xA0 (\240), en rangos de ereg */
        //if(!ereg("[\200-\237]", $string) && !ereg("[\241-\377]", $string))
        if (!preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string))
            return $string;

        //Rechaza las secuencias demasiado cortas
        $string = preg_replace("/[\302-\375]([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\340-\375].([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\360-\375]..([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\370-\375]...([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\374-\375]....([\001-\177])/", "&#65533;\\1", $string);

        //Rechaza los bytes y secuencias ilegales
        //Caracteres de 2 bytes en rango ASCII
        $string = preg_replace("/[\300-\301]./", "&#65533;", $string);
        //Codigo ilegal de 4 bytes (RFC 3629)
        $string = preg_replace("/\364[\220-\277]../", "&#65533;", $string);
        //Codigo ilegal de 4 bytes (RFC 3629)
        $string = preg_replace("/[\365-\367].../", "&#65533;", $string);
        //Codigo ilegal de 5 bytes (RFC 3629)
        $string = preg_replace("/[\370-\373]..../", "&#65533;", $string);
        //Codigo ilegal de6 bytes (RFC 3629)
        $string = preg_replace("/[\374-\375]...../", "&#65533;", $string);
        //Bytes indefinidos
        $string = preg_replace("/[\376-\377]/", "&#65533;", $string);

        //Rechaza bytes de inicio consecutivos
        $string = preg_replace("/[\302-\364]{2,}/", "&#65533;", $string);

        //Decodifica caracteres unicode de 4 bytes
        $string = preg_replace("/([\360-\364])([\200-\277])([\200-\277])([\200-\277])/e", "'&#'.((ord('\\1')&7)<<18 | (ord('\\2')&63)<<12 | (ord('\\3')&63)<<6 | (ord('\\4')&63)).';'", $string);

        //Decodifica caracteres unicode de 3 bytes
        $string = preg_replace("/([\340-\357])([\200-\277])([\200-\277])/e", "'&#'.((ord('\\1')&15)<<12 | (ord('\\2')&63)<<6 | (ord('\\3')&63)).';'", $string);

        //Decodifica caracteres unicode de 2 bytes
        $string = preg_replace("/([\300-\337])([\200-\277])/e", "'&#'.((ord('\\1')&31)<<6 | (ord('\\2')&63)).';'", $string);

        //Rechaza los bytes faltantes
        $string = preg_replace("/[\200-\277]/", "&#65533;", $string);

        return $string;
    }

//Fin de UTF8toEntities($string)
    //MAOH - 07 Mayo 2012 - Fin
    //MAOH - 03 Jul 2012 - Funcion que cambia los puntos por comas para exportar en excel
    static function cambiarFormatoValoresAExcel($valor) {
        $valor_tmp = $valor;

        //Si es un array
        if (is_array($valor)) {
            foreach ($valor as $k => $v) {
                if (is_array($v)) //Si es un array, se llama de forma recursiva
                    $valor_tmp[$k] = Herramientas::cambiarFormatoValoresAExcel($v);
                else
                    $valor_tmp[$k] = str_replace(".", ",", $v);
            }
        }
        else {
            $valor_tmp = str_replace(".", ",", $valor);
        }

        return $valor_tmp;
    }

//Function cambiarFormatoValoresAExcel($valor)
    //MAOH - 03 Jul 2012 - Fin

    /**
     * Funcion que se encarga de eliminar las tildes de una cadena
     * MD - 01-08-2012
     * * */
    static function eliminarTildes($cadena) {

        $cadena = str_replace('á', 'a', $cadena);
        $cadena = str_replace('é', 'e', $cadena);
        $cadena = str_replace('í', 'i', $cadena);
        $cadena = str_replace('ó', 'o', $cadena);
        $cadena = str_replace('ú', 'u', $cadena);
        $cadena = str_replace('ñ', 'n', $cadena);

        return $cadena;
    }

//Fin de eliminarTildes()

    static function quitarTildes($cadena) {

        $no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ", "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹");
        $permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
        $texto = str_replace($no_permitidas, $permitidas, $cadena);

        return $texto;
    }

//function quitarTildes($cadena)

    /**
     * Funcion que permite identificar la codificación de una cadena,
     * los valores posibles a retornar son ISO_8859_1, ASCII,  UTF_8
     * Tomado de : http://www.forosdelweb.com/f18/como-detectar-codificacion-string-448344/
     * DM - 2012-10-10
     * */
    static function detectarCodificacion($texto) {
        $c = 0;
        $ascii = true;
        for ($i = 0; $i < strlen($texto); $i++) {
            $byte = ord($texto[$i]);
            if ($c > 0) {
                if (($byte >> 6) != 0x2) {
                    return "ISO_8859_1";
                } else {
                    $c--;
                }
            } elseif ($byte & 0x80) {
                $ascii = false;
                if (($byte >> 5) == 0x6) {
                    $c = 1;
                } elseif (($byte >> 4) == 0xE) {
                    $c = 2;
                } elseif (($byte >> 3) == 0x14) {
                    $c = 3;
                } else {
                    return "ISO_8859_1";
                }
            }//Fin de elseif ($byte&0x80)
        }//Fin de for ($i = 0;$i<strlen($texto);$i++)

        return ($ascii) ? "ASCII" : "UTF_8";
    }

//Fin de detectarCodificacion

    /**
     * Funcion que se encarga de verificar si el correo tiene mas correos
     * separadas por (;) o (,), de ser asi, solo deja el primero y
     * no tiene en cuenta los demas
     * DM - 2012-11-01
     * */
    static function depurarCorreo($correo) {
        //Cambia los ; por , para procesar solo los registros con ,
        $correo = str_replace(";", ",", $correo);
        $correo = str_replace("\\'", "", $correo);
        $correo = str_replace("'", "", $correo);
        $correo = str_replace("\\\"", "", $correo);
        $correo = str_replace("\"", "", $correo);
        $correo = str_replace("\n", "", $correo);
        $correo = str_replace("\r", "", $correo);

        //obtiene todos los correos que esten en la cadena separados por (,)
        $lista_correos = preg_split("/,/", $correo);

        foreach ($lista_correos as $valor_correo) {
            //retira los espacios en blanco y verifica que tenga algun valor
            $valor_correo = trim($valor_correo);
            if ($valor_correo != "") {
                return $valor_correo;
            }//Fin de if( $valor_correo != "")
        }//Fin de foreach($lista_correos as $valor_correo)
        //solo retorna el primer correo
        return "";
    }

//Fin de depurarCorreo()

    /**
     * Funcion que se encarga de identifica el navegador del usuario
     * DM - 2013-03-07
     * * */
    function obtenerNavegador($user_agent) {
        $navegadores = array(
            'Opera' => 'Opera',
            'Mozilla Firefox' => '(Firebird)|(Firefox)',
            'Galeon' => 'Galeon',
            'Mozilla' => 'Gecko',
            'MyIE' => 'MyIE',
            'Lynx' => 'Lynx',
            'Netscape' => '(Mozilla\/4\.75)|(Netscape6)|(Mozilla\/4\.08)|(Mozilla\/4\.5)|(Mozilla\/4\.6)|(Mozilla\/4\.79)',
            'Konqueror' => 'Konqueror',
            'MSIE 9' => '(MSIE 9\.[0-9]+)',
            'MSIE 8' => '(MSIE 8\.[0-9]+)',
            'MSIE 7' => '(MSIE 7\.[0-9]+)',
            'MSIE 6' => '(MSIE 6\.[0-9]+)',
            'MSIE 5' => '(MSIE 5\.[0-9]+)',
            'MSIE 4' => '(MSIE 4\.[0-9]+)',
        );

        foreach ($navegadores as $navegador => $pattern) {
            $coindicencias = array();
            if (preg_match('/' . $pattern . '/', $user_agent, $coindicencias))
                return $navegador;
        }//Fin de foreach($navegadores as $navegador=> $pattern)

        return 'Desconocido';
    }

//Fin de obtenerNavegador

    /**
     * Funcion que se encarga de organizar los filtros de id en
     * rangos, de tal manera que se pueda minimizar la cantidad de ids
     * que se pasan en un filtro
     * DM - 2013-03-01
     * * */
    static function obtenerFiltroPorRangos($datos) {
        //Si no hay un grupo de ids entonces no aplica el filtro
        if (!( is_array($datos['lista_ids']) && sizeof($datos['lista_ids']) > 0 )) {
            return "";
        }//Fin de if( !( is_array($datos['lista_ids']) && sizeof($datos['lista_ids']) > 0 ) )
        //Obtiene los id de pregunta_establecimiento y los ordena
        //para generar los filtros en rangos y no tener que
        //enviar todos los ids
        $lista_ids = $datos['lista_ids'];
        sort($lista_ids);
        $tamano = sizeof($lista_ids);

        //lista con los rangos obtenidos
        $rangos = array();
        //lista de valores que no tienen rangos
        $sin_rangos = array();
        //posicion para llevar la cantidad de rangos encontrados
        $pos_rango = 0;
        //indica si se encontro un rango y se tiene en cuenta en el proximo ciclo
        //para saber si se puede formar un unico rango o se debe crear otro
        $esta_en_rango = false;

        //Recorre los elementos de la lista por cada 3 posiciones para
        //saber si encuentra un rango de al menos 3 elementos
        for ($i = 0; $i < $tamano; $i+=2) {
            $posicion_1 = $i + 1;

            $valor = $lista_ids[$i];
            $valor_1 = $valor + 1;

            if (isset($lista_ids[$posicion_1])) {
                if ($esta_en_rango) {

                    //verifica que el ultimo valor del rango
                    //sea anterior al valor del rango que llega
                    $estan_en_mismo_rango = $rangos[$pos_rango - 1]['fin'] + 1 == $valor;

                    if ($estan_en_mismo_rango && $valor_1 == $lista_ids[$posicion_1]) {
                        //si esta en un rango  entonces utiliza
                        //el rango anterior y re asigna el valor final del rango
                        $rangos[$pos_rango - 1]['fin'] = $valor_1;
                        $rangos[$pos_rango - 1]['cantidad'] ++;
                        continue;
                    } else if ($valor_1 == $lista_ids[$posicion_1]) {
                        $rangos[$pos_rango] = array();
                        $rangos[$pos_rango]['ini'] = $valor;
                        $rangos[$pos_rango]['fin'] = $valor_1;
                        $rangos[$pos_rango]['cantidad'] = 2;
                        $pos_rango++;
                        continue;
                    } else if ($valor == $rangos[$pos_rango - 1]['fin'] + 1) {
                        //Si el valor forma parte del rango anterior
                        $rangos[$pos_rango - 1]['fin'] = $valor;
                        $rangos[$pos_rango - 1]['cantidad'] ++;
                        //Reduce el contador para que inicie del siguiente valor
                        //para revisar si forma parte de algun rango
                        $i--;
                        $esta_en_rango = false;
                        continue;
                    }//Fin de else if( $valor == $rangos[$pos_rango-1]['fin'] + 1)
                    else {
                        $esta_en_rango = false;
                        $sin_rangos[] = $valor;
                        //Reduce el contador para que inicie del siguiente valor
                        //para revisar si forma parte de algun rango
                        $i--;
                        continue;
                    }
                }//Fin de if( $esta_en_rango )
                else {
                    if ($valor_1 == $lista_ids[$posicion_1]) {
                        $rangos[$pos_rango] = array();
                        $rangos[$pos_rango]['ini'] = $valor;
                        $rangos[$pos_rango]['fin'] = $valor_1;
                        $rangos[$pos_rango]['cantidad'] = 2;
                        $esta_en_rango = true;
                        $pos_rango++;
                        continue;
                    } else {
                        $esta_en_rango = false;
                        $sin_rangos[] = $valor;
                        //Reduce el contador para que inicie del siguiente valor
                        //para revisar si forma parte de algun rango
                        $i--;
                        continue;
                    }
                }//Fin de else de if( $esta_en_rango )
            }//Fin de else if( isset($pee_ids[ $posicion_1 ]) )
            else {
                $esta_en_rango = false;
                $sin_rangos[] = $valor;
            }
        }//Fin de for( $i=0; $i < $tamano; $i+=2)


        $condiciones = array();
        $nombre_campo = $datos['nombre_campo'];

        if (sizeof($rangos) > 0) {
            foreach ($rangos as $valor_rango) {
                if ($valor_rango['cantidad'] > 2) {
                    $condiciones[] = $nombre_campo . " BETWEEN " . $valor_rango['ini'] . " AND " . $valor_rango['fin'] . " ";
                }//Fin de if( $valor_rango['cantidad'] > 2)
                else {
                    $sin_rangos[] = $valor_rango['ini'];
                    $sin_rangos[] = $valor_rango['fin'];
                }
            }//Fin de foreach($rangos as $valor_rango)
        }//Fin de if( sizeof($rangos) > 0 )

        if (sizeof($sin_rangos) > 0) {
            $condiciones[] = $nombre_campo . " IN (" . implode(",", $sin_rangos) . ") ";
        }//Fin de if( sizeof($sin_rangos) > 0 )

        return "(" . implode(" OR ", $condiciones) . ")";
    }

//Fin de obtenerFiltroPorRangos

    /**
     * Función que se encarga de obtener los colores de los
     * graficos de la aplicación
     * DM - 2013-09-20
     * */
    static function obtenerColorGraficos($posicion, $tipo_grafico = '') {
        global $_COLORES_INFORMES, $_colores;

        switch ($tipo_grafico) {
            case 'bench':
                //si es un hotel sin cadena, entonces asigna el color gris
                $es_hotel_sin_cadena = $_SESSION['titpo_usuario'] == 'H';
                $es_hotel_sin_cadena &= $_SESSION['est_cad_id'] == '' || $_SESSION['est_cad_id'] == 0;
                if ($es_hotel_sin_cadena) {
                    return $_colores['bench'];
                }
                break;
            case 'cadena':
                //si es un hotel de cadena entonces retorna el valor del color gris
                if ($_SESSION['tipo_usuario'] == 'H') {
                    return $_colores['cadena'];
                } else if ($_SESSION['tipo_usuario'] == 'C') {
                    return $_colores['hotel'];
                }
                break;
            case 'cadena_2':
                //si es un hotel de cadena entonces retorna el valor del color gris
                if ($_SESSION['tipo_usuario'] == 'H') {
                    return $_colores['cadena_2'];
                } else if ($_SESSION['tipo_usuario'] == 'C') {
                    return $_colores['hotel_2'];
                }
                break;
            case 'media_hoteles':
                if ($_SESSION['tipo_usuario'] == 'C') {
                    return $_colores['cadena'];
                }
                break;
            case 'media_hoteles_2':
                if ($_SESSION['tipo_usuario'] == 'C') {
                    return $_colores['cadena_2'];
                }
                break;
            default:
                break;
        }//Fin de switch($tipo_grafico)
        //si no se asigno ningun color en el switch entonces lo asigna de la lista de colores
        $size = sizeof($_COLORES_INFORMES);
        $posicion = $posicion % $size;

        return $_COLORES_INFORMES[$posicion];
    }

//Fin de obtenerColorGraficos

    /**
     * Función que se encarga de aplicar trim a todos los campos que
     * llegan en el arrglo siempre y cuando no sea un arreglo el valor,
     * si es un arreglo hace un llamado recursivo
     * DM - 2013-09-23
     * * */
    static function trimCamposFormulario($datos) {
        foreach ($datos as $key => $valor) {
            //si el campo no es un arreglo le aplica el trim
            if (!is_array($valor)) {
                $datos[$key] = trim($valor);
            } else {
                $valor = Herramientas::trimCamposFormulario($valor);
                $datos[$key] = $valor;
            }
        }

        return $datos;
    }//Fin de trimCamposFormulario

    /**
     * Función que se encarga de crear un campo radio
     * con las opciones de SI y NO, de acuerdo al
     * idioma de la interfaz
     * DM - 2013-11-15
     * */
    static function crearCampoSiNo($datos) {
        global $idi_despliegue;

        $checked = $datos['valor'] == '0' ? 'checked="checked"' : '';
        $contenido = $idi_despliegue['no'] . '<input ' . $datos['atributos'] . ' ' . $checked . ' type="radio" name="' . $datos['nombre_campo'] . '" value="0" />';

        $contenido .= '&nbsp;&nbsp;&nbsp;&nbsp;';

        $checked = $datos['valor'] == 1 ? 'checked="checked"' : '';
        $contenido .= $idi_despliegue['si'] . '<input ' . $datos['atributos'] . ' ' . $checked . ' type="radio" name="' . $datos['nombre_campo'] . '" value="1" />';

        return $contenido;
    }

//Fin de crearCampoSiNo

    /**
     * Se encarga de generar un selector con el diccionario de datos
     * de acuerdo al tipo de datos que se requiere obtener y el
     * idioma que se solicita, si no se pasa el idioma se
     * asigna el idioma español por defecto
     * DM - 2014-01-27
     * */
    static function crearSelectorDiccionarioDatos($datos) {
        global $_obj_database, $idi_despliegue, $obj_usuarios;

        //si no existe un idioma por defecto asignado entonces asigna el español
        $datos['idi_id'] = $datos['idi_id'] == '' ? 1 : $datos['idi_id'];

        if ($datos['tipo_diccionario'] == '') {
            return '<select><option>-</option></select>';
        }//Fin de if( $datos['tipo_diccionario'] == '')
        //de acuerdo al tipo de datos, debe crear los campos
        switch ($datos['tipo_diccionario']) {
            case 'pais':
                $datos['sql'] = "SELECT pad_pai_id, pad_nombre
              								FROM pais_idioma
              								WHERE pad_idi_id = '" . $datos['idi_id'] . "'
              								ORDER BY pad_nombre ASC ";
                $datos['atributos']['codigo'] = "pad_pai_id";
                $datos['atributos']['mostrar'] = "pad_nombre";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'ciudad':
                $datos['sql'] = "SELECT cii_ciu_id, cii_nombre
              								FROM ciudad_idioma
              								WHERE cii_idi_id = '" . $datos['idi_id'] . "'
              								ORDER BY cii_nombre ASC ";
                $datos['atributos']['codigo'] = "cii_ciu_id";
                $datos['atributos']['mostrar'] = "cii_nombre";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'region':
                $datos['sql'] = "SELECT rei_reg_id, rei_nombre
              								FROM region_idioma
              								WHERE rei_idi_id = '" . $datos['idi_id'] . "'
              								ORDER BY rei_nombre ASC ";
                $datos['atributos']['codigo'] = "rei_reg_id";
                $datos['atributos']['mostrar'] = "rei_nombre";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'zona':
                $datos['sql'] = "SELECT sui_sub_id, sui_nombre
              								FROM subzona_idioma
              								WHERE sui_idi_id = '" . $datos['idi_id'] . "'
              								ORDER BY sui_nombre ASC ";
                $datos['atributos']['codigo'] = "sui_sub_id";
                $datos['atributos']['mostrar'] = "sui_nombre";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'idioma':
                $datos_campo['nombre_campo'] = 'idi_nombre';
                $nombre_campo = Herramientas::obtenerCampoIdioma($datos_campo);
                $datos['sql'] = "SELECT idi_id, " . $nombre_campo . " AS idi_nombre
              								FROM idiomas
              								WHERE idi_estado=1
          								    AND idi_uso_aplicacion=1
          								    ORDER BY idi_nombre ASC ";
                if (is_array($datos['idiomas_permitidos'])) {
                    $datos['sql'] = "SELECT idi_id, idi_nombre
                  								FROM idiomas
                  								WHERE idi_id IN (" . implode(",", $datos['idiomas_permitidos']) . ")
              								    ORDER BY idi_nombre ASC ";
                }//Fin de if( is_array($datos['idiomas_permitidos']) )
                $datos['atributos']['codigo'] = "idi_id";
                $datos['atributos']['mostrar'] = "idi_nombre";
                return Herramientas::crearSelectForma($datos);
                break;

            case 'establecimientos':
                $datos['sql'] = "SELECT est_id, est_nombre
              								FROM establecimiento
              								WHERE est_estado=1
          								    ORDER BY est_nombre ASC
                                              ";
                $datos['atributos']['codigo'] = "est_id";
                $datos['atributos']['mostrar'] = "est_nombre";
                return Herramientas::crearSelectFormaPersonalizada($datos, $idi_despliegue['formularios_label_tipo_establecimiento']);
                break;

            case 'idioma_establecimiento':
                $datos_campo['nombre_campo'] = 'idi_nombre';
                $nombre = Herramientas::obtenerCampoIdioma($datos_campo);
                $condicion = "";
                if (is_array($datos['idiomas_permitidos_establecimientos'])) {
                    $condicion = "AND esi_idi_id IN (" . implode(',', $datos['idiomas_permitidos_establecimientos']) . ")
                            ";
                }//Fin de if( is_array($datos['idiomas_permitidos_establecimientos']) )
                if ($datos['enc_id'] != '') {
                    $condicion .= " AND esi_enc_id = '" . $datos['enc_id'] . "'";
                }
                $datos['sql'] = "SELECT DISTINCT(esi_idi_id), " . $nombre . " as idi_nombre
                              FROM idiomas, establecimiento_idioma
                  						WHERE esi_est_id = '" . $datos['est_id'] . "'
                              AND esi_idi_id = idi_id
                              " . $condicion . "
                                ORDER BY idi_id ASC";
                $datos['atributos']['codigo'] = "esi_idi_id";
                $datos['atributos']['mostrar'] = "idi_nombre";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'tipo_establecimiento':
                $datos['sql'] = "SELECT tii_tie_id, tii_texto
              								FROM tipo_establecimiento_idioma
              								WHERE tii_idi_id = '" . $datos['idi_id'] . "'
              								ORDER BY tii_texto ASC ";
                $datos['atributos']['codigo'] = "tii_tie_id";
                $datos['atributos']['mostrar'] = "tii_texto";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'tipo_establecimiento_proveedor':
                $datos['sql'] = "SELECT tii_tie_id, tii_texto
              								FROM tipo_establecimiento_idioma
              								WHERE tii_idi_id = '" . $datos['idi_id'] . "'
                                              and tii_tie_id NOT IN (1,4)
              								ORDER BY tii_texto ASC ";
                $datos['atributos']['codigo'] = "tii_tie_id";
                $datos['atributos']['mostrar'] = "tii_texto";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'tipo_oferta':

                $datos['sql'] = "SELECT toi_tio_id, toi_texto
              								FROM tipo_oferta_idioma
              								WHERE toi_idi_id = '" . $datos['idi_id'] . "'
                              " . $datos['condicion'] . "
              								ORDER BY toi_tio_id ASC ";
                $datos['atributos']['codigo'] = "toi_tio_id";
                $datos['atributos']['mostrar'] = "toi_texto";


                return Herramientas::crearSelectForma($datos);
            case 'tipo_categoria':
                $datos['sql'] = "SELECT tai_tic_id, tai_texto
              								FROM tipo_categoria_idioma, tipo_categoria
              								WHERE tai_idi_id = '" . $datos['idi_id'] . "'
                              AND tic_id = tai_tic_id
              								ORDER BY tai_texto ASC ";
                $datos['atributos']['codigo'] = "tai_tic_id";
                $datos['atributos']['mostrar'] = "tai_texto";
                return Herramientas::crearSelectForma($datos);
                break;
            /*case 'encuesta':
                //DM - 2019-06-17 redmine[13410]
                $datos['sql'] = "SELECT DISTINCT uen_enc_id, 
                                        CONCAT('". $datos['prefijo_opc_form'] ."', IF(eei_nombre IS NULL OR eei_nombre = '',eni_nombre,eei_nombre) ) AS eni_nombre
                              FROM usuario_establecimiento, usuario_encuesta, 
                                    encuesta_idioma LEFT JOIN establecimiento_encuesta_idioma ON 
                                                                (
                                                                    eni_enc_id = eei_enc_id
                                                                    AND eei_est_id = ".$datos['est_id']."
                                                                )
                              WHERE use_establecimiento_id =  '" . $datos['est_id'] . "'
                                  AND uen_usu_id = use_usuario_id
                                  AND uen_enc_id = eni_enc_id
                                  AND eni_idi_id = '" . $datos['idi_id'] . "'
                                  ORDER BY eni_nombre ASC
                              ";
                $datos['atributos']['codigo'] = "uen_enc_id";
                $datos['atributos']['mostrar'] = "eni_nombre";
                return Herramientas::crearSelectForma($datos);
                break;*/
            /*case 'encuesta_landing':
                //DM - 2020-04-20 redmine[13193]
                $datos['sql'] = "SELECT *
                                FROM
                                    (    
                                        (
                                            SELECT DISTINCT CONCAT('F_',uen_enc_id) AS uen_enc_id, 
                                                CONCAT('". $datos['prefijo_opc_form'] ."', IF(eei_nombre IS NULL OR eei_nombre = '',eni_nombre,eei_nombre) ) AS eni_nombre,
                                                0 AS orden
                                            FROM usuario_establecimiento, usuario_encuesta, 
                                                encuesta_idioma LEFT JOIN establecimiento_encuesta_idioma ON 
                                                                        (
                                                                            eni_enc_id = eei_enc_id
                                                                            AND eei_est_id = ".$datos['est_id']."
                                                                        )
                                            WHERE use_establecimiento_id =  '" . $datos['est_id'] . "'
                                              AND uen_usu_id = use_usuario_id
                                              AND uen_enc_id = eni_enc_id
                                              AND eni_idi_id = '" . $datos['idi_id'] . "'
                                              ORDER BY eni_nombre ASC
                                        )
                                        UNION
                                        (
                                              SELECT DISTINCT CONCAT('L_',lap_id) AS uen_enc_id, 
                                                              CONCAT('". $datos['prefijo_opc_landing'] ."',lap_nombre) AS eni_nombre,
                                                              1 AS orden
                          					FROM landing_pages, landing_pages_idiomas
                                        		WHERE lap_id = lpi_lap_id
                          					AND lap_est_id =  '" . $datos['est_id'] . "'
                                        		AND lpi_idi_id = '" . $datos['idi_id'] . "'
                                        		ORDER BY eni_nombre ASC                              
                                        )
                                    ) AS t
                                ORDER BY orden ASC, eni_nombre ASC
                              ";
                $datos['atributos']['codigo'] = "uen_enc_id";
                $datos['atributos']['mostrar'] = "eni_nombre";
                return Herramientas::crearSelectForma($datos);
                break;*/
            case 'messenger':
                $datos['atributos']['codigo']= "mss_fb_id";
                $datos['atributos']['mostrar']= "mss_fb";
                return Herramientas::crearSelectForma($datos);
                break;
            case 'tipo_contenido_plantilla':
                if ($datos['est_id'] != '') {

                    //verifica si el establecimiento tiene activa la funcionalida de
                    //despedida por email o email de agradecimiento
                    $activo_email_agradecimiento = $obj_usuarios->verificarFuncionalidadActivaEstablecimiento($datos['est_id'], 59);

                    //verifica si el establecimiento tiene activa la funcionalida de
                    //despedida de encuesta
                    $activo_pagina_despedida = $obj_usuarios->verificarFuncionalidadActivaEstablecimiento($datos['est_id'], 29);

                    //verifica si el establecimiento tiene activa la funcionalida de
                    //descripcion de plantilla de email
                    $activo_email_solicitud = $obj_usuarios->verificarFuncionalidadActivaEstablecimiento($datos['est_id'], 25);

                    //DMG 2015-05-20 verifica si el establecimiento tiene activa la funcionalida de
                    //descripcion de plantilla de email
                    $activo_cicerone_store = $obj_usuarios->verificarFuncionalidadActivaEstablecimiento($datos['est_id'], 67);

                    $filtros_tcp = array(0);

                    if ($activo_email_agradecimiento) {
                        $filtros_tcp[] = 20;
                    }
                    if ($activo_pagina_despedida) {
                        $filtros_tcp[] = 21;
                    }
                    if ($activo_email_solicitud) {
                        $filtros_tcp[] = 22;
                    }
                    if ($activo_cicerone_store) {
                        $filtros_tcp[] = 34;
                        $filtros_tcp[] = 19;
                    }

                    $datos['sql'] = "SELECT tci_tcp_id, tci_texto
                                  FROM tipo_contenido_plantilla_idioma
                                  WHERE tci_idi_id =  '" . $datos['idi_id'] . "'
                                    and tci_tcp_id iN (" . implode(',', $filtros_tcp) . ")
                                  ORDER BY tci_texto ASC ";
                }//Fin de if( $datos['est_id'] != '' )
                else {
                    $datos['sql'] = "SELECT tci_tcp_id, tci_texto
                                  FROM tipo_contenido_plantilla_idioma
                                  WHERE tci_idi_id =  '" . $datos['idi_id'] . "'
                                  ORDER BY tci_texto ASC ";
                }//Fin de else if( $datos['est_id'] != '' )

                $datos['atributos']['codigo'] = "tci_tcp_id";
                $datos['atributos']['mostrar'] = "tci_texto";
                return Herramientas::crearSelectFormaPersonalizada($datos, $idi_despliegue['todas']);
                break;
                
            //JMM-2017-08-08 - Desarrollo #10532
			case 'landing_page':
                $datos['sql'] = "SELECT DISTINCT lap_id, lap_nombre
                					FROM landing_pages, landing_pages_idiomas
                              		WHERE lap_id = lpi_lap_id
                					AND lap_est_id =  '" . $datos['est_id'] . "'
                              		AND lpi_idi_id = '" . $datos['idi_id'] . "'
                              		ORDER BY lap_fecha_registro DESC";
                
                $datos['atributos']['codigo'] = "lap_id";
                $datos['atributos']['mostrar'] = "lap_nombre";
                
                return Herramientas::crearSelectForma($datos);
                break;
                
            //JMM-2017-09-19 - Desarrollo #105322
            case 'idioma_landing_page':
               	$datos_campo['nombre_campo'] = 'idi_nombre';
               	$nombre = Herramientas::obtenerCampoIdioma($datos_campo);
               	
               	$condicion = "";
                if (is_array($datos['idiomas_landing_page'])) {
                	$condicion = "AND esi_idi_id IN (" . implode(',', $datos['idiomas_landing_page']) . ")";
                }//Fin de if (is_array($datos['idiomas_landing_page']))
                
                $datos['sql'] = "SELECT DISTINCT(esi_idi_id), " . $nombre . " as idi_nombre
                              		FROM idiomas, establecimiento_idioma
                  					WHERE esi_est_id = '" . $datos['est_id'] . "'
                              		AND esi_idi_id = idi_id
                              		" . $condicion . "
                                	ORDER BY idi_id ASC";
                
                $datos['atributos']['codigo'] = "esi_idi_id";
                $datos['atributos']['mostrar'] = "idi_nombre";
                
                return Herramientas::crearSelectForma($datos);
                break;
        }//Fin de swtich($datos['tipo_diccionario'])
    }

//Fin de crearSelectorDiccionarioDatos

    /**
     * Se encarga de retornar la IP de conexion del usuario
     * DM - 2014-03-28
     * */
    static function obtenerIPConexion() {
        $ip = "No detectada";

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_VIA'])) {
            $ip = $_SERVER['HTTP_VIA'];
        }

        return $ip;
    }

//Fin de obtenerIPConexion()

    /**
     * Se encarga de retornar el nombre que el asigno el usuario primario
     * del establecimiento a la encuesta
     * DM - 2014-09-24
     * */
    /*static function obtenerNombreEncuestaEstablecimiento($datos) {
        global $_obj_database;

        $sql = "SELECT eei_nombre, eei_idi_id
                FROM establecimiento_encuesta_idioma
                WHERE eei_est_id = " . $datos['est_id'] . "
                    and eei_enc_id = " . $datos['enc_id'] . "
                    ORDER BY eei_idi_id ASC
                ";

        $lista_encuestas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'eei_idi_id');

        $encuesta = $_obj_database->obtenerRegistro("encuesta left join encuesta_idioma ON (eni_enc_id = enc_id
                                                    and eni_idi_id = " . $datos['idi_id'] . "
                                                )", "enc_id = " . $datos['enc_id']
                                                );

        //si no se ha definido un nombre para el establecimiento y la encuesta toma el que tiene por defecto
        $encuesta['eni_nombre'] = $encuesta['eni_nombre'] == "" ? $encuesta['enc_codigo'] : $encuesta['eni_nombre'];

        if (count($lista_encuestas) == 0) {
            return array('default' => $encuesta['eni_nombre'],
                'nombre' => ''
            );
        }//Fin de if( count($lista_encuestas) == 0 )
        //Si existe nombre en el idioma actual
        //entonces lo retorna
        $encuesta_idioma = $lista_encuestas[$datos['idi_id']];
        if (is_array($encuesta_idioma)) {
            return array('default' => $encuesta['eni_nombre'],
                'nombre' => $encuesta_idioma['eei_nombre']
            );
        }//Fin de if( is_array($encuesta_idioma) )
        //Retorna el primer registro que encuentre empezando por español
        $encuesta_idioma = array_shift($lista_encuestas);

        return array('default' => $encuesta['eni_nombre'],
            'nombre' => $encuesta_idioma['eei_nombre']
        );
    }
*/
//Fin de obtenerNombreEncuestaEstablecimiento

    /**
     * Se encarga de ordenar un arreglo asociativo por el campo que se pase
     * como parámetro
     * DM - 2014-09-25
     * * */
    static function ordenarArregloPorCampo($arreglo, $campo) {
        $listado_temporal = array();
        foreach ($arreglo as $key => $val) {
            //obtiene el campo por el cual se va a ordenar
            //en caso de ser texto se asigna como mayusculas
            //porque php no ordena bien si esta en minuscula con la funcion asort
            $listado_temporal[$key] = strtoupper($val[$campo]);
        }//Fin de foreach($arreglo as $key => $val)
        //ordena el listado
        asort($listado_temporal);

        //Asigna el nuevo orden de acuerdo al ordenamiento
        $nuevo_arreglo = array();
        foreach ($listado_temporal as $key => $tmp) {
            $nuevo_arreglo[$key] = $arreglo[$key];
        }//Fin de foreach($listado_temporal as $key => $tmp)

        return $nuevo_arreglo;
    }

//Fin de ordenarArregloPorCampo()

    /**
     * Se encarga de generar el html que contiene la información de las politicas de cookies
     * DM - 2014-10-08
     * */
    static function generarContenidoPoliticasCookies($datos) {
        global $_PATH_IMAGENES, $idi_despliegue, $_obj_database;


        $contenido_plantilla = $_obj_database->obtenerRegistro("contenido_plantillas, tipo_contenido_plantilla", "
                                        cpl_tcp_id = tcp_id
                                        and cpl_idi_id = '" . $_SESSION['idi_id'] . "'
                                        and cpl_estado = 1
                                        and tcp_codigo = 'contenido_politicas_cookies'
                                        ");

        //si no existe en el idioma obtiene la de español
        if (!is_array($contenido_plantilla)) {
            $contenido_plantilla = $_obj_database->obtenerRegistro("contenido_plantillas, tipo_contenido_plantilla", "
                                            cpl_tcp_id = tcp_id
                                            and cpl_idi_id = 1
                                            and cpl_estado = 1
                                            and tcp_codigo = 'contenido_politicas_cookies'
                                            ");
        }//Fiin de if( !is_array($contenido_plantilla) )

        $contenido = '
            <script type="text/javascript">
            function mostrarInfoPoliticas()
            {
                var ancho = 800;

                $(\'#container\').css(\'width\',ancho+\'px\');
                $(\'#light_cookies_info\').css(\'width\',ancho+\'px\');

                var ancho_ventana = $( window ).width();
                var posicion_izq = (ancho_ventana/2) - 400;

                var posicion_scroll = $(window).scrollTop() + 40;
                $(\'#light_cookies_info\').show();
                $(\'#light_cookies_info\').css(\'top\',posicion_scroll+\'px\');
                $(\'#light_cookies_info\').css(\'left\',posicion_izq+\'px\');
                $(\'#fade_cookies_info\').show();

            }//Fin de mostrarInfoPoliticas
            </script>
            <div id="container">
         	<div id="light_cookies_info" class="white_content">
        		<table border="0" width="100%">
        			<tr>
        				<td class="descripcion_funcionalidad_ligthbox">' . $idi_despliegue['label_informacion'] . '</td>
        				<td width="60%">&nbsp</td>
        				<td width="20"><img src="' . $_PATH_IMAGENES . '/cerrar_ligthbox.png" onclick = "document.getElementById(\'light_cookies_info\').style.display=\'none\';document.getElementById(\'fade_cookies_info\').style.display=\'none\'"></td>
        			</tr>
        			<tr>
        				<td align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="770" height="1"></td>
        				<td colspan="2"></td>
        			</tr>
        			<tr>
        				<td colspan="3">' . $contenido_plantilla['cpl_contenido'] . '</td>
        			</tr>
        		</table>
        	</div>
            </div>
            <div id="fade_cookies_info" class="black_overlay"></div>
            ';


        return $contenido;
    }

//Fin de generarContenidoPoliticasCookies

    /**
     * funcion para reemplazar caracteres html de la forma &atilde; especificamente para la generacion de xml
     * AS 2015-01-16
     * */
    function reemplazarHtmlCaracteres($string) {
        $busqueda = array('&nbsp;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;[*]', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&lt;', '&gt;', '&amp;');
        $reemplazar = array(
            ' ',
            '¡',
            '¢',
            '£',
            '¤',
            '¥',
            '¦',
            '§',
            '¨',
            '©',
            'ª',
            '«',
            '¬',
            '®',
            '¯',
            '°',
            '±',
            '²',
            '³',
            '´',
            'µ',
            '¶',
            '·',
            '¸',
            '¹',
            'º',
            '»',
            '¼',
            '½',
            '¾',
            '¿',
            'À',
            'Á',
            'Â',
            'Ã',
            'Ä',
            'Å',
            'Æ',
            'Ç',
            'È',
            'É',
            'Ê',
            'Ë',
            'Ì',
            'Í',
            'Î',
            'Ï',
            'Ð',
            'Ñ',
            'Ò',
            'Ó',
            'Ô',
            'Õ',
            'Ö',
            '×',
            'Ø',
            'Ù',
            'Ú',
            'Û',
            'Ü',
            'Ý',
            'Þ',
            'ß',
            'à',
            'á',
            'â',
            'ã',
            'ä',
            'å',
            'æ',
            'ç',
            'è',
            'é',
            'ê',
            'ë',
            'ì',
            'í',
            'î',
            'ï',
            'ð',
            'ñ',
            'ò',
            'ó',
            'ô',
            'õ',
            'ö',
            '÷',
            'ø',
            'ù',
            'ú',
            'û',
            'ü',
            'ý',
            'þ',
            'ÿ',
            '<',
            '>',
            '&'
        );
        $variable = str_replace($busqueda, $reemplazar, $string);
        return $variable;
    }

    /**
     * Se encarga de codificar una cadena en base 64 para que sea interpretada por la
     * aplicación
     * DM - 2015-06-12
     * */
    static function codificarBase64($cadena) {
        //agrega un codigo para saber que la cadena esta en base 64
        return base64_encode($cadena) . '_b6A';
    }

//Fin de codificarBase64

    /**
     * Se encarga de revisar que campos estan en base 64 y volverlos a su valor
     * normal
     * DM - 2015-06-11
     * */
    static function decodificarBase64CamposTextoFormulario($datos,$datos_parametros = array()) {

        //si no es un arreglo revisa si es una cadena que debe decodificar
        if (!is_array($datos)) {
            //Si es un número no hace nada
            if (is_numeric($datos)) {
                return $datos;
            }//Fin de if( is_numeric($datos) )
            //Verifica si es una cadena en base 64
            //verifica si tiene _b6A al final
            if (substr($datos, strlen($datos) - 4) !== '_b6A') {
                return $datos;
            }

            $datos = substr($datos, 0, strlen($datos) - 4);
            return base64_decode($datos);
        }//Fin de if( !is_array($datos) )

        foreach ($datos as $clave => $valor) {
            if (is_numeric($valor)) {
                continue;
            }

            if (is_array($valor)) {
                $valor = Herramientas::decodificarBase64CamposTextoFormulario($valor,$datos_parametros);
                $datos[$clave] = $valor;
                continue;
            }
            $valor = trim($valor);
            //Verifica si es una cadena en base 64 verificando
            //si tiene _b6A al final
            if (substr($valor, strlen($valor) - 4) !== "_b6A") {
                continue;
            }

            $valor = substr($valor, 0, strlen($valor) - 4);

            $valor = base64_decode($valor);
            
            if( isset($datos_parametros['omitir_conversion_ascii']) )
            {
                $datos['omitir_conversion_ascii'] = $datos_parametros['omitir_conversion_ascii']; 
            }

            $convertir_ascii = mb_detect_encoding($valor) === false;
            $convertir_ascii |= mb_detect_encoding($valor) == 'UTF-8';
            $convertir_ascii &=!isset($datos['omitir_conversion_ascii']);
            if ($convertir_ascii) {
                $valor = mb_convert_encoding($valor, 'UTF-8', 'ASCII');
            }
            $datos[$clave] = $valor;
        }//Fin de foreach($datos as $clave => $valor)

        return $datos;
    }//Fin de decodificarBase64CamposTextoFormulario
    
    
    /*
     * AS 2015-01-21
     * funcion para verificar si existe un caracter especial al final de la cadena si fuera asi reotrnaria uno con el fin de
     * cuando se recorte una cadena no genere codigo que no se puede interpretar por el navegador
     */

    static function recortarCadena($cadena, $cantidad) {

        if (preg_match("/(á|é|í|ó|ú|ñ+)/", $cadena)) {
            return substr($cadena, 0, ($cantidad - 1)) . " ...";
        }
        return substr($cadena, 0, ($cantidad - 1)) . " ...";
    }

    /**
     * Se encarga de convertir una fecha de acuerdo a la zona
     * horaria que se pasa por parametro
     * DM - 2015-03-26
     * */
    function convertirFechaZonaHoraria($fecha, $horas_zona_horaria, $opciones = array()) {
        //Obtiene la diferencia horaria de la fecha de respuesta a partir de la hora cero
        $diferencia_hora = date('P', strtotime($fecha));
        $diferencia_hora_partes = preg_split('/:/', $diferencia_hora);

        //Cantidad de horas a partir de la hora cero GMT
        $diferencia_hora = (int) $diferencia_hora_partes[0];

        //Obtiene el time de la fecha
        $time_respuesta = strtotime($fecha);

        //Nivela la hora de respuesta a la hora cero de acuerdo a la diferencia horaria con la que se guardo
        if ($diferencia_hora < 0) {
            //Si el valor es negativo debe sumarlo para dejarlo en la hora cero
            $time_respuesta = strtotime('+' . abs($diferencia_hora) . ' hour', $time_respuesta);
        }//Fin de if( $diferencia_hora < 0 )
        else {
            //Si el valor es positivo debe restarlo para dejarlo en la hora cero
            $time_respuesta = strtotime('-' . abs($diferencia_hora) . ' hour', $time_respuesta);
        }

        //Obtiene la zona horaria configurada
        //Carga la cantidad de horas a partir de la hora cero GMT
        $zona_horaria_partes = preg_split('/\./', $horas_zona_horaria);

        //Obtiene las horas y minutos que debe sumar o restar

        $zona_singo = '+';
        $zona_horas = (int) $zona_horaria_partes[0];
        //Si la hora es negativa debe restar horas y minutos
        if ($zona_horas < 0) {
            $zona_singo = '-';
        }
        $zona_horas = abs($zona_horas);
        //Los minutos estan en un rango de 0-10, debe convertirlo a minutos de 0-60
        $zona_minutos = ((int) $zona_horaria_partes[1]) * 6;

        //A partir de esta zona horaria calcula la suma o resta de horas y minutos
        $time_respuesta_zona = strtotime($zona_singo . $zona_horas . ' hour', $time_respuesta);

        $time_respuesta_zona = strtotime($zona_singo . $zona_minutos . ' minute', $time_respuesta_zona);

        //agrega ceros iniciales en caso de que los valores sean menos que 10
        $zona_horas = $zona_horas < 10 ? '0' . $zona_horas : $zona_horas;
        $zona_minutos = $zona_minutos < 10 ? '0' . $zona_minutos : $zona_minutos;

        $formato_hora = " (h:i A)";
        if ($opciones['ocultar_formato_horas'] == 1) {
            $formato_hora = "";
        }

        if ($opciones['ocultar_diferencia_horas'] == 1) {
            return date('d/m/Y' . $formato_hora, $time_respuesta_zona);
        }
        return date('d/m/Y' . $formato_hora, $time_respuesta_zona) . ' ' . $zona_singo . $zona_horas . ':' . $zona_minutos;
    }

//Fin de convertirFechaZonaHoraria

    /**
     * Se encarga de generar el html que permite compartir
     * información en la red social twitter y permite controlar
     * cuando se ha compartido o no un comentario
     * DM - 2015-06-25
     * */
    static function generarHtmlCompartirTwitter($datos) {
        global $_PATH_IMAGENES;
        $contenido = '<script type="text/javascript">
                    $(document).ready(function(){

                    window.twttr = (function(d, s, id)
                    {
                        var js, fjs = d.getElementsByTagName(s)[0],
                          t = window.twttr || {};
                        if (d.getElementById(id)) return t;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "https://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js, fjs);

                        t._e = [];
                        t.ready = function(f) {
                          t._e.push(f);
                        };

                        return t;
                      }(document, "script", "twitter-wjs"));



                       // Wait for the asynchronous resources to load
                    twttr.ready(function (twttr) {
                      // Now bind our custom intent events
                      twttr.events.bind(\'tweet\', publicarTwitter);
                    });

                    });



                    function publicarTwitter(intentEvent)
                    {
                        if (!intentEvent) return;

      					var enlace = "m=auxiliar&accion=almacenar_track&est_id=' . $datos['est_id'] . '&eof_id=' . $datos['eof_id'] . '&tipo_track=twitter_cicerone";

      					$.ajax({
      						type: "POST",
      						url: "index.php",
      						data: enlace,
      						success: function(msg)
      						{
      						}
      					});

                    }//Fin de publicarTwitter

                    </script>

                  <div class="div_contenedor">
                    <div class="div_fila">
                        <a href="https://twitter.com/intent/tweet?original_referer='.$datos['url'].'" onClick="publicarTwitter();return false;"><img  style="cursor:pointer;" src="' . $_PATH_IMAGENES . '/nuevo_logo_tw_min.png"></a>                        
                    </div>
                  </div>

                    ';

        return $contenido;
    }

//Fin de generarHtmlCompartirTwitter

    /**
     * Se encarga de generar el html que permite compartir
     * información en la red social facebook y permite controlar
     * cuando se ha compartido o no un comentario
     * DM - 2015-06-25
     * */
    static function generarHtmlCompartirFacebook($datos) {
        global $idi_despliegue, $_PATH_IMAGENES, $_CONFIG;

        $contenido = '<script type="text/javascript">
                var envio_datos_fb = true;

              $(document).ready(function()
              {
                window.fbAsyncInit = function() {
                  FB.init({appId: \'' . $_CONFIG['facebook_app_id'] . '\', status: true, cookie: true,
                           xfbml: true});
                };
                (function() {
                  var e = document.createElement(\'script\'); e.async = true;
                  e.src = document.location.protocol +
                    \'//connect.facebook.net/en_US/all.js\';
                  document.getElementById(\'fb-root\').appendChild(e);
                }());
              });

              function estadoCapaMensajeFacebook(estado_visible,tipo_mensaje)
              {
                if( estado_visible == 0)
                {
                    $(\'#fondo_facebook\').hide();
                    $(\'#mensaje_facebook\').hide();
                    $(\'#texto_mensaje_facebook\').html("");
                }
                else
                {
                    $(\'#fondo_facebook\').show();
                    $(\'#mensaje_facebook\').show();

                    switch(tipo_mensaje)
                    {
                        case 1:
                            $(\'#texto_mensaje_facebook\').html("' . $idi_despliegue['label_compartido_facebook_bien'] . '");
                            break;
                        case 0:
                            $(\'#texto_mensaje_facebook\').html("' . $idi_despliegue['label_compartido_facebook_mal'] . '");
                            break;
                    }//Fin de switch(tipo_mensaje)

                    var ancho = $(\'#mensaje_facebook\').width();
                    var ancho_ventana = $(window).width();
                    var left = parseInt( (ancho_ventana - ancho)/2 );
                    $(\'#mensaje_facebook\').css("left",left+"px");

                }
              }//Fin de estadoCapaMensajeFacebook()

              //Funcion para publicar un mensaje en el muro
              var publicarFacebook = function()
              {
                $(\'#div_boton_fb\').hide();
                //DM - 2019-05-23 redmine[13277]
                //Se retira imagen cargando

              	FB.ui(
              	{
                    method: "share",
              		picture : "' . $datos['logo'] . '",
              		href : "' . $datos['url'] . '",
              		name : "' . $datos['nombre'] . '",
              		description : "' . $datos['descripcion'] . '"
              	},
              	function(publicado)
              	{
                    //DM - 2019-05-21 redmine[13277]
              		if( publicado != undefined  && publicado.error_code != 4201 )
              		{
                        estadoCapaMensajeFacebook(1,1);

          				//Para que no pueda enviar mas de una vez
          				if(envio_datos_fb)
          				{
          					envio_datos_fb = false;
          					var enlace = "m=auxiliar&accion=almacenar_track&est_id=' . $datos['est_id'] . '&eof_id=' . $datos['eof_id'] . '&tipo_track=fb_cicerone";

          					$.ajax({
          						type: "POST",
          						url: "index.php",
          						data: enlace,
          						success: function(msg)
          						{
          						}
          					});
          				}

                        $(\'#div_boton_fb\').show();

              		}
              		else
              		{
                        estadoCapaMensajeFacebook(1,0);
              			$(\'#div_boton_fb\').show();
              		}
              	});
              }
                </script>

              <div id="fb-root"></div>
              <div class="div_contenedor">
              	<div class="div_fila">
              		<div id="div_boton_fb" >
                        <img style="cursor:pointer;" onclick="publicarFacebook();" src="' . $_PATH_IMAGENES . '/nuevo_logo_fb_min.png">
                    </div>
              		<div id="div_loading" style="display: none;"><img src="<!--path_imagenes-->/loading.gif" /></div>
              	</div>
              </div>
              <div id="fondo_facebook" style="display:none;z-index:99999999;position:fixed;top:0px;left:0px;width:100%;height:100vh;background-color:#000;opacity:0.8;filter:alpha(opacity=80);">&nbsp;</div>
                <div id="mensaje_facebook" style="display:none;z-index:9999999999;position:fixed;background-color:#FFF;padding:10px;left:45%;top:40%;min-width:200px;max-width:400px;min-height:50px;padding-top:4px;-webkit-border-radius:5px;-moz-border-radius: 5px;border-radius: 5px;">
                    <div onClick="estadoCapaMensajeFacebook(0);" style="float:right;cursor:pointer;" >
                    <img src="' . $_PATH_IMAGENES . '/cerrar_ligthbox.png">
                    </div>
                    <div id="texto_mensaje_facebook" style="padding-top:14px;">
                    </div>
                    </div>
                ';

        return $contenido;
    }

//Fin de generarHtmlCompartirFacebook

    /**
     * Función para generar un excel standar
     * OR 2015-11-17
     * */
    static function generarExcel($datos) {
        global $_PATH_SERVIDOR, $idi_despliegue;


        $datos['datos_formulario'] = is_array($datos['datos_formulario']) ? $datos['datos_formulario'] : array();
        //Asigna los datos del formulario
        $datos = array_merge($datos, $datos['datos_formulario']);



        $obj_archivos = new Archivos($_FILES);
        $fecha_i = $datos['fecha_i'];
        $fecha_f = $datos['fecha_f'];

        $_obj_establecimiento = new Establecimientos();

        if ($_SESSION[tipo_usuario] != 'R' && is_numeric($datos['est_id'])) {
            $info_establecimiento = $_obj_establecimiento->obtenerInfoEstablecimiento($datos['est_id']);
            $datos['est_nombre'] = $info_establecimiento['est_nombre'];
        }

        if ($datos['est_nombre'] == '') {
            $nombre_establecimiento = $_SESSION['cad_nombre'];
        } else {
            $nombre_establecimiento = $datos['est_nombre'];
        }

        if ($nombre_establecimiento == '') {
            $nombre_establecimiento = $_SESSION['usu_nombre'];
        }

        $inicio_titulo = $idi_despliegue['comentarios'];

        if ($_SESSION[tipo_usuario] != 'R') {
            $cabeceraInforme =  $nombre_establecimiento . ' ' . $idi_despliegue['texto_fecha_desde'] . ' ' . $fecha_i . ' ' . $idi_despliegue['texto_fecha_hasta'] . ' ' . $fecha_f;
        } else {
            $cabeceraInforme = $idi_despliegue['informe_impacto_campania'] . ' ' . $idi_despliegue['texto_fecha_desde'] . ' ' . $fecha_i . ' ' . $idi_despliegue['texto_fecha_hasta'] . ' ' . $fecha_f;
        }
        
        $cabeceraInforme = utf8_decode( $cabeceraInforme );

        $excel = '<table border="1">';
        $excel .= '<div style ="font-size: 20px;font-weight: bold;" align="center">' . $cabeceraInforme . '</div>';
        $excel .= '<tr>';
        foreach ($datos['cabecera_excel'] as $campos => $nombre_columna) {
            $excel .= '<th style="background-color:#F0F0F0;color:#666666;font-size: 11px;text-align: center;font-weight: bold;padding-left: 3px;padding-right: 3px;">' . $nombre_columna . '</th>';
        }
        $excel .= '</tr>';


        $fecha_generado = str_replace('-', '', date('Y-m-d'));
        $cadena_aleatoria = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
        $nombre_archivo = $fecha_generado . $cadena_aleatoria;
        if( $datos['nombre_fichero'] != "" )
        {
            $nombre_archivo = $datos['nombre_fichero'];  
        }
        $ruta_archivo = $_PATH_SERVIDOR . "tmp/" . $nombre_archivo . ".xls";
        
        if ( is_array($datos['valores_excel']) && (sizeof($datos['valores_excel']) > 0)) {
            foreach ($datos['valores_excel'] as $campos => $info_columna) {
                if (is_array($info_columna)) {
                    $contador++;
                    $excel .= '<tr>';
                    foreach ($datos['cabecera_excel'] as $columa => $valor_columna) {
                        $excel .= '<td>' . $info_columna[$columa] . '</td>';
                    }
                    $excel .= '</tr>';
                    if ($contador > 100) {
                        $archivo_existe = Archivos::verificarSiArchivoExiste($_PATH_SERVIDOR . "/tmp/", $nombre_archivo . ".xls");
                        $archivo_tmp = $obj_archivos->crearArchivo($ruta_archivo, $excel);
                        $contador = 0;
                        $excel = "";
                    } else if ($contador <= 100) {
                        $archivo_existe = Archivos::verificarSiArchivoExiste($_PATH_SERVIDOR . "/tmp/", $nombre_archivo . ".xls");
                        $archivo_tmp = $obj_archivos->crearArchivo($ruta_archivo, $excel);
                        $contador = 0;
                        $excel = "";
                    }
                } else {
                    $excel .= '<td>' . $info_columna . '</td>';
                }
            }
        }else {
            $columnas = count($datos['cabecera_excel']);
            $columnas = $columnas > 0 ? $columnas : 1;

            $excel .= '</tr>
                        <td aling="center" colspan="' . $columnas . '">
                            ' . $idi_despliegue['mensaje_excel_sin_registros'] . '
                        </td>
                    </tr>';
        }

        $excel .= '</table>';

        $archivo_existe = Archivos::verificarSiArchivoExiste($_PATH_SERVIDOR . "/tmp/", $nombre_archivo . ".xls");
        $archivo_tmp = $obj_archivos->crearArchivo($ruta_archivo, $excel);
        
        $nombre_archivo = str_replace(" ","_",$nombre_archivo);

        //retorna la ruta del archivo para recuperarlo luego
        $datos_excel[ruta] = $ruta_archivo;
        $datos_excel[nombre] = $nombre_archivo . ".xls";
        return $datos_excel;
    }

//fin generarExcel($datos)

    /**
     * Función que trae el nombre de un país en el idioma que se esté usando HQ a partir del ID
     * OR 2016-02-19
     * */
    function obtenerPaisNombre($pai_id) {
        global $_obj_database;

        /* DMG-2015-05-22 Se agrega el idioma de sesion */
        $idi_id = $_SESSION[idi_id] == "" ? 1 : $_SESSION[idi_id];

        $sql = "select pad_nombre
                from pais_idioma
                 where pad_pai_id='$pai_id'
                 and pad_idi_id =" . $idi_id;

        $resultado = $_obj_database->obtenerResultado($sql);
        return $resultado[0];
    }

    /**
     * Se encarga de generar un enlace externo que permite ingresar a una pantalla interna
     * de la aplicación, después de que el usuario se autentica
     * DM - 2016-04-04
     * */
    static function generarEnlaceExterno($datos) {
        global $_PATH_WEB;

        $enlace_acceso = $_PATH_WEB . '/index.php?m=usuarios&accion=url_acceso';
        $enlace_acceso .= '&cod=' . $datos['codigo'];
        $enlace_acceso .= '&url=' . Herramientas::codificarBase64($datos['enlace']);
        return $enlace_acceso;
    }

//Fin de generarEnlaceExterno
//Fin de obtenerPaisNombre


    /*
     * Utilidad: Limpia las comillas simples presentes en el texto para
     * no tener conflictos con la BD
     * Autor: AMP
     * Fecha: 10/6/16
     *
     */
    static function limpiarCamposParaBD($cadena) {
        $a = str_replace("\\'", "'", $cadena);
        $b = str_replace("\'", "'", $a);
        return str_replace("'", "\'", $b);
    }
    
    /**
     * Se encarga de obtener el time en segundos en la zona horaria del servidor
     * de acuerdo a la fecha y hora enviadas y la zona horaria
     * DM - 2019-06-27
     **/
    static function obtenerTimeServidorDeFechaTimeZone($datos)
    {
        $time = strtotime( $datos['fecha'] );
               
        //Obtiene los segunos de la zona base configurada
        $segundos_timezone = timezone_offset_get( new DateTimeZone( $datos['zona_horaria'] ), new DateTime() );
        
        //obtengo la hora cero
        $time -= $segundos_timezone;
                
        //Obtiene la zona base del servidor
        $zona_horaria_base = date_default_timezone_get();
        //obtiene los segundos de la zona base 
        $segundos_timezone_base = timezone_offset_get( new DateTimeZone( $zona_horaria_base ), new DateTime() );
        
        //Obtiene la fecha actual generados a partir de la zona base a GTM ceros
        $time += $segundos_timezone_base;
        
        return $time;
    }//Fin de obtenerTimeServidorDeFechaTimeZone
    
    /**
     * Se encarga de generar el time en segundos 
     * para la zona horaria solicitada
     ***/
    static function obtenerTimeZoneActual($datos)
    {
        //Obtiene la zona base del servidor
        $zona_horaria_base = date_default_timezone_get();
        //obtiene los segundos de la zona base 
        $segundos_timezone_base = timezone_offset_get( new DateTimeZone( $zona_horaria_base ), new DateTime() );
        //Obtiene la fecha actual generados a partir de la zona base a GTM ceros
        $time_actual = time() - $segundos_timezone_base;
        
        //Obtiene los segunos de la zona base configurada
        $segundos_timezone = timezone_offset_get( new DateTimeZone( $datos['zona_horaria'] ), new DateTime() );
        
        //lo suma a la fecha base GTM 0 generada
        $time_actual += $segundos_timezone;
        
        return $time_actual;  
    }//Fin de obtenerTimeZoneActual
    
    /**
     * Se encarga de validar si la accion que se esta intentando 
     * usar, se encuentra en el modulo solicitado, de lo contrario no retorna nada
     * DM - 2019-11-01
     **/
    static function validarAccionModulo($datos){
        global $_PATH_SERVIDOR;
        
        $accion = $datos['accion'];
        $modulo = $datos['m']; 
        
        //verifica si el modulo es una de las carpetas de la aplicacion
        $src_modulo = $_PATH_SERVIDOR.$modulo; 
        //Si el modulo no es una carpeta 
        //debe revisar si tiene caractares extraños
        if( !is_dir($src_modulo) )
        {
            //obtiene todas las carpetas de la aplicacion y busca
            //si alguna corresponde con el modulo solicitado            
            $dh = opendir($_PATH_SERVIDOR);
            
            //guarda el nombre del modulo en caso de encontrar uno
            $modulo_reasignado = '';
                        
            while ( ($file = readdir($dh)) !== false)
            {
                if (is_dir($_PATH_SERVIDOR . $file) && $file!="." && $file!=".."){
                
                    //busca el nombre de la carpeta dentro de la cadena que llego 
                    if( strpos($modulo, $file) !== false ){
                    
                        //Si existe entonces ahora busca si 
                        //el modulo guardado tiene mas letras que el que llega para
                        //saber si lo cambia o no, debido a que se puede 
                        //estar solicitando un modulo igual a otro que ya existe con nombre similar
                        if( strlen($file) > strlen($modulo_reasignado) )
                        {
                            $modulo_reasignado = $file;
                        }
                    }//Fin de if( strpos($modulo, $file) !== false ){
                }
            }//Fin de while ( ($file = readdir($dh)) !== false)
            closedir($dh);
            
            //realiza el cambio del modulo si existe uno para hacerlo
            $modulo = $modulo_reasignado;
            
        }//Fin de if( !is_dir($src_modulo) )
        
        if( $modulo != "" && $accion != "" ){
            //Busca si la accion se encuentra entre las que tiene el modulo solicitado
            $src_modulo = $_PATH_SERVIDOR.$modulo.'/index.php';
        
            //Obtiene el contenido del fichero    
            $contenido_fichero = file_get_contents($src_modulo);
            
            //Busca si la accion existe en el fichero
            $exite_accion = strpos($contenido_fichero,"'".$accion."'") !== FALSE;
            $exite_accion |= strpos($contenido_fichero,'"'.$accion.'"') !== FALSE;
            
            //si la accion no existe en el modulo 
            //entonces busca si existe de forma parcial
            if( !$exite_accion ){
                    
                $accion_reasignada = '';
                for($i=4; $i< strlen($accion); $i++)
                {
                    //Obtiene una porcion de la accion para buscar si existe
                    $accion_tmp = substr($accion,0,$i);
                    
                    //Busca si la accion re asignada existe en el fichero
                    $exite_accion = strpos($contenido_fichero,"'".$accion_tmp."'") !== FALSE;
                    $exite_accion |= strpos($contenido_fichero,'"'.$accion_tmp.'"') !== FALSE;
                    //Si la re asignacion existe verifica si
                    //es existe otra con menos cantidad de letras pues pueden ser similares
                    //y debe usar la de mayor cantidad
                    if(  $exite_accion ){
                        if( strlen($accion_tmp) > strlen($accion_reasignada) ){
                            $accion_reasignada = $accion_tmp; 
                        }//Fin de if( strlen($accion_tmp) > strlen($accion_reasignada) ){
                    }//Fin de if(  $exite_accion ){
                                        
                }//Fin de for($i=0; $i< strlen($accion); $i++)
                
                //realiza el cambio si existe para hacerlo
                $accion = $accion_reasignada;                           
                           
            }//Fin de if( !$exite_accion ){
            
        }//Fin de if( $modulo != "" ){       
        
        $datos['m'] = $modulo;
        $datos['accion'] = $accion;
        
        return $datos;
    }//Fin de validarAccionModulo
    
    
}//class
?>