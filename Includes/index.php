<?php
require_once("Database.php");
require_once("Archivos.php");
require_once("Menu.php");
require_once("Interfaz.php");
require_once("Herramientas.php");
require_once("Mensajes.php");
require_once("Correo.php");
require_once("funciones.php");
//require_once("Traductor.php");

function validarAcceso($perfiles_autorizados)
{
	//captura el perfil del usuario
	$perfil = $_SESSION['tipo_usuario'];
		
	if(sizeof($perfiles_autorizados)>0)
	{
		//verifica si el perfil se encuentra en los autorizados
		$existe = array_search($perfil, $perfiles_autorizados);
		
		if(is_numeric($existe))
		{
			//Asigna el perfil correspondiente
			$perfil = $_SESSION['tipo_usuario'];
		}
		else
		{
			//elimina el acceso de todos los perfiles
			$perfil = "-";		
		}
	}//Fin de if( sizeof($perfiles_autorizados)>0 )
	
	return $perfil;
}//Fin de validarAcceso()
?>