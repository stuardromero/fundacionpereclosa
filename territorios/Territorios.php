<?php

class Territorios{

    var $_plantillas = "";

    function __construct() {
        global $_PATH_SERVIDOR;
        $this->_plantillas = $_PATH_SERVIDOR."/territorios/Plantillas";
    }

    /** abrirFormularioRegistrarTerritorio
     * parametro: $datos
     * autor : BDL
     * descripcion: ABRE EL CONTENIDO DE REGISTRO DE TERRITORIOS
    **/
    function abrirFormularioRegistrarTerritorio($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registrar.html");

        $label_inicio = "Territoris";
        $msg_descripcion = "Registre de Territoris";
        $breadcump_territorios = "Territoris";
        $input_centro_territorio = "Territori*";
        $input_centro_servicio_territorial = "Servei Territorial*";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $servicio_territorial = $this->obtenerSelectServicioTerritorial($datos);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_territorios", $breadcump_territorios, $contenido);
        
        Interfaz::asignarToken("input_ter_nombre", $input_centro_territorio, $contenido);
        Interfaz::asignarToken("input_ter_codigo_servicio_territiorial", $input_centro_servicio_territorial, $contenido);
        Interfaz::asignarToken("select_ter_servicio_territiorial", $servicio_territorial, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    /** registrarTerritorio
     * Registrar Territorio en la BD
     * BDL::21-04-2021
     */
    function registrarTerritorio($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        $datos = Herramientas::trimCamposFormulario($datos);

        $campos = array(
        "ter_nombre",     
        "ter_ste_id",
        );        
        
        $datos['tabla'] = 'territorio';
        $datos['ter_ste_id'] = intval($datos['ter_ste_id']);
        $sql = $_obj_database->generarSQLInsertar($datos, $campos);

        $res = $_obj_database->ejecutarSql($sql);

        if($res == 1){
            return 'TER-01';
        } else {
            return 'TER-04';
        }

    }

    /** listadoTerritorios
     * Contenido listado de Territorios
     * BDL::21-04-2021
     */
    function listadoTerritorios($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_territorios.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
        $label_inicio = "Territoris";
        $msg_descripcion = "Llistat de Territoris";
        $breadcump_territorios = "Territoris";

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_territorios_listar", $breadcump_territorios, $contenido);

        return $contenido;
    }

    /** obtenerDatosTerritorio
     * Retorna los datos del registro del territorio
     * BDL::21-04-2021
     */
    function obtenerDatosTerritorio($ter_id) {
        global $_obj_database;

        $sql = "SELECT * FROM territorio
				WHERE ter_id=$ter_id";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    /** abrirFormularioEditarTerritorio
     * ABRE EL CONTENIDO DE EDITAR DE TERRITORIO
     * BDL::21-04-2021
     */
    function abrirFormularioEditarTerritorio($datos,$valores){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_territorios.html");

        $label_inicio = "Territoris";
        $msg_descripcion = "Edita Territori";
        $breadcump_territorios = "Territoris";
        $input_centro_territorio = "Territori*";
        $input_centro_servicio_territorial = "Servei Territorial*";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $valores = $valores[0];

        $servicio_territorial = $this->obtenerSelectServicioTerritorial($datos,$valores);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_territorios", $breadcump_territorios, $contenido);
        
        Interfaz::asignarToken("input_ter_nombre", $input_centro_territorio, $contenido);
        Interfaz::asignarToken("ter_ter_nombre",$valores["ter_nombre"], $contenido);
        Interfaz::asignarToken("input_ter_codigo_servicio_territiorial", $input_centro_servicio_territorial, $contenido);
        Interfaz::asignarToken("select_ter_servicio_territiorial", $servicio_territorial, $contenido);
        Interfaz::asignarToken("ter_ter_id", $datos["ter_id"], $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    /** editarTerritorio
     * EDITAR INFORMACION DE TERRITORIO
     * BDL::21-04-2021
    */
    function editarTerritorio($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        $datos = Herramientas::trimCamposFormulario($datos);

        $campos = array(
            "ter_nombre",     
            "ter_ste_id",
        );        
            
        $datos['tabla'] = 'territorio';
        $datos['ter_ste_id'] = intval($datos['ter_ste_id']);
        $datos['condicion'] = "ter_id = '" . $datos['ter_id'] . "' ";

        $res = $_obj_database->actualizarDatosTabla($datos, $campos);

        if (!$res) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }

        if($res == 1){
            return 'TER-02';
        } else {
            return 'TER-05';
        }
    }

    /** EliminarAlumnos
     * ELIMINAR INFORMACION DE TERRITORIO
     * BDL::21-04-2021
    */
    function eliminarTerritorios($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        if (!$_obj_database->iniciarTransaccion()) {
            //problma al inicar la transaccion
            return -1003;
        }
        $ter_id = $datos["ter_id"];
        $sql = "DELETE FROM territorio WHERE ter_id ='".$ter_id."';";

        if (!$_obj_database->ejecutarSql($sql)) {
            $_obj_database->cancelarTransaccion();
            return -1003;
        } else {
            $_obj_database->terminarTransaccion();
        }

        return 'TER-03';
    }

    /** obtenerSelectServicioTerritorial
     * Retorna selector de servicios territoriales
     * BDL::21-04-2021
     */
    function obtenerSelectServicioTerritorial($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT DISTINCT(ste_id), ste_nombre
                FROM servicio_territorial 
                WHERE 1;"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $ter_ste_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['ter_ste_id'])){
                $ter_ste_id = $valores['ter_ste_id'];
            }
        }

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='ter_ste_id' name='ter_ste_id' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar servei territorial</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='ter_ste_id' name='ter_ste_id' aria-label='Default select example' required>";
            $select .= "<option value='0' selected>Seleccionar servei territorial</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ste_id'] == $ter_ste_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ste_id']."' ".$selected.">".$resultado['ste_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }
}