<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Includes/Interfaz.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("Territorios.php");

$obj_usuarios = new Usuarios();
$obj_territorios = new Territorios();
$obj_interfaz = new Interfaz();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarJS("formulario.js");

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
    case 'fm_registrar'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];  
            $opciones['CADENA'] = 'El Territori';           
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
		$contenido = $obj_territorios->abrirFormularioRegistrarTerritorio($datos);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'registrarTerritorio'.validarAcceso(array(1)):
        $resultado = $obj_territorios->registrarTerritorio($datos);
        if( $resultado=='TER-01' ) 
		{                 
			header('Location: index.php?m=territorios&accion=fm_registrar&tipo_gestion=1&msg=TER-01');    
			break;            
		}//Fin de if( $resultado==1 )      
		else {
            $opciones['CADENA'] = "El Territori";      
			$opciones['ACCION'] = 1;//ingresar      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);  
			$contenido = $obj_territorios->abrirFormularioRegistrarTerritorio($datos);
			$_obj_interfaz->asignarContenido($contenido);
        }                
        //$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'listarTerritorios'.validarAcceso(array(1)):
        if(isset($datos['msg'])){
            $opciones['ACCION'] = $datos['tipo_gestion'];
            $opciones['CADENA'] = 'El territori';
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }
        $contenido = $obj_territorios->listadoTerritorios($datos);
        $campos = array(
            "ter_id" => "ID",
            "ter_nombre" => "Territori", 
            "ste_nombre" => "Servei Territorial",
        );  	
        
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        $opciones['actualizar'] = "";
        $opciones['eliminar'] = "";
        
        $datos['sql'] = "SELECT ter.ter_id, ter.ter_nombre, ste.ste_nombre
        FROM territorio as ter
        INNER JOIN servicio_territorial AS ste 
        ON ter.ter_ste_id = ste.ste_id
        WHERE 1
        ORDER BY ter.ter_id DESC;";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
        $arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
    
        $contenido .= $_obj_interfaz->crearListadoTerritorios($arreglo_datos);

        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'fm_editar'.validarAcceso(array(1)):
        $valores = $obj_territorios->obtenerDatosTerritorio($datos['ter_id']);
        if( !is_array($valores))
        {
            $opciones['ACCION']=0;
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
        }
        $datos['TIPO_ACCION'] = "editar";
        $contenido = $obj_territorios->abrirFormularioEditarTerritorio($datos,$valores);
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'editarTerritorio'.validarAcceso(array(1)):
        $resultado = $obj_territorios->editarTerritorio($datos);
        if( $resultado=='TER-02' ) 
        {         
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=territorios&accion=listarTerritorios&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php        
            //header('Location: index.php?m=alumnos&accion=listarTerritorios&tipo_gestion=1&msg='.$resultado);    
            break;            
        }//Fin de if( $resultado==1 )      
        
        break;
    case 'fm_eliminar'.validarAcceso(array(1)):
        $resultado = $obj_territorios->eliminarTerritorios($datos);
        ?>
        <script type="text/javascript"> 
            window.location="index.php?m=territorios&accion=listarTerritorios&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
        </script> 
        <?php
        //$enlace = "index.php?m=alumnos&accion=listarTerritorios&tipo_gestion=3&msg=".$resultado."";
        //header('Location: '.$enlace); 
        break;
}