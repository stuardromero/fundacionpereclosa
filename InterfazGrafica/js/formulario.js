$(document).ready(function () {  
    $('#contrasena').keyup(function () {  
        $('#seguridadContrasena').html(checkStrength($('#contrasena').val()))  
    })  
    function checkStrength(contrasena) {  
        var strength = 0  
        if (contrasena.length < 6) {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-muy-corta')  
            return 'Molt Insegura'  
        }  
        if (contrasena.length > 7) strength += 1  
        // If contrasena contains both lower and uppercase characters, increase strength value.  
        if (contrasena.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1  
        // If it has numbers and characters, increase strength value.  
        if (contrasena.match(/([a-zA-Z])/) && contrasena.match(/([0-9])/)) strength += 1  
        // If it has one special character, increase strength value.  
        if (contrasena.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1  
        // If it has two special characters, increase strength value.  
        if (contrasena.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1  
        // Calculated strength value, we can return messages  
        // If value is less than 2  
        if (strength < 2) {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-corta')  
            return 'Insegura'  
        } else if (strength == 2) {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-buena')  
            return 'Segura'  
        } else {  
            $('#seguridadContrasena').removeClass()  
            $('#seguridadContrasena').addClass('contrasena-excelente')  
            return 'Molt Segura'  
        }  
    }  
});
$(document).ready(function () {  
    asignarCursos();

    $('#est_edu_id').change(function(){
        asignarCursos();
    });

    function asignarCursos(){
        $.ajax({
            type:"POST",
            url:"index.php?m=alumnos&accion=obtenerCursosEtapaEducativa",
            data:"edu_id="+$('#est_edu_id').val(),
            success: function(r){
                $('#selectCurso').html(r);
            }
        });
    }
});