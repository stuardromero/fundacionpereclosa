<?php

class Alumnos{

    var $_plantillas = "";

    function __construct() {
        global $_PATH_SERVIDOR;
        $this->_plantillas = $_PATH_SERVIDOR."/alumnos/Plantillas";
    }

    /** abrirFormularioRegistrarAlumnos
     * parametro: $datos
     * autor : BDL
     * descripcion: ABRE EL CONTENIDO DE REGISTRO DE USUARIOS
    **/
    function abrirFormularioRegistrarAlumnos($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registrar.html");

        $label_inicio = "Alumnes";
        $msg_descripcion = "Registre d'Alumnes";
        $breadcump_alumnos = "Alumnes";
        $input_alumn_idalu = "IDALU";
        $input_alumn_nombre = "Nombre i cognoms de l'alumne*";
        $input_alumn_genero = "Gènere*";
        $input_alumn_codigo_familia = 'Codi Família 
        <a href="#" data-toggle="tooltip" style="margin-bottom: 4px;padding: 0px 0px;display: inline-flex;" data-placement="right" title="El codi de família s\'obté en el Mòdul de Famílies." class="btn icon btn-info" style="padding:0.1rem;">
        <i data-feather="info"></i></a>';
        $input_alumn_nombre_padre = "Nombre Pare*";
        $input_alumn_nombre_madre = "Nombre Mare*";
        $input_alumn_centro_educativo = "Centre Escolar*";
        $input_alumn_etapa_educativa = "Etapa Educativa*";
        $input_alumn_curso_etapa_educativa = "Curs*";
        $input_alumn_referent = "Observacions*";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $centros_escolares = $this->obteberSelectFormaCentroEducativo($datos);
        $etapas_educativas = $this->obteberSelectFormaEtapaEducativa($datos);
        $familias_select = $this->obteberSelectFormaFamilias($datos);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_alumnos", $breadcump_alumnos, $contenido);
        

        Interfaz::asignarToken("input_alumn_idalu", $input_alumn_idalu, $contenido);
        Interfaz::asignarToken("input_alumn_nombre", $input_alumn_nombre, $contenido);
        Interfaz::asignarToken("input_alumn_genero", $input_alumn_genero, $contenido);
        Interfaz::asignarToken("input_alumn_codigo_familia", $input_alumn_codigo_familia, $contenido);
        Interfaz::asignarToken("input_alumn_nombre_padre", $input_alumn_nombre_padre, $contenido);
        Interfaz::asignarToken("input_alumn_nombre_madre", $input_alumn_nombre_madre, $contenido);
        Interfaz::asignarToken("input_alumn_centro_educativo", $input_alumn_centro_educativo, $contenido);
        Interfaz::asignarToken("input_alumn_etapa_educativa", $input_alumn_etapa_educativa, $contenido);
        Interfaz::asignarToken("input_alumn_curso_etapa_educativa", $input_alumn_curso_etapa_educativa, $contenido);
        Interfaz::asignarToken("input_alumn_referent", $input_alumn_referent, $contenido);
        Interfaz::asignarToken("select_alumn_centro_educativo", $centros_escolares, $contenido);
        Interfaz::asignarToken("select_alumn_etapa_educativa", $etapas_educativas, $contenido);
        Interfaz::asignarToken("select_alumn_familias", $familias_select, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    function registrarAlumnos($datos){
        global $_obj_database, $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_interfaz;

        // if(!isset($_SESSION))
        // {
        //     session_start();
        // }

        $datos = Herramientas::trimCamposFormulario($datos);

        //var_dump($datos);
        //exit();
        $campos = array(
        "est_idalu",     
        "est_nombre", 	
        "est_genero",
        "est_edu_id",
        "est_fam_id",
        "est_cur_id", 
        "est_referente_servicio_social",
        "est_ces_id",
        "est_desactivo"
        );
        
        $encrypt_name = $_obj_interfaz->encrypt_decrypt('encrypt', $datos['est_nombre']);
        $datos['est_nombre'] = $encrypt_name;

        $datos['tabla'] = 'estudiante';
        $datos['est_edu_id'] = intval($datos['est_edu_id']);
        $datos['est_fam_id'] = intval($datos['est_fam_id']);
        $datos['est_cur_id'] = intval($datos['est_cur_id']);
        $datos['est_ces_id'] = intval($datos['est_ces_id']);
        $datos['est_desactivo'] = "off";
        $sql = $_obj_database->generarSQLInsertar($datos, $campos);

        $res = $_obj_database->ejecutarSql($sql);

        if($res == 1){
            return 'U-19';
        } else {
            return 'U-20';
        }

    }

    function obteberSelectFormaCentroEducativo($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT ces.ces_id, ces.ces_nombre 
        FROM centro_escolar as ces 
        INNER JOIN usuario_centro_escolar as usu_ces
        ON usu_ces.usc_ces_id = ces.ces_id
        WHERE usu_ces.usc_usu_id_promotor = ".$usu_id.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $est_ces_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['est_ces_id'])){
                $est_ces_id = $valores['est_ces_id'];
            }
        }
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='est_ces_id' aria-label='Default select example' disabled>";
            $select .= "<option>No té Centres educatius assignats</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='est_ces_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar centre escolar</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ces_id'] == $est_ces_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ces_id']."' ".$selected.">".$resultado['ces_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
        
    }

    function obteberSelectFormaEtapaEducativa($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT DISTINCT(edu_id), edu_nombre
                FROM etapa_educativa 
                WHERE 1;"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $est_edu_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['est_edu_id'])){
                $est_edu_id = $valores['est_edu_id'];
            }
        }

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='est_edu_id' name='est_edu_id' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar etapa educativa</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='est_edu_id' name='est_edu_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar etapa educativa</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['edu_id'] == $est_edu_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['edu_id']."' ".$selected.">".$resultado['edu_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }

    function obteberSelectFormaCurso($datos,$valores = NULL){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $etapa_educativa = $datos['edu_id'];

        $est_cur_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['est_cur_id'])){
                $etapa_educativa = $valores['est_edu_id'];
                $est_cur_id = $valores['est_cur_id'];
            }
        }

        $sql = "SELECT DISTINCT(cur_id), cur_nombre
                FROM cursos 
                WHERE cur_edu_id = ".$etapa_educativa.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='est_cur_id' name='est_cur_id' aria-label='Default select example' disabled>";
            $select .= "<option value='0'>Seleccionar curs</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='est_cur_id' name='est_cur_id' aria-label='Default select example' required>";
            $select .= "<option selected>Seleccionar curs</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['cur_id'] == $est_cur_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['cur_id']."' ".$selected.">".$resultado['cur_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;        
    }

    function listadoAlumnos($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_alumnos.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
        $label_inicio = "Alumnes";
        $msg_descripcion = "Llistat Alumnes";
        $breadcump_alumnos = "Alumnes";

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_alumnos_listar", $breadcump_alumnos, $contenido);

        return $contenido;
    }

    function EliminarAlumnos($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT * FROM intervencion_estudiante WHERE ies_est_id = ".$datos['est_id'].";";
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultados){
            $resultado = 'U-26';
        }
        else{
            $sql = "DELETE FROM estudiante WHERE est_idalu = '" . $datos['est_idalu'] . "' AND est_id = '".$datos['est_id']."'";
            if (!$_obj_database->ejecutarSql($sql)) {
                $_obj_database->cancelarTransaccion();
                return -1003;
            } else {
                $_obj_database->terminarTransaccion();
            }
            $resultado = 'U-22';
        }

        return $resultado;
        
    }

    function abrirFormularioEditarAlumnos($datos,$valores){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_interfaz;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_alumnos.html");

        $label_inicio = "Alumnes";
        $msg_descripcion = "Edita Alumnes";
        $breadcump_alumnos = "Alumnes";
        $input_alumn_idalu = "IDALU*";
        $input_alumn_nombre = "Nombre Alumne*";
        $input_alumn_genero = "Gènere*";
        $input_alumn_codigo_familia = 'Codi Família*  
        <a href="#" data-toggle="tooltip" style="margin-bottom: 4px;padding: 0px 0px;display: inline-flex;" data-placement="right" title="El codi de família s\'obté en el Mòdul de Famílies." class="btn icon btn-info" style="padding:0.1rem;">
        <i data-feather="info"></i></a>';
        $input_alumn_nombre_padre = "Nombre Pare*";
        $input_alumn_nombre_madre = "Nombre Mare*";
        $input_alumn_centro_educativo = "Centre Escolar*";
        $input_alumn_etapa_educativa = "Etapa Educativa*";
        $input_alumn_curso_etapa_educativa = "Curs*";
        $input_alumn_referent = "Observacions*";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $valores = $valores[0];

        $centros_escolares = $this->obteberSelectFormaCentroEducativo($datos,$valores);
        $etapas_educativas = $this->obteberSelectFormaEtapaEducativa($datos,$valores);
        $cursos = $this->obteberSelectFormaCurso($datos,$valores);
        $genero = $this->obteberSelectFormaGenero($datos);
        $familias_select = $this->obteberSelectFormaFamilias($datos, $valores);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_alumnos", $breadcump_alumnos, $contenido);

        if($valores['est_desactivo'] == "on"){
            $val_check = "checked";
        }

        $decrypt_name = $_obj_interfaz->encrypt_decrypt('decrypt', $valores["est_nombre"]);

        Interfaz::asignarToken("val_check", $val_check, $contenido);
        Interfaz::asignarToken("input_alumn_idalu", $input_alumn_idalu, $contenido);
        Interfaz::asignarToken("est_alumn_idalu",$datos["est_idalu"], $contenido);
        Interfaz::asignarToken("input_alumn_nombre", $input_alumn_nombre, $contenido);
        Interfaz::asignarToken("est_alumn_nombre",$decrypt_name, $contenido);
        Interfaz::asignarToken("input_alumn_genero", $input_alumn_genero, $contenido);
        Interfaz::asignarToken("select_alumn_genero", $genero, $contenido);
        Interfaz::asignarToken("input_alumn_codigo_familia", $input_alumn_codigo_familia, $contenido);
        Interfaz::asignarToken("est_alumn_fam_id", $valores["est_fam_id"], $contenido);
        Interfaz::asignarToken("input_alumn_nombre_padre", $input_alumn_nombre_padre, $contenido);
        Interfaz::asignarToken("input_alumn_nombre_madre", $input_alumn_nombre_madre, $contenido);
        Interfaz::asignarToken("input_alumn_centro_educativo", $input_alumn_centro_educativo, $contenido);
        Interfaz::asignarToken("input_alumn_etapa_educativa", $input_alumn_etapa_educativa, $contenido);
        Interfaz::asignarToken("input_alumn_curso_etapa_educativa", $input_alumn_curso_etapa_educativa, $contenido);
        Interfaz::asignarToken("input_alumn_referent", $input_alumn_referent, $contenido);
        Interfaz::asignarToken("est_alumn_referent", $valores["est_referente_servicio_social"], $contenido);
        Interfaz::asignarToken("select_alumn_centro_educativo", $centros_escolares, $contenido);
        Interfaz::asignarToken("est_alum_id", $datos["est_id"], $contenido);
        Interfaz::asignarToken("select_alumn_etapa_educativa", $etapas_educativas, $contenido);
        Interfaz::asignarToken("select_alumn_curso_etapa_educativa", $cursos, $contenido);
        Interfaz::asignarToken("select_alumn_familias", $familias_select, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    function obteberSelectFormaGenero($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $est_id = $datos['est_id'];

        $sql = "SELECT est_genero
                FROM estudiante 
                WHERE est_id = ".$est_id.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='est_genero' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar Gènere</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='est_genero' aria-label='Default select example' required>";
            if($resultados[0]['est_genero'] === 'M'){
                $select .= "<option value='M' selected>Home</option>";
                $select .= "<option value='F'>Femení</option>";
            } else if($resultados[0]['est_genero'] === 'F') {
                $select .= "<option value='M'>Home</option>";
                $select .= "<option value='F' selected>Femení</option>";
            }
            
            $select .="</select>";
        }

        return $select;
    }

    function obteberSelectFormaFamilias($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT fam.fam_id, fam.fam_nombre_padre, fam.fam_nombre_madre 
        FROM familia as fam 
        WHERE fam.fam_usu_id_promotor = ".$usu_id." 
        ORDER BY fam.fam_id DESC;"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $est_fam_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['est_fam_id'])){
                $est_fam_id = $valores['est_fam_id'];
            }
        }
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='est_fam_id' aria-label='Default select example' disabled>";
            $select .= "<option>No té família assignats</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='est_fam_id' aria-label='Default select example'>";
            $select .= "<option value='' selected>Seleccionar família</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['fam_id'] == $est_fam_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['fam_id']."' ".$selected.">".$resultado['fam_id']."-".$resultado['fam_nombre_padre']."-".$resultado['fam_nombre_madre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
        
    }

    function obtenerDatosAlumno($est_id) {
        global $_obj_database;

        $sql = "SELECT * FROM estudiante
				WHERE est_id=$est_id";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    function editarAlumnos($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_interfaz;

        // if(!isset($_SESSION))
        // {
        //     session_start();
        // }

        $datos = Herramientas::trimCamposFormulario($datos);

        //var_dump($datos);
        //exit();
        $campos = array(
        "est_idalu",     
        "est_nombre", 	
        "est_genero",
        "est_edu_id",
        "est_fam_id",
        "est_cur_id", 
        "est_referente_servicio_social",
        "est_ces_id",
        "est_desactivo"
        );        

        $encrypt_name = $_obj_interfaz->encrypt_decrypt('encrypt', $datos["est_nombre"]);
        $datos["est_nombre"] = $encrypt_name;
        
        $datos['tabla'] = 'estudiante';
        $datos['est_edu_id'] = intval($datos['est_edu_id']);
        $datos['est_fam_id'] = intval($datos['est_fam_id']);
        $datos['est_cur_id'] = intval($datos['est_cur_id']);
        $datos['est_ces_id'] = intval($datos['est_ces_id']);
        if(!isset($datos['est_desactivo'])){
            $datos['est_desactivo'] = "off";
        }
        $datos['condicion'] = "est_id = '" . $datos['est_id'] . "' ";

        $res = $_obj_database->actualizarDatosTabla($datos, $campos);

        if (!$res) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }

        if($res == 1){
            return 'U-21';
        } else {
            return 'U-20';
        }
        
    }
}

?>
