
<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Includes/Interfaz.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("Alumnos.php");

$obj_usuarios = new Usuarios();
$obj_alumnos = new Alumnos();
$obj_interfaz = new Interfaz();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarJS("formulario.js");

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
    case 'fm_registrar'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];  
            $opciones['CADENA'] = 'El Alumne';           
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
		$contenido = $obj_alumnos->abrirFormularioRegistrarAlumnos($datos);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'registrarAlumnos'.validarAcceso(array(2)):
        $resultado = $obj_alumnos->registrarAlumnos($datos);
        if( $resultado=='U-18' ) 
		{                 
			header('Location: index.php?m=alumnos&accion=fm_registrar&tipo_gestion=1&msg=U-18');    
			break;            
		}//Fin de if( $resultado==1 )      
		else {
            $opciones['CADENA'] = "El Alumne";      
			$opciones['ACCION'] = 1;//ingresar      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);  
			$contenido = $obj_alumnos->abrirFormularioRegistrarAlumnos($datos);
			$_obj_interfaz->asignarContenido($contenido);
        }                
        //$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'obtenerCursosEtapaEducativa'.validarAcceso(array(2)):
        $resultado = $obj_alumnos->obteberSelectFormaCurso($datos);
        print_r($resultado);
        exit;
        break;    
    case 'listaralumnos'.validarAcceso(array(2)):
        if(isset($datos['msg'])){
            $opciones['ACCION'] = $datos['tipo_gestion'];
            $opciones['CADENA'] = 'El usuario';
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }
        $contenido = $obj_alumnos->listadoAlumnos($datos);
		$campos = array(
            "est_id" => "ID",
			"est_idalu" => "IDALU", 
			"est_nombre" => "Nom",
            "est_genero" => "Gènere",
            "edu_nombre" => "Etapa Educativa",
            "est_fam_id" => "Codi Família", 
            "cur_nombre" => "Curs",
            "est_referente_servicio_social" => "Observacions",
            "ces_nombre" => "Centre Escolar",
            "est_desactivo" => "Estat"
		);  	
        
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
		$datos['sql'] = "SELECT est.est_id, est.est_idalu, est.est_nombre, est.est_genero, est.est_fam_id ,edu.edu_nombre, cur.cur_nombre,
        est.est_referente_servicio_social, ces.ces_nombre, est.est_desactivo
        FROM estudiante as est
        INNER JOIN centro_escolar as ces 
        ON ces.ces_id = est.est_ces_id
        INNER JOIN etapa_educativa as edu
        ON edu.edu_id = est.est_edu_id
        INNER JOIN cursos as cur
        ON cur.cur_id = est.est_cur_id
        INNER JOIN usuario_centro_escolar as usc
        ON usc.usc_ces_id = ces.ces_id
        WHERE usc.usc_usu_id_promotor = ".$usu_id."
        ORDER BY est.est_id DESC;";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
		$contenido .= $_obj_interfaz->crearListadoAlumnos($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'fm_editar'.validarAcceso(array(2)):
        $valores = $obj_alumnos->obtenerDatosAlumno($datos['est_id']);
        if( !is_array($valores))
		{
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
        $datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_alumnos->abrirFormularioEditarAlumnos($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'editaralumnos'.validarAcceso(array(2)):
        $resultado = $obj_alumnos->editarAlumnos($datos);
        if( $resultado=='U-21' ) 
        {         
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=alumnos&accion=listaralumnos&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php        
            //header('Location: index.php?m=alumnos&accion=listaralumnos&tipo_gestion=1&msg='.$resultado);    
            break;            
        }//Fin de if( $resultado==1 )   
        
        break;
    case 'fm_eliminar'.validarAcceso(array(2)):
        $resultado = $obj_alumnos->EliminarAlumnos($datos);
        ?>
        <script type="text/javascript"> 
            window.location="index.php?m=alumnos&accion=listaralumnos&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
        </script> 
        <?php
        //$enlace = "index.php?m=alumnos&accion=listaralumnos&tipo_gestion=3&msg=".$resultado."";
		//header('Location: '.$enlace);
		
        break;
}

?>