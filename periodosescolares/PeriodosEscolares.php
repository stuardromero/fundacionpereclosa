<?php

/**
 * 	Nombre: PeriodosEscolares.php
 * 	Descripción: Maneja lo referente a las cotizaciones del sistema
 */
class PeriodosEscolares {

  var $_plantillas = "";

  public function __construct() {
      global $_PATH_SERVIDOR;

      $this->_plantillas = $_PATH_SERVIDOR . "periodosescolares/Plantillas";
      $this->_herramientas = new Herramientas;
      $this->campos_busqueda_listado = array("usu_id","usu_nombre",   
		          "usu_login","usu_correo", "usu_apellido");  
  }

    /** abrirFormularioPeriodosEscolares
     * parametro: $datos
     * autor : SA - 04/12/2020
     * descripcion: ABRE EL Formulario para anexar periodos escolares
     **/
    public function abrirFormularioPeriodosEscolares($datos) {
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_periodos_escolares.html");
      $msg_titulo = 'Períodes Acadèmics';
      $msg_descripcion = "Registreu el vostre Curs Escolar";
      $input_anos_lectivos = 'Curs Escolar';
      $input_periodo = 'Trimestre';
      $input_fecha_inicio = 'Data Inici';
      $input_fecha_fin = 'Data Fi';

      $img_ruta = "Includes/images";

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("boton_procesar", $boton_procesar, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("input_anos_lectivos", $input_anos_lectivos, $contenido);
      Interfaz::asignarToken("input_periodo", $input_periodo, $contenido);
      Interfaz::asignarToken("input_fecha_inicio", $input_fecha_inicio, $contenido);
      Interfaz::asignarToken("input_fecha_fin", $input_fecha_fin, $contenido);

      return $contenido;
    }

    /** actPeriodosAcademicos
     * parametro: $datos
     * autor : SA - 04/12/2020
     * descripcion: Inserta los datos del periodo escolar en base de datos
     **/
    function actPeriodosAcademicos($datos) {
      global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $sql_transaccion = "INSERT INTO periodos_escolares (pes_ano_lectivo, pes_periodo, pes_fecha_inicio, pes_fecha_fin) VALUES('" .$datos['anos_lectivos']."', 'I', '" .$datos['fecha_inicio1']."', '" .$datos['fecha_fin1']."')";

      $_obj_database->ejecutarSql($sql_transaccion);

      $sql_transaccion = "INSERT INTO periodos_escolares (pes_ano_lectivo, pes_periodo, pes_fecha_inicio, pes_fecha_fin) VALUES('" .$datos['anos_lectivos']."', 'II', '" .$datos['fecha_inicio2']."', '" .$datos['fecha_fin2']."')";

      $_obj_database->ejecutarSql($sql_transaccion);

      $sql_transaccion = "INSERT INTO periodos_escolares (pes_ano_lectivo, pes_periodo, pes_fecha_inicio, pes_fecha_fin) VALUES('" .$datos['anos_lectivos']."', 'III', '" .$datos['fecha_inicio3']."', '" .$datos['fecha_fin3']."')";

      $_obj_database->ejecutarSql($sql_transaccion);

      return $contenido;
    }
    function listadoPeriodosEscolares($datos){
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_periodos_escolares.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
      $label_inicio = "Períodes Acadèmics";
      $msg_descripcion = "Llistat Períodes Acadèmics";

      Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      
      return $contenido;
    }
    /** EliminarPeriodosEscolares
    * parametro: $datos
    * autor : SA 19/01/2021
    * descripcion: Elimina Periodos Escolares
    **/
    function EliminarPeriodosEscolares($datos){
      global $_obj_database;
    
      if(isset($_SESSION))
      {
          $usu_id = $_SESSION['usu_id'];
      }

      if (!$_obj_database->iniciarTransaccion()) {
          //problema al inicar la transaccion
          return -1003;
      }

      /*$sql = "SELECT * FROM periodos_escolares WHERE pes_id   = '" . $datos['pes_id'] . "';";
      $res = $_obj_database->obtenerRegistrosAsociativos($sql);

      $valor = $res[0];

      $sql = "DELETE FROM periodos_escolares WHERE pes_ano_lectivo = '" . $valor['pes_ano_lectivo'] . "';";*/

      $sql = "DELETE FROM periodos_escolares WHERE pes_id = '" . $datos['pes_id'] . "';";

      if (!$_obj_database->ejecutarSql($sql)) {
          $_obj_database->cancelarTransaccion();
          return -1003;
      } else {
          $_obj_database->terminarTransaccion();
      }

      return 'PE-3';
    }

    function obtenerDatosPeriodosEscolares($pes_id) {
      global $_obj_database;
    
      $sql = "SELECT * FROM periodos_escolares WHERE pes_id=$pes_id";
    
      $res = $_obj_database->obtenerRegistrosAsociativos($sql);

      return ($res);
    }

    function abrirFormularioEditarPeriodosEscolares($datos, $valores){
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;
    
      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_periodos_escolares.html");
      
      if(isset($datos['mensaje'])){
        $mensaje = $datos['mensaje'];
      }

      $msg_titulo = 'Períodes Acadèmics';
      $msg_descripcion = "Editar el vostre Any Lectiu";
      $input_anos_lectivos = 'Any Lectiu';
      $input_periodo = 'Període';
      $input_fecha_inicio = 'Data Inici';
      $input_fecha_fin = 'Data Fi';

      $valores = $valores[0];

      $img_ruta = "Includes/images";

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("input_anos_lectivos", $input_anos_lectivos, $contenido);
      Interfaz::asignarToken("input_periodo", $input_periodo, $contenido);
      Interfaz::asignarToken("input_fecha_inicio", $input_fecha_inicio, $contenido);
      Interfaz::asignarToken("input_fecha_fin", $input_fecha_fin, $contenido);
      Interfaz::asignarToken("input_fecha_fin", $input_fecha_fin, $contenido);
    
      Interfaz::asignarToken("pes_val_id",$valores["pes_id"], $contenido);
      Interfaz::asignarToken("pes_val_anos_lectivos",$valores["pes_ano_lectivo"], $contenido);
      Interfaz::asignarToken("pes_val_fecha_inicio",$valores["pes_fecha_inicio"], $contenido);
      Interfaz::asignarToken("pes_val_fecha_fin",$valores["pes_fecha_fin"], $contenido);
    
      Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    
      return $contenido;
    }

    function editarPeriodosEscolares($datos){
      global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;
    
      $datos = Herramientas::trimCamposFormulario($datos);  
    
      $sql = "UPDATE periodos_escolares SET pes_fecha_inicio = '". $datos['fecha_inicio'] ."', pes_fecha_fin = '". $datos['fecha_fin'] ."' WHERE pes_id = '". $datos['pes_id'] ."';";

      $res = $_obj_database->ejecutarSql($sql); 
    
      if (!$res) {
          $_obj_database->cancelarTransaccion();
          $_db_obj['see']->cancelarTransaccion();
          return -1003;
      }
    
      if($res == 1){
          return 'PE-2';
      } else {
          return 'U-20';
      }
    
    }
}
?>