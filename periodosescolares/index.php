<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("PeriodosEscolares.php");

$obj_usuarios = new Usuarios();
$obj_periodos_escolares = new PeriodosEscolares();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");


$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'];

switch($datos['accion'])
{
	case 'periodos_escolares'.validarAcceso(array(1)):
		$contenido = $obj_periodos_escolares->abrirFormularioPeriodosEscolares($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'actPeriodosAcademicos'.validarAcceso(array(1)):
		$contenido = $obj_periodos_escolares->actPeriodosAcademicos($datos);
		$_obj_interfaz->asignarContenido($contenido);
		$enlace = "index.php?m=periodosescolares&accion=listar_periodos_escolares&tipo_gestion=3&msg=PE-1";
		header('Location: '.$enlace);
		break;
	case 'listar_periodos_escolares'.validarAcceso(array(1)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_periodos_escolares->listadoPeriodosEscolares($datos);
		$campos = array(
			"pes_id" => "ID", 
			"pes_ano_lectivo" => "Any Lectiu",
			"pes_periodo" => "Trimestre ",
			"pes_fecha_inicio" => "Data Inici",
			"pes_fecha_fin" => "Data Fi"
		);  	

		if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        $opciones['actualizar'] = "usc_usu_id_admin=$usu_id";
		$opciones['eliminar'] = "usc_usu_id_admin=$usu_id";
        
		$datos['sql'] = "SELECT * FROM periodos_escolares ORDER BY pes_id DESC";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['ADMINISTRADOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
		$contenido .= $_obj_interfaz->crearListadoPeriodosAcademicos($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'fm_eliminar'.validarAcceso(array(1)):
		$resultado = $obj_periodos_escolares->EliminarPeriodosEscolares($datos);
		
		$enlace = "index.php?m=periodosescolares&accion=listar_periodos_escolares&tipo_gestion=3&msg=".$resultado."";
		header('Location: '.$enlace);
		
		break;

	case 'fm_editar'.validarAcceso(array(1)):
		$valores = $obj_periodos_escolares->obtenerDatosPeriodosEscolares($datos['pes_id']);
		if(!is_array($valores))
		{
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
		$datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_periodos_escolares->abrirFormularioEditarPeriodosEscolares($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'editarPeriodosEscolares'.validarAcceso(array(1)):
		$resultado = $obj_periodos_escolares->editarPeriodosEscolares($datos);
		if( $resultado=='PE-2' ) 
		{                 
			header('Location: index.php?m=periodosescolares&accion=listar_periodos_escolares&tipo_gestion=1&msg='.$resultado);    
			break;            
		}    
		
		break;
}
?>
