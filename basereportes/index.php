<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Includes/Interfaz.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("BaseReportes.php");

$obj_usuarios = new Usuarios();
$obj_base_reportes = new BaseReportes();
$obj_interfaz = new Interfaz();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarJS("formulario.js");

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
    case 'evolucion_absentismo_p'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Promotor($datos);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'evolucion_absentismo_a'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Admin($datos);
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'evolucion_absentismo_c'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Coor($datos);
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'informeCentroEscolarP'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Promotor($datos);
    
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'informeCentroEscolarA'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Admin($datos);
    
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'informeCentroEscolarC'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Coor($datos);
    
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'cnt_motivo_intervencion_p'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_P($datos);
    
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'cnt_motivo_intervencion_a'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_A($datos);
    
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'cnt_motivo_intervencion_c'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_C($datos);
    
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'tipointervencionA'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_A($datos);
    
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'tipointervencionP'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_P($datos);
    
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'tipointervencionC'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_C($datos);
    
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeTipoIntenvencion_a'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "tin_nombre" => "Tipus D'Intervenció",
            "per_i" => "Total intervencions Primer Trimestre",
            "per_ii" => "Total intervencions Segon Trimestre",
            "per_iii" => "Total intervencions Tercer Trimestre",
            "total" => "Total intervencions Durant El Curs",
		);

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $tipos_intervencion = $obj_base_reportes->obtenerTiposIntervencion();
        $i = 0;
        $reporte = array();
        foreach($tipos_intervencion as $intervencion){
            $datos['tin_id'] = $intervencion['tin_id'];
            $nombre_tin = $intervencion['tin_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiftipoIntervencion_a_c($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "tin_nombre" => $nombre_tin,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeTipoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_A($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeTipoIntenvencion_a_exportar'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "tin_nombre" => "Tipus D'Intervenció",
            "per_i" => "Total intervencions Primer Trimestre",
            "per_ii" => "Total intervencions Segon Trimestre",
            "per_iii" => "Total intervencions Tercer Trimestre",
            "total" => "Total intervencions Durant El Curs",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $tipos_intervencion = $obj_base_reportes->obtenerTiposIntervencion();
        $i = 0;
        $reporte = array();
        foreach($tipos_intervencion as $intervencion){
            $datos['tin_id'] = $intervencion['tin_id'];
            $nombre_tin = $intervencion['tin_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiftipoIntervencion_a_c($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "tin_nombre" => $nombre_tin,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeTipoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_AExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeTipoIntenvencion_c'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "tin_nombre" => "Tipus D'Intervenció",
            "per_i" => "Total intervencions Primer Trimestre",
            "per_ii" => "Total intervencions Segon Trimestre",
            "per_iii" => "Total intervencions Tercer Trimestre",
            "total" => "Total intervencions Durant El Curs",
		);

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $tipos_intervencion = $obj_base_reportes->obtenerTiposIntervencion();
        $i = 0;
        $reporte = array();
        foreach($tipos_intervencion as $intervencion){
            $datos['tin_id'] = $intervencion['tin_id'];
            $nombre_tin = $intervencion['tin_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiftipoIntervencion_a_c($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "tin_nombre" => $nombre_tin,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeTipoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_C($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeTipoIntenvencion_c_exportar'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "tin_nombre" => "Tipus D'Intervenció",
            "per_i" => "Total intervencions Primer Trimestre",
            "per_ii" => "Total intervencions Segon Trimestre",
            "per_iii" => "Total intervencions Tercer Trimestre",
            "total" => "Total intervencions Durant El Curs",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $tipos_intervencion = $obj_base_reportes->obtenerTiposIntervencion();
        $i = 0;
        $reporte = array();
        foreach($tipos_intervencion as $intervencion){
            $datos['tin_id'] = $intervencion['tin_id'];
            $nombre_tin = $intervencion['tin_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiftipoIntervencion_a_c($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "tin_nombre" => $nombre_tin,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeTipoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_CExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeTipoIntenvencion_p'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "tin_nombre" => "Tipus D'Intervenció",
            "per_i" => "Total intervencions Primer Trimestre",
            "per_ii" => "Total intervencions Segon Trimestre",
            "per_iii" => "Total intervencions Tercer Trimestre",
            "total" => "Total intervencions Durant El Curs",
		);

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $tipos_intervencion = $obj_base_reportes->obtenerTiposIntervencion();
        $i = 0;
        $reporte = array();
        foreach($tipos_intervencion as $intervencion){
            $datos['tin_id'] = $intervencion['tin_id'];
            $nombre_tin = $intervencion['tin_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosTipoIntervencion($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosTipoIntervencion($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosTipoIntervencion($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiftipoIntervencion($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "tin_nombre" => $nombre_tin,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeTipoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_P($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeTipoIntenvencion_p_exportar'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "tin_nombre" => "Tipus D'Intervenció",
            "per_i" => "Total intervencions Primer Trimestre",
            "per_ii" => "Total intervencions Segon Trimestre",
            "per_iii" => "Total intervencions Tercer Trimestre",
            "total" => "Total intervencions Durant El Curs",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $tipos_intervencion = $obj_base_reportes->obtenerTiposIntervencion();
        $i = 0;
        $reporte = array();
        foreach($tipos_intervencion as $intervencion){
            $datos['tin_id'] = $intervencion['tin_id'];
            $nombre_tin = $intervencion['tin_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosTipoIntervencion($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosTipoIntervencion($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosTipoIntervencion($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiftipoIntervencion($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "tin_nombre" => $nombre_tin,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeTipoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteTipoIntervencion_PExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeAbsentismo_A'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $campos = array(
            //"est_id" => "Nombre I Cognoms Alumnes",
            "est_id" => "ID Alumne",
            "per_i" => "Absentisme Primer Trimestre",
            "per_ii" => "Absentisme Segon Trimestre",
            "per_iii" => "Absentisme Tercer Trimestre",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        if(empty($anio_lectivo) || empty($centro_escolar)){
            $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Admin($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $alumnos = $obj_base_reportes->obtenerAlumnosPorCentroEscolar($centro_escolar,$anio_lectivo);
        $i = 0;
        $reporte = array();
        foreach ($alumnos as $alumno){
            $id_alumno = $alumno['est_id'];
            $datos['est_id'] = $id_alumno;
            $res_per_1 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "III");
            $reporte[$i] = array(
                "est_id" => $alumno['est_id'],
                "est_nombre" => $alumno['est_nombre'],
                "per_i" => $res_per_1['Absentismo'],
                "obs_per_i" => $res_per_1['observacion_est'],
                "per_ii" => $res_per_2['Absentismo'],
                "obs_per_ii" => $res_per_2['observacion_est'],
                "per_iii" => $res_per_3['Absentismo'],
                "obs_per_iii" => $res_per_3['observacion_est'],
            );
            $i++;
        }

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeAbsentismo($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Admin($datos);
        
		$_obj_interfaz->asignarContenido($contenido);

        break;
    case 'resultadoInformeAbsentismo_A_exportar'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $campos = array(
            //"est_id" => "Nombre I Cognoms Alumnes",
            "est_id" => "ID Alumne",
            "per_i" => "Absentisme Primer Trimestre",
            "per_ii" => "Absentisme Segon Trimestre",
            "per_iii" => "Absentisme Tercer Trimestre",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        if(empty($anio_lectivo) || empty($centro_escolar)){
            $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_AdminExportar($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $alumnos = $obj_base_reportes->obtenerAlumnosPorCentroEscolar($centro_escolar,$anio_lectivo);
        $i = 0;
        $reporte = array();
        foreach ($alumnos as $alumno){
            $id_alumno = $alumno['est_id'];
            $datos['est_id'] = $id_alumno;
            $res_per_1 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "III");
            $reporte[$i] = array(
                "est_id" => $alumno['est_id'],
                "est_nombre" => $alumno['est_nombre'],
                "per_i" => $res_per_1['Absentismo'],
                "obs_per_i" => $res_per_1['observacion_est'],
                "per_ii" => $res_per_2['Absentismo'],
                "obs_per_ii" => $res_per_2['observacion_est'],
                "per_iii" => $res_per_3['Absentismo'],
                "obs_per_iii" => $res_per_3['observacion_est'],
            );
            $i++;
        }

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeAbsentismo($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_AdminExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);

        break;
    case 'resultadoInformeAbsentismo_C'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $campos = array(
            //"est_id" => "Nombre I Cognoms Alumnes",
            "est_id" => "ID Alumne",
            "per_i" => "Absentisme Primer Trimestre",
            "per_ii" => "Absentisme Segon Trimestre",
            "per_iii" => "Absentisme Tercer Trimestre",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        if(empty($anio_lectivo) || empty($centro_escolar)){
            $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Coor($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $alumnos = $obj_base_reportes->obtenerAlumnosPorCentroEscolar($centro_escolar,$anio_lectivo);
        $i = 0;
        $reporte = array();
        foreach ($alumnos as $alumno){
            $id_alumno = $alumno['est_id'];
            $datos['est_id'] = $id_alumno;
            $res_per_1 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "III");
            $reporte[$i] = array(
                "est_id" => $alumno['est_id'],
                "est_nombre" => $alumno['est_nombre'],
                "per_i" => $res_per_1['Absentismo'],
                "obs_per_i" => $res_per_1['observacion_est'],
                "per_ii" => $res_per_2['Absentismo'],
                "obs_per_ii" => $res_per_2['observacion_est'],
                "per_iii" => $res_per_3['Absentismo'],
                "obs_per_iii" => $res_per_3['observacion_est'],
            );
            $i++;
        }        

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeAbsentismo($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Coor($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeAbsentismo_C_exportar'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $campos = array(
            //"est_id" => "Nombre I Cognoms Alumnes",
            "est_id" => "ID Alumne",
            "per_i" => "Absentisme Primer Trimestre",
            "per_ii" => "Absentisme Segon Trimestre",
            "per_iii" => "Absentisme Tercer Trimestre",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        if(empty($anio_lectivo) || empty($centro_escolar)){
            $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Coor_exportar($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $alumnos = $obj_base_reportes->obtenerAlumnosPorCentroEscolar($centro_escolar,$anio_lectivo);
        $i = 0;
        $reporte = array();
        foreach ($alumnos as $alumno){
            $id_alumno = $alumno['est_id'];
            $datos['est_id'] = $id_alumno;
            $res_per_1 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "III");
            $reporte[$i] = array(
                "est_id" => $alumno['est_id'],
                "est_nombre" => $alumno['est_nombre'],
                "per_i" => $res_per_1['Absentismo'],
                "obs_per_i" => $res_per_1['observacion_est'],
                "per_ii" => $res_per_2['Absentismo'],
                "obs_per_ii" => $res_per_2['observacion_est'],
                "per_iii" => $res_per_3['Absentismo'],
                "obs_per_iii" => $res_per_3['observacion_est'],
            );
            $i++;
        }        

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeAbsentismo($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Coor_exportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeAbsentismo'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $campos = array(
            //"est_id" => "Nombre I Cognoms Alumnes",
            "est_id" => "ID Alumne",
            "per_i" => "Absentisme Primer Trimestre",
            "per_ii" => "Absentisme Segon Trimestre",
            "per_iii" => "Absentisme Tercer Trimestre",
		);

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        if(empty($anio_lectivo) || empty($centro_escolar)){
            $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Promotor($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }
        
        $alumnos = $obj_base_reportes->obtenerAlumnosPorCentroEscolar($centro_escolar,$anio_lectivo);
        $i = 0;
        $reporte = array();
        if(!empty($alumnos)) : 
        foreach ($alumnos as $alumno){
            $id_alumno = $alumno['est_id'];
            $datos['est_id'] = $id_alumno;
            $res_per_1 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "III");
            $reporte[$i] = array( 
                "est_id" => $alumno['est_id'],
                "est_nombre" => $alumno['est_nombre'],
                "per_i" => $res_per_1['Absentismo'],
                "obs_per_i" => $res_per_1['observacion_est'],
                "per_ii" => $res_per_2['Absentismo'],
                "obs_per_ii" => $res_per_2['observacion_est'],
                "per_iii" => $res_per_3['Absentismo'],
                "obs_per_iii" => $res_per_3['observacion_est'],
            );
            $i++;
        }        
        endif;
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeAbsentismo($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_Promotor($datos);
        
		$_obj_interfaz->asignarContenido($contenido);

        break;
    case 'resultadoInformeAbsentismoExportar'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $campos = array(
            //"est_id" => "Nombre I Cognoms Alumnes",
            "est_id" => "ID Alumne",
            "per_i" => "Absentisme Primer Trimestre",
            "per_ii" => "Absentisme Segon Trimestre",
            "per_iii" => "Absentisme Tercer Trimestre",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        if(empty($anio_lectivo) || empty($centro_escolar)){
            $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_PromotorExportar($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }
        
        $alumnos = $obj_base_reportes->obtenerAlumnosPorCentroEscolar($centro_escolar,$anio_lectivo);
        $i = 0;
        $reporte = array();
        foreach ($alumnos as $alumno){
            $id_alumno = $alumno['est_id'];
            $datos['est_id'] = $id_alumno;
            $res_per_1 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerAbsentismoPorPeriodoAlumn($datos,$periodo = "III");
            $reporte[$i] = array( 
                "est_id" => $alumno['est_id'],
                "est_nombre" => $alumno['est_nombre'],
                "per_i" => $res_per_1['Absentismo'],
                "obs_per_i" => $res_per_1['observacion_est'],
                "per_ii" => $res_per_2['Absentismo'],
                "obs_per_ii" => $res_per_2['observacion_est'],
                "per_iii" => $res_per_3['Absentismo'],
                "obs_per_iii" => $res_per_3['observacion_est'],
            );
            $i++;
        }        

        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeAbsentismo($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioEvoAbsentismo_PromotorExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);

        break;
    case 'resultadoInformeMotivoIntenvencion'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "min_nombre" => "Motiu D'Intervenció",
            "per_i" => "Nombre D'Aumnes Atesos Primer Trimestre",
            "per_ii" => "Nombre D'Aumnes Atesos Segon Trimestre",
            "per_iii" => "Nombre D'Aumnes Atesos Tercer Trimestre",
            "total" => "Nombre D'Aumnes Atesos Durant El Curs Escolar",
		);

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $motivos_intervencion = $obj_base_reportes->obtenerMotivosIntervencion();
        $i = 0;
        $reporte = array();
        foreach($motivos_intervencion as $intervencion){
            $datos['min_id'] = $intervencion['min_id'];
            $nombre_min = $intervencion['min_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiferentesMotivoIntervencion($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "min_nombre" => $nombre_min,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeMotivoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_P($datos);
        
		$_obj_interfaz->asignarContenido($contenido);

        break;
    case 'resultadoInformeMotivoIntenvencion_exportar'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "min_nombre" => "Motiu D'Intervenció",
            "per_i" => "Nombre D'Aumnes Atesos Primer Trimestre",
            "per_ii" => "Nombre D'Aumnes Atesos Segon Trimestre",
            "per_iii" => "Nombre D'Aumnes Atesos Tercer Trimestre",
            "total" => "Nombre D'Aumnes Atesos Durant El Curs Escolar",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $motivos_intervencion = $obj_base_reportes->obtenerMotivosIntervencion();
        $i = 0;
        $reporte = array();
        foreach($motivos_intervencion as $intervencion){
            $datos['min_id'] = $intervencion['min_id'];
            $nombre_min = $intervencion['min_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiferentesMotivoIntervencion($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "min_nombre" => $nombre_min,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeMotivoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_PExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);

        break;
    case 'resultadoInformeMotivoIntenvencion_a'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "min_nombre" => "Motiu D'Intervenció",
            "per_i" => "Nombre D'Aumnes Atesos Primer Trimestre",
            "per_ii" => "Nombre D'Aumnes Atesos Segon Trimestre",
            "per_iii" => "Nombre D'Aumnes Atesos Tercer Trimestre",
            "total" => "Nombre D'Aumnes Atesos Durant El Curs Escolar",
		);

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $motivos_intervencion = $obj_base_reportes->obtenerMotivosIntervencion();
        $i = 0;
        $reporte = array();
        foreach($motivos_intervencion as $intervencion){
            $datos['min_id'] = $intervencion['min_id'];
            $nombre_min = $intervencion['min_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiferentesMotivoIntervencion($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "min_nombre" => $nombre_min,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeMotivoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_A($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeMotivoIntenvencion_a_exportar'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "min_nombre" => "Motiu D'Intervenció",
            "per_i" => "Nombre D'Aumnes Atesos Primer Trimestre",
            "per_ii" => "Nombre D'Aumnes Atesos Segon Trimestre",
            "per_iii" => "Nombre D'Aumnes Atesos Tercer Trimestre",
            "total" => "Nombre D'Aumnes Atesos Durant El Curs Escolar",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $motivos_intervencion = $obj_base_reportes->obtenerMotivosIntervencion();
        $i = 0;
        $reporte = array();
        foreach($motivos_intervencion as $intervencion){
            $datos['min_id'] = $intervencion['min_id'];
            $nombre_min = $intervencion['min_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiferentesMotivoIntervencion($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "min_nombre" => $nombre_min,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeMotivoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_AExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeMotivoIntenvencion_c'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "min_nombre" => "Motiu D'Intervenció",
            "per_i" => "Nombre D'Aumnes Atesos Primer Trimestre",
            "per_ii" => "Nombre D'Aumnes Atesos Segon Trimestre",
            "per_iii" => "Nombre D'Aumnes Atesos Tercer Trimestre",
            "total" => "Nombre D'Aumnes Atesos Durant El Curs Escolar",
		);

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $motivos_intervencion = $obj_base_reportes->obtenerMotivosIntervencion();
        $i = 0;
        $reporte = array();
        foreach($motivos_intervencion as $intervencion){
            $datos['min_id'] = $intervencion['min_id'];
            $nombre_min = $intervencion['min_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiferentesMotivoIntervencion_a_c($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "min_nombre" => $nombre_min,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
	
        $tabla_result = $_obj_interfaz->crearListadoInformeMotivoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_C($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeMotivoIntenvencion_c_exportar'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        } 

        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];
        $anio_lectivo = $datos['per_academico'];
        $centro_escolar = $datos['ces_id'];

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);
        $nombre_escolar = $obj_base_reportes->obtenerNombreCentroEscolar($centro_escolar);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $anio_lectivo,
            "centro_escolar" => $nombre_escolar['ces_nombre']
        );

        $campos = array(
            "min_nombre" => "Motiu D'Intervenció",
            "per_i" => "Nombre D'Aumnes Atesos Primer Trimestre",
            "per_ii" => "Nombre D'Aumnes Atesos Segon Trimestre",
            "per_iii" => "Nombre D'Aumnes Atesos Tercer Trimestre",
            "total" => "Nombre D'Aumnes Atesos Durant El Curs Escolar",
        );

        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['usu_id'] = $usu_id;
        $motivos_intervencion = $obj_base_reportes->obtenerMotivosIntervencion();
        $i = 0;
        $reporte = array();
        foreach($motivos_intervencion as $intervencion){
            $datos['min_id'] = $intervencion['min_id'];
            $nombre_min = $intervencion['min_nombre'];
            $res_per_1 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "I");
            $res_per_2 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "II");
            $res_per_3 = $obj_base_reportes->obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = "III");

            //$total = $res_per_1['cnt'] + $res_per_2['cnt'] + $res_per_3['cnt'];
            $total = $obj_base_reportes->obtenerCntNinosDiferentesMotivoIntervencion_a_c($datos);
            $total = $total['cnt'];
            $reporte[$i] = array(
                "min_nombre" => $nombre_min,
                "per_i" => $res_per_1['cnt'],
                "per_ii" => $res_per_2['cnt'],
                "per_iii" => $res_per_3['cnt'],
                "total" => $total,
            );
            $i++;
        }

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $reporte;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
    
        $tabla_result = $_obj_interfaz->crearListadoInformeMotivoIntervencion($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCantidadXIntervencion_CExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeCentro'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  

        $per_academico = $datos['per_academico'];

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $per_academico,
        );

        if($per_academico == NULL){
            $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Promotor($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $campos = array(
            "ces_nombre" => "Centre Educatiu",
            "rce_graduados_eso_ninos" => "Nois",
            "rce_graduados_eso_ninas" => "Noies",
            "total_graduados_eso" => "Total",
            "rce_haran_bachillerato_ninos" => "Nois",
            "rce_haran_bachillerato_ninas" => "Noies",
            "total_haran_bachillerato" => "Total",
            "rce_haran_cfgm_ninos" => "Nois",
            "rce_haran_cfgm_ninas" => "Noies",
            "total_haran_cfgm" => "Total",
            "rce_haran_pfi_ninos" => "Nois",
            "rce_haran_pfi_ninas" => "Noies",
            "total_haran_pfi" => "Total",
            "rce_graduados_2n_bachillerato_ninos" => "Nois",
            "rce_graduados_2n_bachillerato_ninas" => "Noies",
            "total_graduados_bachillerato" => "Total",
            "rce_ingresen_universidad_ninos" => "Nois",
            "rce_ingresen_universidad_ninas" => "Noies",
            "total_ingresen_universidad" => "Total",
            "periodo_lectivo" => "Períodes Acadèmics", 
		);  	
        
        	
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        	
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
		$datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, rce_graduados_eso_ninos, rce_graduados_eso_ninas, 
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, 
		  rce_haran_bachillerato_ninos, rce_haran_bachillerato_ninas, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        rce_haran_cfgm_ninos, rce_haran_cfgm_ninas, (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm,
		  rce_haran_pfi_ninos, rce_haran_pfi_ninas, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi,
        rce_graduados_2n_bachillerato_ninos, rce_graduados_2n_bachillerato_ninas, 
		  (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total_graduados_bachillerato, 
        rce_ingresen_universidad_ninos, rce_ingresen_universidad_ninas, (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_ingresen_universidad,
		  CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo
        FROM reporte_centro_educativo AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN usuario_centro_escolar as usc
        ON usc.usc_ces_id = ces.ces_id
        WHERE usc.usc_usu_id_promotor = $usu_id
        AND pes.pes_ano_lectivo LIKE '".$per_academico."';";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
        $tabla_result = $_obj_interfaz->crearListadoInformeCentro($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Promotor($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeCentro_exportar'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  

        $per_academico = $datos['per_academico'];

        $datos['FORMATO_BUSQ'] = array(
            "anio_lectivo" => $per_academico,
        );

        if($per_academico == NULL){
            $contenido = $obj_base_reportes->abrirFormularioReporteCentro_PromotorExportar($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $campos = array(
            "ces_nombre" => "Centre Educatiu",
            "rce_graduados_eso_ninos" => "Nois",
            "rce_graduados_eso_ninas" => "Noies",
            "total_graduados_eso" => "Total",
            "rce_haran_bachillerato_ninos" => "Nois",
            "rce_haran_bachillerato_ninas" => "Noies",
            "total_haran_bachillerato" => "Total",
            "rce_haran_cfgm_ninos" => "Nois",
            "rce_haran_cfgm_ninas" => "Noies",
            "total_haran_cfgm" => "Total",
            "rce_haran_pfi_ninos" => "Nois",
            "rce_haran_pfi_ninas" => "Noies",
            "total_haran_pfi" => "Total",
            "rce_graduados_2n_bachillerato_ninos" => "Nois",
            "rce_graduados_2n_bachillerato_ninas" => "Noies",
            "total_graduados_bachillerato" => "Total",
            "rce_ingresen_universidad_ninos" => "Nois",
            "rce_ingresen_universidad_ninas" => "Noies",
            "total_ingresen_universidad" => "Total",
            "periodo_lectivo" => "Períodes Acadèmics", 
        );  	
        
            
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
            
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        $opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
        $datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, rce_graduados_eso_ninos, rce_graduados_eso_ninas, 
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, 
            rce_haran_bachillerato_ninos, rce_haran_bachillerato_ninas, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        rce_haran_cfgm_ninos, rce_haran_cfgm_ninas, (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm,
            rce_haran_pfi_ninos, rce_haran_pfi_ninas, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi,
        rce_graduados_2n_bachillerato_ninos, rce_graduados_2n_bachillerato_ninas, 
            (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total_graduados_bachillerato, 
        rce_ingresen_universidad_ninos, rce_ingresen_universidad_ninas, (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_ingresen_universidad,
            CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo
        FROM reporte_centro_educativo AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN usuario_centro_escolar as usc
        ON usc.usc_ces_id = ces.ces_id
        WHERE usc.usc_usu_id_promotor = $usu_id
        AND pes.pes_ano_lectivo LIKE '".$per_academico."';";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
        $arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
    
        $tabla_result = $_obj_interfaz->crearListadoInformeCentro($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_PromotorExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeCentroAdmin'.validarAcceso(array(1,2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  

        $per_academico = $datos['per_academico'];
        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];

        if($per_academico == NULL || $servicio_territorial == NULL || $territorio == NULL){
            $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Admin($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $per_academico,
        );

        $campos = array(
            "ces_nombre" => "Centre Educatiu",
            "rce_graduados_eso_ninos" => "Nois",
            "rce_graduados_eso_ninas" => "Noies",
            "total_graduados_eso" => "Total",
            "rce_haran_bachillerato_ninos" => "Nois",
            "rce_haran_bachillerato_ninas" => "Noies",
            "total_haran_bachillerato" => "Total",
            "rce_haran_cfgm_ninos" => "Nois",
            "rce_haran_cfgm_ninas" => "Noies",
            "total_haran_cfgm" => "Total",
            "rce_haran_pfi_ninos" => "Nois",
            "rce_haran_pfi_ninas" => "Noies",
            "total_haran_pfi" => "Total",
            "rce_graduados_2n_bachillerato_ninos" => "Nois",
            "rce_graduados_2n_bachillerato_ninas" => "Noies",
            "total_graduados_bachillerato" => "Total",
            "rce_ingresen_universidad_ninos" => "Nois",
            "rce_ingresen_universidad_ninas" => "Noies",
            "total_ingresen_universidad" => "Total",
            "periodo_lectivo" => "Períodes Acadèmics", 
		);  	
        
        	
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        	
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
		$datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, rce_graduados_eso_ninos, rce_graduados_eso_ninas, 
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, 
		  rce_haran_bachillerato_ninos, rce_haran_bachillerato_ninas, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        rce_haran_cfgm_ninos, rce_haran_cfgm_ninas, (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm,
		  rce_haran_pfi_ninos, rce_haran_pfi_ninas, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi,
        rce_graduados_2n_bachillerato_ninos, rce_graduados_2n_bachillerato_ninas, 
		  (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total_graduados_bachillerato, 
        rce_ingresen_universidad_ninos, rce_ingresen_universidad_ninas, (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_ingresen_universidad,
		  CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo
        FROM reporte_centro_educativo AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN territorio AS ter
        ON ter.ter_id = ces.ces_ter_id
        INNER JOIN servicio_territorial AS ste
        ON ste.ste_id = ter.ter_ste_id
        WHERE pes.pes_ano_lectivo LIKE '$per_academico'
        AND ste.ste_id = $servicio_territorial
        AND ter.ter_id = $territorio;";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
        $tabla_result = $_obj_interfaz->crearListadoInformeCentro($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Admin($datos);
        
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeCentroAdmin_exportar'.validarAcceso(array(1,2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  

        $per_academico = $datos['per_academico'];
        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];

        if($per_academico == NULL || $servicio_territorial == NULL || $territorio == NULL){
            $contenido = $obj_base_reportes->abrirFormularioReporteCentro_AdminExportar($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $per_academico,
        );

        $campos = array(
            "ces_nombre" => "Centre Educatiu",
            "rce_graduados_eso_ninos" => "Nois",
            "rce_graduados_eso_ninas" => "Noies",
            "total_graduados_eso" => "Total",
            "rce_haran_bachillerato_ninos" => "Nois",
            "rce_haran_bachillerato_ninas" => "Noies",
            "total_haran_bachillerato" => "Total",
            "rce_haran_cfgm_ninos" => "Nois",
            "rce_haran_cfgm_ninas" => "Noies",
            "total_haran_cfgm" => "Total",
            "rce_haran_pfi_ninos" => "Nois",
            "rce_haran_pfi_ninas" => "Noies",
            "total_haran_pfi" => "Total",
            "rce_graduados_2n_bachillerato_ninos" => "Nois",
            "rce_graduados_2n_bachillerato_ninas" => "Noies",
            "total_graduados_bachillerato" => "Total",
            "rce_ingresen_universidad_ninos" => "Nois",
            "rce_ingresen_universidad_ninas" => "Noies",
            "total_ingresen_universidad" => "Total",
            "periodo_lectivo" => "Períodes Acadèmics", 
        );  	
        
            
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
            
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        $opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
        $datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, rce_graduados_eso_ninos, rce_graduados_eso_ninas, 
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, 
            rce_haran_bachillerato_ninos, rce_haran_bachillerato_ninas, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        rce_haran_cfgm_ninos, rce_haran_cfgm_ninas, (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm,
            rce_haran_pfi_ninos, rce_haran_pfi_ninas, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi,
        rce_graduados_2n_bachillerato_ninos, rce_graduados_2n_bachillerato_ninas, 
            (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total_graduados_bachillerato, 
        rce_ingresen_universidad_ninos, rce_ingresen_universidad_ninas, (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_ingresen_universidad,
            CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo
        FROM reporte_centro_educativo AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN territorio AS ter
        ON ter.ter_id = ces.ces_ter_id
        INNER JOIN servicio_territorial AS ste
        ON ste.ste_id = ter.ter_ste_id
        WHERE pes.pes_ano_lectivo LIKE '$per_academico'
        AND ste.ste_id = $servicio_territorial
        AND ter.ter_id = $territorio;";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
        $arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
    
        $tabla_result = $_obj_interfaz->crearListadoInformeCentro($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_AdminExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeCentroCoor'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  

        $per_academico = $datos['per_academico'];
        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];

        if($per_academico == NULL || $servicio_territorial == NULL || $territorio == NULL){
            $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Coor($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $per_academico,
        );

        $campos = array(
            "ces_nombre" => "Centre Educatiu",
            "rce_graduados_eso_ninos" => "Nois",
            "rce_graduados_eso_ninas" => "Noies",
            "total_graduados_eso" => "Total",
            "rce_haran_bachillerato_ninos" => "Nois",
            "rce_haran_bachillerato_ninas" => "Noies",
            "total_haran_bachillerato" => "Total",
            "rce_haran_cfgm_ninos" => "Nois",
            "rce_haran_cfgm_ninas" => "Noies",
            "total_haran_cfgm" => "Total",
            "rce_haran_pfi_ninos" => "Nois",
            "rce_haran_pfi_ninas" => "Noies",
            "total_haran_pfi" => "Total",
            "rce_graduados_2n_bachillerato_ninos" => "Nois",
            "rce_graduados_2n_bachillerato_ninas" => "Noies",
            "total_graduados_bachillerato" => "Total",
            "rce_ingresen_universidad_ninos" => "Nois",
            "rce_ingresen_universidad_ninas" => "Noies",
            "total_ingresen_universidad" => "Total",
            "periodo_lectivo" => "Períodes Acadèmics", 
		);  	
        
            
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
            
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        $opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
        $datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, rce_graduados_eso_ninos, rce_graduados_eso_ninas, 
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, 
		  rce_haran_bachillerato_ninos, rce_haran_bachillerato_ninas, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        rce_haran_cfgm_ninos, rce_haran_cfgm_ninas, (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm,
		  rce_haran_pfi_ninos, rce_haran_pfi_ninas, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi,
        rce_graduados_2n_bachillerato_ninos, rce_graduados_2n_bachillerato_ninas, 
		  (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total_graduados_bachillerato, 
        rce_ingresen_universidad_ninos, rce_ingresen_universidad_ninas, (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_ingresen_universidad,
		  CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo
        FROM reporte_centro_educativo AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN territorio AS ter
        ON ter.ter_id = ces.ces_ter_id
        INNER JOIN servicio_territorial AS ste
        ON ste.ste_id = ter.ter_ste_id
        WHERE pes.pes_ano_lectivo LIKE '$per_academico'
        AND ste.ste_id = $servicio_territorial
        AND ter.ter_id = $territorio;";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
        $arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
    
        $tabla_result = $_obj_interfaz->crearListadoInformeCentro($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_Coor($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'resultadoInformeCentroCoor_exportar'.validarAcceso(array(4)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];          
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  

        $per_academico = $datos['per_academico'];
        $servicio_territorial = $datos['ste_id'];
        $territorio = $datos['territorio_id'];

        if($per_academico == NULL || $servicio_territorial == NULL || $territorio == NULL){
            $contenido = $obj_base_reportes->abrirFormularioReporteCentro_CoorExportar($datos);
            $_obj_interfaz->asignarContenido($contenido);
            break;
        }

        $nombre_servicio = $obj_base_reportes->obtenerNombreServicioTerr($servicio_territorial);
        $nombre_territorio = $obj_base_reportes->obtenerNombreTerritorio($territorio);

        $datos['FORMATO_BUSQ'] = array(
            "servicio_territorial" => $nombre_servicio['ste_nombre'],
            "territorio" => $nombre_territorio['ter_nombre'],
            "anio_lectivo" => $per_academico,
        );

        $campos = array(
            "ces_nombre" => "Centre Educatiu",
            "rce_graduados_eso_ninos" => "Nois",
            "rce_graduados_eso_ninas" => "Noies",
            "total_graduados_eso" => "Total",
            "rce_haran_bachillerato_ninos" => "Nois",
            "rce_haran_bachillerato_ninas" => "Noies",
            "total_haran_bachillerato" => "Total",
            "rce_haran_cfgm_ninos" => "Nois",
            "rce_haran_cfgm_ninas" => "Noies",
            "total_haran_cfgm" => "Total",
            "rce_haran_pfi_ninos" => "Nois",
            "rce_haran_pfi_ninas" => "Noies",
            "total_haran_pfi" => "Total",
            "rce_graduados_2n_bachillerato_ninos" => "Nois",
            "rce_graduados_2n_bachillerato_ninas" => "Noies",
            "total_graduados_bachillerato" => "Total",
            "rce_ingresen_universidad_ninos" => "Nois",
            "rce_ingresen_universidad_ninas" => "Noies",
            "total_ingresen_universidad" => "Total",
            "periodo_lectivo" => "Períodes Acadèmics", 
        );  	
        
            
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
            
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        $opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
        $datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, rce_graduados_eso_ninos, rce_graduados_eso_ninas, 
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, 
            rce_haran_bachillerato_ninos, rce_haran_bachillerato_ninas, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        rce_haran_cfgm_ninos, rce_haran_cfgm_ninas, (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm,
            rce_haran_pfi_ninos, rce_haran_pfi_ninas, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi,
        rce_graduados_2n_bachillerato_ninos, rce_graduados_2n_bachillerato_ninas, 
            (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total_graduados_bachillerato, 
        rce_ingresen_universidad_ninos, rce_ingresen_universidad_ninas, (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_ingresen_universidad,
            CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo
        FROM reporte_centro_educativo AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN territorio AS ter
        ON ter.ter_id = ces.ces_ter_id
        INNER JOIN servicio_territorial AS ste
        ON ste.ste_id = ter.ter_ste_id
        WHERE pes.pes_ano_lectivo LIKE '$per_academico'
        AND ste.ste_id = $servicio_territorial
        AND ter.ter_id = $territorio;";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
        $arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
    
        $tabla_result = $_obj_interfaz->crearListadoInformeCentro($arreglo_datos);

        $datos["result_table_centre"]= $tabla_result;
        $contenido = $obj_base_reportes->abrirFormularioReporteCentro_CoorExportar($datos);
        
        $_obj_interfaz->asignarContenido($contenido);
        break;
    case 'obtenerTerritorios'.validarAcceso(array(1,4)):
        $perfil = $_SESSION['tipo_usuario'];
        if($perfil == "4"){
            $resultado = $obj_base_reportes->obtenerSelectFormaTerritoriosCoor($datos);
        }else{
            $resultado = $obj_base_reportes->obtenerSelectFormaTerritorios($datos);
        }
        print_r($resultado);
        exit;
        break;
    case 'obtenerCentrosPorTerritorio'.validarAcceso(array(1,4)):
        $resultado = $obj_base_reportes->obteberSelectFormaCentrosEscolares_Admin($datos);
        print_r($resultado);
        exit;
        break;
    case 'registrarreporte'.validarAcceso(array(2)):
        $resultado = $obj_base_reportes->registrarreporte($datos);
        if( $resultado=='RPCT-01' ) 
		{   
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=reportecentro&accion=fm_registrar&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php
            $datos['msg'] = $resultado;
            $datos['tipo_gestion'] = 1;
            $contenido = $obj_base_reportes->abrirFormularioRegistrarreporte2($datos);
            $_obj_interfaz->asignarContenido($contenido);
			//header('Location: index.php?m=reportecentro&accion=fm_registrar&tipo_gestion=1&msg=RPCT-01');    
			break;            
		}//Fin de if( $resultado==1 )      
		else {
            $opciones['CADENA'] = "El informe";      
			$opciones['ACCION'] = 1;//ingresar      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);  
			$contenido = $obj_base_reportes->abrirFormularioRegistrarreporte2($datos);
			$_obj_interfaz->asignarContenido($contenido);
        }                
        //$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'obtenerCursosEtapaEducativa'.validarAcceso(array(2)):
        $resultado = $obj_base_reportes->obteberSelectFormaCurso($datos);
        print_r($resultado);
        exit;
        break;    
    case 'listarreporte'.validarAcceso(array(2)):
        if(isset($datos['msg'])){
            $opciones['ACCION'] = $datos['tipo_gestion'];
            $opciones['CADENA'] = 'El usuario';
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }
        $contenido = $obj_base_reportes->listadoreporte($datos);
		
        $campos = array(
            "ces_nombre" => "Centre Educatiu",
			"periodo_lectivo" => "Períodes Acadèmics", 
			"total_graduados_eso" => "Total graduen 4t d’ESO",
            "total_haran_bachillerato" => "Total faran batxillerat",
            "total_haran_cfgm" => "Total faran un CFGM",
            "total_haran_pfi" => "Total faran un PFI",
            "total__graduados_2n_bachillerato" => "Total graduen 2n Batxillerat",
            "total_universidad" => "Total ingressen en la Universitat",
            "rce_ob_estudios" => "Estudis universitaris",
		);  	
        
        	
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        	
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
		$datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo,
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi, 
		  (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total__graduados_2n_bachillerato, 
		  (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_universidad, rce_ob_estudios
        FROM reporte_centro_educativo_2 AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN usuario_centro_escolar as usc
        ON usc.usc_ces_id = ces.ces_id
        WHERE usc.usc_usu_id_promotor = ".$usu_id.";";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
		$contenido .= $_obj_interfaz->crearListadoreporte($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'fm_editar'.validarAcceso(array(2)):
        $valores = $obj_base_reportes->obtenerDatosReporte($datos['rce_id']);
        if( !is_array($valores))
		{
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
        $datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_base_reportes->abrirFormularioEditarreporte($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'editarreporte'.validarAcceso(array(2)):
        $resultado = $obj_base_reportes->editarreporte($datos);
        if( $resultado=='RPCT-02' ) 
        {         
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=reportecentro&accion=listarreporte&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php        
            //header('Location: index.php?m=reportecentro&accion=listarreporte&tipo_gestion=1&msg='.$resultado);    
            break;            
        }//Fin de if( $resultado==1 )      
        
        break;
    case 'fm_eliminar'.validarAcceso(array(2)):
        $resultado = $obj_base_reportes->Eliminarreporte($datos);
        ?>
        <script type="text/javascript"> 
            window.location="index.php?m=reportecentro&accion=listarreporte&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
        </script> 
        <?php
        //$enlace = "index.php?m=reportecentro&accion=listarreporte&tipo_gestion=3&msg=".$resultado."";
		//header('Location: '.$enlace);
		
        break;
}

?>