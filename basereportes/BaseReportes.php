<?php

class BaseReportes{

    var $_plantillas = "";

    function __construct() {
        global $_PATH_SERVIDOR;
        $this->_plantillas = $_PATH_SERVIDOR."/basereportes/Plantillas";
    }

    /**
     * BDL:: 03-02-2021
     * Funcion : abrirFormularioEvoAbsentismo_Promotor
     * Parametros: $datos
     * Contenido Reporte de Evolucion de absentismo 1er a 3er trimestre
    */

    function abrirFormularioEvoAbsentismo_Promotor($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_evolucion_absentismo_p.html");
        
        $label_inicio = "Dades de l'absentisme durant el curs";
        $msg_descripcion = "Dades de l'absentisme durant el curs";
        $breadcump_informe_centro = "Dades de l'absentisme durant el curs";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'evolucion_absentismo_p1' && $datos['accion'] != 'evolucion_absentismo_p2' && $datos['accion'] != 'evolucion_absentismo_p4'){
            if($datos['accion'] == 'resultadoInformeAbsentismo1' || $datos['accion'] == 'resultadoInformeAbsentismo2' || $datos['accion'] == 'resultadoInformeAbsentismo4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeAbsentismoExportar&ces_id='.$datos['ces_id'].'&per_academico='.$datos['per_academico'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * SA:: 26-03-2021
     * Funcion : abrirFormularioEvoAbsentismo_PromotorExportar
     * Parametros: $datos
     * Contenido Reporte de Evolucion de absentismo 1er a 3er trimestre
    */

    function abrirFormularioEvoAbsentismo_PromotorExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/evolucion_absentismo_p_exportar.html");
        
        $label_inicio = "Dades de l'absentisme durant el curs";
        $msg_descripcion = "Dades de l'absentisme durant el curs";
        $breadcump_informe_centro = "Dades de l'absentisme durant el curs";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'evolucion_absentismo_p1' && $datos['accion'] != 'evolucion_absentismo_p2' && $datos['accion'] != 'evolucion_absentismo_p4'){
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * BDL:: 03-02-2021
     * Funcion : abrirFormularioEvoAbsentismo_Admin
     * Parametros: $datos
     * Contenido Reporte de Evolucion de absentismo 1er a 3er trimestre
    */

    function abrirFormularioEvoAbsentismo_Admin($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_evolucion_absentismo_a.html");
        
        $label_inicio = "Dades de l'absentisme durant el curs";
        $msg_descripcion = "Dades de l'absentisme durant el curs";
        $breadcump_informe_centro = "Dades de l'absentisme durant el curs";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTer($datos);
        $select_territorio = $this->obtenerSelectFormaTerritorios($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'evolucion_absentismo_a1' && $datos['accion'] != 'evolucion_absentismo_a2' && $datos['accion'] != 'evolucion_absentismo_a4'){
            if($datos['accion'] == 'resultadoInformeAbsentismo_A1' || $datos['accion'] == 'resultadoInformeAbsentismo_A2' || $datos['accion'] == 'resultadoInformeAbsentismo_A4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeAbsentismo_A_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * SA:: 26-03-2021
     * Funcion : abrirFormularioEvoAbsentismo_AdminExportar
     * Parametros: $datos
     * Contenido Reporte de Evolucion de absentismo 1er a 3er trimestre
    */

    function abrirFormularioEvoAbsentismo_AdminExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/evolucion_absentismo_a_exportar.html");
        
        $label_inicio = "Dades de l'absentisme durant el curs";
        $msg_descripcion = "Dades de l'absentisme durant el curs";
        $breadcump_informe_centro = "Dades de l'absentisme durant el curs";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTer($datos);
        $select_territorio = $this->obtenerSelectFormaTerritorios($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'evolucion_absentismo_a1' && $datos['accion'] != 'evolucion_absentismo_a2' && $datos['accion'] != 'evolucion_absentismo_a4'){
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    function abrirFormularioEvoAbsentismo_Coor($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_evolucion_absentismo_c.html");
        
        $label_inicio = "Dades de l'absentisme durant el curs";
        $msg_descripcion = "Dades de l'absentisme durant el curs";
        $breadcump_informe_centro = "Dades de l'absentisme durant el curs";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'evolucion_absentismo_c1' && $datos['accion'] != 'evolucion_absentismo_c2' && $datos['accion'] != 'evolucion_absentismo_c4'){
            if($datos['accion'] == 'resultadoInformeAbsentismo_C1' || $datos['accion'] == 'resultadoInformeAbsentismo_C2' || $datos['accion'] == 'resultadoInformeAbsentismo_C4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeAbsentismo_C_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    function abrirFormularioEvoAbsentismo_Coor_exportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/evolucion_absentismo_c_exportar.html");
        
        $label_inicio = "Dades de l'absentisme durant el curs";
        $msg_descripcion = "Dades de l'absentisme durant el curs";
        $breadcump_informe_centro = "Dades de l'absentisme durant el curs";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'evolucion_absentismo_c1' && $datos['accion'] != 'evolucion_absentismo_c2' && $datos['accion'] != 'evolucion_absentismo_c4'){
            if($datos['accion'] == 'resultadoInformeAbsentismo_C1' || $datos['accion'] == 'resultadoInformeAbsentismo_C2' || $datos['accion'] == 'resultadoInformeAbsentismo_C4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeAbsentismo_C_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : abrirFormularioReporteCentro_Promotor
     * Parametros: $datos
     * Contenido Reporte de centro
    */
    function abrirFormularioReporteCentro_Promotor($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_centros_p.html");

        $label_inicio = "Dades de promoció escolar alumnat";
        $msg_descripcion = "Dades de promoció escolar alumnat";
        $breadcump_informe_centro = "Informe Centre";
        $input_select_ano_lectivo = "Curs Escolar";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $anio_lectivo = $datos['FORMATO_BUSQ']['anio_lectivo'];

        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $anio_lectivo . "</td></tr>";
  
        //Botones 
        if($datos['accion'] != 'informeCentroEscolarP1' && $datos['accion'] != 'informeCentroEscolarP2' && $datos['accion'] != 'informeCentroEscolarP4'){
            if($datos['accion'] == 'resultadoInformeCentro1' || $datos['accion'] == 'resultadoInformeCentro2' || $datos['accion'] == 'resultadoInformeCentro4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeCentro_exportar&per_academico='.$datos['per_academico'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
        }
        
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        
        return $contenido;
    }

    /**
     * SA:: 25-03-2021
     * Funcion : abrirFormularioReporteCentro_PromotorExportar
     * Parametros: $datos
     * Contenido Reporte de centro exportar
    */
    function abrirFormularioReporteCentro_PromotorExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_centros_p_exportar.html");

        $label_inicio = "Dades de promoció escolar alumnat";
        $msg_descripcion = "Dades de promoció escolar alumnat";
        $breadcump_informe_centro = "Informe Centre";
        $input_select_ano_lectivo = "Curs Escolar";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $anio_lectivo = $datos['FORMATO_BUSQ']['anio_lectivo'];

        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $anio_lectivo . "</td></tr>";
  
        //Botones 
        if($datos['accion'] != 'informeCentroEscolarP1' && $datos['accion'] != 'informeCentroEscolarP2' && $datos['accion'] != 'informeCentroEscolarP4'){
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        
        return $contenido;
    }

     /**
     * BDL:: 12-02-2021
     * Funcion : abrirFormularioReporteCentro_Admin
     * Parametros: $datos
     * Contenido Reporte de centro
    */
    function abrirFormularioReporteCentro_Admin($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_centros_a.html");

        $label_inicio = "Dades de promoció escolar alumnat";
        $msg_descripcion = "Dades de promoció escolar alumnat";
        $breadcump_informe_centro = "Informe Centre";
        $input_select_ano_lectivo = "Curs Escolar";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTer($datos);
        $select_territorio = $this->obtenerSelectFormaTerritorios($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $anio_lectivo = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $anio_lectivo . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'informeCentroEscolarA1' && $datos['accion'] != 'informeCentroEscolarA2' && $datos['accion'] != 'informeCentroEscolarA4'){
            if($datos['accion'] == 'resultadoInformeCentroAdmin1' || $datos['accion'] == 'resultadoInformeCentroAdmin2' || $datos['accion'] == 'resultadoInformeCentroAdmin4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeCentroAdmin_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);


        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial", $input_select_servicio_territorial, $contenido);
        Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
        Interfaz::asignarToken("input_select_territorio", $input_select_territorio, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        
        return $contenido;
    }

     /**
     * SA:: 25-03-2021
     * Funcion : abrirFormularioReporteCentro_AdminExportar
     * Parametros: $datos
     * Contenido Reporte de centro exportar
    */
    function abrirFormularioReporteCentro_AdminExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_centros_a_exportar.html");

        $label_inicio = "Dades de promoció escolar alumnat";
        $msg_descripcion = "Dades de promoció escolar alumnat";
        $breadcump_informe_centro = "Informe Centre";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $anio_lectivo = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $anio_lectivo . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
        Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        
        return $contenido;
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : abrirFormularioReporteCentro_Coor
     * Parametros: $datos
     * Contenido Reporte de centro
    */
    function abrirFormularioReporteCentro_Coor($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_centros_c.html");

        $label_inicio = "Dades de promoció escolar alumnat";
        $msg_descripcion = "Dades de promoció escolar alumnat";
        $breadcump_informe_centro = "Informe Centre";
        $input_select_ano_lectivo = "Curs Escolar";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $centro_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $centro_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'informeCentroEscolarC1' && $datos['accion'] != 'informeCentroEscolarC2' && $datos['accion'] != 'informeCentroEscolarC4'){
            if($datos['accion'] == 'resultadoInformeCentroCoor1' || $datos['accion'] == 'resultadoInformeCentroCoor2' || $datos['accion'] == 'resultadoInformeCentroCoor4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeCentroCoor_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial", $input_select_servicio_territorial, $contenido);
        Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
        Interfaz::asignarToken("input_select_territorio", $input_select_territorio, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        
        return $contenido;
    }

    /**
     * SA:: 26-03-2021
     * Funcion : abrirFormularioReporteCentro_CoorExportar
     * Parametros: $datos
     * Contenido Reporte de centro
    */
    function abrirFormularioReporteCentro_CoorExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_centros_c_exportar.html");

        $label_inicio = "Dades de promoció escolar alumnat";
        $msg_descripcion = "Dades de promoció escolar alumnat";
        $breadcump_informe_centro = "Informe Centre";
        $input_select_ano_lectivo = "Curs Escolar";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $centro_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $centro_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'informeCentroEscolarC1' && $datos['accion'] != 'informeCentroEscolarC2' && $datos['accion'] != 'informeCentroEscolarC4'){
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial", $input_select_servicio_territorial, $contenido);
        Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
        Interfaz::asignarToken("input_select_territorio", $input_select_territorio, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        
        return $contenido;
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : abrirFormularioReporteCantidadXIntervencion_P
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por motivo de intervencion
     * Dades quantitatives per motiu d\'intervenció
    */
    function abrirFormularioReporteCantidadXIntervencion_P($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_motivo_intervencion_p.html");
        
        $label_inicio = "Motiu d'intervenció";
        $msg_descripcion = "Dades quantitatives per motiu d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per motiu d'intervenció";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'cnt_motivo_intervencion_p1' && $datos['accion'] != 'cnt_motivo_intervencion_p2' && $datos['accion'] != 'cnt_motivo_intervencion_p4'){
            if($datos['accion'] == 'resultadoInformeMotivoIntenvencion1' || $datos['accion'] == 'resultadoInformeMotivoIntenvencion2' || $datos['accion'] == 'resultadoInformeMotivoIntenvencion4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeMotivoIntenvencion_exportar&&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

     /**
     * BDL:: 29-03-2021
     * Funcion : abrirFormularioReporteCantidadXIntervencion_PExportar
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por motivo de intervencion
     * Dades quantitatives per motiu d\'intervenció
    */
    function abrirFormularioReporteCantidadXIntervencion_PExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_motivo_intervencion_p_exportar.html");
        
        $label_inicio = "Motiu d'intervenció";
        $msg_descripcion = "Dades quantitatives per motiu d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per motiu d'intervenció";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'cnt_motivo_intervencion_p1' && $datos['accion'] != 'cnt_motivo_intervencion_p2' && $datos['accion'] != 'cnt_motivo_intervencion_p4'){
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }


    /**
     * BDL:: 12-02-2021
     * Funcion : abrirFormularioReporteCantidadXIntervencion_A
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por motivo de intervencion
     * Dades quantitatives per motiu d\'intervenció
    */
    function abrirFormularioReporteCantidadXIntervencion_A($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_motivo_intervencion_a.html");
        
        $label_inicio = "Motiu d'intervenció";
        $msg_descripcion = "Dades quantitatives per motiu d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per motiu d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTer($datos);
        $select_territorio = $this->obtenerSelectFormaTerritorios($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'cnt_motivo_intervencion_a1' && $datos['accion'] != 'cnt_motivo_intervencion_a2' && $datos['accion'] != 'cnt_motivo_intervencion_a4'){
            if($datos['accion'] == 'resultadoInformeMotivoIntenvencion_a1' || $datos['accion'] == 'resultadoInformeMotivoIntenvencion_a2' || $datos['accion'] == 'resultadoInformeMotivoIntenvencion_a4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeMotivoIntenvencion_a_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * SA:: 29-03-2021
     * Funcion : abrirFormularioReporteCantidadXIntervencion_AExportar
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por motivo de intervencion
     * Dades quantitatives per motiu d\'intervenció
    */
    function abrirFormularioReporteCantidadXIntervencion_AExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_motivo_intervencion_a_exportar.html");
        
        $label_inicio = "Motiu d'intervenció";
        $msg_descripcion = "Dades quantitatives per motiu d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per motiu d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTer($datos);
        $select_territorio = $this->obtenerSelectFormaTerritorios($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'cnt_motivo_intervencion_a1' && $datos['accion'] != 'cnt_motivo_intervencion_a2' && $datos['accion'] != 'cnt_motivo_intervencion_a4'){
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : abrirFormularioReporteCantidadXIntervencion_C
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por motivo de intervencion
     * Dades quantitatives per motiu d\'intervenció
    */
    function abrirFormularioReporteCantidadXIntervencion_C($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_motivo_intervencion_c.html");
        
        $label_inicio = "Motiu d'intervenció";
        $msg_descripcion = "Dades quantitatives per motiu d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per motiu d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'cnt_motivo_intervencion_c1' && $datos['accion'] != 'cnt_motivo_intervencion_c2' && $datos['accion'] != 'cnt_motivo_intervencion_c4'){
            if($datos['accion'] == 'resultadoInformeMotivoIntenvencion_c1' || $datos['accion'] == 'resultadoInformeMotivoIntenvencion_c2' || $datos['accion'] == 'resultadoInformeMotivoIntenvencion_c4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeMotivoIntenvencion_c_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * BDL:: 29-03-2021
     * Funcion : abrirFormularioReporteCantidadXIntervencion_CExportar
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por motivo de intervencion
     * Dades quantitatives per motiu d\'intervenció
    */
    function abrirFormularioReporteCantidadXIntervencion_CExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_motivo_intervencion_c_exportar.html");
        
        $label_inicio = "Motiu d'intervenció";
        $msg_descripcion = "Dades quantitatives per motiu d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per motiu d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'cnt_motivo_intervencion_c1' && $datos['accion'] != 'cnt_motivo_intervencion_c2' && $datos['accion'] != 'cnt_motivo_intervencion_c4'){
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * BDL:: 24-03-2021
     * Funcion : abrirFormularioReporteTipoIntervencion_A
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por tipo de intervencion
     * Dades quantitatives per tipus d'intervenció
    */
    function abrirFormularioReporteTipoIntervencion_A($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_tipo_intervencion_a.html");
        
        $label_inicio = "Tipus d'intervenció";
        $msg_descripcion = "Dades quantitatives per tipus d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per tipus d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTer($datos);
        $select_territorio = $this->obtenerSelectFormaTerritorios($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'tipointervencionA1' && $datos['accion'] != 'tipointervencionA2' && $datos['accion'] != 'tipointervencionA4'){
            if($datos['accion'] == 'resultadoInformeTipoIntenvencion_a1' || $datos['accion'] == 'resultadoInformeTipoIntenvencion_a2' || $datos['accion'] == 'resultadoInformeTipoIntenvencion_a4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeTipoIntenvencion_a_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * SA:: 29-03-2021
     * Funcion : abrirFormularioReporteTipoIntervencion_AExportar
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por tipo de intervencion
     * Dades quantitatives per tipus d'intervenció
    */
    function abrirFormularioReporteTipoIntervencion_AExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_tipo_intervencion_a_exportar.html");
        
        $label_inicio = "Tipus d'intervenció";
        $msg_descripcion = "Dades quantitatives per tipus d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per tipus d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTer($datos);
        $select_territorio = $this->obtenerSelectFormaTerritorios($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'tipointervencionA1' && $datos['accion'] != 'tipointervencionA2' && $datos['accion'] != 'tipointervencionA4'){
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * BDL:: 24-03-2021
     * Funcion : abrirFormularioReporteTipoIntervencion_C
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por tipo de intervencion
     * Dades quantitatives per tipus d'intervenció
    */
    function abrirFormularioReporteTipoIntervencion_C($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_tipo_intervencion_c.html");
        
        $label_inicio = "Tipus d'intervenció";
        $msg_descripcion = "Dades quantitatives per tipus d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per tipus d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'tipointervencionC1' && $datos['accion'] != 'tipointervencionC2' && $datos['accion'] != 'tipointervencionC4'){
            if($datos['accion'] == 'resultadoInformeTipoIntenvencion_c1' || $datos['accion'] == 'resultadoInformeTipoIntenvencion_c2' || $datos['accion'] == 'resultadoInformeTipoIntenvencion_c4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeTipoIntenvencion_c_exportar&ste_id='.$datos['ste_id'].'&territorio_id='.$datos['territorio_id'].'&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * SA:: 29-03-2021
     * Funcion : abrirFormularioReporteTipoIntervencion_CExportar
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por tipo de intervencion
     * Dades quantitatives per tipus d'intervenció
    */
    function abrirFormularioReporteTipoIntervencion_CExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_tipo_intervencion_c_exportar.html");
        
        $label_inicio = "Tipus d'intervenció";
        $msg_descripcion = "Dades quantitatives per tipus d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per tipus d'intervenció";
        $input_select_servicio_territorial = "Servei Territorial";
        $input_select_territorio = "Territori";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares_Admin($datos);
        $select_servicio_territorial = $this->obtenerSelectFormaServicioTerCoor($datos);
        $select_territorio = $this->obtenerSelectFormaTerritoriosCoor($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $servicio_territorial = $datos['FORMATO_BUSQ']['servicio_territorial'];
        $territorio = $datos['FORMATO_BUSQ']['territorio'];
        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio . "</td></tr>";
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'tipointervencionC1' && $datos['accion'] != 'tipointervencionC2' && $datos['accion'] != 'tipointervencionC4'){
            Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
            Interfaz::asignarToken("territorio", $info_territorio, $contenido);
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_servicio_territorial",$input_select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("select_servicio_territorial",$select_servicio_territorial ,$contenido);
        Interfaz::asignarToken("input_select_territorio",$input_select_territorio ,$contenido);
        Interfaz::asignarToken("select_territorio",$select_territorio ,$contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * BDL:: 24-03-2021
     * Funcion : abrirFormularioReporteTipoIntervencion_P
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por tipo de intervencion
     * Dades quantitatives per tipus d'intervenció
    */
    function abrirFormularioReporteTipoIntervencion_P($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_tipo_intervencion_p.html");
        
        $label_inicio = "Tipus d'intervenció";
        $msg_descripcion = "Dades quantitatives per tipus d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per tipus d'intervenció";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'tipointervencionP1' && $datos['accion'] != 'tipointervencionP2' && $datos['accion'] != 'tipointervencionP4'){
            if($datos['accion'] == 'resultadoInformeTipoIntenvencion_p1' || $datos['accion'] == 'resultadoInformeTipoIntenvencion_p2' || $datos['accion'] == 'resultadoInformeTipoIntenvencion_p4'){
                $botones = '<a href="index.php?m=basereportes&accion=resultadoInformeTipoIntenvencion_p_exportar&per_academico='.$datos['per_academico'].'&ces_id='.$datos['ces_id'].'" class="btn btn-warning">Generar Exportació</a>';
            }
            else{
                $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
            }
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }
        Interfaz::asignarToken("botones", $botones, $contenido);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }

    /**
     * SA:: 29-03-2021
     * Funcion : abrirFormularioReporteTipoIntervencion_PExportar
     * Parametros: $datos
     * Abrir Formulario reporte de cantidad de ninos por tipo de intervencion
     * Dades quantitatives per tipus d'intervenció
    */
    function abrirFormularioReporteTipoIntervencion_PExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_tipo_intervencion_p_exportar.html");
        
        $label_inicio = "Tipus d'intervenció";
        $msg_descripcion = "Dades quantitatives per tipus d'intervenció";
        $breadcump_informe_centro = "Dades quantitatives per tipus d'intervenció";
        $input_select_centro_escolar = "Centre Educatiu";
        $input_select_ano_lectivo = "Curs Escolar";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        
        $select_ano_lectivo = $this->obteberSelectFormaAnoLectivo($datos);
        $select_centro_escolar = $this->obteberSelectFormaCentrosEscolares($datos);
        $table_results_informe_centre = $datos['result_table_centre'];

        $curso_escolar = $datos['FORMATO_BUSQ']['anio_lectivo'];
        $centro_escolar = $datos['FORMATO_BUSQ']['centro_escolar'];

        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar . "</td></tr>";
        $info_curso_escolar = "<tr><th>Curs Escolar: </th>";
        $info_curso_escolar .= "<td>" . $curso_escolar . "</td></tr>";

        //Botones 
        if($datos['accion'] != 'tipointervencionP1' && $datos['accion'] != 'tipointervencionP2' && $datos['accion'] != 'tipointervencionP4'){
            Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
            Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        }

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_informe_centro", $breadcump_informe_centro, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);
        Interfaz::asignarToken("input_select_centro_escolar", $input_select_centro_escolar, $contenido);
        Interfaz::asignarToken("select_centro_escolar", $select_centro_escolar, $contenido);
        Interfaz::asignarToken("input_select_ano_lectivo", $input_select_ano_lectivo, $contenido);
        Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
        Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        return $contenido;
    }


    /**
     * BDL:: 12-02-2021
     * Funcion : obteberSelectFormaCentrosEscolares
     * Parametros: $datos
     * Contenido Selector de Centros Escolares por Usuario
    */
    function obteberSelectFormaCentrosEscolares($datos){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT ces.ces_id, ces.ces_nombre 
        FROM centro_escolar as ces 
        INNER JOIN usuario_centro_escolar as usu_ces
        ON usu_ces.usc_ces_id = ces.ces_id
        WHERE usu_ces.usc_usu_id_promotor = ".$usu_id.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        $ces_id = "";
        if(isset($datos['ces_id'])){
            $ces_id = $datos['ces_id'];
        }
        
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='ces_id' aria-label='Default select example' disabled required>";
            $select .= "<option value=''>No té Centres educatius assignats</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='ces_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Centre Educatiu</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ces_id'] == $ces_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ces_id']."' ".$selected.">".$resultado['ces_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }


    /**
     * BDL:: 12-02-2021
     * Funcion : obteberSelectFormaCentrosEscolares_Admin
     * Parametros: $datos
     * Contenido Selector de Centros Escolares por Usuario
    */
    function obteberSelectFormaCentrosEscolares_Admin($datos){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $territorio = intval($datos['territorio_id']);

        $sql = "SELECT ces.ces_id, ces.ces_nombre
        FROM centro_escolar AS ces
        INNER JOIN territorio AS ter
        ON ter.ter_id = ces.ces_ter_id
        WHERE ter.ter_id = $territorio"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        $ces_id = "";
        if(isset($datos['ces_id'])){
            $ces_id = $datos['ces_id'];
        }
        
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='ces_id' aria-label='Default select example' disabled required>";
            $select .= "<option value=''>Seleccionar Centre Educatiu</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='ces_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Centre Educatiu</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ces_id'] == $ces_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ces_id']."' ".$selected.">".$resultado['ces_nombre']."</option>";
            }
            $select .="</select>";
        }
        
        return $select;
    }

    /**
     * BDL:: 20-02-2021
     * Funcion : obtenerAlumnosPorCentroEscolar
     * Parametros: $datos
     * Obtener todos los estudiantes de un centro escolar
    */
    function obtenerAlumnosPorCentroEscolar($ces_id,$anio_lectivo){
        global $_obj_database;

        $sql = "SELECT est.est_id, est.est_nombre
        FROM estudiante AS est
        WHERE est.est_ces_id = $ces_id";

        $sql_2 = "SELECT DISTINCT(est.est_id), est.est_nombre
        FROM intervencion AS inter
        INNER JOIN intervencion_estudiante AS ines
        ON ines.ies_int_id = inter.int_id
        INNER JOIN estudiante AS est
        ON est.est_id = ines.ies_est_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        WHERE inter.int_ces_id = $ces_id
        AND pes.pes_ano_lectivo LIKE '$anio_lectivo'";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql_2);
        return ($res);
    }

    /**
     * BDL:: 20-02-2021
     * Funcion : obtenerAbsentismoPorPeriodoAlumn
     * Parametros: $datos
     * Obtener todos los estudiantes de un centro escolar
    */
    function obtenerAbsentismoPorPeriodoAlumn($datos, $periodo = ""){
        global $_obj_database;

        $est_id = $datos['est_id'];
        $per_academico = $datos['per_academico'];

        $sql = "SELECT est.est_id, est.est_nombre AS Alumno, tab.tab_nombre AS Absentismo, 
        pes.pes_periodo AS Periodo, inter.int_fecha AS fecha_intervencion,
        intes.ies_observacion AS observacion_est
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN motivo_intervencion AS mint
        ON mint.min_id = inter.int_min_id
        INNER JOIN tipo_absentismo AS tab
        ON tab.tab_id = inter.int_tab_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        where mint.min_nombre LIKE 'ABSENTISME'
        AND est.est_id = $est_id
        AND pes.pes_ano_lectivo LIKE '$per_academico'
        AND pes.pes_periodo LIKE '$periodo'
        ORDER BY inter.int_id DESC
        LIMIT 1";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res[0]);
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : obteberSelectFormaAnoLectivo
     * Parametros: $datos
     * Contenido Selector de anos lectivos
    */
    function obteberSelectFormaAnoLectivo($datos){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        /*$sql = "SELECT DISTINCT pes_id, pes_ano_lectivo AS ano_lectivo FROM periodos_escolares WHERE 1 ORDER BY pes_id DESC";*/
        
        $sql = "SELECT DISTINCT(pes.pes_ano_lectivo) AS ano_lectivo
        FROM periodos_escolares AS pes         
        WHERE 1 ORDER BY ano_lectivo DESC"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $per_academico = ""; 
        if(isset($datos['per_academico'])){
            $per_academico = $datos['per_academico'];
        }
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='per_academico' aria-label='Default select example' disabled>";
            $select .= "<option value=''>Seleccionar Curs Escolar</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='per_academico' aria-label='Default select example' required>";
            $select .= "<option selected value='' >Seleccionar Curs Escolar</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ano_lectivo'] == $per_academico){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ano_lectivo']."' ".$selected.">".$resultado['ano_lectivo']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : obtenerSelectFormaServicioTer
     * Parametros: $datos
     * Contenido Selector de servicio territorial
    */
    function obtenerSelectFormaServicioTer($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $servicio_territorial = $datos['ste_id'];

        if(isset($datos['ste_id'])){
            $servicio_territorial = $datos['ste_id'];
        }

        $sql = "SELECT DISTINCT(ste_id), ste_nombre
                FROM servicio_territorial
                WHERE 1"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='ste_id' name='ste_id' aria-label='Default select example' disabled>";
            $select .= "<option value=''>Seleccionar Servei Territorial</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='ste_id' name='ste_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Servei Territorial</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ste_id'] == $servicio_territorial){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ste_id']."' ".$selected.">".$resultado['ste_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select; 
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : obtenerSelectFormaServicioTerCoor
     * Parametros: $datos
     * Contenido Selector de servicio territorial
    */
    function obtenerSelectFormaServicioTerCoor($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $servicio_territorial = $datos['ste_id'];

        if(isset($datos['ste_id'])){
            $servicio_territorial = $datos['ste_id'];
        }

        $sql = "SELECT DISTINCT(ste.ste_id), ste.ste_nombre
            FROM servicio_territorial AS ste
            INNER JOIN territorio AS ter
            ON ter.ter_ste_id = ste.ste_id
            INNER JOIN usuario_territorio AS ust
            ON ust.ust_ter_id = ter.ter_id
            WHERE ust.ust_usu_id_coordinador = $usu_id"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='ste_id' name='ste_id' aria-label='Default select example' disabled>";
            $select .= "<option value=''>Seleccionar Servei Territorial</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='ste_id' name='ste_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Servei Territorial</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ste_id'] == $servicio_territorial){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ste_id']."' ".$selected.">".$resultado['ste_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select; 
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : obtenerSelectFormaTerritorios
     * Parametros: $datos
     * Contenido Selector de Territorios, dependiendo del servicio territorial
    */
    function obtenerSelectFormaTerritorios($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $servicio_territorial = intval($datos['ste_id']);

        $territorio = ""; 
        if(isset($datos['territorio_id'])){
            $territorio = $datos['territorio_id'];
        }

        $sql = "SELECT DISTINCT(ter_id), ter_nombre
                FROM territorio
                WHERE ter_ste_id = ".$servicio_territorial.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='territorio_id' name='territorio_id' aria-label='Default select example' disabled>";
            $select .= "<option value='' >Seleccionar Territori</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='territorio_id' name='territorio_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Territori</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ter_id'] == $territorio){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ter_id']."' ".$selected.">".$resultado['ter_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select; 
    }

    /**
     * BDL:: 12-02-2021
     * Funcion : obtenerSelectFormaTerritoriosCoor
     * Parametros: $datos
     * Contenido Selector de Territorios, dependiendo del servicio territorial y coordinador
    */
    function obtenerSelectFormaTerritoriosCoor($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $servicio_territorial = intval($datos['ste_id']);

        $territorio = ""; 
        if(isset($datos['territorio_id'])){
            $territorio = $datos['territorio_id'];
        }

        $sql = "SELECT DISTINCT(ter.ter_id), ter.ter_nombre
                FROM territorio AS ter
                INNER JOIN usuario_territorio AS ust
                ON ust.ust_ter_id = ter.ter_id
                WHERE ust.ust_usu_id_coordinador = $usu_id
                AND ter.ter_ste_id = ".$servicio_territorial.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='territorio_id' name='territorio_id' aria-label='Default select example' disabled>";
            $select .= "<option value='' >Seleccionar Territori</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='territorio_id' name='territorio_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Territori</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ter_id'] == $territorio){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ter_id']."' ".$selected.">".$resultado['ter_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select; 
    }

    /**
     * BDL:: 26-02-2021
     * Funcion : obtenerMotivosIntervencion
     * Parametros: $datos
     * Todos los motivos de intervencion
    */
    function obtenerMotivosIntervencion(){
        global $_obj_database;

        $sql = "SELECT DISTINCT(mine.min_id), mine.min_nombre
        FROM motivo_intervencion AS mine
        WHERE 1 ORDER BY mine.min_nombre ASC";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    /**
     * BDL:: 26-02-2021
     * Funcion : obtenerTiposIntervencion
     * Todos los tipos de intervencion
    */
    function obtenerTiposIntervencion(){
        global $_obj_database;

        $sql = "SELECT DISTINCT(tine.tin_id), tine.tin_nombre
        FROM tipo_intervencion AS tine
        WHERE 1 ORDER BY tine.tin_nombre ASC";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    /**
     * Cantidad de registro por motivo intervencion
     */
    function obtenerCntNinosMotivoIntervencion($datos, $periodo = ""){
        global $_obj_database;

        $min_id = intval($datos['min_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  mti.min_id, mti.min_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN motivo_intervencion AS mti
        ON mti.min_id = inter.int_min_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND pes.pes_periodo LIKE '$periodo'
        AND mti.min_id = $min_id
        AND ces.ces_id = $ces_id
        AND usu.usu_id = $usu_id
        GROUP BY mti.min_id, mti.min_nombre";
        
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }


    /**
     * Toltal de ninos distintos registrados por motivo intervencion
     */
    function obtenerCntNinosDiferentesMotivoIntervencion($datos){
        global $_obj_database;

        $min_id = intval($datos['min_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  mti.min_id, mti.min_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN motivo_intervencion AS mti
        ON mti.min_id = inter.int_min_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND mti.min_id = $min_id
        AND ces.ces_id = $ces_id
        GROUP BY mti.min_id, mti.min_nombre";
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }

    function obtenerCntNinosDiferentesMotivoIntervencion_a_c($datos){
        global $_obj_database;

        $min_id = intval($datos['min_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  mti.min_id, mti.min_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN motivo_intervencion AS mti
        ON mti.min_id = inter.int_min_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND mti.min_id = $min_id
        AND ces.ces_id = $ces_id
        GROUP BY mti.min_id, mti.min_nombre";
        
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }

    /**
     * Cantidad de registro por motivo intervencion
     */
    function obtenerCntNinosMotivoIntervencion_a_c($datos, $periodo = ""){
        global $_obj_database;

        $min_id = intval($datos['min_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  mti.min_id, mti.min_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN motivo_intervencion AS mti
        ON mti.min_id = inter.int_min_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND pes.pes_periodo LIKE '$periodo'
        AND mti.min_id = $min_id
        AND ces.ces_id = $ces_id
        GROUP BY mti.min_id, mti.min_nombre";
        
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }

    /**
     * Toltal de ninos distintos registrados por tipo intervencion
     */
    function obtenerCntNinosDiftipoIntervencion($datos){
        global $_obj_database;

        $tin_id = intval($datos['tin_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  tin.tin_id, tin.tin_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN tipo_intervencion AS tin
        ON tin.tin_id = inter.int_tin_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND tin.tin_id = $tin_id
        AND ces.ces_id = $ces_id
        AND usu.usu_id = $usu_id
        GROUP BY tin.tin_id, tin.tin_nombre";
        
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }

    function obtenerCntNinosDiftipoIntervencion_a_c($datos){
        global $_obj_database;

        $tin_id = intval($datos['tin_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  tin.tin_id, tin.tin_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN tipo_intervencion AS tin
        ON tin.tin_id = inter.int_tin_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND tin.tin_id = $tin_id
        AND ces.ces_id = $ces_id
        GROUP BY tin.tin_id, tin.tin_nombre";
        
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }

    /**
     * Cantidad de registro por tipo intervencion
     */
    function obtenerCntNinosTipoIntervencion($datos, $periodo = ""){
        global $_obj_database;

        $tin_id = intval($datos['tin_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  tin.tin_id, tin.tin_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN tipo_intervencion AS tin
        ON tin.tin_id = inter.int_tin_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND pes.pes_periodo LIKE '$periodo'
        AND tin.tin_id = $tin_id
        AND ces.ces_id = $ces_id
        AND usu.usu_id = $usu_id
        GROUP BY tin.tin_id, tin.tin_nombre";
        
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }

    /**
     * Cantidad de registro por tipo intervencion
     */
    function obtenerCntNinosTipoIntervencion_a_c($datos, $periodo = ""){
        global $_obj_database;

        $tin_id = intval($datos['tin_id']);
        $anio_lectivo = $datos['per_academico'];
        $ces_id = intval($datos['ces_id']);
        $usu_id = $datos['usu_id'];
        $sql = "SELECT  tin.tin_id, tin.tin_nombre, COUNT(DISTINCT(est.est_id)) AS cnt
        FROM estudiante AS est
        INNER JOIN intervencion_estudiante AS intes
        ON intes.ies_est_id = est.est_id
        INNER JOIN intervencion AS inter
        ON inter.int_id = intes.ies_int_id
        INNER JOIN tipo_intervencion AS tin
        ON tin.tin_id = inter.int_tin_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = inter.int_pes_id
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = inter.int_ces_id
        INNER JOIN usuario_centro_escolar AS usec
        ON usec.usc_ces_id = ces.ces_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = usec.usc_usu_id_promotor
        WHERE pes.pes_ano_lectivo LIKE '$anio_lectivo'
        AND pes.pes_periodo LIKE '$periodo'
        AND tin.tin_id = $tin_id
        AND ces.ces_id = $ces_id
        GROUP BY tin.tin_id, tin.tin_nombre";
        
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        return ($res['0']);
    }

    function obtenerNombreServicioTerr($servicio_territorial){
        global $_obj_database;

        $servicio_territorial = intval($servicio_territorial);
        
        $sql = "SELECT ste.ste_nombre
        FROM servicio_territorial AS ste
        WHERE ste.ste_id = $servicio_territorial";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);

        return $res[0];
    }

    function obtenerNombreTerritorio($territorio){
        global $_obj_database;

        $territorio = intval($territorio);

        $sql = "SELECT ter.ter_nombre
        FROM territorio AS ter
        WHERE ter.ter_id = $territorio";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);

        return $res[0];
    }

    function obtenerNombreCentroEscolar($centro_escolar){
        global $_obj_database;

        $centro_escolar = intval($centro_escolar);

        $sql = "SELECT ces.ces_nombre 
        FROM centro_escolar AS ces
        WHERE ces.ces_id = $centro_escolar";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);

        return $res[0];
    }

}

?>