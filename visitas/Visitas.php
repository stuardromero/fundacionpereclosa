<?php

/**
 * 	Nombre: Visitas.php
 * 	Descripción: Maneja lo referente a las cotizaciones del sistema
 */
class Visitas {

  var $_plantillas = "";

  public function __construct() {
      global $_PATH_SERVIDOR;

      $this->_plantillas = $_PATH_SERVIDOR . "visitas/Plantillas";
      $this->_herramientas = new Herramientas;
      $this->campos_busqueda_listado = array("usu_id","usu_nombre",   
		          "usu_login","usu_correo", "usu_apellido");  
  }

    /** abrirFormularioIntervencion
     * parametro: $datos
     * autor : SA - 04/12/2020
     * descripcion: ABRE EL Formulario para anexar periodos escolares
     **/
    public function abrirFormularioIntervencion($datos) {
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database, $_obj_interfaz;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_intervencion.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	

      $msg_titulo = 'Intervenció';
      $msg_descripcion = "Registreu les vostres intervencions ";
      $img_ruta = "Includes/images";
      $fecha = "Data";
      $mot_int = "Motiu de la intervencio";
      $tipo_int = "Tipus d'intervenció";
      $label_estudiante = "ESTUDIANTS";

      $sql = "SELECT vis_fecha FROM visita WHERE vis_id = ".$datos['vis_id'].";";
      $get_fecha = $_obj_database->obtenerRegistrosAsociativos($sql);

      $mot_int_select = '<select class="choices form-select multiple-remove" id="int_min_id" name="int_min_id" required><option value="" selected>Escollir...</option>';

      $sql = "SELECT * FROM motivo_intervencion;";

      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      foreach($resultado_sql as $motivos){
        $mot_int_select .= '<option value="'.$motivos['min_id'].'">'.$motivos['min_nombre'].'</option>';
      }
      $mot_int_select .= '</select>';

      $sql = "SELECT est_id, est_nombre FROM estudiante WHERE est_ces_id = ".$datos['ces_id']." AND est_desactivo != 'on';";

      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      if($resultado_sql){
        $estudiantes = '<select class="choices form-select multiple-remove" id="estudiantes" name="estudiantes[]" multiple required>';
        $estudiantes .= "<option value='' disabled>Escrigui els seus Estudiants</option>";
        foreach($resultado_sql as $tipos){
          $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $tipos['est_nombre']);
          $estudiantes .= '<option value="'.$tipos['est_id'].'">'.$alumn_name.'</option>';
        }
        $estudiantes .= '</select>';

        $boton_enviar = '<button type="submit" class="btn btn-primary mr-1 mb-1">Enviar</button>';
      }
      else{
        $estudiantes .= "<p style='background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;'>No hi ha estudiants associats a el Centre Escolar</p>";
        $boton_enviar = '<a href="index.php?m=visitas&accion=listar_intervencion&vis_id='.$datos['vis_id'].'&msg=V-2" class="btn btn-primary mr-1 mb-1">Tornar</a>';
      }
      
      $tipo_int_select = '<select class="choices form-select multiple-remove" id="int_tin_id" name="int_tin_id[]" multiple required>';

      $sql = "SELECT * FROM tipo_intervencion;";

      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      $tipo_int_select .= "<option value=''>Escriu el teu Tipus d'Intervenció</option>";

      foreach($resultado_sql as $tipos){
        $tipo_int_select .= '<option value="'.$tipos['tin_id'].'">'.$tipos['tin_nombre'].'</option>';
      }
      $tipo_int_select .= '</select>';

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("fecha_actual", $get_fecha[0]['vis_fecha'], $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("fecha", $fecha, $contenido);
      Interfaz::asignarToken("mot_int", $mot_int, $contenido);
      Interfaz::asignarToken("mot_int_select", $mot_int_select, $contenido);
      Interfaz::asignarToken("tipo_int", $tipo_int, $contenido);
      Interfaz::asignarToken("tipo_int_select", $tipo_int_select, $contenido);
      Interfaz::asignarToken("label_estudiante", $label_estudiante, $contenido);
      Interfaz::asignarToken("estudiantes", $estudiantes, $contenido);
      Interfaz::asignarToken("vis_id", $datos['vis_id'], $contenido);
      Interfaz::asignarToken("ces_id", $datos['ces_id'], $contenido);
      Interfaz::asignarToken("boton_enviar", $boton_enviar, $contenido);

      return $contenido;
    }

    /** actIntervencion
     * parametro: $datos
     * autor : SA - 04/12/2020
     * descripcion: Inserta los datos del periodo escolar en base de datos
     **/
    function actIntervencion($datos) {
      global $_obj_database, $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_interfaz;

      $sql = "DELETE FROM intervencion WHERE int_revisado = 0;";
      if (!$_obj_database->ejecutarSql($sql)) {
        $resultado_delete = $_obj_database->cancelarTransaccion();
      } else {
        $resultado_delete = $_obj_database->terminarTransaccion();
      }

      if($resultado_delete == 1){
        $sql = "SELECT * FROM periodos_escolares WHERE pes_fecha_inicio <= '".$datos['int_fecha']."' 
                AND pes_fecha_fin >= '".$datos['int_fecha']."';";

        $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

        $periodo_escolar = $resultado_sql[0];

        if($datos['tab_id']){
          $tab_id = $datos['tab_id'];
        }
        else{
          $tab_id = 0;
        }

        $sql_transaccion = "INSERT INTO intervencion (int_ces_id, int_fecha, int_min_id, int_tab_id, int_pes_id, int_tin_id, int_otro_motivo_intervencion, int_otro_tipo_intervencion, int_vis_id, int_revisado) VALUES ";
        $i=0;

        foreach($datos['int_tin_id'] as $key => $tin_id){
          $sql_transaccion .= '(' .$datos['int_ces_id'].', "' .$datos['int_fecha'].'", ' .$datos['int_min_id'].', ' .$tab_id.', ' .$periodo_escolar['pes_id'].', '.$tin_id.', "' .$datos['int_otro_motivo_intervencion'].'", "' .$datos['int_otro_tipo_intervencion'].'", '.$datos['int_vis_id'].', 0)';
          if($i < count($datos['int_tin_id'])-1){
            $sql_transaccion .= ", ";
          }
          $i++;
        }
        $sql_transaccion .= ";";

        $_obj_database->ejecutarSql($sql_transaccion);
      }

      $sql = "SELECT int_id, int_tin_id FROM intervencion 
              WHERE int_revisado = 0 AND int_vis_id = ".$datos['int_vis_id'].";";

      $intervenciones = $_obj_database->obtenerRegistrosAsociativos($sql);

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_estudiantes_intervenciones.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	

      $msg_titulo = "intervenció d'Estudiants";
      $msg_descripcion = "Registre les observacions per estudiants per al registre de la intervenció";
      $img_ruta = "Includes/images";
      $fecha = "Data";
      $mot_int = "Motiu de la intervencio";

      foreach($intervenciones as $key => $intervenciones_group){
        foreach($datos['estudiantes'] as $estudiante){
          $sql = "SELECT est_nombre, est_idalu FROM estudiante WHERE est_id = ".$estudiante.";";
  
          $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);
  
          $result = $resultado_sql[0];

          $sql = "SELECT tin_nombre FROM tipo_intervencion WHERE tin_id = ".$intervenciones_group['int_tin_id'].";";
  
          $nombre_tin_id = $_obj_database->obtenerRegistrosAsociativos($sql);
  
          $nombre_tin_id = $nombre_tin_id[0];
          
          //decrypt est_name
          $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $result['est_nombre']);

          $estudiantes_datos .= "<div class='col-md-4 col-12'>
                                  <div class='form-group'>
                                    <h4>".$nombre_tin_id['tin_nombre']."</h4>
                                    <h5>".$alumn_name." - ".$result['est_idalu']."</h5>
                                    <label for='first-name-column'>Observacions/acords</label>
                                    <textarea rows='10' required cols='50' id='observaciones' placeholder='Observacions/Acords' name='observaciones[]' class='form-control'></textarea>
                                    <input type='hidden' id='est_id' value='".$estudiante."' name='est_id[]'>
                                    <input type='hidden' id='".$intervenciones_group['int_id']."' value='".$intervenciones_group['int_id']."' name='int_id[]'>
                                  </div>
                                </div>";
  
        }
      }
      

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("fecha_actual", $datos['int_fecha'], $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("fecha", $fecha, $contenido);
      Interfaz::asignarToken("estudiantes_datos", $estudiantes_datos, $contenido);
      Interfaz::asignarToken("int_id", $int_id, $contenido);

      return $contenido;
    }

    /** abrirFormularioVisitas
     * parametro: $datos
     * autor : SA - 30/12/2020
     * descripcion: Formulario para registrar las visitas
     **/
    public function abrirFormularioVisitas($datos) {
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database;

      $fecha_actual = date("Y-m-d");
      $hora_actual = date("H:i");

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_visitas.html");
      $msg_titulo = "Visites";
      $msg_descripcion = "Registreu les vostres visites ";
      $img_ruta = "Includes/images";
      $fecha = "Data";
      $hora_inicio = "Hora D'Inici";
      $centro_escolar = "Centre Educatiu";

      $centro_escolar_select = '<select class="form-select" id="vis_ces_id " name="vis_ces_id" required><option value="" selected>Escollir...</option>';

      $sql = "SELECT usuario_centro_escolar.usc_ces_id, centro_escolar.ces_nombre 
              FROM usuario_centro_escolar 
              JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
              WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$datos['usu_id'].";";

      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      foreach($resultado_sql as $centros){
        $centro_escolar_select .= '<option value="'.$centros['usc_ces_id'].'">'.$centros['ces_nombre'].'</option>';
      }
      $centro_escolar_select .= '</select>';

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("fecha_actual", $fecha_actual, $contenido);
      Interfaz::asignarToken("hora_actual", $hora_actual, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("fecha", $fecha, $contenido);
      Interfaz::asignarToken("hora_inicio", $hora_inicio, $contenido);
      Interfaz::asignarToken("centro_escolar", $centro_escolar, $contenido);
      Interfaz::asignarToken("centro_escolar_select", $centro_escolar_select, $contenido);
      Interfaz::asignarToken("usu_id_promotor", $datos['usu_id'], $contenido);

      return $contenido;
    }

    /** actVisitas
     * parametro: $datos
     * autor : SA - 04/12/2020
     * descripcion: Inserta los datos del periodo escolar en base de datos
     **/
    function actVisitas($datos) {
      global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $sql_transaccion = "INSERT INTO visita (vis_fecha, vis_hora_inicio, vis_ces_id, vis_usu_id_promotor) VALUES('".$datos['vis_fecha']."', '" .$datos['vis_hora_inicio']."', " .$datos['vis_ces_id'].", " .$datos['usu_id_promotor'].")";
      
      $_obj_database->ejecutarSql($sql_transaccion);

      return $contenido;
    }

    /** listadoVisitas
     * parametro: $datos
     * autor : SA - 31/12/2020
     * descripcion: Lista las visitas registradas
     **/
    function listadoVisitas($datos){
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_visitas.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
      $label_inicio = "Visites";
      $msg_descripcion = "Llistat Visites";

      //Revisar si esta abierta una visita
      $sql = "SELECT * FROM visita WHERE vis_usu_id_promotor = ".$_SESSION['usu_id']." AND vis_hora_fin is NULL;";
      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      if($resultado_sql){
        $visitas_abiertas .= '<a href="index.php?m=visitas&accion=cerrar_visitas" class="btn icon icon-left btn-primary"><i data-feather="check-circle"></i> Tancar Visita Oberta</a>';
        $visitas_abiertas .= '<a href="index.php?m=visitas&accion=Intervencion&vis_id='.$datos['vis_id'].'&ces_id='.$datos['ces_id'].'" class="btn icon icon-left btn-info"><i data-feather="info"></i> Afegir intervenció Oberta</a>';
        $visitas_abiertas .= '<a href="index.php?m=visitas&accion=listar_intervencion&vis_id='.$datos['vis_id'].'" class="btn icon icon-left btn-success"><i data-feather="check-circle"></i> Llistat intervenció Oberta</a>';
      }

      Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("visitas_abiertas", $visitas_abiertas, $contenido);

      return $contenido;
  }

  /** abrirFormularioCerrarVisitas
     * parametro: $datos
     * autor : SA - 04/01/2021
     * descripcion: Formulario para cerrar las visitas
     **/
    public function abrirFormularioCerrarVisitas($datos) {
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database;

      $fecha_actual = date("Y-m-d");
      $hora_actual = date("H:i");

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_cerrar_visitas.html");
      $msg_titulo = 'Tancar Visita';
      $msg_descripcion = "Tancar Visita";
      $img_ruta = "Includes/images";
      $fecha = "Data";
      $hora_inicio = "Hora D'Inici";
      $centro_escolar = "Centre Educatiu";

      $sql = "SELECT vis.vis_id, vis.vis_fecha, vis.vis_hora_inicio, vis.vis_hora_fin, vis.vis_horas_trabajadas, ces.ces_nombre, vis.vis_observaciones
              FROM visita as vis 
              JOIN centro_escolar as ces ON ces.ces_id = vis.vis_ces_id 
              WHERE vis.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND vis.vis_hora_fin IS NULL;";
      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      $visita = $resultado_sql[0];

      $fecha_num = $visita['vis_fecha'];
      $hora_ini = $visita['vis_hora_inicio'];
      $centro = $visita['ces_nombre'];
      $vis_id = $visita['vis_id'];

      $hora_fin = "Hora Final";
      $observaciones = "Observacions/acords";

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("fecha", $fecha, $contenido);
      Interfaz::asignarToken("hora_inicio", $hora_inicio, $contenido);
      Interfaz::asignarToken("hora_actual", $hora_actual, $contenido);
      Interfaz::asignarToken("centro_escolar", $centro_escolar, $contenido);
      Interfaz::asignarToken("fecha_num", $fecha_num, $contenido);
      Interfaz::asignarToken("hora_ini", $hora_ini, $contenido);
      Interfaz::asignarToken("centro", $centro, $contenido);
      Interfaz::asignarToken("hora_fin", $hora_fin, $contenido);
      Interfaz::asignarToken("observaciones", $observaciones, $contenido);
      Interfaz::asignarToken("id", $vis_id, $contenido);

      return $contenido;
    }

    /** actCerrarVisitas
     * parametro: $datos
     * autor : SA - 04/01/2021
     * descripcion: Inserta los datos para cerrar los datos 
     **/
    function actCerrarVisitas($datos) {
      global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $sql = "SELECT * FROM visita WHERE vis_id =  ".$datos['vis_id'].";";
      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      foreach($resultado_sql as $visita){

        $checkTime = strtotime($datos['vis_hora_fin']);

        $loginTime = strtotime($visita['vis_hora_inicio']);
        $diff = $checkTime - $loginTime;

        $hora_fin = gmdate("H:i:s", $diff);
        
      }

      $sql = 'UPDATE visita SET vis_hora_fin = "'. $datos['vis_hora_fin'] .'", 
              vis_horas_trabajadas = "'. $hora_fin .'", vis_observaciones = "'. $datos['vis_observaciones'] .'", 
              vis_ubicacion_geografica = "'.$datos['latitude'].','.$datos['longitude'].'" WHERE vis_id =  '.$datos['vis_id'].';';
      $res = $_obj_database->ejecutarSql($sql); 


      if($res == 1){
        header('Location: index.php?m=visitas&accion=listarVisitas');
      }

      return $contenido;
    }

    /** listadoIntervencion
     * parametro: $datos
     * autor : SA - 05/01/2021
     * descripcion: Lista las visitas registradas
     **/
    function listadoIntervencion($datos){
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_intervencion.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
      $label_inicio = "intervenció";
      $msg_descripcion = "Llistat intervenció";

      //Revisar si esta abierta una visita
      $sql = "SELECT * FROM visita WHERE vis_usu_id_promotor = ".$_SESSION['usu_id']." AND vis_hora_fin is NULL;";
      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      if($resultado_sql && $datos['vis_id'] == $datos['vis_id_consulta']){
        $visitas_abiertas .= '<a href="index.php?m=visitas&accion=cerrar_visitas" class="btn icon icon-left btn-primary"><i data-feather="check-circle"></i> Tancar Visita Oberta</a>';
        $visitas_abiertas .= '<a href="index.php?m=visitas&accion=Intervencion&vis_id='.$datos['vis_id'].'&ces_id='.$datos['ces_id'].'" class="btn icon icon-left btn-info"><i data-feather="info"></i> Afegir intervenció Oberta</a>';
        $visitas_abiertas .= '<a href="index.php?m=visitas&accion=listar_intervencion&vis_id='.$datos['vis_id'].'" class="btn icon icon-left btn-success"><i data-feather="check-circle"></i> Llistat intervenció Oberta</a>';
      }
    
      $agregar_intervencion = '<a href="index.php?m=visitas&accion=Intervencion&vis_id='.$datos['vis_id_consulta'].'&ces_id='.$datos['ces_id_consulta'].'" class="btn icon icon-left btn-warning"><i data-feather="info"></i> Afegir intervenció</a>';
      $agregar_intervencion .= '<a href="index.php?m=visitas&accion=listarVisitas" class="btn icon icon-left btn-outline-dark"><i data-feather="check-circle"></i> Tornar a la llista de les Visites</a>';

      Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("visitas_abiertas", $visitas_abiertas, $contenido);
      Interfaz::asignarToken("agregar_intervencion", $agregar_intervencion, $contenido);


      return $contenido;
  }

  public function obtenerTiposAbsentismo($datos) {
    global $_obj_database;
  
    $sql = "SELECT * FROM tipo_absentismo";
  
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
  
    return json_encode(
      $resultado
    );
  }

  /** actEstIntervencion
   * parametro: $datos
   * autor : SA - 04/12/2020
   * descripcion: Inserta los datos del periodo escolar en base de datos
   **/
  function actEstIntervencion($datos) {
    global $_obj_database, $_PATH_IMAGENES, $_PATH_WEB, $_opciones;

    foreach($datos['observaciones'] as $key => $value){

      $sql_transaccion = 'INSERT INTO intervencion_estudiante (ies_int_id, ies_est_id, ies_observacion) VALUES('.$datos['int_id'][$key].', ' .$datos['est_id'][$key].', "' .$value.'");';
      
      $_obj_database->ejecutarSql($sql_transaccion);

        $sql = "UPDATE intervencion SET int_revisado = 1 WHERE int_id = ".$datos['int_id'][$key].";";

      $res = $_obj_database->ejecutarSql($sql); 
    }
    return $contenido;
  }
  
  
  /** abrirFormularioHorasAdicionales
   * parametro: $datos
   * autor : BDL - 27/01/2021
   * descripcion: Abrir el formulario para el registro de horas
   */
  function abrirFormularioHorasAdicionales($datos){
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registrar_horas_ad.html");

    $label_inicio = "Hores d'intervenció fora de centre";
    $msg_descripcion = "Hores d'intervenció";
    $breadcump_reporte = "Hores d'intervenció";
    $input_visita_h_a_fecha = "Data*";
    $input_visita_h_a_horas = "Hores Treballades*";
    $input_visita_h_a_observaciones = "Observacions";
    $prom_id = $datos['prom_id'];
    if(isset($datos['mensaje'])){
        $mensaje = $datos['mensaje'];
    }

    Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("input_horas_adicionales", $breadcump_reporte, $contenido);


    Interfaz::asignarToken("input_visita_h_a_fecha", $input_visita_h_a_fecha, $contenido);
    Interfaz::asignarToken("input_visita_h_a_horas", $input_visita_h_a_horas, $contenido);
    Interfaz::asignarToken("input_visita_h_a_observaciones", $input_visita_h_a_observaciones, $contenido);
    Interfaz::asignarToken("prom_id", $prom_id, $contenido);
    
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);

    return $contenido;
  }
  
  /** registrarHorasAdicionales
   * parametros: $datos
   * autor: BDL - 27/01/2021
   * descripcion: Insertar datos de registro de horas adiconales
   */
  function registrarHorasAdicionales($datos){
    global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

    $datos = Herramientas::trimCamposFormulario($datos);

    $campos = array(
    "hap_fecha",     
    "hap_horas_trabajadas", 	
    "hap_observaciones",
    "hap_usu_id_promotor",
    );        
    
    $datos['tabla'] = 'horas_adicionales_promotor';
    $datos['hap_fecha'] = date("Y-m-d", strtotime($datos['hap_fecha']));
    $datos['hap_usu_id_promotor'] = intval($datos['hap_usu_id_promotor']);

    $sql = $_obj_database->generarSQLInsertar($datos, $campos);

    $res = $_obj_database->ejecutarSql($sql);

    if($res == 1){
        return 'VTAD-01';
    } else {
        return 'VTAD-04';
    }

  }

  /** listadoHorasAdicionales
   * parametro: $datos
   * autor : BDL - 28/01/2021
   * descripcion: Lista Horas adicionas fuera del centro
   **/
  function listadoHorasAdicionales($datos){
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_horas_ad.html");

    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
    $label_inicio = "Hores d'intervenció fora de centre";
    $msg_descripcion = "Llistat Hores d'intervenció";

    Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("visitas_abiertas", $visitas_abiertas, $contenido);

    return $contenido;
  }

  /** eliminarHorasAdicionales
   * parametro: $datos
   * autor : BDL - 28/01/2021
   * descripcion: Eliminar registro de la tabla horas_adicionales_promotor
   **/
  function eliminarHorasAdicionales($datos){
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;
    if(isset($_SESSION))
    {
        $usu_id = $_SESSION['usu_id'];
    }

    if (!$_obj_database->iniciarTransaccion()) {
        //problma al inicar la transaccion
        return -1003;
    }

    $hap_id = $datos['hap_id'];

    $sql = "DELETE FROM horas_adicionales_promotor WHERE hap_id ='$hap_id';";
    if (!$_obj_database->ejecutarSql($sql)) {
        $_obj_database->cancelarTransaccion();
        return -1003;
    } else {
        $_obj_database->terminarTransaccion();
    }
    return 'VTAD-03';
  }

  /** abrirFormEditarHorasAdicionales
   * parametro: $datos
   * autor : BDL - 28/01/2021
   * descripcion: Formulario para editar horas_adicionales_promotor
   **/

  function abrirFormEditarHorasAdicionales($datos,$valores){
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_horas_adicionales.html");

    $label_inicio = "Hores d'intervenció fora de centre";
    $msg_descripcion = "Hores d'intervenció";
    $breadcump_reporte = "Hores d'intervenció";
    $input_visita_h_a_fecha = "Data*";
    $input_visita_h_a_horas = "Hores Treballades*";
    $input_visita_h_a_observaciones = "Observacions";
    //$prom_id = $datos['prom_id'];

    if(isset($datos['mensaje'])){
        $mensaje = $datos['mensaje'];
    }

    $valores = $valores[0];
    $fecha = date('Y-m-d', strtotime($valores['hap_fecha']));

    Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("input_horas_adicionales", $breadcump_reporte, $contenido);


    Interfaz::asignarToken("input_visita_h_a_fecha", $input_visita_h_a_fecha, $contenido);
    Interfaz::asignarToken("hap_fecha", $fecha, $contenido);
    Interfaz::asignarToken("input_visita_h_a_horas", $input_visita_h_a_horas, $contenido);
    Interfaz::asignarToken("hap_horas_trabajadas", $valores['hap_horas_trabajadas'], $contenido);
    Interfaz::asignarToken("input_visita_h_a_observaciones", $input_visita_h_a_observaciones, $contenido);
    Interfaz::asignarToken("hap_observaciones", $valores['hap_observaciones'], $contenido);
    Interfaz::asignarToken("prom_id", $valores['hap_usu_id_promotor'], $contenido);
    Interfaz::asignarToken("hap_id", $valores['hap_id'], $contenido);
    
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);

    return $contenido;
  }

  /** editarHorasAdicionales
   * parametro: $datos
   * autor : BDL - 28/01/2021
   * descripcion: Editar registro de la tabla horas_adicionales_promotor
   **/

  function editarHorasAdicionales($datos){
    global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;
        
    $datos = Herramientas::trimCamposFormulario($datos);

    $campos = array(
      "hap_fecha",     
      "hap_horas_trabajadas", 	
      "hap_observaciones",
      "hap_usu_id_promotor",
    );        
      
    $datos['tabla'] = 'horas_adicionales_promotor';
    $datos['hap_fecha'] = date("Y-m-d", strtotime($datos['hap_fecha']));
    $datos['condicion'] = "hap_id = '" . $datos['hap_id'] . "' ";

    $res = $_obj_database->actualizarDatosTabla($datos, $campos);

    if (!$res) {
        $_obj_database->cancelarTransaccion();
        $_db_obj['see']->cancelarTransaccion();
        return -1003;
    }

    if($res == 1){
        return 'VTAD-02';
    } else {
        return 'VTAD-05';
    }
  }

  function obtenerDatosHorasAdicionales($hap_id){
    global $_obj_database;

    $sql = "SELECT * FROM horas_adicionales_promotor
    WHERE hap_id=$hap_id";

    $res = $_obj_database->obtenerRegistrosAsociativos($sql);
    return ($res);
  }

  /** visualizarIntervencion
   * parametro: $datos
   * autor : SA - 02/02/2021
   * descripcion: Visualizacion de la informacion de las intervenciones
   **/
  public function visualizarIntervencion($datos) {
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database, $_obj_interfaz;

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/visualizacion_intervenciones.html");
    $msg_titulo = "Intervenció";
    $msg_descripcion = "Visualització d'Intervencions";
    $img_ruta = "Includes/images";

    $sql = "SELECT intervencion.int_id, intervencion.int_fecha, intervencion.int_vis_id, centro_escolar.ces_nombre, 
            motivo_intervencion.min_nombre, intervencion.int_tab_id, periodos_escolares.pes_ano_lectivo, periodos_escolares.pes_periodo, 
            periodos_escolares.pes_fecha_inicio, periodos_escolares.pes_fecha_fin, tipo_intervencion.tin_nombre, 
            intervencion.int_otro_motivo_intervencion, intervencion.int_otro_tipo_intervencion  
            FROM intervencion 
            JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
            JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id 
            JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
            JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
            WHERE intervencion.int_id = ".$datos['int_id'].";";

    $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

    $intervencion = $resultado_sql[0];

    $num_int_id = $intervencion['int_id'];
    $fecha_intervencion = $intervencion['int_fecha'];
    $nombre_centro_escolar = $intervencion['ces_nombre'];
    $motivo_intervencion = $intervencion['min_nombre'];
    if($intervencion['int_tab_id'] != 0){
      $sql = "SELECT tab_nombre FROM tipo_absentismo WHERE tab_id = ".$intervencion['int_tab_id'].";";
      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);
      $nombre_tab = $resultado_sql[0]['tab_nombre'];
    }
    $pes_ano_lectivo = $intervencion['pes_ano_lectivo'];
    $pes_periodo = $intervencion['pes_periodo'];
    $pes_fecha_inicio = $intervencion['pes_fecha_inicio'];
    $pes_fecha_fin = $intervencion['pes_fecha_fin'];
    $tin_nombre = $intervencion['tin_nombre'];
    $int_otro_motivo_intervencion = $intervencion['int_otro_motivo_intervencion'];
    $int_otro_tipo_intervencion = $intervencion['int_otro_tipo_intervencion'];

    //Estudiantes

    $sql = "SELECT estudiante.est_id, estudiante.est_idalu, estudiante.est_nombre, intervencion_estudiante.ies_observacion
            FROM intervencion_estudiante 
            JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id
            WHERE intervencion_estudiante.ies_int_id = ".$num_int_id.";";
    $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

    foreach($resultado_sql as $key => $estudiante){
      $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $estudiante['est_nombre']);
      $datos_estudiante .= '<div class="col-12 col-md-6 col-lg-6">
                          <div class="card">
                            <div class="card-header">';
      $datos_estudiante .= '<h3 class="card-title">'.$alumn_name.' - '.$estudiante['est_idalu'].'</h4>';
      $datos_estudiante .= '<p style="font-weight:700;">Observaciones</p>';
      $datos_estudiante .= '<textarea rows="10" cols="50" disabled>'.$estudiante['ies_observacion'].'</textarea></div>';
      $datos_estudiante .= '</div></div>';
    }
    
    if($_SESSION['tipo_usuario'] == 2){
      if(!$datos['info']){
        $botones = '<a href="index.php?m=visitas&accion=listar_intervencion&vis_id='.$intervencion['int_vis_id'].'" class="btn icon icon-left btn-outline-dark"><i data-feather="check-circle"></i> Tornar a la llista de les intervencions</a>
        <a href="index.php?m=visitas&accion=listarVisitas" class="btn icon icon-left btn-outline-info"><i data-feather="check-circle"></i> Tornar a la llista de les visites</a>';
      }
    }
    else{
      if(!$datos['info']){
        $botones = "<a href='index.php?m=visitas&accion=visualizar_intervenciones' class='btn icon icon-left btn-outline-dark'><i data-feather='check-circle'></i> Tornar a les recerques d'intervencions</a>";
      }
      if($datos['vis_id']){
        $botones = "<a href='index.php?m=visitas&accion=actVisualizarIntervenciones&promotor=".$datos['promotor']."&vis_id=".$datos['vis_id']."&fecha_inicio=".$datos['fecha_inicio']."&fecha_fin=".$datos['fecha_fin']."' class='btn icon icon-left btn-outline-dark'><i data-feather='check-circle'></i> Tornar a les recerques d'intervencions</a>";
      }
    }

    Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);

    Interfaz::asignarToken("num_int_id", $num_int_id, $contenido);
    Interfaz::asignarToken("fecha_intervencion", $fecha_intervencion, $contenido);
    Interfaz::asignarToken("nombre_centro_escolar", $nombre_centro_escolar, $contenido);
    Interfaz::asignarToken("motivo_intervencion", $motivo_intervencion, $contenido);
    Interfaz::asignarToken("nombre_tab", $nombre_tab, $contenido);
    Interfaz::asignarToken("pes_ano_lectivo", $pes_ano_lectivo, $contenido);
    Interfaz::asignarToken("pes_periodo", $pes_periodo, $contenido);
    Interfaz::asignarToken("pes_fecha_inicio", $pes_fecha_inicio, $contenido);
    Interfaz::asignarToken("pes_fecha_fin", $pes_fecha_fin, $contenido);
    Interfaz::asignarToken("tin_nombre", $tin_nombre, $contenido);
    Interfaz::asignarToken("int_otro_motivo_intervencion", $int_otro_motivo_intervencion, $contenido);
    Interfaz::asignarToken("int_otro_tipo_intervencion", $int_otro_tipo_intervencion, $contenido);
    Interfaz::asignarToken("datos_estudiante", $datos_estudiante, $contenido);
    Interfaz::asignarToken("int_vis_id", $intervencion['int_vis_id'], $contenido);
    Interfaz::asignarToken("botones", $botones, $contenido);

    return $contenido;
  }

  /** editarIntervenciones
   * parametro: $datos
   * autor : SA - 04/02/2021
   * descripcion: Editar Interveciones
   **/
  function editarIntervenciones($datos){
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database, $_obj_interfaz;
    
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/editar_intervencion.html");

    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	

    $sql = "SELECT * FROM intervencion WHERE int_id = ".$datos['int_id']."";
    $intervencion_varias = $_obj_database->obtenerRegistrosAsociativos($sql);

    $intervencion = $intervencion_varias[0];

    $msg_titulo = "Intervenció";
    $msg_descripcion = "Edita Intervencions";
    $img_ruta = "Includes/images";

    $fecha = "Date";
    $mot_int = "Motiu de la intervencio";
    $tipo_int = "Tipus d'intervenció";
    $label_estudiante = "ESTUDIANTS";

    $fecha_actual = $intervencion['int_fecha'];

    $mot_int_select = '<select class="choices form-select multiple-remove" id="int_min_id" name="int_min_id" required><option value="" selected>Escollir...</option>';

    //EDITAR MOTIVO DE INTERVENCION
    $sql = "SELECT * FROM motivo_intervencion;";

    $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

    foreach($resultado_sql as $motivos){
      if($motivos['min_id'] == $intervencion['int_min_id']){
        $mot_int_select .= '<option value="'.$motivos['min_id'].'" selected>'.$motivos['min_nombre'].'</option>';
        if($motivos['min_nombre'] == "ALTRES"){
          $altres_mot_int_select = "<label for='first-name-column'>Un altre motiu d'Intervencion</label>";
          $altres_mot_int_select .= '<textarea required id="int_otro_motivo_intervencion" rows="10" cols="50" name="int_otro_motivo_intervencion" class="form-control">'.$intervencion['int_otro_motivo_intervencion'].'</textarea>';
        }
        else{
          if($motivos['min_nombre'] == "ABSENTISME"){
            $sql = "SELECT * FROM tipo_absentismo;";
            $all_tab = $_obj_database->obtenerRegistrosAsociativos($sql);

            $tab_int_select = "<label for='first-name-column'>TIPUS D'ABSENTISME</label>";
            $tab_int_select .= '<select class="choices form-select multiple-remove" name="tab_id" id="tab_id" required>';

            foreach($all_tab as $tab){
              if($tab['tab_id'] == $intervencion['int_tab_id']){
                $tab_int_select .= '<option value="'.$tab['tab_id'].'" selected>'.$tab['tab_nombre'].'</option>';
              }
              else{
                $tab_int_select .= '<option value="'.$tab['tab_id'].'">'.$tab['tab_nombre'].'</option>';
              }
            }
            $tab_int_select .= '</select>';
          }
        }
      }
      else{
        $mot_int_select .= '<option value="'.$motivos['min_id'].'">'.$motivos['min_nombre'].'</option>';
      }
    }
    $mot_int_select .= '</select>';

    //OBTENER ESTUDIANTES DE ESE CENTRO ESCOLAR Y SELECCIONAR LOS GUARDADOS
    $sql = "SELECT est_id, est_nombre FROM estudiante WHERE est_ces_id = ".$intervencion['int_ces_id']." AND est_desactivo != 'on';";
    $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

    $sql = "SELECT intervencion_estudiante.ies_id, intervencion_estudiante.ies_est_id, intervencion_estudiante.ies_observacion,
            estudiante.est_idalu, estudiante.est_nombre 
            FROM intervencion_estudiante 
            JOIN estudiante ON estudiante.est_id = ies_est_id
            WHERE ies_int_id = ".$intervencion['int_id'].";";
    $inter_est = $_obj_database->obtenerRegistrosAsociativos($sql);

    foreach($resultado_sql as $key => $estudiantes){
      foreach($inter_est as $est){
        if($estudiantes['est_id'] == $est['ies_est_id']){
          $resultado_sql[$key]['get'] = 1;
        }
      }
    }

    if($resultado_sql){
      $estudiantes = '<select class="choices form-select multiple-remove" id="estudiantes" name="estudiantes[]" multiple="multiple" required>';
      foreach($resultado_sql as $tipos){
        $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $tipos['est_nombre']);
        if($tipos['get'] == 1){
          $estudiantes .= '<option selected value="'.$tipos['est_id'].'">'.$alumn_name.'</option>';
        }
        else{
          $estudiantes .= '<option value="'.$tipos['est_id'].'">'.$alumn_name.'</option>';
        }
      }
      $estudiantes .= '</select>';
      $boton_enviar = '<button type="submit" class="btn btn-primary mr-1 mb-1">Enviar</button>';
    }
    else{
      $estudiantes .= "<p style='background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;'>No hi ha estudiants associats a el Centre Escolar</p>";
      $boton_enviar = '<a href="index.php?m=visitas&accion=listar_intervencion&vis_id='.$intervencion['int_vis_id'].'" class="btn btn-primary mr-1 mb-1">Tornar</a>';
    }
    
    //TRAE TODOS LOS TIPOS DE INTERVENCION Y SE COLOCAN LOS SELECCIONADO
    $tipo_int_select = '<select class="choices form-select multiple-remove" id="int_tin_id" name="int_tin_id" required>';

    $sql = "SELECT * FROM tipo_intervencion;";

    $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

    //Marcar los que fueron escogidas
    foreach($resultado_sql as $key => $tipos){
      if($tipos['tin_id'] == $intervencion['int_tin_id']){
        $resultado_sql[$key]['get'] = 1;
      }
      else{
        $resultado_sql[$key]['get'] = 0;
      }
    }

    //Mostrar los tipos escogidos mas los disponibles 
    foreach($resultado_sql as $tipos){
      if($tipos['get'] == 1){
        $tipo_int_select .= '<option selected value="'.$tipos['tin_id'].'">'.$tipos['tin_nombre'].'</option>';
        if($tipos['tin_nombre'] == "Altres "){
          $altres_tin_int_select = "<label for='first-name-column'>Un altre tipus d'Intervencion</label>";
          $altres_tin_int_select .= '<textarea required id="int_otro_tipo_intervencion" rows="10" cols="50" name="int_otro_tipo_intervencion" class="form-control">'.$intervencion['int_otro_tipo_intervencion'].'</textarea>';
        }
      }
      else{
        $tipo_int_select .= '<option value="'.$tipos['tin_id'].'">'.$tipos['tin_nombre'].'</option>';
      }
    }
    $tipo_int_select .= '</select>';

    //Marcar intervenciones a editar
    $sql = "UPDATE intervencion SET int_revisado = 1 WHERE int_id =  ".$intervencion['int_id'].";";
    $res = $_obj_database->ejecutarSql($sql);

    Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("fecha", $fecha, $contenido);
    Interfaz::asignarToken("fecha_actual", $fecha_actual, $contenido);
    Interfaz::asignarToken("mot_int", $mot_int, $contenido);
    Interfaz::asignarToken("mot_int_select", $mot_int_select, $contenido);
    Interfaz::asignarToken("tipo_int", $tipo_int, $contenido);
    Interfaz::asignarToken("tipo_int_select", $tipo_int_select, $contenido);
    Interfaz::asignarToken("label_estudiante", $label_estudiante, $contenido);
    Interfaz::asignarToken("estudiantes", $estudiantes, $contenido);
    Interfaz::asignarToken("int_id", $intervencion['int_id'], $contenido);
    Interfaz::asignarToken("altres_mot_int_select", $altres_mot_int_select, $contenido);
    Interfaz::asignarToken("altres_tin_int_select", $altres_tin_int_select, $contenido);
    Interfaz::asignarToken("tab_int_select", $tab_int_select, $contenido);
    Interfaz::asignarToken("boton_enviar", $boton_enviar, $contenido);

    return $contenido;
  }

  /** actEditarIntervencion
     * parametro: $datos
     * autor : SA - 04/02/2021
     * descripcion: Edita la intervencion con los estudiantes
     **/
    function actEditarIntervencion($datos) {
      global $_obj_database, $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_interfaz;

      if($datos['tab_id']){
        $tab_id = $datos['tab_id'];
      }
      else{
        $tab_id = 0;
      }

      $sql = 'UPDATE intervencion SET int_min_id = '.$datos['int_min_id'].', int_tin_id = '.$datos['int_tin_id'].', 
              int_tab_id = '.$tab_id.', int_otro_motivo_intervencion = "'.$datos['int_otro_motivo_intervencion'].'", 
              int_otro_tipo_intervencion = "'.$datos['int_otro_tipo_intervencion'].'", 
              int_revisado = 0 WHERE int_id =  '.$datos['int_id'].';';
      $res = $_obj_database->ejecutarSql($sql);

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_estudiantes_intervenciones_editar.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

      foreach($datos['estudiantes'] as $estudiante){
        $sql = "SELECT est_nombre, est_idalu FROM estudiante WHERE est_id = ".$estudiante.";";

        $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

        $result = $resultado_sql[0];

        $sql = "SELECT tin_nombre FROM tipo_intervencion WHERE tin_id = ".$datos['int_tin_id'].";";

        $nombre_tin_id = $_obj_database->obtenerRegistrosAsociativos($sql);

        $nombre_tin_id = $nombre_tin_id[0];

        $sql = "SELECT ies_observacion FROM intervencion_estudiante WHERE ies_int_id = ".$datos['int_id']." AND ies_est_id = ".$estudiante.";";

        $observaciones_ies_est = $_obj_database->obtenerRegistrosAsociativos($sql);
        $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $result['est_nombre']);
        $estudiantes_datos .= "<div class='col-md-4 col-12'>
                                <div class='form-group'>
                                  <h4>".$nombre_tin_id['tin_nombre']."</h4>
                                  <h5>".$alumn_name." - ".$result['est_idalu']."</h5>
                                  <label for='first-name-column'>Observacions/Acords</label>
                                  <textarea rows='10' required cols='50' id='observaciones' name='observaciones[]' class='form-control'>".$observaciones_ies_est[0]['ies_observacion']."</textarea>
                                  <input type='hidden' id='est_id' value='".$estudiante."' name='est_id[]'>
                                  <input type='hidden' id='".$datos['int_id']."' value='".$datos['int_id']."' name='int_id'>
                                </div>
                              </div>";

      }
      
      $msg_titulo = "intervenció d'Estudiants";
      $msg_descripcion = "Registre les observacions per estudiants per al registre de la intervenció";
      $img_ruta = "Includes/images";
      $fecha = "Date";
      $mot_int = "Motiu de la intervencio";

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("fecha_actual", $datos['int_fecha'], $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("fecha", $fecha, $contenido);
      Interfaz::asignarToken("estudiantes_datos", $estudiantes_datos, $contenido);
      Interfaz::asignarToken("int_id", $int_id, $contenido);

      return $contenido;
    }
    /** actEditarEstIntervencion
     * parametro: $datos
     * autor : SA - 04/12/2020
     * descripcion: Inserta los datos del periodo escolar en base de datos
     **/
    function actEditarEstIntervencion($datos) {
      global $_obj_database, $_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $sql = "SELECT * FROM intervencion_estudiante WHERE ies_int_id = ".$datos['int_id'].";";

      $int_cantidad = $_obj_database->obtenerRegistrosAsociativos($sql);

      $cantidad_inter_est = count($int_cantidad);

      if(count($int_cantidad) == count($datos['observaciones'])){
        //update
        foreach($datos['observaciones'] as $key => $value){
          $sql = 'UPDATE intervencion_estudiante 
                  SET ies_est_id = '.$datos['est_id'][$key].', ies_observacion = "'.$value.'" 
                  WHERE ies_id = '.$int_cantidad[$key]['ies_id'].';';
          $res = $_obj_database->ejecutarSql($sql); 
        }
      }
      else{
        if(count($int_cantidad) > count($datos['observaciones'])){
          $i=0;
          foreach($int_cantidad as $key => $value){
            if($datos['est_id'][$i] == $value['ies_est_id']){
              //update
              $sql = "SELECT * FROM intervencion_estudiante WHERE ies_int_id = ".$datos['int_id']." AND 
                      ies_est_id = ".$datos['est_id'][$i].";";
              $est_inter = $_obj_database->obtenerRegistrosAsociativos($sql);
  
              $sql = 'UPDATE intervencion_estudiante 
                      SET ies_observacion = "'.$datos['observaciones'][$i].'"
                      WHERE ies_int_id = '.$datos['int_id'].' AND ies_est_id = '.$datos['est_id'][$i].';';
              $res = $_obj_database->ejecutarSql($sql); 
              $i++;
            }
            else{
              $datos['borrar'][$key] = $value['ies_est_id'];
            }
            
          }
          //delete
          foreach($datos['borrar'] as $key => $value){
            $sql = "DELETE FROM intervencion_estudiante WHERE ies_int_id = " . $datos['int_id'] . " AND ies_est_id = ".$value.";";
            
            if (!$_obj_database->ejecutarSql($sql)) {
                $_obj_database->cancelarTransaccion();
                return -1003;
            } else {
                $_obj_database->terminarTransaccion();
            }
          }
        }
        else{
          //update and insert
          $i=0;
          foreach($datos['observaciones'] as $key => $value){
            if($datos['est_id'][$key] == $int_cantidad[$i]['ies_est_id']){
              //update
              $sql = "SELECT * FROM intervencion_estudiante WHERE ies_int_id = ".$datos['int_id']." AND ies_est_id = ".$int_cantidad[$i]['ies_est_id'].";";
              $est_inter = $_obj_database->obtenerRegistrosAsociativos($sql);
  
              $sql = 'UPDATE intervencion_estudiante SET ies_observacion = "'.$datos['observaciones'][$i].'" 
                      WHERE ies_int_id = '.$datos['int_id'].' AND ies_est_id = '.$int_cantidad[$i]['ies_est_id'].';';
              $res = $_obj_database->ejecutarSql($sql); 
              $i++;
            }
            else{
              $datos['insert'][$key] = $datos['est_id'][$key];
            }
          }
          $i=0;
          foreach($datos['insert'] as $key => $value){
            if($i < count($datos['observaciones'])){
              if($datos['est_id'][$i] == $datos['insert'][$key]){
                $sql_transaccion = 'INSERT INTO intervencion_estudiante (ies_int_id, ies_est_id, ies_observacion) 
                VALUES('.$datos['int_id'].', '. $datos['est_id'][$i] .', "' .$datos['observaciones'][$i].'")';
                $_obj_database->ejecutarSql($sql_transaccion);
                $i++;
              }
            }
          }
        }
      }
      return $contenido;
    }

    /** eliminarIntervencion
   * parametro: $datos
   * autor : SA 05/02/2021
   * descripcion: Elimina las intervenciones junto con los estudiantes a esa intervencion
   **/
  function eliminarIntervencion($datos){
    global $_obj_database;
    
    if(isset($_SESSION))
    {
        $usu_id = $_SESSION['usu_id'];
    }

    if (!$_obj_database->iniciarTransaccion()) {
        //problema al inicar la transaccion
        return -1003;
    }

    $sql = "DELETE FROM intervencion_estudiante WHERE ies_int_id = '" . $datos['int_id'] . "';";
    if (!$_obj_database->ejecutarSql($sql)) {
        $_obj_database->cancelarTransaccion();
        return -1003;
    } else {
        $_obj_database->terminarTransaccion();
    }

    $sql = "DELETE FROM intervencion WHERE int_id = '" . $datos['int_id'] . "';";
    if (!$_obj_database->ejecutarSql($sql)) {
        $_obj_database->cancelarTransaccion();
        return -1003;
    } else {
        $_obj_database->terminarTransaccion();
    }

    return 'INT-3';
    
  }

    /** formVisualizarIntervencion
     * parametro: $datos
     * autor : SA - 09/02/2021
     * descripcion: Formulario para consultar intervenciones segun fechas y promotores
     **/
    public function formVisualizarIntervencion($datos) {
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_visualizar_intervencion_admin.html");

      $fecha_actual = date("Y-m-d");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	

      $msg_titulo = "Visualització d'Intervencions per Promotor";
      $msg_descripcion = "Introduïu les dades per fer la consulta";
      $img_ruta = "Includes/images";
      $fecha = "Seleccioneu els rangs de Data a consultar";
      $promotor = "Esculli el seu promotor";

      //Se obtienen los datos de los promotores
      //Administrador
      if($_SESSION['tipo_usuario'] == 1){
        $sql = "SELECT * FROM usuarios WHERE usu_per_id = 2;";
      }
      else{
        //Coordinador
        if($_SESSION['tipo_usuario'] == 4){
          $sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre 
                  FROM usuario_territorio 
                  JOIN usuario_centro_escolar ON usuario_territorio.ust_ter_id = usuario_centro_escolar.usc_ter_id
                  JOIN usuarios ON usuarios.usu_id = usuario_centro_escolar.usc_usu_id_promotor
                  WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id']." AND 
                  usuarios.usu_per_id = 2;";
        }
        //Director
        if($_SESSION['tipo_usuario'] == 3){
          $sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre 
                  FROM usuario_centro_escolar 
                  JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
                  JOIN usuarios ON usuarios.usu_id = usuario_centro_escolar.usc_usu_id_promotor 
                  WHERE centro_escolar.ces_usu_id_director = ".$_SESSION['usu_id']." AND 
                  usuarios.usu_per_id = 2;";
        }
      }
      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);
      //Se corroboro que existieran datos
      if($resultado_sql){
        $promotores_int_select = '<select class="form-select" id="promotor" name="promotor" required><option value="" selected>Escollir...</option>';
        
        foreach($resultado_sql as $promotores){
          $promotores_int_select .= '<option value="'.$promotores['usu_id'].'">'.$promotores['usu_nombre'].'</option>';
        }

        $promotores_int_select .= '</select>';
        $boton = '<button type="submit" class="btn btn-primary mr-1 mb-1">Consultar</button>';
      }
      else{
        $promotores_int_select = "<p style='background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;'>No hi ha promotors en el seu territori.</p>";
        $boton = '<a class=" btn btn-primary" href="index.php?m=Usuarios&accion=dashboard">Regresar</a>';
      }

      Interfaz::asignarToken("boton", $boton, $contenido);
      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("fecha_actual", $fecha_actual, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("fecha", $fecha, $contenido);
      Interfaz::asignarToken("promotor", $promotor, $contenido);
      Interfaz::asignarToken("promotores_int_select", $promotores_int_select, $contenido);

      return $contenido;
    }

    /** actVisualizarIntervenciones
     * parametro: $datos
     * autor : SA - 04/01/2021
     * descripcion: Inserta los datos para cerrar los datos 
     **/
    function actVisualizarIntervenciones($datos) {
      global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones; 

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_intervencion_administrador.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
      $label_inicio = "Intervencion";
      $msg_descripcion = "Llistat Intervencion";

      Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);

      $fechas = $datos['fecha_inicio'] . " fins " . $datos['fecha_fin'];
      Interfaz::asignarToken("fechas", $fechas, $contenido);

      $sql = "SELECT usu_nombre, usu_apellido FROM usuarios WHERE usu_id = ".$datos['promotor'].";";
      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      $promotor = $resultado_sql[0]['usu_nombre'] . " " . $resultado_sql[0]['usu_apellido'];
      Interfaz::asignarToken("promotor", $promotor, $contenido);

      return $contenido;
    }

    /** editarVisitas
     * parametro: $datos
     * autor : SA - 16/03/2021
     * descripcion: Formulario para editar las vsitas
     **/
    public function editarVisitas($datos) {
      global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_visitas.html");
      $msg_titulo = "Visites";
      $msg_descripcion = "Edició de les visites";
      $img_ruta = "Includes/images";
      $fecha = "Data";
      $hora_inicio = "Hora D'Inici";
      $hora_fin = "Hora Final";
      $centro_escolar = "Centre Educatiu";
      $observaciones = "Observacions/acords";

      $fecha_visita = $datos['visita']['vis_fecha'];
      $hora_inicio_visita = $datos['visita']['vis_hora_inicio'];
      $hora_fin_visita = $datos['visita']['vis_hora_fin'];

      //Centro Escolar
      $centro_escolar_select = '<select class="form-select" id="vis_ces_id " name="vis_ces_id" required><option value="" selected>Escollir...</option>';

      $sql = "SELECT usuario_centro_escolar.usc_ces_id, centro_escolar.ces_nombre 
              FROM usuario_centro_escolar 
              JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
              WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$datos['visita']['vis_usu_id_promotor'].";";

      $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

      foreach($resultado_sql as $centros){
        if($datos['visita']['vis_ces_id'] == $centros['usc_ces_id']){
          $centro_escolar_select .= '<option selected value="'.$centros['usc_ces_id'].'">'.$centros['ces_nombre'].'</option>';
        }
        else{
          $centro_escolar_select .= '<option value="'.$centros['usc_ces_id'].'">'.$centros['ces_nombre'].'</option>';
        }
        
      }
      $centro_escolar_select .= '</select>';

      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
      Interfaz::asignarToken("fecha_actual", $fecha_visita, $contenido);
      Interfaz::asignarToken("hora_actual", $hora_inicio_visita, $contenido);
      Interfaz::asignarToken("hora_fin_visita", $hora_fin_visita, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("fecha", $fecha, $contenido);
      Interfaz::asignarToken("hora_inicio", $hora_inicio, $contenido);
      Interfaz::asignarToken("hora_fin", $hora_fin, $contenido);
      Interfaz::asignarToken("centro_escolar", $centro_escolar, $contenido);
      Interfaz::asignarToken("centro_escolar_select", $centro_escolar_select, $contenido);
      Interfaz::asignarToken("observaciones", $observaciones, $contenido);
      Interfaz::asignarToken("observacion", $datos['visita']['vis_observaciones'], $contenido);
      Interfaz::asignarToken("vis_id", $datos['visita']['vis_id'], $contenido);
 
      return $contenido;
    }

     /** actEditarVisitas
     * parametro: $datos
     * autor : SA - 16/03/2021
     * descripcion: Actualiza los datos de la edicion de una visita
     **/
    function actEditarVisitas($datos) {
      global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $checkTime = strtotime($datos['vis_hora_fin']);
      $loginTime = strtotime($datos['vis_hora_inicio']);
      $diff = $checkTime - $loginTime;
      $horas_trabajadas = gmdate("H:i:s", $diff);

      $sql = "SELECT * FROM visita WHERE vis_id = ".$datos['vis_id'].";";
      $resultado_visita = $_obj_database->obtenerRegistrosAsociativos($sql);

      //Entro cuando los centros son diferentes
      if($resultado_visita[0]['vis_ces_id'] != $datos['vis_ces_id']){
        $sql = "SELECT * FROM intervencion WHERE int_vis_id = ".$datos['vis_id'].";";
        $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

        if($resultado_sql){
          $mensaje = "V-7";
        }
        else{
          $sql = 'UPDATE visita SET vis_fecha = "'. $datos['vis_fecha'] .'", 
                  vis_hora_inicio = "'. $datos['vis_hora_inicio'] .'", 
                  vis_hora_fin = "'. $datos['vis_hora_fin'] .'", 
                  vis_horas_trabajadas = "'. $horas_trabajadas .'", 
                  vis_observaciones = "'. $datos['vis_observaciones'] .'", 
                  vis_ces_id  = '.$datos['vis_ces_id'].' WHERE vis_id =  '.$datos['vis_id'].';';
          $res = $_obj_database->ejecutarSql($sql);
          if($res == 1){
            $mensaje = 'V-4';
          }
        }
      }
      else{
        //Cuando la hora fin sea mayor a la de inicio
        if($datos['vis_hora_fin'] > $datos['vis_hora_inicio']){
          $sql = 'UPDATE visita SET vis_fecha = "'. $datos['vis_fecha'] .'", 
              vis_hora_inicio = "'. $datos['vis_hora_inicio'] .'", 
              vis_hora_fin = "'. $datos['vis_hora_fin'] .'", 
              vis_horas_trabajadas = "'. $horas_trabajadas .'", 
              vis_observaciones = "'. $datos['vis_observaciones'] .'", 
              vis_ces_id  = '.$datos['vis_ces_id'].' WHERE vis_id =  '.$datos['vis_id'].';';

          $res = $_obj_database->ejecutarSql($sql);
          $sql = "SELECT * FROM periodos_escolares WHERE pes_fecha_inicio <= '".$datos['vis_fecha']."' AND pes_fecha_fin >= '".$datos['vis_fecha']."';";
  
          $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);
  
          $periodo_escolar = $resultado_sql[0];

          $sql = 'UPDATE intervencion SET int_fecha = "'. $datos['vis_fecha'] .'", 
                  int_pes_id = '.$periodo_escolar['pes_id'].' WHERE int_vis_id =  '.$datos['vis_id'].';';
          $int = $_obj_database->ejecutarSql($sql);
          if($res == 1 && $int == 1){
            $mensaje = 'V-4';
          }
        }
        else{
          $mensaje = "V-8";
        }
      }
      return $mensaje;
    }

  /** eliminarVisita
   * parametro: $datos
   * autor : SA 16/03/2021
   * descripcion: Elimina la visita solo si no tiene intervenciones
   **/
  function eliminarVisita($datos){
    global $_obj_database;

    if (!$_obj_database->iniciarTransaccion()) {
        //problema al inicar la transaccion
        return -1003;
    }

    $sql = "SELECT * FROM intervencion WHERE int_vis_id = ".$datos['vis_id'].";";
    $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

    if($resultado_sql){
      $mensaje = "V-6";
    }
    else{
      //No hay intervenciones asociadas a la visita
      $sql = "DELETE FROM visita WHERE vis_id = '" . $datos['vis_id'] . "';";
      if (!$_obj_database->ejecutarSql($sql)) {
          $_obj_database->cancelarTransaccion();
          return -1003;
      } else {
          $_obj_database->terminarTransaccion();
          $mensaje = "V-5";
      }
    }

    return $mensaje;
    
  }
}
?>