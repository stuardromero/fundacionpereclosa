<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("Visitas.php");

$obj_usuarios = new Usuarios();
$obj_visitas = new Visitas();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");


$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'];

switch($datos['accion'])
{
	case 'Intervencion'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_visitas->abrirFormularioIntervencion($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'actIntervencion'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_visitas->actIntervencion($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'actEstIntervencion'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_visitas->actEstIntervencion($datos);
		$_obj_interfaz->asignarContenido($contenido);

		$datos['sql'] = "SELECT visita.vis_id FROM intervencion_estudiante 
						JOIN intervencion ON intervencion.int_id = intervencion_estudiante.ies_int_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						WHERE intervencion_estudiante.ies_int_id = ".$datos['int_id'][0].";";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		header('Location: index.php?m=visitas&accion=listar_intervencion&vis_id='.$resultado[0]['vis_id'].'&msg=INT-EST-1');
		break;
	case 'agregar_visitas'.validarAcceso(array(2)):
		$datos['usu_id'] = $_SESSION['usu_id'];

		$sql = "SELECT * FROM visita WHERE vis_usu_id_promotor = ".$_SESSION['usu_id']." AND vis_hora_fin is NULL;";
		$resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if($resultado_sql){
			$enlace = "index.php?m=visitas&accion=listarVisitas&tipo_gestion=3&msg=V-1";
			header('Location: '.$enlace);
		}
		else{
			$contenido = $obj_visitas->abrirFormularioVisitas($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		break;
	case 'actVisitas'.validarAcceso(array(2)):
		$contenido = $obj_visitas->actVisitas($datos);
		$_obj_interfaz->asignarContenido($contenido);
		header('Location: index.php?m=visitas&accion=listarVisitas&msg=V-0'); 
		break;
	case 'listarVisitas'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$sql = "SELECT * FROM visita WHERE vis_usu_id_promotor = ".$_SESSION['usu_id']." AND vis_hora_fin is NULL;";

		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		$datos['vis_id'] = $resultado[0]['vis_id'];
		$datos['ces_id'] = $resultado[0]['vis_ces_id'];

		$contenido = $obj_visitas->listadoVisitas($datos);
		$campos = array(
			"vis_id" => "ID", 
			"vis_fecha" => "Data",
			"vis_hora_inicio" => "Hora d'inici",
			"vis_hora_fin" => "Hora Final",
			"vis_horas_trabajadas" => "Hores Treballades", 
			"ces_nombre" => "Centre Escolar",
			"vis_observaciones" => "Observacions"
		); 

		if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

		$opciones['actualizar'] = "usc_usu_id_promotor=$usu_id";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id";


		$datos['sql'] = "SELECT vis.vis_id, vis.vis_fecha, vis.vis_hora_inicio, vis.vis_hora_fin, vis.vis_horas_trabajadas, ces.ces_nombre, vis.vis_observaciones
						 FROM visita as vis 
						 JOIN centro_escolar as ces ON ces.ces_id = vis.vis_ces_id 
						 WHERE vis.vis_usu_id_promotor = $usu_id ORDER BY vis_id DESC;";

		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $resultado;
		$arreglo_datos['OPCIONES'] = $opciones;
	
		$contenido .= $_obj_interfaz->crearListadoVisitas($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'editarIntervenciones'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_visitas->editarIntervenciones($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'actEditarIntervencion'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_visitas->actEditarIntervencion($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'actEditarEstIntervencion'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_visitas->actEditarEstIntervencion($datos);
		$_obj_interfaz->asignarContenido($contenido);

		$datos['sql'] = "SELECT int_vis_id FROM intervencion WHERE int_id = ".$datos['int_id'].";";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		header('Location: index.php?m=visitas&accion=listar_intervencion&vis_id='.$resultado[0]['int_vis_id'].'&msg=INT-EST-1');
		break;
	case 'eliminar_intervencion'.validarAcceso(array(2)):
		$resultado = $obj_visitas->eliminarIntervencion($datos);
		
		$enlace = "index.php?m=visitas&accion=listarVisitas&msg=".$resultado."";
		header('Location: '.$enlace);
		
		break;
	case 'cerrar_visitas'.validarAcceso(array(2)):
		$contenido = $obj_visitas->abrirFormularioCerrarVisitas($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'actCerrarVisitas'.validarAcceso(array(2)):
		$contenido = $obj_visitas->actCerrarVisitas($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'listar_intervencion'.validarAcceso(array(2)):	
		$datos['vis_id_consulta'] = $datos['vis_id'];

		$sql = "SELECT vis_ces_id FROM visita WHERE vis_id = ".$datos['vis_id_consulta'].";";

		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		$datos['ces_id_consulta'] = $resultado[0]['vis_ces_id'];

		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}	
		
		$sql = "SELECT * FROM visita WHERE vis_usu_id_promotor = ".$_SESSION['usu_id']." AND vis_hora_fin is NULL;";

		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		$datos['vis_id'] = $resultado[0]['vis_id'];
		$datos['ces_id'] = $resultado[0]['vis_ces_id'];

		$contenido = $obj_visitas->listadoIntervencion($datos);

		$campos = array(
			"int_id" => "#", 
			"int_fecha" => "Data",
			"int_min_id" => "Motiu de la Intervencio",
			"int_tab_id" => "Tipus d'Absentisme",
			"int_pes_id " => "Períodes Acadèmics", 
			"int_tin_id" => "Tipus d'Intervencions",
			"int_otro_motivo_intervencion" => "Altres motiu d'Intervencions",
			"int_otro_tipo_intervencion" => "Altres tipus d'Intervencions",
			"int_ces_id" => "Centre Escolar",
			"est_nombre" => "Alumnes"
		);  
		

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}
		$opciones['actualizar'] = "usc_usu_id_promotor=$usu_id";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id";

		if(isset($datos['vis_id_consulta'])){
			$datos['sql'] = "SELECT * FROM intervencion WHERE int_vis_id = ".$datos['vis_id_consulta']." ORDER BY int_id DESC;";
		}
		else{
			$datos['sql'] = "SELECT * FROM intervencion WHERE int_vis_id = ".$datos['vis_id']." ORDER BY int_id DESC;";
		}
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $resultado;
		$arreglo_datos['OPCIONES'] = $opciones;
	
		$contenido .= $_obj_interfaz->crearListadoIntervencion($arreglo_datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'obtener_tipos_absentismo'.validarAcceso(array(2)):
		$contenido = $obj_visitas->obtenerTiposAbsentismo($datos);
		print_r($contenido);

		exit;
		break;
	case 'fm_horas_adicionales'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		$datos['prom_id'] = $usu_id;

		$contenido = $obj_visitas->abrirFormularioHorasAdicionales($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'registrarHorasAdicionales'.validarAcceso(array(2)):
		$resultado = $obj_visitas->registrarHorasAdicionales($datos);
		if( $resultado=='VTAD-01' ) 
		{   
			?>
			<script type="text/javascript"> 
				window.location="index.php?m=visitas&accion=fm_horas_adicionales&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
			</script> 
			<?php
			$datos['msg'] = $resultado;
			$datos['tipo_gestion'] = 1;
			//$contenido = $obj_reporte->abrirFormularioRegistrarreporte($datos);
			//$_obj_interfaz->asignarContenido($contenido);
			//header('Location: index.php?m=reportecentro&accion=fm_registrar&tipo_gestion=1&msg=VTAD-01');    
			break;            
		}//Fin de if( $resultado==1 )      
		else {
			$opciones['CADENA'] = "El informe";      
			$opciones['ACCION'] = 1;//ingresar      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);  
			$contenido = $obj_reporte->abrirFormularioRegistrarreporte($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		break;
	case 'listarHorasAdicionales'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}	

		$contenido = $obj_visitas->listadoHorasAdicionales($datos);

		$campos = array(
			"hap_id" => "#", 
			"hap_fecha" => "Data",
			"hap_horas_trabajadas" => "Hores Treballades",
			"hap_observaciones" => "Observacions",
		);  
		
		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		$opciones['actualizar'] = "usc_usu_id_promotor=$usu_id";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id";

		$datos['sql'] = "SELECT * FROM horas_adicionales_promotor WHERE hap_usu_id_promotor = ".$usu_id.";";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $resultado;
		$arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar_horas_adicionales";
		$arreglo_datos['ACCION_ELIMINAR']="eliminarHorasAdicionales";

		$contenido .= $_obj_interfaz->crearListadoHorasAdicionales($arreglo_datos);
		$_obj_interfaz->asignarContenido($contenido);
		//crearListadoHorasAdicionales
		break;
	case 'eliminarHorasAdicionales'.validarAcceso(array(2)):
		$resultado = $obj_visitas->eliminarHorasAdicionales($datos);
		?>
		<script type="text/javascript"> 
			window.location="index.php?m=visitas&accion=listarHorasAdicionales&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
		</script> 
		<?php
		//$enlace = "index.php?m=reportecentro&accion=listarreporte&tipo_gestion=3&msg=".$resultado."";
		//header('Location: '.$enlace);
		break;
	case 'fm_editar_horas_adicionales'.validarAcceso(array(2)):
		$valores = $obj_visitas->obtenerDatosHorasAdicionales($datos['hap_id']);
		if( !is_array($valores))
		{
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
		$datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_visitas->abrirFormEditarHorasAdicionales($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'editarHorasAdicionales'.validarAcceso(array(2)):
		$resultado = $obj_visitas->editarHorasAdicionales($datos);
		if( $resultado=='VTAD-02' ) 
        {         
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=visitas&accion=listarHorasAdicionales&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php        
            //header('Location: index.php?m=reportecentro&accion=listarreporte&tipo_gestion=1&msg='.$resultado);    
            break;            
        }//Fin de if( $resultado==1 )      
		break;
	case 'ver_intervencion'.validarAcceso(array(1,2,3,4)):
		$contenido = $obj_visitas->visualizarIntervencion($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;	
	case 'visualizar_intervenciones'.validarAcceso(array(1,3,4)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_visitas->formVisualizarIntervencion($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'actVisualizarIntervenciones'.validarAcceso(array(1,3,4)):
		$contenido = $obj_visitas->actVisualizarIntervenciones($datos);

		$campos = array(
			"int_id" => "ID", 
			"int_fecha" => "Data",
			"int_min_id" => "Motiu de la Intervencio",
			"int_tab_id" => "Tipus d'Absentisme",
			"int_pes_id " => "Curs Escolar", 
			"int_tin_id" => "Tipus d'Intervencions",
			"int_otro_motivo_intervencion" => "Altres tipus d'Intervencions",
			"int_ces_id" => "Centre Escolar"
		);  
		

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		if($datos['vis_id']){
			$sql = "SELECT visita.vis_id, intervencion.int_id, intervencion.int_fecha, centro_escolar.ces_nombre, 
              motivo_intervencion.min_nombre, intervencion.int_tab_id, periodos_escolares.pes_ano_lectivo, 
              periodos_escolares.pes_periodo, periodos_escolares.pes_fecha_inicio, periodos_escolares.pes_fecha_fin, 
              tipo_intervencion.tin_nombre, intervencion.int_otro_motivo_intervencion 
              FROM visita 
              JOIN intervencion ON visita.vis_id = intervencion.int_vis_id 
              JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
              JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id 
              JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
              JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
              WHERE visita.vis_id = ".$datos['vis_id']." AND visita.vis_fecha 
              BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."' ORDER BY intervencion.int_id DESC;";
		}
		else{
			$sql = "SELECT visita.vis_id, intervencion.int_id, intervencion.int_fecha, centro_escolar.ces_nombre, 
              motivo_intervencion.min_nombre, intervencion.int_tab_id, periodos_escolares.pes_ano_lectivo, 
              periodos_escolares.pes_periodo, periodos_escolares.pes_fecha_inicio, periodos_escolares.pes_fecha_fin, 
              tipo_intervencion.tin_nombre, intervencion.int_otro_motivo_intervencion 
              FROM visita 
              JOIN intervencion ON visita.vis_id = intervencion.int_vis_id 
              JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
              JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id 
              JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
              JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
              WHERE vis_usu_id_promotor = ".$datos['promotor']." AND vis_fecha 
              BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."' ORDER BY intervencion.int_id DESC;";

			if($_SESSION['tipo_usuario'] == 3){
				$sql = "SELECT visita.vis_id, intervencion.int_id, intervencion.int_fecha, centro_escolar.ces_nombre, 
						motivo_intervencion.min_nombre, intervencion.int_tab_id, periodos_escolares.pes_ano_lectivo, 
						periodos_escolares.pes_periodo, periodos_escolares.pes_fecha_inicio, periodos_escolares.pes_fecha_fin, 
						tipo_intervencion.tin_nombre, intervencion.int_otro_motivo_intervencion 
						FROM visita 
						JOIN intervencion ON visita.vis_id = intervencion.int_vis_id 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						WHERE visita.vis_usu_id_promotor = ".$datos['promotor']." AND 
						centro_escolar.ces_usu_id_director = ".$_SESSION['usu_id']." AND visita.vis_fecha 
						BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."' 
						ORDER BY intervencion.int_id DESC;";
			}
		}
		
      	$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $resultado;
		$arreglo_datos['OPCIONES'] = $opciones;
		$arreglo_datos['DATOS'] = $datos;
	
		if($resultado){
			$contenido .= $_obj_interfaz->crearListadoIntervencionAdmin($arreglo_datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		else{
			?>
            <script type="text/javascript"> 
                window.location="index.php?m=visitas&accion=visualizar_intervenciones&msg=INT-ADMIN-1"; 
            </script> 
            <?php
			//header('Location: index.php?m=visitas&accion=visualizar_intervenciones&msg=INT-ADMIN-1');
		}
		break;
	case 'editarVisitas'.validarAcceso(array(2)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$sql = "SELECT * FROM visita WHERE vis_id = ".$datos['vis_id'].";";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		if($resultado[0]['vis_hora_fin']){
			$datos['visita'] = $resultado[0];
			$contenido = $obj_visitas->editarVisitas($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		else{
			header('Location: index.php?m=visitas&accion=listarVisitas&msg=V-3');
		}
		break;
	case 'actEditarVisitas'.validarAcceso(array(2)):
		$contenido = $obj_visitas->actEditarVisitas($datos);
		header('Location: index.php?m=visitas&accion=listarVisitas&msg=' . $contenido);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'eliminar_visita'.validarAcceso(array(2)):
		$resultado = $obj_visitas->eliminarVisita($datos);
		
		$enlace = "index.php?m=visitas&accion=listarVisitas&msg=".$resultado."";
		header('Location: '.$enlace);
		
		break;
}
?>
