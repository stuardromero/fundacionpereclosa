<?php

class Reporte{

    var $_plantillas = "";

    function __construct() {
        global $_PATH_SERVIDOR;
        $this->_plantillas = $_PATH_SERVIDOR."/reportecentro/Plantillas";
    }

    /** abrirFormularioRegistrarreporte
     * parametro: $datos
     * autor : BDL
     * descripcion: ABRE EL CONTENIDO DE REGISTRO DE USUARIOS
    **/
    function abrirFormularioRegistrarreporte($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registrar.html");

        $label_inicio = "Informe Centre";
        $msg_descripcion = "Informe Centre Escolar";
        $breadcump_reporte = "Informe Centre Escolar";
        $input_repc_centro_educativo = "Centre Escolar*";
        $input_repc_periodo_escolar = "Períodes Acadèmics";
        $input_repc_graduados_eso = "Graduats ESO*";
        $input_repc_posobligatorio = "Pos Obligatorio*";
        $input_repc_culm_bachiller = "Culminació Batxiller*";
        $input_repc_cnt_universidad = "Universitat*";
        $input_repc_ob_estudios = "Estudis universitaris";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $centros_escolares = $this->obteberSelectFormaCentroEducativo($datos);
        $periodo_escolar = $this->obteberSelectFormaPeriodosEscolares($datos);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_reporte", $breadcump_reporte, $contenido);


        Interfaz::asignarToken("input_repc_centro_educativo", $input_repc_centro_educativo, $contenido);
        Interfaz::asignarToken("input_repc_periodo_escolar", $input_repc_periodo_escolar, $contenido);
        Interfaz::asignarToken("input_repc_graduados_eso", $input_repc_graduados_eso, $contenido);
        Interfaz::asignarToken("input_repc_posobligatorio", $input_repc_posobligatorio, $contenido);
        Interfaz::asignarToken("input_repc_culm_bachiller", $input_repc_culm_bachiller, $contenido);
        Interfaz::asignarToken("input_repc_cnt_universidad", $input_repc_cnt_universidad, $contenido);
        Interfaz::asignarToken("input_repc_ob_estudios", $input_repc_ob_estudios, $contenido);
        Interfaz::asignarToken("select_repc_centro_educativo", $centros_escolares, $contenido);
        Interfaz::asignarToken("select_repc_periodo_escolar", $periodo_escolar, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    /** abrirFormularioRegistrarreporte2
     * parametro: $datos
     * autor : BDL
     * descripcion: ABRE EL CONTENIDO DE REGISTRO DE USUARIOS
    **/
    function abrirFormularioRegistrarreporte2($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registrar_2.html");

        $label_inicio = "Informe Centre";
        $msg_descripcion = "Informe Centre Escolar";
        $breadcump_reporte = "Informe Centre Escolar";
        $input_repc_centro_educativo = "Centre Escolar*";
        $input_repc_periodo_escolar = "Períodes Acadèmics";
        $input_repc_graduados_eso = "Alumnes que graduen 4t de l'ESO*";  
        $input_repc_haran_bachillerato = "Alumnes que faran batxillerat*";
        $input_repc_haran_cfgm = "Alumnes que faran un CFGM*";
        $input_repc_haran_pfi = "Alumnes que faran PFI*";
        $input_repc_graduados_2n_bachillerato = "Alumnes que graduen 2n de batxillerat*";
        $input_repc_cnt_universidad = "Alumnes que ingressen en la Universitat*";
        $input_repc_ob_estudios = "Estudis universitaris";
        $ninos = "nois";
        $ninas = "noies";

        $input_repc_graduados_eso_ninos = "";
        $input_repc_graduados_eso_ninas = "";
        $input_repc_haran_bachillerato_ninos = "";
        $input_repc_haran_bachillerato_ninas = "";
        $input_repc_haran_cfgm_ninos = "";
        $input_repc_haran_cfgm_ninas = "";
        $input_repc_haran_pfi_ninos = "";
        $input_repc_haran_pfi_ninas = "";
        $input_repc_graduados_2n_bachillerato_ninos = "";
        $input_repc_graduados_2n_bachillerato_ninas = "";
        $input_repc_cnt_universidad_ninos = "";
        $input_repc_cnt_universidad_ninas = "";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $centros_escolares = $this->obteberSelectFormaCentroEducativo($datos);
        $periodo_escolar = $this->obteberSelectFormaPeriodosEscolares($datos);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_reporte", $breadcump_reporte, $contenido);


        Interfaz::asignarToken("input_repc_centro_educativo", $input_repc_centro_educativo, $contenido);
        Interfaz::asignarToken("input_repc_periodo_escolar", $input_repc_periodo_escolar, $contenido);
        Interfaz::asignarToken("input_repc_graduados_eso", $input_repc_graduados_eso, $contenido);
        Interfaz::asignarToken("input_repc_graduados_eso_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_graduados_eso_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_bachillerato", $input_repc_haran_bachillerato, $contenido);
        Interfaz::asignarToken("input_repc_haran_bachillerato_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_haran_bachillerato_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_cfgm", $input_repc_haran_cfgm, $contenido);
        Interfaz::asignarToken("input_repc_haran_cfgm_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_haran_cfgm_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_pfi", $input_repc_haran_pfi, $contenido);
        Interfaz::asignarToken("input_repc_haran_pfi_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_haran_pfi_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_graduados_2n_bachillerato", $input_repc_graduados_2n_bachillerato, $contenido);
        Interfaz::asignarToken("input_repc_graduados_2n_bachillerato_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_graduados_2n_bachillerato_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_cnt_universidad", $input_repc_cnt_universidad, $contenido);
        Interfaz::asignarToken("input_repc_cnt_universidad_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_cnt_universidad_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_ob_estudios", $input_repc_ob_estudios, $contenido);      
        Interfaz::asignarToken("select_repc_centro_educativo", $centros_escolares, $contenido);
        Interfaz::asignarToken("select_repc_periodo_escolar", $periodo_escolar, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    function registrarreporte($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        $datos = Herramientas::trimCamposFormulario($datos);

        // $campos = array(
        // "rce_ces_id",     
        // "rce_pes_id", 	
        // "rce_graduados_eso",
        // "rce_posobligatorio",
        // "rce_culminacion_bachiller",
        // "rce_cantidad_universidad", 
        // "rce_ob_estudios",
        // );   
        
        $campos = array(
            "rce_ces_id",     
            "rce_pes_id", 
            "rce_graduados_eso_ninos",
            "rce_graduados_eso_ninas",
            "rce_haran_bachillerato_ninos",
            "rce_haran_bachillerato_ninas",
            "rce_haran_cfgm_ninos",
            "rce_haran_cfgm_ninas",
            "rce_haran_pfi_ninos",
            "rce_haran_pfi_ninas",
            "rce_graduados_2n_bachillerato_ninos",
            "rce_graduados_2n_bachillerato_ninas",
            "rce_ingresen_universidad_ninos",
            "rce_ingresen_universidad_ninas",
            "rce_ob_estudios",	
        ); 
        
        
        $datos['tabla'] = 'reporte_centro_educativo';
        $datos['rce_ces_id'] = intval($datos['rce_ces_id']);
        $datos['rce_pes_id'] = intval($datos['rce_pes_id']);
        // $datos['rce_graduados_eso'] = intval($datos['rce_graduados_eso']);
        // $datos['rce_posobligatorio'] = intval($datos['rce_posobligatorio']);
        // $datos['rce_culminacion_bachiller'] = intval($datos['rce_culminacion_bachiller']);
        // $datos['rce_cantidad_universidad'] = intval($datos['rce_cantidad_universidad']);
        $sql = $_obj_database->generarSQLInsertar($datos, $campos);

        $res = $_obj_database->ejecutarSql($sql);

        if($res == 1){
            return 'RPCT-01';
        } else {
            return 'RPCT-04';
        }

    }

    function obteberSelectFormaCentroEducativo($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT ces.ces_id, ces.ces_nombre 
        FROM centro_escolar as ces 
        INNER JOIN usuario_centro_escolar as usu_ces
        ON usu_ces.usc_ces_id = ces.ces_id
        WHERE usu_ces.usc_usu_id_promotor = ".$usu_id.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $rce_ces_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['rce_ces_id'])){
                $rce_ces_id = $valores['rce_ces_id'];
            }
        }
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='rce_ces_id' aria-label='Default select example' disabled>";
            $select .= "<option>No té Centres educatius assignats</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='rce_ces_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar centre escolar</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ces_id'] == $rce_ces_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ces_id']."' ".$selected.">".$resultado['ces_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
        
    }

    function obteberSelectFormaPeriodosEscolares($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT DISTINCT(pes_id), pes_ano_lectivo, pes_periodo, pes_fecha_inicio, pes_fecha_fin
                FROM periodos_escolares 
                WHERE 1;"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $rce_pes_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['rce_pes_id'])){
                $rce_pes_id = $valores['rce_pes_id'];
            }
        }

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='rce_pes_id' name='rce_pes_id' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar Períodes Acadèmics</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='rce_pes_id' name='rce_pes_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Períodes Acadèmics</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['pes_id'] == $rce_pes_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['pes_id']."' ".$selected.">".$resultado['pes_ano_lectivo'].','.$resultado['pes_periodo']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }

    function obteberSelectFormaCurso($datos,$valores = NULL){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $etapa_educativa = $datos['edu_id'];

        $est_cur_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['est_cur_id'])){
                $etapa_educativa = $valores['est_edu_id'];
                $est_cur_id = $valores['est_cur_id'];
            }
        }

        $sql = "SELECT DISTINCT(cur_id), cur_nombre
                FROM cursos 
                WHERE cur_edu_id = ".$etapa_educativa.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='est_cur_id' name='est_cur_id' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar curs</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='est_cur_id' name='est_cur_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar curs</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['cur_id'] == $est_cur_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['cur_id']."' ".$selected.">".$resultado['cur_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;        
    }

    function listadoreporte($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_reporte.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
        $label_inicio = "Informe Centre";
        $msg_descripcion = "Llistat Informe Centre";
        $breadcump_reporte = "Informe Centre";

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_reporte_listar", $breadcump_reporte, $contenido);

        return $contenido;
    }

    function Eliminarreporte($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        if (!$_obj_database->iniciarTransaccion()) {
            //problma al inicar la transaccion
            return -1003;
        }

        $rce_id = $datos['rce_id'];

        $sql = "DELETE FROM reporte_centro_educativo WHERE rce_id ='$rce_id'";
        if (!$_obj_database->ejecutarSql($sql)) {
            $_obj_database->cancelarTransaccion();
            return -1003;
        } else {
            $_obj_database->terminarTransaccion();
        }

        return 'RPCT-03';
        
    }

    function abrirFormularioEditarreporte($datos,$valores){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_reporte_2.html");

        $label_inicio = "Informe Centre";
        $msg_descripcion = "Informe Centre Escolar";
        $breadcump_reporte = "Informe Centre Escolar";
        $input_repc_centro_educativo = "Centre Escolar*";
        $input_repc_periodo_escolar = "Períodes Acadèmics";
        $input_repc_graduados_eso = "Alumnes que graduen 4t de l'ESO*";  
        $input_repc_haran_bachillerato = "Alumnes que faran batxillerat*";
        $input_repc_haran_cfgm = "Alumnes que faran un CFGM*";
        $input_repc_haran_pfi = "Alumnes que faran PFI*";
        $input_repc_graduados_2n_bachillerato = "Alumnes que graduen 2n de batxillerat*";
        $input_repc_cnt_universidad = "Alumnes que ingressen en la Universitat*";
        $input_repc_ob_estudios = "Estudis universitaris";
        $ninos = "nois";
        $ninas = "noies";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $valores = $valores[0];

        $rce_repc_graduados_eso_ninos = $valores["rce_graduados_eso_ninos"];
        $rce_repc_graduados_eso_ninas = $valores["rce_graduados_eso_ninas"];
        $rce_repc_haran_bachillerato_ninos = $valores["rce_haran_bachillerato_ninos"];
        $rce_repc_haran_bachillerato_ninas = $valores["rce_haran_bachillerato_ninas"];
        $rce_repc_haran_cfgm_ninos = $valores["rce_haran_cfgm_ninos"];
        $rce_repc_haran_cfgm_ninas = $valores["rce_haran_cfgm_ninas"];
        $rce_repc_haran_pfi_ninos = $valores["rce_haran_pfi_ninos"];
        $rce_repc_haran_pfi_ninas = $valores["rce_haran_pfi_ninas"];
        $rce_repc_graduados_2n_bachillerato_ninos = $valores["rce_graduados_2n_bachillerato_ninos"];
        $rce_repc_graduados_2n_bachillerato_ninas = $valores["rce_graduados_2n_bachillerato_ninas"];
        $rce_repc_cnt_universidad_ninos = $valores["rce_ingresen_universidad_ninos"];
        $rce_repc_cnt_universidad_ninas = $valores["rce_ingresen_universidad_ninas"];

        $centros_escolares = $this->obteberSelectFormaCentroEducativo($datos,$valores);
        $periodo_escolar = $this->obteberSelectFormaPeriodosEscolares($datos,$valores);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_reporte", $breadcump_reporte, $contenido);


        Interfaz::asignarToken("input_repc_centro_educativo", $input_repc_centro_educativo, $contenido);
        Interfaz::asignarToken("select_repc_centro_educativo", $centros_escolares, $contenido);
        Interfaz::asignarToken("input_repc_periodo_escolar", $input_repc_periodo_escolar, $contenido);
        Interfaz::asignarToken("select_repc_periodo_escolar", $periodo_escolar, $contenido);

        Interfaz::asignarToken("input_repc_graduados_eso", $input_repc_graduados_eso, $contenido);
        Interfaz::asignarToken("rce_repc_graduados_eso_ninos",$rce_repc_graduados_eso_ninos, $contenido);
        Interfaz::asignarToken("rce_repc_graduados_eso_ninas",$rce_repc_graduados_eso_ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_bachillerato", $input_repc_haran_bachillerato, $contenido);
        Interfaz::asignarToken("rce_repc_haran_bachillerato_ninos",$rce_repc_haran_bachillerato_ninos, $contenido);
        Interfaz::asignarToken("rce_repc_haran_bachillerato_ninas",$rce_repc_haran_bachillerato_ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_cfgm", $input_repc_haran_cfgm, $contenido);
        Interfaz::asignarToken("rce_repc_haran_cfgm_ninos",$rce_repc_haran_cfgm_ninos, $contenido);
        Interfaz::asignarToken("rce_repc_haran_cfgm_ninas",$rce_repc_haran_cfgm_ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_pfi", $input_repc_haran_pfi, $contenido);
        Interfaz::asignarToken("rce_repc_haran_pfi_ninos",$rce_repc_haran_pfi_ninos, $contenido);
        Interfaz::asignarToken("rce_repc_haran_pfi_ninas",$rce_repc_haran_pfi_ninas, $contenido);
        Interfaz::asignarToken("input_repc_graduados_2n_bachillerato", $input_repc_graduados_2n_bachillerato, $contenido);
        Interfaz::asignarToken("rce_repc_graduados_2n_bachillerato_ninos",$rce_repc_graduados_2n_bachillerato_ninos, $contenido);
        Interfaz::asignarToken("rce_repc_graduados_2n_bachillerato_ninas",$rce_repc_graduados_2n_bachillerato_ninas, $contenido);
        Interfaz::asignarToken("input_repc_cnt_universidad", $input_repc_cnt_universidad, $contenido);
        Interfaz::asignarToken("rce_repc_cnt_universidad_ninos",$rce_repc_cnt_universidad_ninos, $contenido);
        Interfaz::asignarToken("rce_repc_cnt_universidad_ninas",$rce_repc_cnt_universidad_ninas, $contenido);
        Interfaz::asignarToken("input_repc_graduados_eso_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_graduados_eso_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_bachillerato_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_haran_bachillerato_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_cfgm_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_haran_cfgm_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_haran_pfi_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_haran_pfi_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_graduados_2n_bachillerato_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_graduados_2n_bachillerato_ninas", $ninas, $contenido);
        Interfaz::asignarToken("input_repc_cnt_universidad_ninos", $ninos, $contenido);
        Interfaz::asignarToken("input_repc_cnt_universidad_ninas", $ninas, $contenido);

        Interfaz::asignarToken("input_repc_ob_estudios", $input_repc_ob_estudios, $contenido);
        Interfaz::asignarToken("rce_observ_estudios", $valores["rce_ob_estudios"], $contenido);
        Interfaz::asignarToken("rce_reporte_id", $datos["rce_id"], $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    function obteberSelectFormaGenero($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $est_id = $datos['est_id'];

        $sql = "SELECT est_genero
                FROM estudiante 
                WHERE est_id = ".$est_id.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='est_genero' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar Gènere</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='est_genero' aria-label='Default select example' required>";
            if($resultados[0]['est_genero'] === 'M'){
                $select .= "<option value='M' selected>Home</option>";
                $select .= "<option value='F'>Femení</option>";
            } else if($resultados[0]['est_genero'] === 'F') {
                $select .= "<option value='M'>Home</option>";
                $select .= "<option value='F' selected>Femení</option>";
            }
            
            $select .="</select>";
        }

        return $select;
    }

    function obtenerDatosReporte($rce_id) {
        global $_obj_database;

        $sql = "SELECT * FROM reporte_centro_educativo
				WHERE rce_id = $rce_id";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    function editarreporte($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        // if(!isset($_SESSION))
        // {
        //     session_start();
        // }

        $datos = Herramientas::trimCamposFormulario($datos);

        // $campos = array(
        //     "rce_ces_id",     
        //     "rce_pes_id", 	
        //     "rce_graduados_eso",
        //     "rce_posobligatorio",
        //     "rce_culminacion_bachiller",
        //     "rce_cantidad_universidad", 
        //     "rce_ob_estudios",
        // );       
        $campos = array(
            "rce_ces_id",     
            "rce_pes_id", 
            "rce_graduados_eso_ninos",
            "rce_graduados_eso_ninas",
            "rce_haran_bachillerato_ninos",
            "rce_haran_bachillerato_ninas",
            "rce_haran_cfgm_ninos",
            "rce_haran_cfgm_ninas",
            "rce_haran_pfi_ninos",
            "rce_haran_pfi_ninas",
            "rce_graduados_2n_bachillerato_ninos",
            "rce_graduados_2n_bachillerato_ninas",
            "rce_ingresen_universidad_ninos",
            "rce_ingresen_universidad_ninas",
            "rce_ob_estudios",	
        );  
        
        $datos['tabla'] = 'reporte_centro_educativo';
        $datos['rce_ces_id'] = intval($datos['rce_ces_id']);
        $datos['rce_pes_id'] = intval($datos['rce_pes_id']);
        // $datos['rce_graduados_eso'] = intval($datos['rce_graduados_eso']);
        // $datos['rce_posobligatorio'] = intval($datos['rce_posobligatorio']);
        // $datos['rce_culminacion_bachiller'] = intval($datos['rce_culminacion_bachiller']);
        // $datos['rce_cantidad_universidad'] = intval($datos['rce_cantidad_universidad']);
        $datos['condicion'] = "rce_id = '" . $datos['rce_id'] . "' ";

        $res = $_obj_database->actualizarDatosTabla($datos, $campos);

        if (!$res) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }

        if($res == 1){
            return 'RPCT-02';
        } else {
            return 'RPCT-05';
        }
        
    }
}

?>
