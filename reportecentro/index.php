
<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Includes/Interfaz.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("Reporte.php");

$obj_usuarios = new Usuarios();
$obj_reporte = new Reporte();
$obj_interfaz = new Interfaz();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarJS("formulario.js");

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
    //case 'fm_registrar'.$obj_usuarios->validarAcceso(array("A","P")):
    //case 'fm_registrar'.$obj_usuarios->validarAcceso(array()):7
    case 'fm_registrar'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];  
            $opciones['CADENA'] = 'El Alumne';           
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
		$contenido = $obj_reporte->abrirFormularioRegistrarreporte2($datos);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'registrarreporte'.validarAcceso(array(2)):
        $resultado = $obj_reporte->registrarreporte($datos);
        if( $resultado=='RPCT-01' ) 
		{   
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=reportecentro&accion=fm_registrar&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php
            $datos['msg'] = $resultado;
            $datos['tipo_gestion'] = 1;
            $contenido = $obj_reporte->abrirFormularioRegistrarreporte2($datos);
            $_obj_interfaz->asignarContenido($contenido);
			//header('Location: index.php?m=reportecentro&accion=fm_registrar&tipo_gestion=1&msg=RPCT-01');    
			break;            
		}//Fin de if( $resultado==1 )      
		else {
            $opciones['CADENA'] = "El informe";      
			$opciones['ACCION'] = 1;//ingresar      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);  
			$contenido = $obj_reporte->abrirFormularioRegistrarreporte2($datos);
			$_obj_interfaz->asignarContenido($contenido);
        }                
        //$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'obtenerCursosEtapaEducativa'.validarAcceso(array(2)):
        $resultado = $obj_reporte->obteberSelectFormaCurso($datos);
        print_r($resultado);
        exit;
        break;    
    case 'listarreporte'.validarAcceso(array(2)):
        if(isset($datos['msg'])){
            $opciones['ACCION'] = $datos['tipo_gestion'];
            $opciones['CADENA'] = 'El usuario';
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }
        $contenido = $obj_reporte->listadoreporte($datos);
		// $campos = array(
        //     "ces_nombre" => "Centre Escolar",
		// 	"periodo_lectivo" => "Períodes Acadèmics", 
		// 	"rce_graduados_eso" => "Graduats ESO",
        //     "rce_posobligatorio" => "Pos Obligatorio",
        //     "rce_culminacion_bachiller" => "Culminació Batxiller",
        //     "rce_cantidad_universidad" => "Universitat",
        //     "rce_ob_estudios" => "Estudis universitaris",
		// );  	
        $campos = array(
            "rce_id" => "#",
            "ces_nombre" => "Centre Escolar",
			"periodo_lectivo" => "Períodes Acadèmics", 
			"total_graduados_eso" => "Total graduen 4t de l'ESO",
            "total_haran_bachillerato" => "Total faran batxillerat",
            "total_haran_cfgm" => "Total faran un CFGM",
            "total_haran_pfi" => "Total faran un PFI",
            "total__graduados_2n_bachillerato" => "Total graduen 2n de batxillerat",
            "total_universidad" => "Total ingressen en la Universitat",
            "rce_ob_estudios" => "Estudis universitaris",
		);  	
        
        	
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        	
        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id&est_idalu=%est_idalu%";
        
		$datos['sql'] = "SELECT DISTINCT(rce.rce_id), ces.ces_nombre, CONCAT(pes.pes_ano_lectivo, ',', pes.pes_periodo) AS periodo_lectivo,
        (rce_graduados_eso_ninos + rce_graduados_eso_ninas) AS total_graduados_eso, (rce_haran_bachillerato_ninos + rce_haran_bachillerato_ninas) AS total_haran_bachillerato,
        (rce_haran_cfgm_ninos + rce_haran_cfgm_ninas) AS total_haran_cfgm, (rce_haran_pfi_ninos + rce_haran_pfi_ninas) AS total_haran_pfi, 
		  (rce_graduados_2n_bachillerato_ninos + rce_graduados_2n_bachillerato_ninas) AS total__graduados_2n_bachillerato, 
		  (rce_ingresen_universidad_ninos + rce_ingresen_universidad_ninas) AS total_universidad, rce_ob_estudios
        FROM reporte_centro_educativo AS rce 
        INNER JOIN centro_escolar AS ces
        ON ces.ces_id = rce.rce_ces_id
        INNER JOIN periodos_escolares AS pes
        ON pes.pes_id = rce.rce_pes_id
        INNER JOIN usuario_centro_escolar as usc
        ON usc.usc_ces_id = ces.ces_id
        WHERE usc.usc_usu_id_promotor = ".$usu_id.";";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
		$contenido .= $_obj_interfaz->crearListadoreporte($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'fm_editar'.validarAcceso(array(2)):
        $valores = $obj_reporte->obtenerDatosReporte($datos['rce_id']);
        if( !is_array($valores))
		{
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
        $datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_reporte->abrirFormularioEditarreporte($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'editarreporte'.validarAcceso(array(2)):
        $resultado = $obj_reporte->editarreporte($datos);
        if( $resultado=='RPCT-02' ) 
        {         
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=reportecentro&accion=listarreporte&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php        
            //header('Location: index.php?m=reportecentro&accion=listarreporte&tipo_gestion=1&msg='.$resultado);    
            break;            
        }//Fin de if( $resultado==1 )      
        
        break;
    case 'fm_eliminar'.validarAcceso(array(2)):
        $resultado = $obj_reporte->Eliminarreporte($datos);
        ?>
        <script type="text/javascript"> 
            window.location="index.php?m=reportecentro&accion=listarreporte&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
        </script> 
        <?php
        //$enlace = "index.php?m=reportecentro&accion=listarreporte&tipo_gestion=3&msg=".$resultado."";
		//header('Location: '.$enlace);
		
        break;
}

?>