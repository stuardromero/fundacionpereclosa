<?php

/**
 * 	Nombre: Usuarios.php
 * 	Descripción: Maneja lo referente a las cotizaciones del sistema
 */
class Usuarios {

  var $_plantillas = "";
  var $_herramientas = null;

  public function __construct() {
      global $_PATH_SERVIDOR;

      $this->_plantillas = $_PATH_SERVIDOR . "Usuarios/Plantillas";
      $this->_herramientas = new Herramientas;

      $this->campos_busqueda_listado = array("usu_id","usu_nombre",   
		          "usu_login","usu_correo", "usu_apellido");  
  }

  function abrirMsgSoporte($datos) {
      global $_PATH_IMAGENES, $_PATH_WEB, $idi_despliegue;

      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/msg_soporte.html");

      Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

      Interfaz::asignarToken("btn_cerrar", $idi_despliegue['btn_cerrar'], $contenido);

      return $contenido;
  }

//Fin de abrirMsgSoporte()

/** abrirFormularioHome
   * parametro: $datos
   * autor : JCB - DESARROLLO AUTOPACIFICO
   * descripcion: ABRE EL CONTENIDO DE LA PLANTILAL FM_HOME
   **/
  public function abrirFormularioAutenticacionUsuarios($datos) {
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_PATH_SERVIDOR;

    $boton_procesar = "Autenticar";

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_autenticacion_usuario.html");
    $msg_titulo = 'Iniciar Sessió';
    $msg_descripcion = "Benvingut/da al sistema d'autenticació de la Fundació PereClosa";
    $label_usuario = 'USUARI';
    $label_contrasena = 'CONTRASENYA';
    $texto_olvido_contrasena = 'Heu oblidat la contrasenya?';
    $link_olvido_contrasena = 'index.php?m=Usuarios&accion=forma_olvido_clave';

    $img_ruta = "Includes/images";

    $mensaje = $datos['mensaje'];
    if ($datos['mensaje'] !== '') {
      $mensaje = '<div class="alert alert-warning">'.$datos['mensaje'].'</div>';
    }

    Interfaz::asignarToken("imagenes", $img_ruta, $contenido);

    Interfaz::asignarToken("boton_procesar", $boton_procesar, $contenido);
    Interfaz::asignarToken("texto_olvido_contrasena", $texto_olvido_contrasena, $contenido);
    Interfaz::asignarToken("link_olvido_contrasena", $link_olvido_contrasena, $contenido);

    Interfaz::asignarToken("mensaje", $mensaje, $contenido);

    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("label_usuario", $label_usuario, $contenido);
    Interfaz::asignarToken("label_contrasena", $label_contrasena, $contenido);

    return $contenido;
  }


/** abrirFormularioHome
 * parametro: $datos
 * autor : JCB - DESARROLLO AUTOPACIFICO
 * descripcion: ABRE EL CONTENIDO DE LA PLANTILAL FM_HOME
 **/
public function abrirFormularioHome($datos) {
  global $_PATH_IMAGENES, $_PATH_WEB, $_opciones;

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_home.html");

    $img_ruta = "Includes/Imagenes";

    switch($datos['tipo_usuario']){
      case 1:
        $botones = '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=usuarios&accion=listar_usuarios' class='btn btn-primary'>Gestió d'Usuaris</a>";
        $botones .= "<a href='index.php?m=usuarios&accion=listar_usuarios' class='btn btn-outline-info'>Llistat Usuari</a>";
        $botones .= "<a href='index.php?m=usuarios&accion=anadir_usuarios' class='btn btn-outline-info'>Afegir Usuari</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=centros&accion=listarcentros' class='btn btn-info'>Gestió de Centres Escolars</a>";
        $botones .= "<a href='index.php?m=centros&accion=listarcentros' class='btn btn-outline-primary'>Llistat Centres Escolars</a>";
        $botones .= "<a href='index.php?m=centros&accion=fm_registrar' class='btn btn-outline-primary'>Afegir Centres Escolars</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=periodosescolares&accion=listar_periodos_escolares' class='btn btn-primary'>Anys Lectius</a>";
        $botones .= "<a href='index.php?m=periodosescolares&accion=listar_periodos_escolares' class='btn btn-outline-info'>Llistat Anys Lectius</a>";
        $botones .= "<a href='index.php?m=periodosescolares&accion=periodos_escolares' class='btn btn-outline-info'>Afegir Anys Lectius</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=visitas&accion=visualizar_intervenciones' class='btn btn-info'>Intervencions</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=reportes&accion=descripcion_intervencion' class='btn btn-outline-info'>Seguiment alumnat (última visita)</a>";
        $botones .= "<a href='index.php?m=reportes&accion=descripcion_estudiantes' class='btn btn-outline-info'>Seguiment de l'alumne/a</a>";
        $botones .= "<a href='index.php?m=reportes&accion=horas_trabajadas_centros_escolares' class='btn btn-outline-info'>Hores treballades en Centres Escolars</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=informeCentroEscolarA' class='btn btn-outline-info'>Dades de promoció escolar alumnat</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_ninos_atendidos' class='btn btn-outline-info'>Alumnat atès pel promotor/a escolar per gènere i per etapa educativa</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_ninos_atendidos_centro_escolar' class='btn btn-outline-info'>Alumnat atés pel promotor/a als centres educatius durant el curs escolar</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_familia' class='btn btn-outline-info'>Famílies ateses pel promotor/a als centres educatius durant el curs escolar</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=evolucion_absentismo_a' class='btn btn-outline-info'>Dades de l'absentisme durant el curs</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=cnt_motivo_intervencion_a' class='btn btn-outline-info'>Dades quantitatives per motiu d'intervenció</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=tipointervencionA' class='btn btn-outline-info'>Dades quantitatives per tipus d'intervenció</a>";
        $botones .= "</div></div>";

        break;
      case 2:
        $botones .= '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=familias&accion=listarfamilias' class='btn btn-info'>Gestió de Families</a>";
        $botones .= "<a href='index.php?m=familias&accion=listarfamilias' class='btn btn-outline-primary'>Llistat Families</a>";
        $botones .= "<a href='index.php?m=familias&accion=fm_registrar' class='btn btn-outline-primary'>Afegir Familie</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=alumnos&accion=listaralumnos' class='btn btn-primary'>Gestió d'Alumnes</a>";
        $botones .= "<a href='index.php?m=alumnos&accion=listaralumnos' class='btn btn-outline-info'>Llistat Alumnes</a>";
        $botones .= "<a href='index.php?m=alumnos&accion=fm_registrar' class='btn btn-outline-info'>Afegir Alumn</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=visitas&accion=listarVisitas' class='btn btn-info'>Gestió d'Visites</a>";
        $botones .= "<a href='index.php?m=visitas&accion=listarVisitas' class='btn btn-outline-primary'>Llistat Visites</a>";
        $botones .= "<a href='index.php?m=visitas&accion=agregar_visitas' class='btn btn-outline-primary'>Afegir Visites</a>";
        $botones .= "<a href='index.php?m=visitas&accion=fm_horas_adicionales' class='btn btn-outline-primary'>Hores d'intervenció fora de centre</a>";
        $botones .= "<a href='index.php?m=visitas&accion=listarHorasAdicionales' class='btn btn-outline-primary'>Llistat Hores d'intervenció fora de centre</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-3">
                      <div class="buttons">';
        $botones .= "<a href='index.php?m=reportecentro&accion=listarreporte' class='btn btn-primary'>Informe Centre</a>";
        $botones .= "<a href='index.php?m=reportecentro&accion=listarreporte' class='btn btn-outline-info'>Llistat Informe</a>";
        $botones .= "<a href='index.php?m=reportecentro&accion=fm_registrar' class='btn btn-outline-info'>Afegir Informe</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=reportes&accion=descripcion_intervencion' class='btn btn-outline-info'>Seguiment alumnat (última visita)</a>";
        $botones .= "<a href='index.php?m=reportes&accion=descripcion_estudiantes' class='btn btn-outline-info'>Seguiment de l'alumne/a</a>";
        $botones .= "<a href='index.php?m=reportes&accion=horas_trabajadas_centros_escolares' class='btn btn-outline-info'>Hores treballades en Centres Escolars</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=informeCentroEscolarP' class='btn btn-outline-info'>Dades de promoció escolar alumnat</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_ninos_atendidos' class='btn btn-outline-info'>Alumnat atès pel promotor/a escolar per gènere i per etapa educativa</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_ninos_atendidos_centro_escolar' class='btn btn-outline-info'>Alumnat atés pel promotor/a als centres educatius durant el curs escolar</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_familia' class='btn btn-outline-info'>Famílies ateses pel promotor/a als centres educatius durant el curs escolar</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=evolucion_absentismo_p' class='btn btn-outline-info'>Dades de l'absentisme durant el curs</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=cnt_motivo_intervencion_p' class='btn btn-outline-info'>Dades quantitatives per motiu d'intervenció</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=tipointervencionP' class='btn btn-outline-info'>Dades quantitatives per tipus d'intervenció</a>";
        $botones .= "</div></div>";
        break;
      case 3:
        $botones .= '<div class="col-12 col-md-12">
                      <div class="buttons text-center">';
        $botones .= "<a href='index.php?m=visitas&accion=visualizar_intervenciones' class='btn btn-info'>Intervencions</a>";
        $botones .= "</div></div>";

        break;
      case 4:
        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=reportes&accion=descripcion_intervencion' class='btn btn-outline-info'>Seguiment alumnat (última visita)</a>";
        $botones .= "<a href='index.php?m=reportes&accion=descripcion_estudiantes' class='btn btn-outline-info'>Seguiment de l'alumne/a</a>";
        $botones .= "<a href='index.php?m=reportes&accion=horas_trabajadas_centros_escolares' class='btn btn-outline-info'>Hores treballades en Centres Escolars</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=informeCentroEscolarC' class='btn btn-outline-info'>Dades de promoció escolar alumnat</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_ninos_atendidos' class='btn btn-outline-info'>Alumnat atès pel promotor/a escolar per gènere i per etapa educativa</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_ninos_atendidos_centro_escolar' class='btn btn-outline-info'>Alumnat atés pel promotor/a als centres educatius durant el curs escolar</a>";
        $botones .= "<a href='index.php?m=reportes&accion=informe_familia' class='btn btn-outline-info'>Famílies ateses pel promotor/a als centres educatius durant el curs escolar</a>";
        $botones .= "</div></div>";

        $botones .= '<div class="col-12 col-md-4">
                      <div class="buttons">';
        $botones .= "<a href='#' class='btn btn-primary'>Informes</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=evolucion_absentismo_c' class='btn btn-outline-info'>Dades de l\'absentisme durant el curs</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=cnt_motivo_intervencion_c' class='btn btn-outline-info'>Dades quantitatives per motiu d'intervenció</a>";
        $botones .= "<a href='index.php?m=basereportes&accion=tipointervencionC' class='btn btn-outline-info'>Dades quantitatives per tipus d'intervenció</a>";
        $botones .= "</div></div>";

        
        $botones .= '<div class="col-12 col-md-12">
                      <div class="buttons text-center">';
        $botones .= "<a href='index.php?m=visitas&accion=visualizar_intervenciones' class='btn btn-info'>Intervencions</a>";
        $botones .= "</div></div>";

        break;
      default:
        break;
    }

    Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("botones", $botones, $contenido);

    return $contenido;
  }

  public function autenticarUsuarios($datos) {
    global $_obj_database;

    $datos = Herramientas::trimCamposFormulario($datos);
        
    //codigo usuario no existe
    $respuesta = 'U-0';
    $sql = "select usu_id, usu_nombre, usu_login, usu_clave, usu_correo, usu_estado, usu_token, usu_per_id as tipo_usuario   
          from usuarios  
          where usu_estado = 1 
            and usu_login='".$datos['usuario']."';";

    
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

    if ($_obj_database->obtenerNumeroFilas()>0) {
      if ($resultado[0]['usu_token'] != '') {
        return 'U-5';
      }
      //Clave incorrecta
			//MAOH - 18 Nov 2011 - Tipo clave
      $respuesta='U-2';

      if ( strcmp($resultado[0]['usu_clave'], md5($datos['contrasena'].$resultado[0]['usu_id'])) == 0) {
        $_SESSION['autenticado'] = 1;
        // Registro de acceso del usuario
        $campos = array("log_usu_id" ,"log_fecha");

        $datos['log_usu_id'] = $resultado[0]['usu_id'];
        $datos['log_fecha'] = date("Y-m-d H:i:s");
        $datos['tabla'] = "log_acceso";

        $sql = $_obj_database->generarSQLInsertar($datos, $campos);
        $resultado_sql = $_obj_database->ejecutarSql($sql);

        $_SESSION = array_merge($_SESSION,$resultado[0]);

        unset($_SESSION['contrasena']);
      }
    }

    return $respuesta;
  }

  public function abrirFormularioOlvidoClave($datos)
	{                                            
		global $_PATH_IMAGENES,$_PATH_WEB,$_PATH_ACCION; 

		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_olvido_clave.html");    

		Interfaz::asignarToken("path_accion",$_PATH_ACCION,$contenido);
		Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido); 
		Interfaz::asignarToken("usu_correo", $datos['usu_correo'], $contenido); 
		Interfaz::asignarToken("mensaje", $datos['mensaje'] ,$contenido);	
		Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);	

		return $contenido;
  }//Fin de abrirFormularioOlvidoClave() 
  
  public function solicitarRecordatorioClave($datos)
	{                 
		global $_obj_database;

		$datos = Herramientas::trimCamposFormulario($datos);
		
		//usuario o correo no existe 
		$respuesta = 'U-0';

		$sql = "select usu_login, usu_correo
					from usuarios  
					where usu_estado = 1 
            and usu_login='".$datos['usu_login']."'
            and usu_correo='".$datos['usu_correo']."';";

		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		if($_obj_database->obtenerNumeroFilas() > 0) {
			return true; 
    }//Fin de if ($_obj_database->obtenerNumeroFilas()>0) 

		return $respuesta;
  }//Fin de solicitarsolicitarRecordatorioClave()

  public function generarTokenRecuperacionContrasena($usu_correo) {
    global $_obj_database;

    $token = md5($usu_correo).rand(10,9999);

    $campos = array("usu_token");

    $datos['usu_token'] = $token;
    $datos['tabla'] = "usuarios";
    $datos["condicion"] = "usu_correo = '".$usu_correo."'";

    $sql = $_obj_database->generarSQLActualizar($datos, $campos);

    $resultado_sql = $_obj_database->ejecutarSql($sql);

    if ($resultado_sql == true) {
      return $token;
    }

    return '';
  }

  function obtenerCorreoOlvidoClave($datos, $token)  
	{                        
		global $_PATH_WEB, $_PATH_ACCION; 		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/correo_olvido_clave.html"); 		

    //Interfaz::asignarToken("path_web", $_PATH_WEB.'/index.php', $contenido);
    Interfaz::asignarToken("path_accion", '?m=usuarios&accion=fm_restablecer_contrasena', $contenido);
    Interfaz::asignarToken("nombre", $datos['usu_nombre'], $contenido);	
		Interfaz::asignarToken("correo", $datos['usu_correo'], $contenido);	
		Interfaz::asignarToken("token", $token, $contenido); 
    
    Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);	

		return $contenido;
	}//fin de obtenerContenidoActivacion()
  
  /** obtenerIpCliente
   * parametro:
   * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
   * descripcion: Obtiene la ip del cliente.
   **/
  public function obtenerIpCliente() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
  }
  

  /** registrarUsuarios
   * autor : SA
   * descripcion: Registra los usuarios
   **/
  public function registrarUsuarios($datos)
	{         
		global $_obj_database, $_EMAIL_ADMIN;

    $datos = Herramientas::trimCamposFormulario($datos);

    if($datos['usu_per_id'] == 'Escollir...'){
      $enlace = "index.php?m=usuarios&accion=anadir_usuarios&msg=U-23";
      header('Location: '.$enlace);
      exit;
    }

    $sql = "SELECT * FROM usuarios WHERE usu_login = '". $datos['usu_login'] ."' OR usu_correo = '". $datos['usu_correo'] ."';";
    $res = $_obj_database->obtenerRegistrosAsociativos($sql); 

    if($res){
      $enlace = "index.php?m=usuarios&accion=anadir_usuarios&msg=U-24";
      header('Location: '.$enlace);
      exit;
    }
    else{
      $campos = array(
	      "usu_nombre",     
		    "usu_login", 	
		    "usu_clave",
        "usu_correo",
        "usu_apellido",
        "usu_estado", 
        "usu_per_id"
      );        
      
      $datos['tabla'] = 'usuarios';
      $datos['usu_estado'] = 1;

      $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890*?/";
      $password = "";
      //Reconstruimos la contraseña segun la longitud que se quiera
      for($i=0;$i<8;$i++) {
          //obtenemos un caracter aleatorio escogido de la cadena de caracteres
          $password .= substr($str,rand(0,62),1);
      }

      $obj_correo = new Correo();
      $destinatario = $datos['usu_correo']; 
      $remitente = "Fundació PereClosa " . "<" . $_EMAIL_ADMIN . ">"; 
      $asunto = "Fundació PereClosa - Contrasenya"; 
      $cuerpo = $this->obtenerCorreoAsignacionClave($datos, $password);
      
      $obj_correo->enviarCorreo($remitente, $destinatario, $asunto, $cuerpo);
      
      $datos['usu_clave'] = $password;

      $sql = $_obj_database->generarSQLInsertar($datos, $campos);

      $res = $_obj_database->ejecutarSql($sql);
      $datos['usu_id'] = $res[0]['usu_id'];
      
      if($res == 1){

        $sql = "SELECT usu_id FROM usuarios WHERE usu_login = '". $datos['usu_login'] ."';";
        $res = $_obj_database->obtenerRegistrosAsociativos($sql); 
        $datos['usu_id'] = $res[0]['usu_id'];
        
        $datos['usu_clave'] = md5($password.$datos['usu_id']);
  
        $sql = "UPDATE usuarios SET usu_clave = '". $datos['usu_clave'] ."' WHERE usu_login = '". $datos['usu_login'] ."';";
        $res = $_obj_database->ejecutarSql($sql); 
  
        if($datos['territorios']){
          foreach ($datos['territorios'] as $idTerritorios){
            $sql_transaccion = "INSERT INTO usuario_territorio (ust_usu_id_coordinador, ust_ter_id) VALUES(" .$datos['usu_id'].", '" .$idTerritorios."')";
    
            $_obj_database->ejecutarSql($sql_transaccion);
          }
        }

        if($datos['centros_escolares']){
          foreach ($datos['centros_escolares'] as $id_centros_escolares){
            $sql = "SELECT ces_ter_id FROM centro_escolar WHERE ces_id  = '". $id_centros_escolares ."';";
            $res = $_obj_database->obtenerRegistrosAsociativos($sql); 
            $datos['ces_ter_id'] = $res[0]['ces_ter_id'];

            $sql_transaccion = "INSERT INTO usuario_centro_escolar (usc_usu_id_promotor , usc_ces_id, usc_ter_id) VALUES(" .$datos['usu_id'].", " .$id_centros_escolares.", ".$datos['ces_ter_id'].");";
    
            $_obj_database->ejecutarSql($sql_transaccion);
          }
        }
        
      }
    }
		
		
		return 'U-18';
  }
  
  
  /**
   * Retorna si el usuario actual es un usuario root
   * Stuard Romero 2020-12-01
   ***/
  public static function esUsuarioRoot()
  {
    return $_SESSION['tipo_usuario'] == 1;
  }//Fin de esUsuarioRoot


  /** abrirFormularioGestion
     * parametro: $datos
     * autor : SA - 31/12/2020
     * descripcion: Listado de usuarios
     **/
  function abrirFormularioGestion($datos){
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_listado_usuarios.html"); 

    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

    $label_inicio = "Usuari";
    $msg_descripcion = "Llista de Usuari";

    Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);

		return $contenido;

  }// Fin de abrirFormularioGestion()

  public function obtenerUsuariosGestion($datos) {
    global $_obj_database, $_limit;

    $limit = ( isset($datos['limit']) && is_numeric($datos['limit']) ? $datos['limit'] : 10);
    $offset = ( isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);

    $orden = "order by usu_nombre ASC";
    if ($datos['ordena'] != "") {
        $orden = "order by " . $datos['ordena'] . " " . $datos['tipo_ordena'];
    }

    $condicionAdicionalTipo = '';
    if ($datos['usu_per_id'] != '') {
        $condicionAdicionalTipo = "AND usu_per_id = '" . $datos['usu_per_id'] . "'";
    }

    // $condicionModificarOtrosUsuarios = '';
    // if (!$this->verificarFuncionalidadActiva($_SESSION['usu_id'], 35) && $_SESSION['tipo_usuario'] != 'R') { //Si el usuario NO tiene activa la funcionalidad 35 (Usuarios) solo puede modificar sus datos no los de otros usuarios
    //     $condicionModificarOtrosUsuarios = " AND usu_id = '" . $_SESSION['usu_id'] . "' ";
    // }

    if ($datos['usu_busqueda'] != '') {//Se ingreso algun criterio para realizar la busqueda
      $condicionBusqueda = " AND (usu_nombre LIKE  '%" . $datos['usu_busqueda'] . "%' ||
                                usu_correo LIKE  '%" . $datos['usu_busqueda'] . "%' ||
                                usu_login LIKE  '%" . $datos['usu_busqueda'] . "%' ||
                                usu_id  LIKE  '%" . $datos['usu_busqueda'] . "%' ||
                                )";


    }

    $sql = "SELECT      usu_id, usu_nombre, usu_login, usu_correo
    FROM    usuarios";

    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
  }
  /* 
  * cambiarClaveUsuario
  * SR
  * Cambio de clave del usuario en el log in
  */
  public function cambiarClaveUsuario($datos) {
    global $_obj_database;

    $sql = "select usu_id, usu_nombre, usu_login
            from usuarios  
            where usu_estado = 1 
              and usu_correo='".$datos['usu_correo']."'
              and usu_token='".$datos['usu_token']."';";
    
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

    //El usuario ya cambio su contraseña
    if (count($resultado) === 0) {
      return false;
    }

    if (count($resultado) === 1) {
      $campos = array("usu_token", "usu_clave");
      $datos['usu_token'] = '';

      $datos['usu_clave'] = md5($datos['usu_clave'].$resultado[0]['usu_id']);

      $datos['tabla'] = "usuarios";
      $datos["condicion"] = "usu_correo = '".$datos['usu_correo']."'";

      $sql = $_obj_database->generarSQLActualizar($datos, $campos);

      $resultado_sql = $_obj_database->ejecutarSql($sql);

      if ($resultado_sql) {
        return 'U-13';
      }
    }
    
    return false;
  }


  public function esSolicitarRecuperarClaveValido($datos) {
    global $_obj_database;

    $sql = "select usu_login, usu_correo
					from usuarios  
					where usu_estado = 1 
            and usu_correo='".$datos['correo']."'
            and usu_token='".$datos['token']."';";

    $resultado_sql = $_obj_database->obtenerRegistrosAsociativos($sql);

    if (count($resultado_sql) !== 0) {
      return true;
    }
    
    return false;
  }

  public function abrirFormularioRecuperarClave($datos) {
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_cambiar_clave.html");


    Interfaz::asignarToken("correo", $datos['correo'], $contenido);
    Interfaz::asignarToken("token", $datos['token'], $contenido);

    return $contenido;
  }

    /** abrirFormularioAnadirUsuario
   * parametro: $datos
   * autor : SA 08/12/2020
   * descripcion: ABRE EL FORMULARIO PARA CREAR NUEVOS USUARIOS
   **/
  public function abrirFormularioAnadirUsuario($datos) {
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_obj_database;
    
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_anadir_usuarios.html");

    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

    $msg_titulo = 'Afegir Usuari';
    $msg_descripcion = "Registrar Usuari";
    $input_nombre = 'Nom';
    $input_correo = 'Correu';
    $input_usuario = "Usuari";
    $input_perfil = "Perfil";
    $input_apellido = "Cognom";

    $img_ruta = "Includes/images";

    $sql = "SELECT servicio_territorial.ste_id, servicio_territorial.ste_nombre, territorio.ter_id, territorio.ter_nombre 
            FROM servicio_territorial 
            JOIN territorio ON servicio_territorial.ste_id = territorio.ter_ste_id";
  
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

    foreach($resultado as $value => $territorios){
      $select_territorios .= '<option id="'.$territorios['ter_nombre'].'" value="'.$territorios['ter_id'].'">';
      $select_territorios .= $territorios['ste_nombre'] . " - " . $territorios['ter_nombre'];
      $select_territorios .= '</option>';
    } 

    $sql = "SELECT * FROM centro_escolar;";
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
    foreach($resultado as $value => $centros){
      $select_centros .= '<option id="'.$centros['ces_nombre'].'" value="'.$centros['ces_id'].'">';
      $select_centros .= $centros['ces_nombre'];
      $select_centros .= '</option>';
    } 

    Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
    Interfaz::asignarToken("boton_procesar", $boton_procesar, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("input_nombre", $input_nombre, $contenido);
    Interfaz::asignarToken("input_usuario", $input_usuario, $contenido);
    Interfaz::asignarToken("input_apellido", $input_apellido, $contenido);
    Interfaz::asignarToken("input_correo", $input_correo, $contenido);
    Interfaz::asignarToken("input_perfil", $input_perfil, $contenido);
    Interfaz::asignarToken("select_territorios", $select_territorios, $contenido);
    Interfaz::asignarToken("select_centros", $select_centros, $contenido);
    Interfaz::asignarToken("value_nombre", $datos['usu_nombre'], $contenido);
    Interfaz::asignarToken("value_apellido", $datos['usu_apellido'], $contenido);
    Interfaz::asignarToken("value_usuario", $datos['usu_login'], $contenido);
    Interfaz::asignarToken("value_correo", $datos['usu_correo'], $contenido);
    Interfaz::asignarToken("value_perfil", $datos['usu_per_id'], $contenido);

    return $contenido;
  }

  function obtenerCorreoAsignacionClave($datos, $pass)  
	{                        
		global $_PATH_WEB, $_PATH_ACCION; 		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/correo_clave.html"); 		

    Interfaz::asignarToken("path_web", $_PATH_WEB.'/index.php', $contenido);
    Interfaz::asignarToken("path_accion", '?m=usuarios&accion=fm_restablecer_contrasena', $contenido);
    Interfaz::asignarToken("nombre", $datos['usu_nombre'], $contenido);	
    Interfaz::asignarToken("correo", $datos['usu_correo'], $contenido);	
    Interfaz::asignarToken("user", $datos['usu_login'], $contenido);	
		Interfaz::asignarToken("pass", $pass, $contenido); 
    
    Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);	

		return $contenido;
  }//fin de obtenerContenidoActivacion()
  
  function obtenerListado($datos)
	{

		global $_obj_database, $_limit;

    $datos = Herramientas::trimCamposFormulario($datos);
    
		$offset = (isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);
		$condicion = "";     

    $resultado = "prueba";

		return $resultado;

  }//Fin de obtenerListado() 

   /** EliminarUsuarios
   * parametro: $datos
   * autor : SA 06/01/2021
   * descripcion: Elimina Usuarios
   **/
  function EliminarUsuarios($datos){
    global $_obj_database;
    
    if(isset($_SESSION))
    {
        $usu_id = $_SESSION['usu_id'];
    }

    if (!$_obj_database->iniciarTransaccion()) {
        //problema al inicar la transaccion
        return -1003;
    }

    $sql = "DELETE FROM usuarios WHERE usu_id = '" . $datos['usu_id'] . "';";
    if (!$_obj_database->ejecutarSql($sql)) {
        $_obj_database->cancelarTransaccion();
        return -1003;
    } else {
        $_obj_database->terminarTransaccion();
    }

    $sql = "SELECT ust_id FROM usuario_territorio WHERE ust_usu_id_coordinador  = '" . $datos['usu_id'] . "';";
    $res = $_obj_database->obtenerRegistrosAsociativos($sql);

    foreach($res as $territorio){
      $sql = "DELETE FROM usuario_territorio WHERE ust_usu_id_coordinador = '" . $datos['usu_id'] . "';";

      if (!$_obj_database->ejecutarSql($sql)) {
          $_obj_database->cancelarTransaccion();
          return -1003;
      } else {
          $_obj_database->terminarTransaccion();
      }
    }

    $sql = "SELECT usc_id FROM usuario_centro_escolar WHERE usc_usu_id_promotor = '" . $datos['usu_id'] . "';";
    $res = $_obj_database->obtenerRegistrosAsociativos($sql);

    foreach($res as $centro_escolar){
      $sql = "DELETE FROM usuario_territorio WHERE usc_usu_id_promotor = '" . $datos['usu_id'] . "';";

      if (!$_obj_database->ejecutarSql($sql)) {
          $_obj_database->cancelarTransaccion();
          return -1003;
      } else {
          $_obj_database->terminarTransaccion();
      }
    }

    return 'U-22';
    
}

function obtenerDatosUsuario($usu_id) {
  global $_obj_database;

  $sql = "SELECT * FROM usuarios
  WHERE usu_id=$usu_id";

  $res = $_obj_database->obtenerRegistrosAsociativos($sql);
  return ($res);
}

function abrirFormularioEditarUsuarios($datos,$valores){
  global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;

  $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/editar_usuario.html");

  $label_inicio = "Usuari";
  $msg_descripcion = "Edita Usuari";
 
  $input_nombre = 'Nom';
  $input_correo = 'Correu';
  $input_usuario = "Usuari";
  $input_perfil = "Perfil";
  $input_apellido = "Cognom";

  if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
  }

  Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

  $valores = $valores[0];
  
  switch($valores['usu_per_id']){
    case 1://Admin
      $valor_perfil = '<select class="form-select" id="'.$input_perfil.'" name="usu_per_id" required>
                          <option value="1">Administrador</option> 
                          <option value="2">Promotor</option>
                          <option value="3">Director</option>
                          <option value="4">Coordinador</option>
                        </select>';
      break;
    case 2://Promotor
      $sql_transaccion = "SELECT usu_id FROM usuarios 
      JOIN horas_adicionales_promotor ON horas_adicionales_promotor.hap_usu_id_promotor = usuarios.usu_id 
      WHERE usuarios.usu_id = ".$valores['usu_id']."
      UNION 
      SELECT usu_id FROM usuarios 
      JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
      WHERE usuarios.usu_id = ".$valores['usu_id']."
      UNION 
      SELECT usu_id FROM usuarios 
      JOIN familia ON familia.fam_usu_id_promotor = usuarios.usu_id 
      WHERE usuarios.usu_id = ".$valores['usu_id'].";";

      $resultados = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);
      if($resultados){
        $valor_perfil = '<p style="margin-top:10px;">Promotor</p>';
        $valor_perfil .= "<p style='background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;'>No es pot canviar el perfil ja que l'usuari seleccionat té registres associats.</p>";
        $valor_perfil .= '<input type="hidden" id="'.$input_perfil.'" name="usu_per_id" value="2">';
      }
      else {
        $valor_perfil = '<select class="form-select" id="'.$input_perfil.'" name="usu_per_id" required>
                          <option value="1">Administrador</option> 
                          <option value="2">Promotor</option>
                          <option value="3">Director</option>
                          <option value="4">Coordinador</option>
                        </select>';
      }

      $sql = 'SELECT centro_escolar.ces_id, centro_escolar.ces_nombre FROM centro_escolar 
                JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_ces_id = centro_escolar.ces_id 
                WHERE usuario_centro_escolar.usc_usu_id_promotor = '.$valores['usu_id'].';';
      $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

      $centros = '<label for="centros-escolares">Centros Escolares</label>
                  <select class="choices form-select multiple-remove" name="centros_escolares[]" multiple id="centros_escolares">';

      $sql = 'SELECT * FROM centro_escolar;';
      $resultados_centros = $_obj_database->obtenerRegistrosAsociativos($sql);

      $i=0;

        foreach($resultados_centros as $key_centros => $value_centros){
          foreach($resultados as $key => $value){
            if($value_centros['ces_id'] == $value['ces_id']){
              $centros .= '<option value="'.$value['ces_id'].'" id="'.$value['ces_id'].'" selected data-tokens="'.$value['ces_nombre'].'">'.$value['ces_nombre'].'</option>';
              unset($resultados_centros[$i]);
            }
          }
          $i++;
        }
        
        foreach($resultados_centros as $key => $value){
          $centros .= '<option value="'.$value['ces_id'].'" id="'.$value['ces_id'].'" data-tokens="'.$value['ces_nombre'].'">'.$value['ces_nombre'].'</option>';
        }

        $centros .= '</select>';

      break;

    case 3://Director
      $sql_transaccion = 'SELECT usu_id FROM usuarios JOIN centro_escolar
      ON centro_escolar.ces_usu_id_director = usuarios.usu_id WHERE usuarios.usu_id = '.$valores['usu_id'].';';
      $resultados = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);
      if($resultados){
        $valor_perfil = '<p style="margin-top:10px;">Director</p>';
        $valor_perfil .= "<p style='background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;'>No es pot canviar el perfil ja que l'usuari seleccionat té registres associats.</p>";
        $valor_perfil .= '<input type="hidden" id="'.$input_perfil.'" name="usu_per_id" value="3">';
      }
      else {
        $valor_perfil = '<select class="form-select" id="'.$input_perfil.'" name="usu_per_id" required>
                          <option value="1">Administrador</option> 
                          <option value="2">Promotor</option>
                          <option value="3">Director</option>
                          <option value="4">Coordinador</option>
                        </select>';
      }
      break;
      
    case 4://Coordinador
      $sql_transaccion = 'SELECT usuarios.usu_id, usuario_territorio.ust_ter_id, territorio.ter_nombre FROM usuarios 
      JOIN usuario_territorio ON usuario_territorio.ust_usu_id_coordinador  = usuarios.usu_id 
      JOIN territorio ON territorio.ter_id = usuario_territorio.ust_ter_id
      WHERE usuarios.usu_id = '.$valores['usu_id'].';';
      $resultados = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);

      if($resultados){
        $valor_perfil = '<p id="perfil_fijo" style="margin-top:10px;">Coordinador</p>';
        $valor_perfil .= "<p style='background-color:#3775EA;color:#fff;padding:20px;border-radius:3px;'>No es pot canviar el perfil ja que l'usuari seleccionat té registres associats.</p>";
        $valor_perfil .= '<input type="hidden" id="'.$input_perfil.'" name="usu_per_id" value="4">';

        //Todos los territorios y servicios territoriales 
        $sql = "SELECT servicio_territorial.ste_id, servicio_territorial.ste_nombre, territorio.ter_id, territorio.ter_nombre 
                FROM servicio_territorial 
                JOIN territorio ON servicio_territorial.ste_id = territorio.ter_ste_id";
        $territorios_servicios = $_obj_database->obtenerRegistrosAsociativos($sql);

        foreach($territorios_servicios as $key => $todos_territorios){
          foreach($resultados as $num => $valor){
            if($todos_territorios['ter_id'] == $valor['ust_ter_id']){
              $territorios_servicios[$key]['ter_usado'] = 1;
            }
          }
        }

        $territorios = '<label for="territorios">TERRITORI</label>
                        <select class="choices form-select multiple-remove" name="territorios[]" multiple required id="territorios">';

        foreach($territorios_servicios as $key => $todos_territorios){
          if($todos_territorios['ter_usado'] == 1){
            $territorios .= '<option value="'.$todos_territorios['ter_id'].'" id="'.$todos_territorios['ter_nombre'].'" selected>'.$todos_territorios['ter_nombre'].' - '. $todos_territorios['ste_nombre'].'</option>';
          }
          else{
            $territorios .= '<option value="'.$todos_territorios['ter_id'].'" id="'.$todos_territorios['ter_nombre'].'">'.$todos_territorios['ter_nombre'].' - '. $todos_territorios['ste_nombre'].'</option>';
          }
          
        }
        $territorios .= '</select>';
      }
      else {
        $valor_perfil = '<select class="form-select" id="'.$input_perfil.'" name="usu_per_id" required>
                          <option value="1">Administrador</option> 
                          <option value="2">Promotor</option>
                          <option value="3">Director</option>
                          <option value="4">Coordinador</option>
                        </select>';

        $sql = "SELECT servicio_territorial.ste_id, servicio_territorial.ste_nombre, territorio.ter_id, territorio.ter_nombre 
                FROM servicio_territorial 
                JOIN territorio ON servicio_territorial.ste_id = territorio.ter_ste_id";
        $territorios_servicios = $_obj_database->obtenerRegistrosAsociativos($sql);

        $territorios = '<label for="territorios">Territorios</label>
                        <select class="choices form-select multiple-remove" name="territorios[]" multiple="multiple" required id="territorios">';

        foreach($territorios_servicios as $key => $todos_territorios){
            $territorios .= '<option value="'.$todos_territorios['ter_id'].'" id="'.$todos_territorios['ter_nombre'].'">'.$todos_territorios['ter_nombre'].' - '. $todos_territorios['ste_nombre'].'</option>';  
        }
        $territorios .= '</select>';
      }

      break;

    default:
      break;
  }
  

  Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
  Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
  Interfaz::asignarToken("usu_id", $datos['usu_id'], $contenido);

  Interfaz::asignarToken("input_nombre", $input_nombre, $contenido);
  Interfaz::asignarToken("usu_val_nombre",$valores["usu_nombre"], $contenido);
  Interfaz::asignarToken("input_usuario", $input_usuario, $contenido);
  Interfaz::asignarToken("usu_val_usuario",$valores["usu_login"], $contenido);

  Interfaz::asignarToken("input_apellido", $input_apellido, $contenido);
  Interfaz::asignarToken("usu_val_apellido",$valores["usu_apellido"], $contenido);
  Interfaz::asignarToken("input_correo", $input_correo, $contenido);
  Interfaz::asignarToken("usu_val_correo",$valores["usu_correo"], $contenido);

  Interfaz::asignarToken("input_perfil", $input_perfil, $contenido);
  Interfaz::asignarToken("valor_perfil", $valor_perfil, $contenido);

  Interfaz::asignarToken("territorios", $territorios, $contenido);
  Interfaz::asignarToken("mensaje", $mensaje, $contenido);
  Interfaz::asignarToken("centros", $centros, $contenido);

  return $contenido;
}

function editarUsuarios($datos){
  global $_obj_database, $_PATH_IMAGENES, $_PATH_WEB, $_opciones;

  $datos = Herramientas::trimCamposFormulario($datos);    

  $sql = "UPDATE usuarios SET usu_nombre = '". $datos['usu_nombre'] ."', usu_correo = '". $datos['usu_correo'] ."', usu_apellido = '". $datos['usu_apellido'] ."', usu_per_id = '". $datos['usu_per_id'] ."' WHERE usu_id = '". $datos['usu_id'] ."';";
  $res = $_obj_database->ejecutarSql($sql); 

  if (!$res) {
      $_obj_database->cancelarTransaccion();
      $_db_obj['see']->cancelarTransaccion();
      return -1003;
  }
  
  if($datos['usu_per_id'] == 2){
    $sql = "SELECT * FROM usuario_centro_escolar WHERE usc_usu_id_promotor = '".$datos['usu_id']."';";
    $resultado_bbdd = $_obj_database->obtenerRegistrosAsociativos($sql);

    if(count($resultado_bbdd) == count($datos['centros_escolares'])){
      //echo "Update";
      foreach($datos['centros_escolares'] as $key => $centros){
        $sql = "SELECT ces_ter_id FROM centro_escolar WHERE ces_id  = '". $centros ."';";
        $res = $_obj_database->obtenerRegistrosAsociativos($sql); 
        $datos['ces_ter_id'] = $res[0]['ces_ter_id'];
        //echo "Update";

        $sql = "UPDATE usuario_centro_escolar SET usc_ces_id = ". $centros .", usc_ter_id = ". $datos['ces_ter_id'] ." WHERE usc_id = ". $resultado_bbdd[$key]['usc_id'].";";
        $res = $_obj_database->ejecutarSql($sql); 
      }

    }
    else{
      if(count($resultado_bbdd) < count($datos['centros_escolares'])){
        foreach($datos['centros_escolares'] as $key => $centros){
          $sql = "SELECT ces_ter_id FROM centro_escolar WHERE ces_id  = '". $centros ."';";
          $res = $_obj_database->obtenerRegistrosAsociativos($sql); 
          $datos['ces_ter_id'] = $res[0]['ces_ter_id'];
          if($key < count($resultado_bbdd)){
            //echo "Update";

            $sql = "UPDATE usuario_centro_escolar SET usc_ces_id = ". $centros .", usc_ter_id = ". $datos['ces_ter_id'] ." WHERE usc_id = ". $resultado_bbdd[$key]['usc_id'].";";
            $res = $_obj_database->ejecutarSql($sql); 
          }
          else{
            //echo "Insert";
            $sql_transaccion = "INSERT INTO usuario_centro_escolar (usc_usu_id_promotor , usc_ces_id, usc_ter_id) VALUES(" .$datos['usu_id'].", " .$centros.", ".$datos['ces_ter_id'].");";
  
            $_obj_database->ejecutarSql($sql_transaccion);
          }
        }
      }
      else{
        //echo "Update and Delete";
        foreach($datos['centros_escolares'] as $key => $centros){
          $sql = "SELECT ces_ter_id FROM centro_escolar WHERE ces_id  = '". $centros ."';";
          $res = $_obj_database->obtenerRegistrosAsociativos($sql); 
          $datos['ces_ter_id'] = $res[0]['ces_ter_id'];

          //echo "Update";

          $sql = "UPDATE usuario_centro_escolar SET usc_ces_id = ". $centros .", usc_ter_id = ". $datos['ces_ter_id'] ." WHERE usc_id = ". $resultado_bbdd[$key]['usc_id'].";";
          $res = $_obj_database->ejecutarSql($sql); 
        }
          //echo "Eliminar";
          $key++;
          $j =count($resultado_bbdd);
        for($i = $key; $i < $j; $i++){
          
          $sql = "DELETE FROM usuario_centro_escolar WHERE usc_id = " . $resultado_bbdd[$i]['usc_id'] . ";";
          
          if (!$_obj_database->ejecutarSql($sql)) {
              $_obj_database->cancelarTransaccion();
              return -1003;
          } else {
              $_obj_database->terminarTransaccion();
          }
        }
      }
    }
  }
  else{
    if($datos['usu_per_id'] == 4){
      $sql = "SELECT * FROM usuario_territorio WHERE ust_usu_id_coordinador = '".$datos['usu_id']."';";
      $resultado_bbdd = $_obj_database->obtenerRegistrosAsociativos($sql);

      if(count($resultado_bbdd) == count($datos['territorios'])){
        //echo "Update";
        foreach($datos['territorios'] as $key => $ter){
          //echo "Update";
          $sql = "UPDATE usuario_territorio SET ust_ter_id = ". $ter ." WHERE ust_id = ". $resultado_bbdd[$key]['ust_id'].";";
          $res = $_obj_database->ejecutarSql($sql); 
        }

      }
      else{
        if(count($resultado_bbdd) < count($datos['territorios'])){
          foreach($datos['territorios'] as $key => $ter){
            if($key < count($resultado_bbdd)){
              //echo "Update";

              $sql = "UPDATE usuario_territorio SET ust_ter_id = ". $ter ." WHERE ust_id = ". $resultado_bbdd[$key]['ust_id'].";";
              $res = $_obj_database->ejecutarSql($sql); 
            }
            else{
              //echo "Insert";
              $sql_transaccion = "INSERT INTO usuario_territorio (ust_usu_id_coordinador, ust_ter_id) VALUES(" .$datos['usu_id'].", " .$ter.");";
    
              $_obj_database->ejecutarSql($sql_transaccion);
            }
          }
        }
        else{
          //echo "Update and Delete";
          foreach($datos['territorios'] as $key => $ter){

            //echo "Update";

            $sql = "UPDATE usuario_territorio SET ust_ter_id = ". $ter ." WHERE ust_id = ". $resultado_bbdd[$key]['ust_id'].";";
            $res = $_obj_database->ejecutarSql($sql); 
          }
            //echo "Eliminar";
            $key++;
            $j =count($resultado_bbdd);
          for($i = $key; $i < $j; $i++){
            
            $sql = "DELETE FROM usuario_territorio WHERE ust_id = " . $resultado_bbdd[$i]['ust_id'] . ";";
            
            if (!$_obj_database->ejecutarSql($sql)) {
                $_obj_database->cancelarTransaccion();
                return -1003;
            } else {
                $_obj_database->terminarTransaccion();
            }
          }
        }
      }
    }
  }

  if($existe_rep != 1){
    $sql = "UPDATE usuarios SET usu_nombre = '". $datos['usu_nombre'] ."', usu_correo = '". $datos['usu_correo'] ."', usu_apellido = '". $datos['usu_apellido'] ."', usu_per_id = '". $datos['usu_per_id'] ."' WHERE usu_id = '". $datos['usu_id'] ."';";
    $res = $_obj_database->ejecutarSql($sql); 

    if (!$res) {
        $_obj_database->cancelarTransaccion();
        $_db_obj['see']->cancelarTransaccion();
        return -1003;
    }

    if($res == 1){
        return 'U-21';
    } else {
        return 'U-20';
    }
  }
  else{
    $enlace = "index.php?m=usuarios&accion=fm_editar&usu_id_admin=".$datos['usu_id_admin']."&usu_id=".$datos['usu_id']."&msg=U-24";
    header('Location: '.$enlace);
    exit;
  }

  

}
  public function obtenerServiciosTerritoriales($datos) {
    global $_obj_database;
  
    $sql = "SELECT servicio_territorial.ste_id, servicio_territorial.ste_nombre, territorio.ter_id, territorio.ter_nombre 
            FROM servicio_territorial 
            JOIN territorio ON servicio_territorial.ste_id = territorio.ter_ste_id";
  
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
  
    return json_encode(
      $resultado
    );
  }

  public function obtenerTerritorios($datos) {
    global $_obj_database;
  
    switch($_SESSION['tipo_usuario']){
      case 4:
        $sql = "SELECT DISTINCT territorio.ter_id AS id_territorio, territorio.ter_nombre AS nombre_territorio  
                FROM territorio 
                JOIN usuario_territorio ON usuario_territorio.ust_ter_id = territorio.ter_id
                WHERE territorio.ter_ste_id = ".$datos['id_servicio_territorial']." AND 
                usuario_territorio.ust_usu_id_coordinador = ".$datos['usu'].";";
        break;
      default:
        $sql = "select ter_id as id_territorio, ter_nombre as nombre_territorio  
        from territorio where ter_ste_id = ".$datos['id_servicio_territorial']." ;";
        break;
    }
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

    return json_encode(
      $resultado
    );
  }

  public function obtenerCentrosEscolares($datos) {
    global $_obj_database;

    if (isset($datos['id_territorio'])) {
      $sql = "SELECT ces_id as id_centro_escolar, ces_nombre as nombre_centro_escolar 
              FROM centro_escolar WHERE ces_ter_id = ".intval ($datos['id_territorio']).";";
    } else {
      if(isset($datos['promotor'])){
        $sql = "SELECT DISTINCT centro_escolar.ces_id, centro_escolar.ces_nombre 
                FROM usuario_centro_escolar 
                JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
                WHERE usuario_centro_escolar.usc_usu_id_promotor =".$datos['promotor'].";";
      }else{
        $sql = "SELECT * FROM centro_escolar;";
      }
    }

    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
  
    return json_encode(
      $resultado
    );
  }

  public function obtenerPromotoresDeCentroEscolar($datos) {
    global $_obj_database;

    if (isset($datos['id_centro_escolar'])) {
      $sql = "SELECT 
        uce.usc_usu_id_promotor, 
        uce.usc_ces_id, 
        u.usu_nombre as nombre_promotor, 
        u.usu_apellido as apellido_promotor, 
        u.usu_id as id_promotor 
      FROM usuario_centro_escolar as uce
      INNER JOIN usuarios as u ON uce.usc_usu_id_promotor=u.usu_id WHERE uce.usc_ces_id = ".intval ($datos['id_centro_escolar']).";";
    }

    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
  
    return json_encode(
      $resultado
    );
  }

  /** obtenerPromotores
  * parametro: $datos
  * autor : SA - 01/05/2021
  * descripcion: Obtiene los promotores mediante ajax con el territorio
  **/
  public function obtenerPromotores($datos) {
    global $_obj_database;

    $sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido 
                FROM usuario_centro_escolar 
                JOIN territorio ON territorio.ter_id = usuario_centro_escolar.usc_ter_id 
                JOIN usuarios ON usuarios.usu_id = usuario_centro_escolar.usc_usu_id_promotor 
                WHERE territorio.ter_id = ".$datos['territorio']." ;";
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

    return json_encode(
      $resultado
    );
  }

  /** obtenerEstudiantes
  * parametro: $datos
  * autor : SA - 01/05/2021
  * descripcion: Obtiene los estudiantes mediante ajax con el centro escolar
  **/
  public function obtenerEstudiantes($datos) {
    global $_obj_database, $_obj_interfaz;

    $sql = "SELECT DISTINCT estudiante.est_id, estudiante.est_idalu, estudiante.est_nombre 
                FROM estudiante 
                JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                WHERE centro_escolar.ces_id = ".$datos['centro_escolar']." ;";
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);


    foreach($resultado as $key => $value){
      $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $value['est_nombre']);
      $resultado[$key]['est_nombre'] = $alumn_name;
    }

    return json_encode(
      $resultado
    );
  }

  /** cambiarContrasena
  * parametro: $datos, $valores
  * autor : SA - 04/02/2021
  * descripcion: Vista del formulario para cambiar la contrasena
  **/
  function cambiarContrasena($datos,$valores){
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }
    $valores = $valores[0];

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/cambiar_contrasena.html");
  
    $label_inicio = "Canviar Contrasenya";
    $msg_descripcion = "Usuari " . $valores['usu_nombre'];
   
    $input_actual = 'Contrasenya Actual';
    $input_nueva = 'clau Nova';
    $input_confirma_nueva = "Confirmar contrasenya nova";
    
    Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("usu_id", $datos['usu_id'], $contenido);
  
    Interfaz::asignarToken("input_actual", $input_actual, $contenido);
    Interfaz::asignarToken("input_nueva", $input_nueva, $contenido);
    Interfaz::asignarToken("input_confirma_nueva", $input_confirma_nueva, $contenido);

    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
  
    return $contenido;
  }

  /**
   * Function ejecutarEcriptadoDeAlumnos
   */

  function ejecutarEncriptadoDeAlumnos($datos){
    global $_obj_interfaz, $_obj_database;

    $sql = "SELECT DISTINCT est_id, est_nombre 
                FROM estudiante  
                WHERE 1 ;";
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
    $campos = array(    
      "est_nombre",
    );        
    foreach($resultado as $est){
      //Encriptar estudiante, se actualiza el nombre del estudiante 
      $alumn_name = $_obj_interfaz->encrypt_decrypt('encrypt', $est['est_nombre']);
      
      $datos['tabla'] = 'estudiante';
      $datos['est_nombre'] = $alumn_name;
        
      $datos['condicion'] = "est_id = '" . $est['est_id'] . "' ";

      $res = $_obj_database->actualizarDatosTabla($datos, $campos);
    }

    return 'Estudiantes actualizados - no repetir este proceso';
  }
  /** actCambiarClave
  * parametro: $datos, $valores
  * autor : SA - 04/02/2021
  * descripcion: Funcion donde se actualiza y verifica el cambio de contrasena
  **/
  public function actCambiarClave($datos, $valores) {
    global $_PATH_WEB, $_opciones, $idi_despliegue, $_obj_database;

    if($valores['usu_clave'] == md5($datos['usu_clave_actual'].$datos['usu_id'])){
      if($datos['usu_clave_nueva'] == $datos['usu_confirma_clave_nueva']){
        $password = md5($datos['usu_confirma_clave_nueva'].$datos['usu_id']);
        $sql = "UPDATE usuarios SET usu_clave = '". $password ."' WHERE usu_id = '". $datos['usu_id'] ."';";
        $res = $_obj_database->ejecutarSql($sql); 

        if($res = 1){
          header('Location: index.php?m=Usuarios&accion=dashboard&msg=U-13');
        }
        else{
          header('Location: index.php?m=Usuarios&accion=cambiar_contrasena&usu_id='.$datos['usu_id'].'&msg=U-12');
        }
      }
      else{
        header('Location: index.php?m=Usuarios&accion=cambiar_contrasena&usu_id='.$datos['usu_id'].'&msg=U-25');
      }
    }
    else{
      header('Location: index.php?m=Usuarios&accion=cambiar_contrasena&usu_id='.$datos['usu_id'].'&msg=U-2');
    }
  }
}//Fin de clase  Usuario

?>
