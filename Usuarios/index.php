<?php

require_once($_PATH_SERVIDOR . "/Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "/Config.php");

require_once("Usuarios.php");

$obj_usuarios = new Usuarios();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");

$_obj_interfaz->adicionarFooterJS("gestion_usuarios.js");

$_obj_interfaz->adicionarCSS("font-awesome.min.css");

$_obj_interfaz->adicionarJS("bootstrap.min.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'];

switch($datos['accion'])
{
	case 'fm_restablecer_contrasena':
		$datos['plantilla'] = 'principal_login';
		
		if ($obj_usuarios->esSolicitarRecuperarClaveValido($datos)) {
			$contenido = $obj_usuarios->abrirFormularioRecuperarClave($datos);
		} else {
			$contenido = $obj_usuarios->mostrarErrorRecuperacionInvalida($datos);

		}

		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'restablecer_contrasena':
		$datos['usu_correo'] = $datos['correo'];
		$datos['usu_token'] = $datos['token'];
		$datos['usu_clave'] = $datos['contrasena'];
		$contenido = $obj_usuarios->cambiarClaveUsuario($datos);
		header('Location: index.php?m=usuarios&accion=dashboard&msg='.$contenido);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'autenticar_usuario'.validarAcceso(array()):
		$datos['plantilla'] = 'principal';
		$resultado = $obj_usuarios->autenticarUsuarios($datos);

		if ( $_SESSION['autenticado'] == 1) {
			header('Location: index.php?m=usuarios&accion=dashboard');
		} else {
			header('Location: index.php?m=Usuarios&msg='.$resultado);
		}
		//AMP Cambio a modal de INSPINIA
		break;
	
	case 'cerrar_sesion'.validarAcceso(array()):
		if( $_SESSION['autenticado'] == 1 )
		{
			@session_regenerate_id();
			@session_destroy();
		}//Fin de if( $_SESSION['autenticado'] == 1 )
		unset($_SESSION);
		header('Location: index.php?m=Usuarios');
		exit;
		break;
	
	case 'administrador'.validarAcceso(array(1)):
		$datos['plantilla'] = 'principal';
		$contenido = $obj_usuarios->mostrarAdministracion($datos);
		//AMP Cambio a modal de INSPINIA
			$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'forma_olvido_clave'.validarAcceso(array()):	
		// $datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
		$datos['plantilla'] = 'principal_login';
		$contenido = $obj_usuarios->abrirFormularioOlvidoClave($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	
	case 'olvido_clave'.validarAcceso(array()):    
		$resultado = $obj_usuarios->solicitarRecordatorioClave($datos);

		if($resultado === true)
		{       
			$token = $obj_usuarios->generarTokenRecuperacionContrasena($datos['usu_correo']);

			$obj_correo = new Correo();
			$destinatario = $datos['usu_correo']; 
			$remitente = $_EMAIL_ADMIN; 
			$asunto = "Fundació PereClosa - Oblit de contrasenya"; 
			$cuerpo = $obj_usuarios->obtenerCorreoOlvidoClave($datos, $token);
			
			//MAOH - 18 Nov 2011 - Cambio clave   
			if($obj_correo->enviarCorreo($remitente, $destinatario, $asunto, $cuerpo)) {
				$resultado = "U-5";
			}
			else
			{
				$resultado = "U-6";
			}	
		}    
		

		header('Location: index.php?m=usuarios&msg='.$resultado);

		break;

	case 'dashboard'.validarAcceso(array(1,2,3,4)):
		$datos['tipo_usuario'] = $_SESSION['tipo_usuario'];
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_usuarios->abrirFormularioHome($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'anadir_usuarios'.validarAcceso(array(1)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_usuarios->abrirFormularioAnadirUsuario($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'guardar_usuario'.validarAcceso(array(1)):
		$resultado = $obj_usuarios->registrarUsuarios($datos);

		$enlace = "index.php?m=usuarios&accion=listar_usuarios&tipo_gestion=3&msg=".$resultado."";
		header('Location: '.$enlace);

		break;
	case 'listar_usuarios'.validarAcceso(array(1)):
		
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$contenido = $obj_usuarios->abrirFormularioGestion($datos);
		$campos = array(
			"usu_id" => "ID", 
			"usu_login" => "Usuari",
			"usu_nombre" => "Nom",
			"usu_apellido" => "Cognom",
			"usu_correo" => "Correu", 
			"usu_per_id" => "Perfil"
		);  	

		if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
		}
		
		$opciones['actualizar'] = "usc_usu_id_promotor=$usu_id";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id";

		$datos['sql'] = "SELECT * FROM usuarios ORDER BY usu_id DESC";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $resultado;
		$arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
		$contenido .= $_obj_interfaz->crearListadoUsuarios($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
	
		break;
	case 'fm_eliminar'.validarAcceso(array(1)):
		$resultado = $obj_usuarios->EliminarUsuarios($datos);
		
		$enlace = "index.php?m=usuarios&accion=listar_usuarios&tipo_gestion=3&msg=".$resultado."";
		header('Location: '.$enlace);
		
		break;
	case 'fm_editar'.validarAcceso(array(1)):
		$datos['usu_id_admin'] = $_SESSION['usu_id'];
		$valores = $obj_usuarios->obtenerDatosUsuario($datos['usu_id']);
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		
		$datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_usuarios->abrirFormularioEditarUsuarios($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'editarUsuarios'.validarAcceso(array(1)):
		$resultado = $obj_usuarios->editarUsuarios($datos);
		if( $resultado=='U-21' ) 
		{                 
			header('Location: index.php?m=usuarios&accion=listar_usuarios&tipo_gestion=1&msg='.$resultado);    
			break;            
		}//Fin de if( $resultado==1 )      
		
		break;
	case 'encrypt_name_alumn'.validarAcceso(array(1)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}
		$contenido = $obj_usuarios->ejecutarEncriptadoDeAlumnos($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'cambiar_contrasena'.validarAcceso(array(1,2,3,4)):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$valores = $obj_usuarios->obtenerDatosUsuario($datos['usu_id']);
		$contenido = $obj_usuarios->cambiarContrasena($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'act_cambiar_contrasena'.validarAcceso(array(1,2,4)):
		$valores = $obj_usuarios->obtenerDatosUsuario($datos['usu_id']);
    	$valores = $valores[0];
		$resultado = $obj_usuarios->actCambiarClave($datos, $valores);
		break;
	case 'obtener_servicios_territoriales'.validarAcceso(array(1)):
		$contenido = $obj_usuarios->obtenerServiciosTerritoriales($datos);
		print_r($contenido);

		exit;
		break;
	case 'obtener_territorios'.validarAcceso(array(1, 2, 4)):
		$contenido = $obj_usuarios->obtenerTerritorios($datos);
		print_r($contenido);

		exit;
		break;
	case 'obtener_promotores'.validarAcceso(array(1, 2, 4)):
		$contenido = $obj_usuarios->obtenerPromotores($datos);
		print_r($contenido);

		exit;
		break;
	case 'obtener_estudiantes'.validarAcceso(array(1, 2, 4)):
		$contenido = $obj_usuarios->obtenerEstudiantes($datos);
		print_r($contenido);

		exit;
		break;
	case 'obtener_centros_escolares'.validarAcceso(array(1, 2, 4)):
		$contenido = $obj_usuarios->obtenerCentrosEscolares($datos);
		print_r($contenido);

		exit;
		break;

	case 'obtener_promotores_ce'.validarAcceso(array(1, 2, 4)):
		$contenido = $obj_usuarios->obtenerPromotoresDeCentroEscolar($datos);
		print_r($contenido);

		exit;
		break;
	default:
		if ($_SESSION['autenticado'] != 1)	{
			$datos['plantilla'] = 'principal_login';
			$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
			$contenido = $obj_usuarios->abrirFormularioAutenticacionUsuarios($datos);
			$_obj_interfaz->asignarContenido($contenido);
			break;
		} 
		break;
}
?>
