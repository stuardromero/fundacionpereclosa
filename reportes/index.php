<?php

require_once($_PATH_SERVIDOR . "/Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "/Config.php");

require_once("Reportes.php");

$obj_reportes = new Reportes();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");

	$_obj_interfaz->adicionarFooterJS("gestion_reportes.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'];

switch($datos['accion'])
{
	case 'descripcion_intervencion'.validarAcceso(array(1, 2, 4)):	
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$contenido = $obj_reportes->abrirFormularioDescripcionIntervencion($datos);  		
		$_obj_interfaz->asignarContenido($contenido);
		break;  
	case 'buscar_intervencion'.validarAcceso(array(1, 2, 4)):
	    if(isset($datos['centro_escolar'])){
	        if($_SESSION['tipo_usuario'] == 2){
    			$sql = "SELECT * FROM visita 
    				WHERE vis_ces_id = ".$datos['centro_escolar']." AND 
    				vis_usu_id_promotor = ".$_SESSION['usu_id']." ORDER BY vis_id DESC LIMIT 1;";
    		}
    		else{
    			$sql = "SELECT * FROM visita 
    				WHERE vis_ces_id = ".$datos['centro_escolar']." ORDER BY vis_id DESC LIMIT 1;";
    		}
    		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    		if($resultado){
    			$visitas = $resultado[0];
    			$datos['visita'] = $visitas['vis_id'];
    
    			$campos = array(
    				"est_nombre" => "Nombre I Cognoms De L'Alumne",
    				"min_nombre" => "Motiu D'Intervenció",
    				"tab_nombre" => "Tipus D'Absentisme",
    				"tin_nombre" => "Tipus D'Intervenció",
    				"observaciones" => "Observacions"
    			);  
    
    			$sql = "SELECT * FROM intervencion 
    					WHERE int_vis_id = ".$visitas['vis_id']." ORDER BY int_id DESC;";
    			$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    			if(!$resultado){
    			    echo "<script>window.location.href='index.php?m=reportes&accion=descripcion_intervencion&msg=REP-2';</script>";
                    exit;
    				//header('Location: index.php?m=reportes&accion=descripcion_intervencion&msg=REP-2');
    			}
    		}
    		else{
    		    echo "<script>window.location.href='index.php?m=reportes&accion=descripcion_intervencion&msg=REP-1';</script>";
                exit;
    			//header('Location: index.php?m=reportes&accion=descripcion_intervencion&msg=REP-1');
    		}
    
    		$arreglo_datos['CAMPOS'] = $campos;
    		$arreglo_datos['VALORES'] = $resultado;
    		$arreglo_datos['OPCIONES'] = $opciones;
    		$arreglo_datos['DATOS'] = $datos;
    
    		$tabla_result = $_obj_interfaz->crearListadoUltimaVisita($arreglo_datos);
    		$datos["result_table_centre"]= $tabla_result;
    		
    		$contenido .= $obj_reportes->abrirFormularioDescripcionIntervencion($datos);
    		$_obj_interfaz->asignarContenido($contenido);
	    }
		else{
		    echo "<script>window.location.href='index.php?m=reportes&accion=descripcion_estudiantes&msg=REP-4';</script>";
            exit;
		}

		break; 
	case 'horas_trabajadas_centros_escolares'.validarAcceso(array(1, 2, 4)):	
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$contenido = $obj_reportes->abrirFormularioHorasTrabajadasCentrosEscolares($datos);  		
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'buscar_horas'.validarAcceso(array(1, 2, 4)):
		$campos = array(
			"usu_nombre" => "Nom i Cognoms",
			"ces_nombre" => "Dins el Centre Educatiu",
			"horas_fuera" => "Fora Dels Centre Educatiu",
			"total " => "Total D'Hores"
		);  

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		if ($_SESSION['tipo_usuario'] == 1) {
			$sql = "SELECT * FROM usuarios WHERE usu_per_id = 2;";
		  }
		  else{
			if ($_SESSION['tipo_usuario'] == 4) {
			  $sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido
					  FROM usuarios 
					  JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
					  WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
			}

			if ($_SESSION['tipo_usuario'] == 2) {
				$sql = "SELECT * FROM usuarios WHERE usu_id = ".$_SESSION['usu_id'].";";
			}

		  }
      	$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $resultado;
		$arreglo_datos['OPCIONES'] = $opciones;
		$arreglo_datos['DATOS'] = $datos;
	
		$tabla_result = $_obj_interfaz->crearListadoHorasTrabajadas($arreglo_datos);
		$datos["result_table_centre"]= $tabla_result;
		
		$contenido .= $obj_reportes->abrirFormularioHorasTrabajadasCentrosEscolares($datos);
		$_obj_interfaz->asignarContenido($contenido);
		
		break;
	case 'informe_ninos_atendidos'.validarAcceso(array(1, 2, 4)):	
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$contenido = $obj_reportes->abrirFormularioNinosAtendidos($datos);  		
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'buscar_ninos_atendidos'.validarAcceso(array(1, 2, 4)):
		if($_SESSION['tipo_usuario'] == 2){
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"eta_edu_1_nois" => "Nois",
				"eta_edu_1_noies" => "Noies",
				"eta_edu_1_Total" => "Total",
				"eta_edu_2_nois" => "Nois",
				"eta_edu_2_noies" => "Noies",
				"eta_edu_2_Total" => "Total",
				"eta_edu_3_nois" => "Nois",
				"eta_edu_3_noies" => "Noies",
				"eta_edu_3_Total" => "Total",
				"eta_edu_4_nois" => "Nois",
				"eta_edu_4_noies" => "Noies",
				"eta_edu_4_Total" => "Total",
				"eta_edu_5_nois" => "Nois",
				"eta_edu_5_noies" => "Noies",
				"eta_edu_5_Total" => "Total"
			);  
		}
		else{
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"usu_nombre" => "Nom i Cognoms",
				"eta_edu_1_nois" => "Nois",
				"eta_edu_1_noies" => "Noies",
				"eta_edu_1_Total" => "Total",
				"eta_edu_2_nois" => "Nois",
				"eta_edu_2_noies" => "Noies",
				"eta_edu_2_Total" => "Total",
				"eta_edu_3_nois" => "Nois",
				"eta_edu_3_noies" => "Noies",
				"eta_edu_3_Total" => "Total",
				"eta_edu_4_nois" => "Nois",
				"eta_edu_4_noies" => "Noies",
				"eta_edu_4_Total" => "Total",
				"eta_edu_5_nois" => "Nois",
				"eta_edu_5_noies" => "Noies",
				"eta_edu_5_Total" => "Total"
			);  
		}
		

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		switch($_SESSION['tipo_usuario']){
			case 1:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			case 2:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
				break;
			case 4:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			default:
				break;
		}
		$promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

		if($promotores && $datos['ano_lectivo']){
			$arreglo_datos['CAMPOS'] = $campos;
			$arreglo_datos['VALORES'] = $promotores;
			$arreglo_datos['OPCIONES'] = $opciones;
			$arreglo_datos['DATOS'] = $datos;
		
			$tabla_result = $_obj_interfaz->crearListadoAlumnosAtendidos($arreglo_datos);
			$datos["result_table_centre"]= $tabla_result;
			
			$contenido .= $obj_reportes->abrirFormularioNinosAtendidos($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		else{
			header('Location: index.php?m=reportes&accion=informe_ninos_atendidos&msg=REP-4');
		}

		break;
	case 'buscar_ninos_atendidos_exportar'.validarAcceso(array(1, 2, 4)):
		if($_SESSION['tipo_usuario'] == 2){
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"eta_edu_1_nois" => "Nois",
				"eta_edu_1_noies" => "Noies",
				"eta_edu_1_Total" => "Total",
				"eta_edu_2_nois" => "Nois",
				"eta_edu_2_noies" => "Noies",
				"eta_edu_2_Total" => "Total",
				"eta_edu_3_nois" => "Nois",
				"eta_edu_3_noies" => "Noies",
				"eta_edu_3_Total" => "Total",
				"eta_edu_4_nois" => "Nois",
				"eta_edu_4_noies" => "Noies",
				"eta_edu_4_Total" => "Total",
				"eta_edu_5_nois" => "Nois",
				"eta_edu_5_noies" => "Noies",
				"eta_edu_5_Total" => "Total"
			);  
		}
		else{
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"usu_nombre" => "Nom i Cognoms",
				"eta_edu_1_nois" => "Nois",
				"eta_edu_1_noies" => "Noies",
				"eta_edu_1_Total" => "Total",
				"eta_edu_2_nois" => "Nois",
				"eta_edu_2_noies" => "Noies",
				"eta_edu_2_Total" => "Total",
				"eta_edu_3_nois" => "Nois",
				"eta_edu_3_noies" => "Noies",
				"eta_edu_3_Total" => "Total",
				"eta_edu_4_nois" => "Nois",
				"eta_edu_4_noies" => "Noies",
				"eta_edu_4_Total" => "Total",
				"eta_edu_5_nois" => "Nois",
				"eta_edu_5_noies" => "Noies",
				"eta_edu_5_Total" => "Total"
			);  
		}
		

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}
		switch($_SESSION['tipo_usuario']){
			case 1:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			case 2:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
				break;
			case 4:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			default:
				break;
		}
		$promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $promotores;
		$arreglo_datos['OPCIONES'] = $opciones;
		$arreglo_datos['DATOS'] = $datos;
	
		$tabla_result = $_obj_interfaz->crearListadoAlumnosAtendidosExportar($arreglo_datos);
		$datos["result_table_centre"]= $tabla_result;
		
		$contenido .= $obj_reportes->abrirFormularioNinosAtendidosExportar($datos);
		$_obj_interfaz->asignarContenido($contenido);

		break;
	case 'informe_ninos_atendidos_centro_escolar'.validarAcceso(array(1, 2, 4)):	
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$contenido = $obj_reportes->abrirFormularioNinosAtendidosCentroEscolar($datos);  		
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'buscar_ninos_atendidos_centro_escolar'.validarAcceso(array(1, 2, 4)):
		if($_SESSION['tipo_usuario'] == 2){
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"pes_period_1_nois" => "Nois",
				"pes_period_1_noies" => "Noies",
				"pes_period_2_nois" => "Nois",
				"pes_period_2_noies" => "Noies",
				"pes_period_3_nois" => "Nois",
				"pes_period_3_noies" => "Noies",
				"pes_period_total_nois" => "Nois",
				"pes_period_total_noies" => "Noies",
				"pes_period_total" => "Total"
			);  
		}
		else{
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"usu_nombre" => "Nom i Cognoms",
				"pes_period_1_nois" => "Nois",
				"pes_period_1_noies" => "Noies",
				"pes_period_2_nois" => "Nois",
				"pes_period_2_noies" => "Noies",
				"pes_period_3_nois" => "Nois",
				"pes_period_3_noies" => "Noies",
				"pes_period_total_nois" => "Nois",
				"pes_period_total_noies" => "Noies",
				"pes_period_total" => "Total"
			);  
		}
		

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		switch($_SESSION['tipo_usuario']){
			case 1:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			case 2:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
				break;
			case 4:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			default:
				break;
		}
		//Devuelve los centros escolares que se veran en el informe
		$promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

		if($promotores && $datos['ano_lectivo']){
			$arreglo_datos['CAMPOS'] = $campos;
			$arreglo_datos['VALORES'] = $promotores;
			$arreglo_datos['OPCIONES'] = $opciones;
			$arreglo_datos['DATOS'] = $datos;

			$tabla_result = $_obj_interfaz->crearListadoAlumnosAtendidosCentroEscolar($arreglo_datos);
			$datos["result_table_centre"]= $tabla_result;
			
			$contenido .= $obj_reportes->abrirFormularioNinosAtendidosCentroEscolar($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		else{
			header('Location: index.php?m=reportes&accion=informe_ninos_atendidos_centro_escolar&msg=REP-4');
		}

		break;
	case 'exportar_ninos_centro_escolar'.validarAcceso(array(1, 2, 4)):
		if($_SESSION['tipo_usuario'] == 2){
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"pes_period_1_nois" => "Nois",
				"pes_period_1_noies" => "Noies",
				"pes_period_2_nois" => "Nois",
				"pes_period_2_noies" => "Noies",
				"pes_period_3_nois" => "Nois",
				"pes_period_3_noies" => "Noies",
				"pes_period_total_nois" => "Nois",
				"pes_period_total_noies" => "Noies",
				"pes_period_total" => "Total"
			);  
		}
		else{
			$campos = array(
				"ces_nombre" => "Centre Educatiu",
				"usu_nombre" => "Nom i Cognoms",
				"pes_period_1_nois" => "Nois",
				"pes_period_1_noies" => "Noies",
				"pes_period_2_nois" => "Nois",
				"pes_period_2_noies" => "Noies",
				"pes_period_3_nois" => "Nois",
				"pes_period_3_noies" => "Noies",
				"pes_period_total_nois" => "Nois",
				"pes_period_total_noies" => "Noies",
				"pes_period_total" => "Total"
			);  
		}
		

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		switch($_SESSION['tipo_usuario']){
			case 1:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			case 2:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
				break;
			case 4:
				$sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre 
						FROM usuarios 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id 
						JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
						WHERE usuario_centro_escolar.usc_ter_id = ".$datos['territorio'].";";
				break;
			default:
				break;
		}
		$promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $promotores;
		$arreglo_datos['OPCIONES'] = $opciones;
		$arreglo_datos['DATOS'] = $datos;

		$tabla_result = $_obj_interfaz->crearListadoAlumnosAtendidosCentroEscolarExportar($arreglo_datos);
		$datos["result_table_centre"]= $tabla_result;
		
		$contenido .= $obj_reportes->abrirFormularioNinosAtendidosCentroEscolarExportar($datos);
		$_obj_interfaz->asignarContenido($contenido);

		break;
	case 'descripcion_estudiantes'.validarAcceso(array(1, 2, 4)):	
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$contenido = $obj_reportes->abrirFormularioDescripcionEstudiantes($datos);  		
		$_obj_interfaz->asignarContenido($contenido);
		break;  
	case 'buscar_estudiantes'.validarAcceso(array(1, 2, 4)):
		if(isset($datos['centro_escolar'])){
	        if($_SESSION['tipo_usuario'] == 2){
    			$sql = "SELECT visita.vis_fecha, motivo_intervencion.min_nombre, intervencion.int_tab_id, 
    					tipo_intervencion.tin_nombre, intervencion_estudiante.ies_observacion
    					FROM visita 
    					JOIN intervencion ON intervencion.int_vis_id = visita.vis_id
    					JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
    					JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
    					JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
    					WHERE visita.vis_fecha BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."'
    					AND visita.vis_ces_id = ".$datos['centro_escolar']." AND 
    					visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
    					intervencion_estudiante.ies_est_id = ".$datos['estudiantes']."
    					ORDER BY visita.vis_id DESC;";
    		}
    		else{
    			$sql = "SELECT visita.vis_fecha, motivo_intervencion.min_nombre, intervencion.int_tab_id, tipo_intervencion.tin_nombre, intervencion_estudiante.ies_observacion
    					FROM visita 
    					JOIN intervencion ON intervencion.int_vis_id = visita.vis_id
    					JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
    					JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
    					JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
    					WHERE visita.vis_fecha BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."'
    					AND visita.vis_ces_id = ".$datos['centro_escolar']." AND 
    					visita.vis_usu_id_promotor = ".$datos['promotor']." AND 
    					intervencion_estudiante.ies_est_id = ".$datos['estudiantes']."
    					ORDER BY visita.vis_id DESC;";
    		}
    		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    		if($resultado){
    			$campos = array(
    				"vis_fecha" => "Data",
    				"min_nombre" => "Motiu D'Intervenció",
    				"tab_nombre" => "Tipus D'Absentisme",
    				"tin_nombre" => "Tipus D'Intervenció",
    				"observaciones" => "Observacions/Acords"
    			); 
    		}
    		else{
    			//header('Location: index.php?m=reportes&accion=horas_trabajadas_centros_escolares&msg=REP-4');
    			echo "<script>window.location.href='index.php?m=reportes&accion=descripcion_estudiantes&msg=REP-4';</script>";
                exit;
    		}
    
    		$arreglo_datos['CAMPOS'] = $campos;
    		$arreglo_datos['VALORES'] = $resultado;
    		$arreglo_datos['OPCIONES'] = $opciones;
    		$arreglo_datos['DATOS'] = $datos;
    
    		$tabla_result = $_obj_interfaz->crearListadoSeguimientoAlumno($arreglo_datos);
    		$datos["result_table_centre"]= $tabla_result;
    		
    		$contenido .= $obj_reportes->abrirFormularioDescripcionEstudiantes($datos);
    		$_obj_interfaz->asignarContenido($contenido);
	    }
		else{
		    echo "<script>window.location.href='index.php?m=reportes&accion=descripcion_estudiantes&msg=REP-4';</script>";
            exit;
		}

		break; 
	case 'buscar_estudiantes_exportar'.validarAcceso(array(1, 2, 4)):
		if($_SESSION['tipo_usuario'] == 2){
			$sql = "SELECT visita.vis_fecha, motivo_intervencion.min_nombre, intervencion.int_tab_id, 
					tipo_intervencion.tin_nombre, intervencion_estudiante.ies_observacion
					FROM visita 
					JOIN intervencion ON intervencion.int_vis_id = visita.vis_id
					JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
					JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
					JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
					WHERE visita.vis_fecha BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."'
					AND visita.vis_ces_id = ".$datos['centro_escolar']." AND 
					visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
					intervencion_estudiante.ies_est_id = ".$datos['estudiantes']."
					ORDER BY visita.vis_id DESC;";
		}
		else{
			$sql = "SELECT visita.vis_fecha, motivo_intervencion.min_nombre, intervencion.int_tab_id, tipo_intervencion.tin_nombre, intervencion_estudiante.ies_observacion
					FROM visita 
					JOIN intervencion ON intervencion.int_vis_id = visita.vis_id
					JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id
					JOIN motivo_intervencion ON motivo_intervencion.min_id = intervencion.int_min_id
					JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id
					WHERE visita.vis_fecha BETWEEN '".$datos['fecha_inicio']."' AND '".$datos['fecha_fin']."'
					AND visita.vis_ces_id = ".$datos['centro_escolar']." AND 
					visita.vis_usu_id_promotor = ".$datos['promotor']." AND 
					intervencion_estudiante.ies_est_id = ".$datos['estudiantes']."
					ORDER BY visita.vis_id DESC;";
		}
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

		if($resultado){
			$campos = array(
				"vis_fecha" => "Data",
				"min_nombre" => "Motiu D'Intervenció",
				"tab_nombre" => "Tipus D'Absentisme",
				"tin_nombre" => "Tipus D'Intervenció",
				"observaciones" => "Observacions/Acords"
			); 
		}
		else{
			header('Location: index.php?m=reportes&accion=descripcion_estudiantes&msg=REP-3');
		}

		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['VALORES'] = $resultado;
		$arreglo_datos['OPCIONES'] = $opciones;
		$arreglo_datos['DATOS'] = $datos;

		$tabla_result = $_obj_interfaz->crearListadoSeguimientoAlumnoExportar($arreglo_datos);
		$datos["result_table_centre"]= $tabla_result;
		
		$contenido .= $obj_reportes->abrirFormularioDescripcionEstudiantesExportar($datos);
		$_obj_interfaz->asignarContenido($contenido);

		break; 
	case 'informe_familia'.validarAcceso(array(1, 2, 4)):	
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El usuario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}

		$contenido = $obj_reportes->abrirFormularioFamilia($datos);  		
		$_obj_interfaz->asignarContenido($contenido);
		break;
	case 'buscar_informe_familia'.validarAcceso(array(1, 2, 4)):
		if($_SESSION['tipo_usuario'] != 2){
			$campos = array(
				"ano_lectivo" => "Nom i Cognoms",
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			);
			$campos_centros = array(
				"ces_nombre" => "Centre Educative",
				"ano_lectivo" => "Nom i Cognoms",
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			);  
		}
		else{
			$campos = array(
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			); 
			$campos_centros = array(
				"ces_nombre" => "Centre Educative",
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			); 
		}

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		switch($_SESSION['tipo_usuario']){
			case 1:
				$sql = "SELECT DISTINCT usuarios.usu_id, familia.fam_id, intervencion.int_pes_id, territorio.ter_id, 
						usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_id, centro_escolar.ces_nombre
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE territorio.ter_id = ".$datos['territorio']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."'
						ORDER BY usuarios.usu_id ASC;";

				$sql_centros = "SELECT DISTINCT centro_escolar.ces_nombre, usuarios.usu_id, usuarios.usu_nombre, 
						usuarios.usu_apellido, centro_escolar.ces_id
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE territorio.ter_id = ".$datos['territorio']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' 
						ORDER BY centro_escolar.ces_nombre ASC;";
				break;
			case 2:
				$sql = "SELECT DISTINCT familia.fam_id, intervencion.int_pes_id, centro_escolar.ces_id, centro_escolar.ces_nombre
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";
				$sql_centros = "SELECT DISTINCT centro_escolar.ces_nombre, usuarios.usu_id, usuarios.usu_nombre, 
						usuarios.usu_apellido, centro_escolar.ces_id
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."'
						ORDER BY centro_escolar.ces_nombre ASC;";
				break;
			case 4:
				$sql = "SELECT DISTINCT usuarios.usu_id, familia.fam_id, intervencion.int_pes_id, territorio.ter_id, 
						usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_id, centro_escolar.ces_nombre
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE territorio.ter_id = ".$datos['territorio']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."'
						ORDER BY usuarios.usu_id ASC;";

				$sql_centros = "SELECT DISTINCT centro_escolar.ces_nombre, usuarios.usu_id, usuarios.usu_nombre, 
						usuarios.usu_apellido, centro_escolar.ces_id
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE territorio.ter_id = ".$datos['territorio']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."' 
						ORDER BY centro_escolar.ces_nombre ASC;";
				break;
			default:
				break;
		}
		//Devuelve las familias que se veran en el informe
		$familias = $_obj_database->obtenerRegistrosAsociativos($sql);
		$familias_centros = $_obj_database->obtenerRegistrosAsociativos($sql_centros);

		if($familias && $datos['ano_lectivo']){
			$arreglo_datos['CAMPOS'] = $campos;
			$arreglo_datos['CAMPOS_CENTRO'] = $campos_centros;
			$arreglo_datos['VALORES'] = $familias;
			$arreglo_datos['VALORES_CENTROS'] = $familias_centros;
			$arreglo_datos['OPCIONES'] = $opciones;
			$arreglo_datos['DATOS'] = $datos;

			$tabla_result = $_obj_interfaz->crearListadoFamilia($arreglo_datos);
			$datos["result_table_centre"]= $tabla_result;
			
			$contenido .= $obj_reportes->abrirFormularioFamilia($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		else{
			//header('Location: index.php?m=reportes&accion=informe_familia&msg=REP-4');
			?>
			<script type="text/javascript"> 
				window.location="index.php?m=reportes&accion=informe_familia&msg=REP-4"; 
			</script> 
			<?php
		}

		break;
	case 'buscar_informe_familia_exportar'.validarAcceso(array(1, 2, 4)):
		if($_SESSION['tipo_usuario'] != 2){
			$campos = array(
				"ano_lectivo" => "Nom i Cognoms",
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			);
			$campos_centros = array(
				"ces_nombre" => "Centre Educative",
				"ano_lectivo" => "Nom i Cognoms",
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			);  
		}
		else{
			$campos = array(
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			); 
			$campos_centros = array(
				"ces_nombre" => "Centre Educative",
				"pes_period_1" => "PRIMER TRIMESTRE",
				"pes_period_2" => "SEGON TRIMESTRE",
				"pes_period_3" => "TERCER TRIMESTRE",
				"pes_period" => "CURS ESCOLAR"
			); 
		}

		if(isset($_SESSION['usu_id'])){
			$usu_id = $_SESSION['usu_id'];
			$opciones['login'] = "usuario_id=%usu_id%";
		} else {
			$usu_id = 0;
		}

		switch($_SESSION['tipo_usuario']){
			case 1:
				$sql = "SELECT DISTINCT usuarios.usu_id, familia.fam_id, intervencion.int_pes_id, 
						usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE territorio.ter_id = ".$datos['territorio']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."'
						ORDER BY usuarios.usu_id ASC;";

				$sql_centros = "SELECT DISTINCT centro_escolar.ces_nombre, usuarios.usu_id, usuarios.usu_nombre, 
						usuarios.usu_apellido, centro_escolar.ces_id
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						WHERE territorio.ter_id = ".$datos['territorio']." ORDER BY centro_escolar.ces_nombre ASC;";
				break;
			case 2:
				$sql = "SELECT DISTINCT familia.fam_id, intervencion.int_pes_id, centro_escolar.ces_id, centro_escolar.ces_nombre
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN usuario_centro_escolar ON usuario_centro_escolar.usc_usu_id_promotor = usuarios.usu_id
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."';";
				$sql_centros = "SELECT DISTINCT centro_escolar.ces_nombre, usuarios.usu_id, usuarios.usu_nombre, 
						usuarios.usu_apellido, centro_escolar.ces_id
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE visita.vis_usu_id_promotor = ".$_SESSION['usu_id']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."'
						ORDER BY centro_escolar.ces_nombre ASC;";
				break;
			case 4:
				$sql = "SELECT DISTINCT usuarios.usu_id, familia.fam_id, intervencion.int_pes_id, 
						usuarios.usu_nombre, usuarios.usu_apellido, centro_escolar.ces_nombre
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE territorio.ter_id = ".$datos['territorio']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."'
						ORDER BY usuarios.usu_id ASC;";

				$sql_centros = "SELECT DISTINCT centro_escolar.ces_nombre, usuarios.usu_id, usuarios.usu_nombre, 
						usuarios.usu_apellido, centro_escolar.ces_id
						FROM intervencion 
						JOIN centro_escolar ON centro_escolar.ces_id = intervencion.int_ces_id 
						JOIN territorio ON territorio.ter_id = centro_escolar.ces_ter_id 
						JOIN tipo_intervencion ON tipo_intervencion.tin_id = intervencion.int_tin_id 
						JOIN intervencion_estudiante ON intervencion_estudiante.ies_int_id = intervencion.int_id 
						JOIN estudiante ON estudiante.est_id = intervencion_estudiante.ies_est_id 
						JOIN familia ON estudiante.est_fam_id = familia.fam_id 
						JOIN visita ON visita.vis_id = intervencion.int_vis_id 
						JOIN usuarios ON usuarios.usu_id = visita.vis_usu_id_promotor 
						JOIN periodos_escolares ON periodos_escolares.pes_id = intervencion.int_pes_id
						WHERE territorio.ter_id = ".$datos['territorio']." AND 
						periodos_escolares.pes_ano_lectivo = '".$datos['ano_lectivo']."'
						ORDER BY centro_escolar.ces_nombre ASC;";
				break;
			default:
				break;
		}
		//Devuelve las familias que se veran en el informe
		$familias = $_obj_database->obtenerRegistrosAsociativos($sql);
		$familias_centros = $_obj_database->obtenerRegistrosAsociativos($sql_centros);

		if($familias && $datos['ano_lectivo']){
			$arreglo_datos['CAMPOS'] = $campos;
			$arreglo_datos['CAMPOS_CENTRO'] = $campos_centros;
			$arreglo_datos['VALORES'] = $familias;
			$arreglo_datos['VALORES_CENTROS'] = $familias_centros;
			$arreglo_datos['OPCIONES'] = $opciones;
			$arreglo_datos['DATOS'] = $datos;

			$tabla_result = $_obj_interfaz->crearListadoFamiliaExportar($arreglo_datos);
			$datos["result_table_centre"]= $tabla_result;
			
			$contenido .= $obj_reportes->abrirFormularioFamiliaExportar($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		else{
			header('Location: index.php?m=reportes&accion=informe_familia&msg=REP-4');
		}

		break;

}
?>
