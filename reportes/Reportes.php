<?php

/**
 * 	Nombre: Reportes.php
 * 	Descripción: Maneja lo referente a las cotizaciones del sistema
 */
class Reportes {

  var $_plantillas = "";
  var $_herramientas = null;

  public function __construct() {
      global $_PATH_SERVIDOR;

      $this->_plantillas = $_PATH_SERVIDOR . "reportes/Plantillas";
      $this->_herramientas = new Herramientas; 
  }

  /** abrirFormularioDescripcionIntervencion
     * parametro: $datos
     * autor : SR - 01/02/2021
     * descripcion: Formulario para obtener reportes
    **/
  public function abrirFormularioDescripcionIntervencion($datos) {
    global $_obj_database;

    $msg_titulo = "Seguiment alumnat (última visita)";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_ver_ultima_intervencion.html");
    if($datos['accion'] != 'descripcion_intervencion1' && $datos['accion'] != 'descripcion_intervencion2' && $datos['accion'] != 'descripcion_intervencion4'){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
      }
  
      $sql = "SELECT DISTINCT ces_nombre FROM centro_escolar WHERE ces_id = ".$datos['centro_escolar'].";";
      $centro_escolar = $_obj_database->obtenerRegistrosAsociativos($sql);

      $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
      $info_centro_escolar .= "<td>" . $centro_escolar[0]['ces_nombre'] . "</td></tr>";
  
      $sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, visita.vis_fecha
              FROM usuarios
              JOIN visita ON visita.vis_usu_id_promotor = usuarios.usu_id
              WHERE visita.vis_id = ".$datos['visita'].";";
      $visita = $_obj_database->obtenerRegistrosAsociativos($sql);

      $promotor = '<tr><th>Nombre I Cognoms Promotor:</th>';
      $promotor .= "<td>" . $visita[0]['usu_nombre'] . " " . $visita[0]['usu_apellido'] . "</td></tr>";
      $visita_fecha = "<tr><th>Data D'Intervenció:</th>";
      $visita_fecha .= "<td>" . $visita[0]['vis_fecha'] . "</td></tr><tr></tr>";
  
      Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
      Interfaz::asignarToken("promotor", $promotor, $contenido);
      Interfaz::asignarToken("fecha_visita", $visita_fecha, $contenido);

    }

    switch($_SESSION['tipo_usuario']){
      case 1:
        $sql = "SELECT * FROM servicio_territorial";
        $usuario = 1;
        break;
      case 2:
        $usuario = 0;
        break;
      case 4:
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre 
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
        $usuario = 1;
        break;
    }
    if($usuario == 1){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">Servicio Territorial:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] == $servicio_territorial['ste_id']){
          $select_servicio_territorial .= '<option selected value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';

          $select_territorio = '<label for="territorio">TERRITORI:</label>';
          if($_SESSION['tipo_usuario'] != 4){
            $sql = "SELECT DISTINCT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial'].";";
          }
          else{
            $sql = "SELECT DISTINCT territorio.ter_id, territorio.ter_nombre  
                  FROM territorio 
                  JOIN usuario_territorio ON usuario_territorio.ust_ter_id = territorio.ter_id
                  WHERE ter_ste_id = ".$datos['servicio_territorial']." AND 
                  usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
          }
          
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';

          $sql = "SELECT centro_escolar.ces_id, centro_escolar.ces_nombre 
                  FROM centro_escolar 
                  WHERE centro_escolar.ces_ter_id = ".$datos['territorio'].";";
          $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_centros_escolares = '<label for="centro-escolar">Centro Escolar:</label>';
          $select_centros_escolares .= "<select class='form-select' name='centro_escolar' id='centro_escolar' required><option selected value=''>Escollir...</option>";
          foreach($resultado_promotores as $centro_escolar){
            if($datos['centro_escolar'] == $centro_escolar['ces_id']){
              $select_centros_escolares .= '<option selected value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
            }
            else{
              $select_centros_escolares .= '<option value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
            }
            
          }
          $select_centros_escolares .= "</select>";
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }
    else{
      $sql = "SELECT centro_escolar.ces_id, centro_escolar.ces_nombre FROM usuario_centro_escolar 
              JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id
              WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_centros_escolares = '<label for="centro-escolar">Centro Escolar:</label>';
      $select_centros_escolares .= "<select class='form-select' name='centro_escolar' id='centro_escolar' required><option selected value=''>Escollir...</option>";
      foreach($resultado_promotores as $centro_escolar){
        if($datos['centro_escolar'] == $centro_escolar['ces_id']){
          $select_centros_escolares .= '<option selected value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
        }
        else{
          $select_centros_escolares .= '<option value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
        }
      }
      $select_centros_escolares .= "</select>";
      
    }

    $table_results_informe_centre = $datos['result_table_centre'];
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
    Interfaz::asignarToken("usuario", $_SESSION['usu_id'], $contenido);
    Interfaz::asignarToken("select_centros_escolares_busqueda", $select_centros_escolares, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);

    return $contenido;
  }

  /** abrirFormularioDescripcionIntervencionExportar
     * parametro: $datos
     * autor : SR - 01/02/2021
     * descripcion: Formulario para obtener reportes y exportar
    **/
    public function abrirFormularioDescripcionIntervencionExportar($datos) {
      global $_obj_database;
  
      $msg_titulo = "Seguiment alumnat (última visita)";
  
      if(isset($datos['mensaje'])){
        $mensaje = $datos['mensaje'];
      }
      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/exportar_intervencion.html");
  
      if($datos['accion'] != 'descripcion_intervencion1' && $datos['accion'] != 'descripcion_intervencion2' && $datos['accion'] != 'descripcion_intervencion4'){
        if($_SESSION['tipo_usuario'] != 2){
          $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
          $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
    
          $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
          $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);
    
          $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
          $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
          $info_territorio = "<tr><th>Territori: </th>";
          $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
    
          Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
          Interfaz::asignarToken("territorio", $info_territorio, $contenido);
        }
    
        $sql = "SELECT DISTINCT ces_nombre FROM centro_escolar WHERE ces_id = ".$datos['centro_escolar'].";";
        $centro_escolar = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
        $info_centro_escolar .= "<td>" . $centro_escolar[0]['ces_nombre'] . "</td></tr>";
    
        $sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido, visita.vis_fecha
                FROM usuarios
                JOIN visita ON visita.vis_usu_id_promotor = usuarios.usu_id
                WHERE visita.vis_id = ".$datos['visita'].";";
        $visita = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $promotor = '<tr><th>Nombre I Cognoms Promotor:</th>';
        $promotor .= "<td>" . $visita[0]['usu_nombre'] . " " . $visita[0]['usu_apellido'] . "</td></tr>";
        $visita_fecha = "<tr><th>Data D'Intervenció:</th>";
        $visita_fecha .= "<td>" . $visita[0]['vis_fecha'] . "</td></tr><tr></tr>";
    
        Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
        Interfaz::asignarToken("promotor", $promotor, $contenido);
        Interfaz::asignarToken("fecha_visita", $visita_fecha, $contenido);
  
        //Botones 
        if($datos['accion'] == 'buscar_intervencion1' && $datos['accion'] == 'buscar_intervencion2' && $datos['accion'] == 'buscar_intervencion4'){
          $botones = '<a href="index.php?m=reportes&accion=exportar_intervencion" class="btn btn-warning">Generar Exportació</a>';
        }
        else{
          $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
        }
        
      }
      Interfaz::asignarToken("botones", $botones, $contenido);
  
      switch($_SESSION['tipo_usuario']){
        case 1:
          $sql = "SELECT * FROM servicio_territorial";
          $usuario = 1;
          break;
        case 2:
          $usuario = 0;
          break;
        case 4:
          $sql = "SELECT servicio_territorial.ste_id, servicio_territorial.ste_nombre 
                  FROM territorio 
                  JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                  JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                  WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
          $usuario = 1;
          break;
      }
      if($usuario == 1){
        $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $select_servicio_territorial = '<label for="servicio_territorial">Servicio Territorial:</label>';
        $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
        foreach($resultado_promotores as $servicio_territorial){
          if($datos['servicio_territorial'] == $servicio_territorial['ste_id']){
            $select_servicio_territorial .= '<option selected value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
  
            $select_territorio = '<label for="territorio">TERRITORI:</label>';
            $sql = "SELECT ter_id, ter_nombre  
                    FROM territorio 
                    WHERE ter_ste_id = ".$datos['servicio_territorial']." ;";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
  
            $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
            foreach($resultado as $key => $ter){
              if($ter['ter_id'] == $datos['territorio']){
                $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
              }
              else{
                $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
              }
            }
            $select_territorio .= '</select>';
  
            $sql = "SELECT centro_escolar.ces_id, centro_escolar.ces_nombre 
                    FROM usuario_centro_escolar 
                    JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id 
                    JOIN territorio ON territorio.ter_id = usuario_centro_escolar.usc_ter_id;";
            $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);
  
            $select_centros_escolares = '<label for="centro-escolar">Centro Escolar:</label>';
            $select_centros_escolares .= "<select class='form-select' name='centro_escolar' id='centro_escolar' required><option selected value=''>Escollir...</option>";
            foreach($resultado_promotores as $centro_escolar){
              if($datos['centro_escolar'] == $centro_escolar['ces_id']){
                $select_centros_escolares .= '<option selected value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
              }
              else{
                $select_centros_escolares .= '<option value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
              }
              
            }
            $select_centros_escolares .= "</select>";
          }
          else{
            $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
          }
        }
        $select_servicio_territorial .= "</select>";
      }
      else{
        $sql = "SELECT centro_escolar.ces_id, centro_escolar.ces_nombre FROM usuario_centro_escolar 
                JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id
                WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
        $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $select_centros_escolares = '<label for="centro-escolar">Centro Escolar:</label>';
        $select_centros_escolares .= "<select class='form-select' name='centro_escolar' id='centro_escolar' required><option selected value=''>Escollir...</option>";
        foreach($resultado_promotores as $centro_escolar){
          if($datos['centro_escolar'] == $centro_escolar['ces_id']){
            $select_centros_escolares .= '<option selected value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
          }
          else{
            $select_centros_escolares .= '<option value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
          }
        }
        $select_centros_escolares .= "</select>";
        
      }
  
      $table_results_informe_centre = $datos['result_table_centre'];
      Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
      
      Interfaz::asignarToken("select_centros_escolares_busqueda", $select_centros_escolares, $contenido);
      Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
      Interfaz::asignarToken("mensaje", $mensaje, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
  
      return $contenido;
    }

  /** abrirFormularioHorasTrabajadasCentrosEscolares
   * parametro: $datos
   * autor : SA - 12/02/2021
   * descripcion: Formulario para obtener reportes de horas trabajadas en centros escolares
  **/
  public function abrirFormularioHorasTrabajadasCentrosEscolares($datos) {
    global $_obj_database;

    $msg_titulo = "Hores treballades en Centres Escolars";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_horas_trabajadas_centros_escolares.html");

    if ($_SESSION['tipo_usuario'] == 1) {
      $sql = "SELECT * FROM servicio_territorial";
    }
    else{
      if ($_SESSION['tipo_usuario'] == 4) {
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre 
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
      }
    }

    if($_SESSION['tipo_usuario'] == 1 || $_SESSION['tipo_usuario'] == 4){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">SERVEI TERRITORIAL:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] && $datos['servicio_territorial'] == $servicio_territorial['ste_id']){
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'" selected>'.$servicio_territorial['ste_nombre'].'</option>';

          $select_territorio = '<label for="territorio">TERRITORI:</label>';
          $sql = "SELECT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial']." ;";
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }
    else{
      if($_SESSION['tipo_usuario'] == 2){
        
        $sql = "SELECT centro_escolar.ces_id, centro_escolar.ces_nombre FROM usuario_centro_escolar 
        JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id
        WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
        $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

        $select_centros_escolares = '<label for="centro-escolar">Centro Escolar:</label>';
        $select_centros_escolares .= "<select class='form-select' name='centro_escolar' id='centro_escolar' required><option selected value=''>Escollir...</option>";
        foreach($resultado_promotores as $centro_escolar){
          $select_centros_escolares .= '<option value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
        }
        $select_centros_escolares .= "</select>";
      }
    }

    if($datos['fecha_inicio']){
      $fecha_inicio = '<input type="date" required id="fecha_inicio" value="'.$datos['fecha_inicio'].'" class="form-control" name="fecha_inicio">';
      $fecha_fin = '<input type="date" required id="fecha_fin" value="'.$datos['fecha_fin'].'" class="form-control" name="fecha_fin">';
    }
    else{
      $fecha_inicio = '<input type="date" required id="fecha_inicio" class="form-control" name="fecha_inicio">';
      $fecha_fin = '<input type="date" required id="fecha_fin" class="form-control" name="fecha_fin">';
    }

    $table_results_informe_centre = $datos['result_table_centre'];

    if($datos['accion'] != "horas_trabajadas_centros_escolares1" && $datos['accion'] != "horas_trabajadas_centros_escolares2" && $datos['accion'] != "horas_trabajadas_centros_escolares4"){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $servicio_territorial = "<h5>SERVEI TERRITORIAL: " . $servicio_territorial[0]['ste_nombre'] . "</h5>";
        $territorio = "<h5>TERRITORI: " . $territorio[0]['ter_nombre'] . "</h5>";
  
        Interfaz::asignarToken("servicio_territorial", $servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $territorio, $contenido);
      }
      
      
      $fechas = $datos['fecha_inicio'] . " fins " . $datos['fecha_fin'];
      $fechas = "<h5>DATES: " . $fechas . "</h5>";
    }

    Interfaz::asignarToken("fechas", $fechas, $contenido);
    Interfaz::asignarToken("fecha_inicio", $fecha_inicio, $contenido);
    Interfaz::asignarToken("fecha_fin", $fecha_fin, $contenido);
    Interfaz::asignarToken("select_centros_escolares", $select_centros_escolares, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
        
    return $contenido;
  }

  /** abrirFormularioNinosAtendidos
   * parametro: $datos
   * autor : SA - 19/02/2021
   * descripcion: Formulario para obtener reportes de ninos atendidos por periodos escolares
  **/
  public function abrirFormularioNinosAtendidos($datos) {
    global $_obj_database;

    $msg_titulo = "Alumnat atès pel promotor/a escolar per gènere i per etapa educativa ";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_ninos_atendidos.html");

    switch($_SESSION['tipo_usuario']){
      case 1:
        $sql = "SELECT * FROM servicio_territorial";
        $sql_sentencia = 1;
        break;
      case 2:
        $sql_sentencia = 0;
        break;
      case 4:
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
        $sql_sentencia = 1;
        break;
      default:
        break;
    }

    if($sql_sentencia == 1){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">SERVEI TERRITORIAL:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] && $servicio_territorial['ste_id'] == $datos['servicio_territorial']){

          $select_territorio = '<label for="territorio">TERRITORI:</label>';

          switch($_SESSION['tipo_usuario']){
            case 1:
              $sql = "SELECT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial']." ;";
              break;
            case 4:
              $sql = "SELECT territorio.ter_id, territorio.ter_nombre  
                  FROM territorio 
                  JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                  WHERE territorio.ter_ste_id = ".$datos['servicio_territorial']." AND 
                  usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
              break;
            default:
              break;
          }
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';

          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'" selected>'.$servicio_territorial['ste_nombre'].'</option>';
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }

    $sql = "SELECT DISTINCT * FROM periodos_escolares ORDER BY pes_id DESC";
    $periodos = $_obj_database->obtenerRegistrosAsociativos($sql);

    $select_ano_lectivo = '<label for="ano_lectivo">CURS ESCOLAR:</label>';
    $select_ano_lectivo .= "<select name='ano_lectivo' id='ano_lectivo' class='form-select' required><option selected='true' value=''>Escollir...</option>";
      
    for($i = 0; $i < count($periodos); $i = $i+3){
      if($datos['ano_lectivo'] && $periodos[$i]['pes_ano_lectivo'] == $datos['ano_lectivo']){
        $select_ano_lectivo .= '<option value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'" selected>'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
      else{
        $select_ano_lectivo .= '<option value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
    }
    $select_ano_lectivo .= "</select>";


    //colocar informacion de los filtros en la busqueda
    $table_results_informe_centre = $datos['result_table_centre'];
    if($datos['accion'] != "informe_ninos_atendidos1" && $datos['accion'] != "informe_ninos_atendidos2" && $datos['accion'] != "informe_ninos_atendidos4"){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);

        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
      }
      $info_curso_escolar = "<tr><th>CURS ESCOLAR: </th>";
      $info_curso_escolar .= "<td>" . $datos['ano_lectivo'] . "</td></tr>";

      Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);

      //Botones 
      if($datos['accion'] == 'buscar_ninos_atendidos1' || $datos['accion'] == 'buscar_ninos_atendidos2' || $datos['accion'] == 'buscar_ninos_atendidos4'){
        $botones = '<a href="index.php?m=reportes&accion=buscar_ninos_atendidos_exportar&servicio_territorial='.$datos['servicio_territorial'].'&territorio='.$datos['territorio'].'&ano_lectivo='.$datos['ano_lectivo'].'" class="btn btn-warning">Generar Exportació</a>';
      }
      else{
        $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
      }
    }
    Interfaz::asignarToken("botones", $botones, $contenido);

    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("usuario", $_SESSION['usu_id'], $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);

    return $contenido;
  }

  /** abrirFormularioNinosAtendidosExportar
   * parametro: $datos
   * autor : SA - 24/03/2021
   * descripcion: Formulario para obtener reportes de ninos atendidos por periodos escolares para exportar
  **/
  public function abrirFormularioNinosAtendidosExportar($datos) {
    global $_obj_database;

    $msg_titulo = "Alumnat atès pel promotor/a escolar per gènere i per etapa educativa ";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/ninos_atendidos_exportar.html");

    switch($_SESSION['tipo_usuario']){
      case 1:
        $sql = "SELECT * FROM servicio_territorial";
        $sql_sentencia = 1;
        break;
      case 2:
        $sql_sentencia = 0;
        break;
      case 4:
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
        $sql_sentencia = 1;
        break;
      default:
        break;
    }

    if($sql_sentencia == 1){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">SERVEI TERRITORIAL:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] && $servicio_territorial['ste_id'] == $datos['servicio_territorial']){

          $select_territorio = '<label for="territorio">TERRITORI:</label>';
          $sql = "SELECT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial']." ;";
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';

          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'" selected>'.$servicio_territorial['ste_nombre'].'</option>';
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }

    $sql = "SELECT DISTINCT * FROM periodos_escolares";
    $periodos = $_obj_database->obtenerRegistrosAsociativos($sql);

    $select_ano_lectivo = '<label for="ano_lectivo">CURS ESCOLAR:</label>';
    $select_ano_lectivo .= "<select name='ano_lectivo' id='ano_lectivo' class='form-select' required><option selected='true' value=''>Escollir...</option>";
      
    for($i = 0; $i < count($periodos); $i = $i+3){
      if($datos['ano_lectivo'] && $periodos[$i]['pes_ano_lectivo'] == $datos['ano_lectivo']){
        $select_ano_lectivo .= '<option value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'" selected>'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
      else{
        $select_ano_lectivo .= '<option value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
    }
    $select_ano_lectivo .= "</select>";


    //colocar informacion de los filtros en la busqueda
    $table_results_informe_centre = $datos['result_table_centre'];
    if($datos['accion'] != "informe_ninos_atendidos1" && $datos['accion'] != "informe_ninos_atendidos2" && $datos['accion'] != "informe_ninos_atendidos4"){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);

        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
      }
      $info_curso_escolar = "<tr><th>CURS ESCOLAR: </th>";
      $info_curso_escolar .= "<td>" . $datos['ano_lectivo'] . "</td></tr>";

      Interfaz::asignarToken("curso_escolar", $info_curso_escolar, $contenido);
    }

    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);

    return $contenido;
  }

  /** abrirFormularioNinosAtendidosCentroEscolar
   * parametro: $datos
   * autor : SR - 09/03/2021
   * descripcion: Formulario para obtener reportes de ninos atendidos por centro escolares
  **/
  public function abrirFormularioNinosAtendidosCentroEscolar($datos) {
    global $_obj_database;

    $msg_titulo = "Alumnat atés pel promotor/a als centres educatius durant el curs escolar";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_ninos_atendidos_centro_escolar.html");

    switch($_SESSION['tipo_usuario']){
      case 1:
        $sql = "SELECT * FROM servicio_territorial";
        $sql_sentencia = 1;
        break;
      case 2:
        $sql_sentencia = 0;
        break;
      case 4:
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
        $sql_sentencia = 1;
        break;
      default:
        break;
    }
    
    if($sql_sentencia == 1){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">SERVEI TERRITORIAL:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] && $servicio_territorial['ste_id'] == $datos['servicio_territorial']){
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'" selected>'.$servicio_territorial['ste_nombre'].'</option>';

          $select_territorio = '<label for="territorio">TERRITORI:</label>';
          $sql = "SELECT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial']." ;";
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }

    $sql = "SELECT DISTINCT * FROM periodos_escolares ORDER BY pes_id DESC";
    $periodos = $_obj_database->obtenerRegistrosAsociativos($sql);

    $select_ano_lectivo = '<label for="ano_lectivo">CURS ESCOLAR:</label>';
    $select_ano_lectivo .= "<select name='ano_lectivo' id='ano_lectivo' class='form-select' required><option selected='true' value=''>Escollir...</option>";
      
    for($i = 0; $i < count($periodos); $i = $i+3){
      if($datos['ano_lectivo'] && $periodos[$i]['pes_ano_lectivo'] == $datos['ano_lectivo']){
        $select_ano_lectivo .= '<option selected value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
      else{
        $select_ano_lectivo .= '<option value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
    }
    $select_ano_lectivo .= "</select>";

    //colocar informacion de los filtros en la busqueda
    if($datos['accion'] != 'informe_ninos_atendidos_centro_escolar1' && $datos['accion'] != 'informe_ninos_atendidos_centro_escolar2' && $datos['accion'] != 'informe_ninos_atendidos_centro_escolar4'){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
      }

      $info_centro_escolar = "<tr><th>Curs Escolar: </th>";
      $info_centro_escolar .= "<td>" . $datos['ano_lectivo'] . "</td></tr>";
      
      Interfaz::asignarToken("curso_escolar", $info_centro_escolar, $contenido);

      //Botones 
      if($datos['accion'] == 'buscar_ninos_atendidos_centro_escolar1' || $datos['accion'] == 'buscar_ninos_atendidos_centro_escolar2' || $datos['accion'] == 'buscar_ninos_atendidos_centro_escolar4'){
        $botones = '<a href="index.php?m=reportes&accion=exportar_ninos_centro_escolar&servicio_territorial='.$datos['servicio_territorial'].'&territorio='.$datos['territorio'].'&ano_lectivo='.$datos['ano_lectivo'].'" class="btn btn-warning">Generar Exportació</a>';
      }
      else{
        $botones = '<button onclick="ExportExcel()" id="boton_exportar_excel" class="btn btn-primary text-right">Exportar a Excel</button>';
      }
    }
    Interfaz::asignarToken("botones", $botones, $contenido);
    

    $table_results_informe_centre = $datos['result_table_centre'];
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);

    Interfaz::asignarToken("select_centros_escolares", $select_centros_escolares, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
    Interfaz::asignarToken("usuario", $_SESSION['usu_id'], $contenido);

    return $contenido;
  }

  /** abrirFormularioNinosAtendidosCentroEscolarExportar
   * parametro: $datos
   * autor : SR - 24/03/2021
   * descripcion: Formulario para obtener reportes de ninos atendidos por centro escolares para exportar
  **/
  public function abrirFormularioNinosAtendidosCentroEscolarExportar($datos) {
    global $_obj_database;

    $msg_titulo = "Alumnat atés pel promotor/a als centres educatius durant el curs escolar";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/ninos_atendidos_centro_escolar_exportar.html");

    switch($_SESSION['tipo_usuario']){
      case 1:
        $sql = "SELECT * FROM servicio_territorial";
        $sql_sentencia = 1;
        break;
      case 2:
        $sql_sentencia = 0;
        break;
      case 4:
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
        $sql_sentencia = 1;
        break;
      default:
        break;
    }
    
    if($sql_sentencia == 1){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">SERVEI TERRITORIAL:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] && $servicio_territorial['ste_id'] == $datos['servicio_territorial']){
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'" selected>'.$servicio_territorial['ste_nombre'].'</option>';

          $select_territorio = '<label for="territorio">TERRITORI:</label>';
          $sql = "SELECT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial']." ;";
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }

    $sql = "SELECT DISTINCT * FROM periodos_escolares";
    $periodos = $_obj_database->obtenerRegistrosAsociativos($sql);

    $select_ano_lectivo = '<label for="ano_lectivo">CURS ESCOLAR:</label>';
    $select_ano_lectivo .= "<select name='ano_lectivo' id='ano_lectivo' class='form-select' required><option selected='true' value=''>Escollir...</option>";
      
    for($i = 0; $i < count($periodos); $i = $i+3){
      if($datos['ano_lectivo'] && $periodos[$i]['pes_ano_lectivo'] == $datos['ano_lectivo']){
        $select_ano_lectivo .= '<option selected value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
      else{
        $select_ano_lectivo .= '<option value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
    }
    $select_ano_lectivo .= "</select>";

    //colocar informacion de los filtros en la busqueda
    if($datos['accion'] != 'informe_ninos_atendidos_centro_escolar1' && $datos['accion'] != 'informe_ninos_atendidos_centro_escolar2' && $datos['accion'] != 'informe_ninos_atendidos_centro_escolar4'){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
      }

      $info_centro_escolar = "<tr><th>Curs Escolar: </th>";
      $info_centro_escolar .= "<td>" . $datos['ano_lectivo'] . "</td></tr>";
      
      Interfaz::asignarToken("curso_escolar", $info_centro_escolar, $contenido);
    }

    $table_results_informe_centre = $datos['result_table_centre'];
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);

    Interfaz::asignarToken("select_centros_escolares", $select_centros_escolares, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);

    return $contenido;
  }

  /** abrirFormularioDescripcionEstudiantes
   * parametro: $datos
   * autor : SA - 30/04/2021
   * descripcion: Formulario para obtener reportes de estudiantes
  **/
  public function abrirFormularioDescripcionEstudiantes($datos) {
    global $_obj_database, $_obj_interfaz;

    $msg_titulo = "Seguiment de l'alumne/a";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_ver_seguimiento_estudiante.html");

    if($datos['accion'] != 'descripcion_estudiantes1' && $datos['accion'] != 'descripcion_estudiantes2' && $datos['accion'] != 'descripcion_estudiantes4'){
      //Informacion de la Consulta
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";

        $sql = "SELECT DISTINCT usu_nombre, usu_apellido
              FROM usuarios WHERE usu_id = ".$datos['promotor'].";";
        $usuario = $_obj_database->obtenerRegistrosAsociativos($sql);

        $promotor = '<tr><th>Nombre I Cognoms Promotor:</th>';
        $promotor .= "<td>" . $usuario[0]['usu_nombre'] . " " . $usuario[0]['usu_apellido'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
        Interfaz::asignarToken("promotor", $promotor, $contenido);
      }
  
      $sql = "SELECT DISTINCT ces_nombre FROM centro_escolar WHERE ces_id = ".$datos['centro_escolar'].";";
      $centro_escolar = $_obj_database->obtenerRegistrosAsociativos($sql);

      $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
      $info_centro_escolar .= "<td>" . $centro_escolar[0]['ces_nombre'] . "</td></tr>";

      $sql = "SELECT DISTINCT est_id, est_idalu, est_nombre
              FROM estudiante WHERE est_id = ".$datos['estudiantes'].";";
      $alumno = $_obj_database->obtenerRegistrosAsociativos($sql);
      $alumn_nombre = $_obj_interfaz->encrypt_decrypt('decrypt', $alumno[0]['est_nombre']);
      $estudiantes = '<tr><th>Alumne:</th>';
      $estudiantes .= "<td>" . $alumno[0]['est_idalu'] . " - " . $alumn_nombre . "</td></tr>";

      $visita_fecha = "<tr><th>Rang de Temps:</th>";
      $visita_fecha .= "<td>" . $datos['fecha_inicio'] . " / ". $datos['fecha_fin'] ."</td></tr><tr></tr>";
  
      Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
      Interfaz::asignarToken("estudiantes", $estudiantes, $contenido);
      Interfaz::asignarToken("fecha_visita", $visita_fecha, $contenido);

      //Botones 
      if($datos['accion'] == 'buscar_estudiantes1' || $datos['accion'] == 'buscar_estudiantes2' || $datos['accion'] == 'buscar_estudiantes4'){
        $botones = '<a href="index.php?m=reportes&accion=buscar_estudiantes_exportar&servicio_territorial='.$datos['servicio_territorial'].'&territorio='.$datos['territorio'].'&promotor='.$datos['promotor'].'&centro_escolar='.$datos['centro_escolar'].'&estudiantes='.$datos['estudiantes'].'&fecha_inicio='.$datos['fecha_inicio'].'&fecha_fin='.$datos['fecha_fin'].'" class="btn btn-warning">Generar Exportació</a>';
      }
    }
    Interfaz::asignarToken("botones", $botones, $contenido);

    switch($_SESSION['tipo_usuario']){
      case 1:
        $sql = "SELECT * FROM servicio_territorial";
        $usuario = 1;
        break;
      case 2:
        $usuario = 0;
        break;
      case 4:
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre 
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
        $usuario = 1;
        break;
    }

    if($usuario == 1){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">Servicio Territorial:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] == $servicio_territorial['ste_id']){
          $select_servicio_territorial .= '<option selected value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';

          $select_territorio = '<label for="territorio">TERRITORI:</label>';
          $sql = "SELECT DISTINCT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial'].";";
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';

          //PROMOTORES
          $sql = "SELECT DISTINCT usuarios.usu_id, usuarios.usu_nombre, usuarios.usu_apellido 
                  FROM usuario_centro_escolar 
                  JOIN territorio ON territorio.ter_id = usuario_centro_escolar.usc_ter_id 
                  JOIN usuarios ON usuarios.usu_id = usuario_centro_escolar.usc_usu_id_promotor 
                  WHERE territorio.ter_id = ".$datos['territorio'].";";
          $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_promotor = '<label for="promotor">NOM I COGNOMS PROMOTOR:</label>';
          $select_promotor .= "<select class='form-select' name='promotor' id='promotor' required>
                                        <option selected value=''>Escollir...</option>";
          foreach($resultado_promotores as $promotor){
            if($datos['promotor'] == $promotor['usu_id']){
              $select_promotor .= '<option selected value="'.$promotor['usu_id'].'" id="'.$promotor['usu_id'].'">'.$promotor['usu_nombre']. ' ' . $promotor['usu_apellido'] . '</option>';
            }
            else{
              $select_promotor .= '<option value="'.$promotor['usu_id'].'" id="'.$promotor['usu_id'].'">'.$promotor['usu_nombre']. ' ' . $promotor['usu_apellido'].'</option>';
            }
            
          }
          $select_promotor .= "</select>";

          //CENTROS ESCOLARES
          $sql = "SELECT centro_escolar.ces_id, centro_escolar.ces_nombre 
                  FROM centro_escolar 
                  WHERE centro_escolar.ces_ter_id = ".$datos['territorio'].";";
          $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_centros_escolares = '<label for="centro-escolar">Centro Escolar:</label>';
          $select_centros_escolares .= "<select class='form-select' name='centro_escolar' id='centro_escolar' required><option selected value=''>Escollir...</option>";
          foreach($resultado_promotores as $centro_escolar){
            if($datos['centro_escolar'] == $centro_escolar['ces_id']){
              $select_centros_escolares .= '<option selected value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
            }
            else{
              $select_centros_escolares .= '<option value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
            }
            
          }
          $select_centros_escolares .= "</select>";

          //ALUMNOS
          $sql = "SELECT DISTINCT estudiante.est_id, estudiante.est_idalu, estudiante.est_nombre 
                  FROM estudiante 
                  JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                  WHERE centro_escolar.ces_id = ".$datos['centro_escolar']." ;";
          $resultado_alumnos = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_estudiantes = '<label for="promotor">ALUMNE:</label>';
          $select_estudiantes .= "<select class='form-select' name='estudiantes' id='estudiantes' required>
                                        <option selected value=''>Escollir...</option>";
          foreach($resultado_alumnos as $estudiantes){
            $decrypt_name = $_obj_interfaz->encrypt_decrypt('decrypt', $estudiantes['est_nombre']);
            if($datos['estudiantes'] == $estudiantes['est_id']){
              $select_estudiantes .= '<option selected value="'.$estudiantes['est_id'].'" id="'.$estudiantes['est_id'].'">'.$estudiantes['est_idalu']. ' - ' . $decrypt_name . '</option>';
            }
            else{
              $select_estudiantes .= '<option value="'.$estudiantes['est_id'].'" id="'.$estudiantes['est_id'].'">'.$estudiantes['est_idalu']. ' - ' . $decrypt_name .'</option>';
            }
            
          }
          $select_estudiantes .= "</select>";
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }
    else{
      $sql = "SELECT centro_escolar.ces_id, centro_escolar.ces_nombre FROM usuario_centro_escolar 
              JOIN centro_escolar ON centro_escolar.ces_id = usuario_centro_escolar.usc_ces_id
              WHERE usuario_centro_escolar.usc_usu_id_promotor = ".$_SESSION['usu_id'].";";
      $resultado_centro_escolar = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_centros_escolares = '<label for="centro-escolar">Centro Escolar:</label>';
      $select_centros_escolares .= "<select class='form-select' name='centro_escolar' id='centro_escolar' required><option selected value=''>Escollir...</option>";
      foreach($resultado_centro_escolar as $centro_escolar){
        if($datos['centro_escolar'] == $centro_escolar['ces_id']){
          $select_centros_escolares .= '<option selected value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
        }
        else{
          $select_centros_escolares .= '<option value="'.$centro_escolar['ces_id'].'" id="'.$centro_escolar['ces_id'].'">'.$centro_escolar['ces_nombre'].'</option>';
        }
      }
      $select_centros_escolares .= "</select>";

      if($datos['accion'] != "descripcion_estudiantes1" && $datos['accion'] != "descripcion_estudiantes2" && $datos['accion'] != "descripcion_estudiantes4"){
        //ALUMNOS
        $sql = "SELECT DISTINCT estudiante.est_id, estudiante.est_idalu, estudiante.est_nombre 
                FROM estudiante 
                JOIN centro_escolar ON centro_escolar.ces_id = estudiante.est_ces_id 
                WHERE centro_escolar.ces_id = ".$datos['centro_escolar']." ;";
        $resultado_alumnos = $_obj_database->obtenerRegistrosAsociativos($sql);

        $select_estudiantes = '<label for="promotor">ALUMNE:</label>';
        $select_estudiantes .= "<select class='form-select' name='estudiantes' id='estudiantes' required>
                                  <option selected value=''>Escollir...</option>";
        foreach($resultado_alumnos as $estudiantes){
          $decrypt_name = $_obj_interfaz->encrypt_decrypt('decrypt', $estudiantes['est_nombre']);
          if($datos['estudiantes'] == $estudiantes['est_id']){
            $select_estudiantes .= '<option selected value="'.$estudiantes['est_id'].'" id="'.$estudiantes['est_id'].'">'.$estudiantes['est_idalu']. ' - ' . $decrypt_name . '</option>';
          }
          else{
            $select_estudiantes .= '<option value="'.$estudiantes['est_id'].'" id="'.$estudiantes['est_id'].'">'.$estudiantes['est_idalu']. ' - ' . $decrypt_name .'</option>';
          }

        }
        $select_estudiantes .= "</select>";
      }
    }

    if($datos['fecha_inicio']){
      $fecha_inicio = '<input type="date" required id="fecha_inicio" value="'.$datos['fecha_inicio'].'" class="form-control" name="fecha_inicio">';
      $fecha_fin = '<input type="date" required id="fecha_fin" value="'.$datos['fecha_fin'].'" class="form-control" name="fecha_fin">';
    }
    else{
      $fecha_inicio = '<input type="date" required id="fecha_inicio" class="form-control" name="fecha_inicio">';
      $fecha_fin = '<input type="date" required id="fecha_fin" class="form-control" name="fecha_fin">';
    }

    $table_results_informe_centre = $datos['result_table_centre'];
    
    Interfaz::asignarToken("fecha_inicio", $fecha_inicio, $contenido);
    Interfaz::asignarToken("fecha_fin", $fecha_fin, $contenido);
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
    Interfaz::asignarToken("usuario", $_SESSION['usu_id'], $contenido);
    Interfaz::asignarToken("tipo_usuario", $_SESSION['tipo_usuario'], $contenido);
    Interfaz::asignarToken("select_centros_escolares_busqueda", $select_centros_escolares, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
    Interfaz::asignarToken("select_promotor", $select_promotor, $contenido);
    Interfaz::asignarToken("select_estudiantes", $select_estudiantes, $contenido);

    return $contenido;
  }

  /** abrirFormularioDescripcionEstudiantesExportar
   * parametro: $datos
   * autor : SA - 01/05/2021
   * descripcion: Formulario para obtener reportes de estudiantes
  **/
  public function abrirFormularioDescripcionEstudiantesExportar($datos) {
    global $_obj_database,$_obj_interfaz, $_obj_interfaz;

    $msg_titulo = "Seguiment de l'alumne/a";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/ver_seguimiento_estudiante_exportar.html");

    if($datos['accion'] != 'descripcion_estudiantes1' && $datos['accion'] != 'descripcion_estudiantes2' && $datos['accion'] != 'descripcion_estudiantes4'){
      //Informacion de la Consulta
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        $sql = "SELECT DISTINCT usu_nombre, usu_apellido
              FROM usuarios WHERE usu_id = ".$datos['promotor'].";";
        $usuario = $_obj_database->obtenerRegistrosAsociativos($sql);

        $promotor = '<tr><th>Nombre I Cognoms Promotor:</th>';
        $promotor .= "<td>" . $usuario[0]['usu_nombre'] . " " . $usuario[0]['usu_apellido'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
        Interfaz::asignarToken("promotor", $promotor, $contenido);
      }
  
      $sql = "SELECT DISTINCT ces_nombre FROM centro_escolar WHERE ces_id = ".$datos['centro_escolar'].";";
      $centro_escolar = $_obj_database->obtenerRegistrosAsociativos($sql);

      $info_centro_escolar = "<tr><th>Centre Educatiu: </th>";
      $info_centro_escolar .= "<td>" . $centro_escolar[0]['ces_nombre'] . "</td></tr>";

      $sql = "SELECT DISTINCT est_id, est_idalu, est_nombre
              FROM estudiante WHERE est_id = ".$datos['estudiantes'].";";
      $alumno = $_obj_database->obtenerRegistrosAsociativos($sql);
      $alumn_name = $_obj_interfaz->encrypt_decrypt('decrypt', $alumno[0]['est_nombre']);
      $estudiantes = '<tr><th>Alumne:</th>';
      $estudiantes .= "<td>" . $alumno[0]['est_idalu'] . " - " . $alumn_name . "</td></tr>";

      $visita_fecha = "<tr><th>Rang de Temps:</th>";
      $visita_fecha .= "<td>" . $datos['fecha_inicio'] . " / ". $datos['fecha_fin'] ."</td></tr><tr></tr>";
  
      Interfaz::asignarToken("centro_escolar", $info_centro_escolar, $contenido);
      Interfaz::asignarToken("promotor", $promotor, $contenido);
      Interfaz::asignarToken("estudiantes", $estudiantes, $contenido);
      Interfaz::asignarToken("fecha_visita", $visita_fecha, $contenido);
    }
    $table_results_informe_centre = $datos['result_table_centre'];
    
    Interfaz::asignarToken("fecha_inicio", $fecha_inicio, $contenido);
    Interfaz::asignarToken("fecha_fin", $fecha_fin, $contenido);
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);
    Interfaz::asignarToken("usuario", $_SESSION['usu_id'], $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);

    return $contenido;
  }

   /** abrirFormularioFamilia
   * parametro: $datos
   * autor : SA - 07/05/2021
   * descripcion: Formulario para obtener reportes las familias por años lectivos
  **/
  public function abrirFormularioFamilia($datos) {
    global $_obj_database;

    $msg_titulo = "Famílies ateses pel promotor/a als centres educatius durant el curs escolar";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informe_familias.html");

    switch($_SESSION['tipo_usuario']){
      case 1:
        $sql = "SELECT * FROM servicio_territorial";
        $sql_sentencia = 1;
        break;
      case 2:
        $sql_sentencia = 0;
        break;
      case 4:
        $sql = "SELECT DISTINCT servicio_territorial.ste_id, servicio_territorial.ste_nombre
                FROM territorio 
                JOIN usuario_territorio ON territorio.ter_id = usuario_territorio.ust_ter_id 
                JOIN servicio_territorial ON servicio_territorial.ste_id = territorio.ter_ste_id 
                WHERE usuario_territorio.ust_usu_id_coordinador = ".$_SESSION['usu_id'].";";
        $sql_sentencia = 1;
        break;
      default:
        break;
    }
    
    if($sql_sentencia == 1){
      $resultado_promotores = $_obj_database->obtenerRegistrosAsociativos($sql);

      $select_servicio_territorial = '<label for="servicio_territorial">SERVEI TERRITORIAL:</label>';
      $select_servicio_territorial .= "<select name='servicio_territorial' id='servicio_territorial' class='form-select' required><option selected='true' disabled='disabled' value=''>Escollir...</option>";
      foreach($resultado_promotores as $servicio_territorial){
        if($datos['servicio_territorial'] && $servicio_territorial['ste_id'] == $datos['servicio_territorial']){
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'" selected>'.$servicio_territorial['ste_nombre'].'</option>';

          $select_territorio = '<label for="territorio">TERRITORI:</label>';
          $sql = "SELECT ter_id, ter_nombre  
                  FROM territorio 
                  WHERE ter_ste_id = ".$datos['servicio_territorial']." ;";
          $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

          $select_territorio .= '<select class="form-select" name="territorio" id="territorio" required>';
          foreach($resultado as $key => $ter){
            if($ter['ter_id'] == $datos['territorio']){
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'" selected>'.$ter['ter_nombre'].'</option>';
            }
            else{
              $select_territorio .= '<option value="'.$ter['ter_id'].'" id="'.$ter['ter_id'].'">'.$ter['ter_nombre'].'</option>';
            }
          }
          $select_territorio .= '</select>';
        }
        else{
          $select_servicio_territorial .= '<option value="'.$servicio_territorial['ste_id'].'" id="'.$servicio_territorial['ste_id'].'">'.$servicio_territorial['ste_nombre'].'</option>';
        }
      }
      $select_servicio_territorial .= "</select>";
    }

    $sql = "SELECT DISTINCT * FROM periodos_escolares ORDER BY pes_id DESC";
    $periodos = $_obj_database->obtenerRegistrosAsociativos($sql);

    $select_ano_lectivo = '<label for="ano_lectivo">CURS ESCOLAR:</label>';
    $select_ano_lectivo .= "<select name='ano_lectivo' id='ano_lectivo' class='form-select' required><option selected='true' value=''>Escollir...</option>";
      
    for($i = 0; $i < count($periodos); $i = $i+3){
      if($datos['ano_lectivo'] && $periodos[$i]['pes_ano_lectivo'] == $datos['ano_lectivo']){
        $select_ano_lectivo .= '<option selected value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
      else{
        $select_ano_lectivo .= '<option value="'.$periodos[$i]['pes_ano_lectivo'].'" id="'.$periodos[$i]['pes_id'].'">'.$periodos[$i]['pes_ano_lectivo'].'</option>';
      }
    }
    $select_ano_lectivo .= "</select>";

    //colocar informacion de los filtros en la busqueda
    if($datos['accion'] != 'informe_familia1' && $datos['accion'] != 'informe_familia2' && $datos['accion'] != 'informe_familia4'){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
      }

      $info_centro_escolar = "<tr><th>Curs Escolar: </th>";
      $info_centro_escolar .= "<td>" . $datos['ano_lectivo'] . "</td></tr>";
      
      Interfaz::asignarToken("curso_escolar", $info_centro_escolar, $contenido);

      //Botones 
      if($datos['accion'] == 'buscar_informe_familia1' || $datos['accion'] == 'buscar_informe_familia2' || $datos['accion'] == 'buscar_informe_familia4'){
        $botones = '<a href="index.php?m=reportes&accion=buscar_informe_familia_exportar&servicio_territorial='.$datos['servicio_territorial'].'&territorio='.$datos['territorio'].'&ano_lectivo='.$datos['ano_lectivo'].'" class="btn btn-warning">Generar Exportació</a>';
      }
    }
    Interfaz::asignarToken("botones", $botones, $contenido);
    

    $table_results_informe_centre = $datos['result_table_centre'];
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);

    Interfaz::asignarToken("select_centros_escolares", $select_centros_escolares, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
    Interfaz::asignarToken("usuario", $_SESSION['usu_id'], $contenido);

    return $contenido;
  }

   /** abrirFormularioFamiliaExportar
   * parametro: $datos
   * autor : SA - 07/05/2021
   * descripcion: Formulario para obtener reportes las familias por años lectivos exportacion
  **/
  public function abrirFormularioFamiliaExportar($datos) {
    global $_obj_database;

    $msg_titulo = "Famílies ateses pel promotor/a als centres educatius durant el curs escolar";

    if(isset($datos['mensaje'])){
      $mensaje = $datos['mensaje'];
    }

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/informe_familias_exportar.html");

    //colocar informacion de los filtros en la busqueda
    if($datos['accion'] != 'informe_familia1' && $datos['accion'] != 'informe_familia2' && $datos['accion'] != 'informe_familia4'){
      if($_SESSION['tipo_usuario'] != 2){
        $sql = "SELECT DISTINCT ste_nombre FROM servicio_territorial WHERE ste_id = ".$datos['servicio_territorial'].";";
        $servicio_territorial = $_obj_database->obtenerRegistrosAsociativos($sql);
  
        $sql = "SELECT DISTINCT ter_nombre FROM territorio WHERE ter_id = ".$datos['territorio'].";";
        $territorio = $_obj_database->obtenerRegistrosAsociativos($sql);

        $info_servicio_territorial = "<tr><th>Servei Territorial: </th>";
        $info_servicio_territorial .= "<td>" . $servicio_territorial[0]['ste_nombre'] . "</td></tr>";
        $info_territorio = "<tr><th>Territori: </th>";
        $info_territorio .= "<td>" . $territorio[0]['ter_nombre'] . "</td></tr>";
  
        Interfaz::asignarToken("servicio_territorial", $info_servicio_territorial, $contenido);
        Interfaz::asignarToken("territorio", $info_territorio, $contenido);
      }

      $info_centro_escolar = "<tr><th>Curs Escolar: </th>";
      $info_centro_escolar .= "<td>" . $datos['ano_lectivo'] . "</td></tr>";
      
      Interfaz::asignarToken("curso_escolar", $info_centro_escolar, $contenido);
    }
    

    $table_results_informe_centre = $datos['result_table_centre'];
    Interfaz::asignarToken("table_results_informe_centre", $table_results_informe_centre, $contenido);

    Interfaz::asignarToken("select_centros_escolares", $select_centros_escolares, $contenido);
    Interfaz::asignarToken("select_servicio_territorial", $select_servicio_territorial, $contenido);
    Interfaz::asignarToken("select_ano_lectivo", $select_ano_lectivo, $contenido);
    Interfaz::asignarToken("mensaje", $mensaje, $contenido);
    Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
    Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    Interfaz::asignarToken("select_territorio", $select_territorio, $contenido);
    Interfaz::asignarToken("usuario", $_SESSION['usu_id'], $contenido);

    return $contenido;
  }
}

?>
