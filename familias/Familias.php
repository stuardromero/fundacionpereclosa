<?php

class Familias{

    var $_plantillas = "";

    function __construct() {
        global $_PATH_SERVIDOR;
        $this->_plantillas = $_PATH_SERVIDOR."/familias/Plantillas";
    }

    /** abrirFormularioRegistrarfamilias
     * parametro: $datos
     * autor : BDL
     * descripcion: ABRE EL CONTENIDO DE REGISTRO DE FAMILIAS
    **/
	function abrirFormularioRegistrarfamilias($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registrar.html");

        $label_inicio = "Famílies";
        $msg_descripcion = "Registre d'Famílies";
        $breadcump_familias = "Famílies";
        $input_fam_nombre_padre = "Nombre i inicial del pare";
        $input_fam_nombre_madre = "Nombre i inicial de la mare";
        $input_fam_numerosa = "Família nombrosa*";
        $input_fam_monoparental = "Família monoparental*";
        $input_fam_observaciones = "Observacions";
        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        //$select_territorios = $this->obteberSelectFormaTerritorios($datos);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_familias", $breadcump_familias, $contenido);

        Interfaz::asignarToken("input_fam_nombre_padre", $input_fam_nombre_padre, $contenido);
        Interfaz::asignarToken("input_fam_nombre_madre", $input_fam_nombre_madre, $contenido);
        Interfaz::asignarToken("input_fam_numerosa", $input_fam_numerosa, $contenido);
        Interfaz::asignarToken("input_fam_monoparental", $input_fam_monoparental, $contenido);
        Interfaz::asignarToken("input_fam_observaciones", $input_fam_observaciones, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;

    }

    /** registrarfamilias
     * parametro: $datos
     * autor : BDL
     * descripcion: REGISTRO DE DATOS DE FAMILIAS EN LA BD
    **/
    function registrarfamilias($datos) {
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        $datos = Herramientas::trimCamposFormulario($datos);

        $campos = array(   
        "fam_nombre_padre", 	
        "fam_nombre_madre",
        "fam_numerosa",
        "fam_monoparental",
        "fam_observaciones",
        "fam_usu_id_promotor",
        );        
        
        $datos['tabla'] = 'familia';
        $datos['fam_ter_id'] = intval($datos['fam_ter_id']);
        $datos['fam_numerosa'] = intval($datos['fam_numerosa']);
        $datos['fam_monoparental'] = intval($datos['fam_monoparental']);
        $datos['fam_usu_id_promotor'] = intval($datos['fam_usu_id_promotor']);

        $sql = $_obj_database->generarSQLInsertar($datos, $campos);

        $res = $_obj_database->ejecutarSql($sql);

        if($res == 1){
            return 'MFAM-01';
        } else {
            return 'MFAM-04';
        }

    }
     
    /** obteberSelectFormaTerritorios
     * parametro: $datos
     * autor : BDL
     * descripcion: CREAR SELECTOR DE TERRITORIOS
    **/
    function obteberSelectFormaTerritorios($datos, $valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT ter.ter_id, ter.ter_nombre 
        FROM territorio as ter ;"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $fam_ter_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['fam_ter_id'])){
                $fam_ter_id = $valores['fam_ter_id'];
            }
        }
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='fam_ter_id' aria-label='Default select example' disabled>";
            $select .= "<option>No té Territori assignats</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='fam_ter_id' aria-label='Default select example' required>";
            $select .= "<option value='' selected>Seleccionar Territori</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ter_id'] == $fam_ter_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ter_id']."' ".$selected.">".$resultado['ter_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }

    /** listadofamilias
     * parametro: $datos
     * autor : BDL
     * descripcion: CREAR LISTADO DE FAMILIAS
    **/
    function listadofamilias($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_familias.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
        $label_inicio = "Families";
        $msg_descripcion = "Llistat Families";
        $breadcump_Families = "Families";

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_familias_listar", $breadcump_Families, $contenido);

        return $contenido;
    }

    /** Eliminarfamilias
     * parametro: $datos
     * autor : BDL
     * descripcion: ELIMINAR FAMILIAS
    **/
    function Eliminarfamilias($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        if (!$_obj_database->iniciarTransaccion()) {
            //problma al inicar la transaccion
            return -1003;
        }

        $fam_id = $datos['fam_id'];

        $sql = "DELETE FROM familia WHERE fam_id = $fam_id";
        if (!$_obj_database->ejecutarSql($sql)) {
            $_obj_database->cancelarTransaccion();
            return -1003;
        } else {
            $_obj_database->terminarTransaccion();
        }

        return 'MFAM-03';
    }

    /** obtenerDatosFamilia
     * parametro: $datos
     * autor : BDL
     * descripcion: OBTENER DATOS DE UNA FAMILIA
    **/
    function obtenerDatosFamilia($fam_id){
        global $_obj_database;

        $sql = "SELECT * FROM familia
				WHERE fam_id=$fam_id";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    /** abrirFormularioEditarfamilias
     * parametro: $datos
     * autor : BDL
     * descripcion: ABRIR FORMULARIO PARA EDITAR UNA FAMILIA
    **/
    function abrirFormularioEditarfamilias($datos, $valores){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_familias.html");

        $label_inicio = "Famílies";
        $msg_descripcion = "Edita Famílie";
        $breadcump_familias = "Famílies";
        $input_fam_nombre_padre = "Nombre i inicial del pare*";
        $input_fam_nombre_madre = "Nombre i inicial de la mare*";
        $input_fam_numerosa = "Família nombrosa*";
        $input_fam_monoparental = "Família monoparental*";
        $input_fam_observaciones = "Observacions";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $valores = $valores[0];

        $val_fam_nombre_padre = $valores["fam_nombre_padre"];
        $val_fam_nombre_madre = $valores["fam_nombre_madre"];
        $val_fam_numerosa = $valores["fam_numerosa"];
        $val_fam_monoparental = $valores["fam_monoparental"];
        $val_fam_observaciones = $valores["fam_observaciones"];
        $val_fam_id = $valores["fam_id"];

        //$select_territorios = $this->obteberSelectFormaTerritorios($datos, $valores);
        $radio_fam_numerosa = $this->obteberInputRadioFamiliaNumerosa($val_fam_numerosa);
        $radio_fam_monoparental = $this->obteberInputRadioFamiliaMonoparental($val_fam_monoparental);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_familias", $breadcump_familias, $contenido);

        Interfaz::asignarToken("input_fam_nombre_padre", $input_fam_nombre_padre, $contenido);
        Interfaz::asignarToken("val_fam_nombre_padre", $val_fam_nombre_padre, $contenido);
        Interfaz::asignarToken("input_fam_nombre_madre", $input_fam_nombre_madre, $contenido);
        Interfaz::asignarToken("val_fam_nombre_madre", $val_fam_nombre_madre, $contenido);
        Interfaz::asignarToken("input_fam_numerosa", $input_fam_numerosa, $contenido);
        Interfaz::asignarToken("radio_fam_numerosa", $radio_fam_numerosa, $contenido);
        Interfaz::asignarToken("input_fam_monoparental", $input_fam_monoparental, $contenido);
        Interfaz::asignarToken("radio_fam_monoparental", $radio_fam_monoparental, $contenido);
        Interfaz::asignarToken("input_fam_observaciones", $input_fam_observaciones, $contenido);
        Interfaz::asignarToken("val_fam_observaciones", $val_fam_observaciones, $contenido);
        Interfaz::asignarToken("val_fam_id", $val_fam_id, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    /** obteberInputRadioFamiliaNumerosa
     * parametro: $datos
     * autor : BDL
     * descripcion: OBTENER RADIO INPUT DE FAMILIAS NUMEROSAS
    **/
    function obteberInputRadioFamiliaNumerosa($valor){
        
        if($valor == 1){
            $contenido = '
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_numerosa" value="1" id="rad_fam_numerosa" checked required>
                <label class="form-check-label" for="rad_fam_numerosa">Sí</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_numerosa" value="0" id="rad_fam_numerosa">
                <label class="form-check-label" for="rad_fam_numerosa">No</label>
            </div>
            ';
        } else if ($valor == 0) {
            $contenido = '
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_numerosa" value="1" id="rad_fam_numerosa" required>
                <label class="form-check-label" for="rad_fam_numerosa">Sí</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_numerosa" value="0" id="rad_fam_numerosa" checked>
                <label class="form-check-label" for="rad_fam_numerosa">No</label>
            </div>
            ';
        }

        return $contenido;
    }

    /** obteberInputRadioFamiliaMonoparental
     * parametro: $datos
     * autor : BDL
     * descripcion: OBTENER RADIO INPUT DE FAMILIAS MONOPARENTAL
    **/
    function obteberInputRadioFamiliaMonoparental($valor){

        if($valor == 1){
            $contenido = '
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_monoparental" value="1" id="rad_fam_monoparental" checked required>
                <label class="form-check-label" for="rad_fam_monoparental">Sí</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_monoparental" value="0" id="rad_fam_monoparental">
                <label class="form-check-label" for="rad_fam_monoparental">No</label>
            </div>
            ';
        } else if ($valor == 0) {
            $contenido = '
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_monoparental" value="1" id="rad_fam_monoparental" required>
                <label class="form-check-label" for="rad_fam_monoparental">Sí</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="fam_monoparental" value="0" id="rad_fam_monoparental" checked>
                <label class="form-check-label" for="rad_fam_monoparental">No</label>
            </div>
            ';
        }

        return $contenido;
    }


    /** editarfamilias
     * parametro: $datos
     * autor : BDL
     * descripcion: EDITAR DATOS DE UNA FAMILIA DB
    **/
    function editarfamilias($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        $datos = Herramientas::trimCamposFormulario($datos);

        $campos = array(   
            "fam_nombre_padre", 	
            "fam_nombre_madre",
            "fam_numerosa",
            "fam_monoparental",
            "fam_observaciones",
            "fam_usu_id_promotor",
        );        
        
        $datos['tabla'] = 'familia';
        $datos['fam_ter_id'] = intval($datos['fam_ter_id']);
        $datos['fam_numerosa'] = intval($datos['fam_numerosa']);
        $datos['fam_monoparental'] = intval($datos['fam_monoparental']);
        $datos['fam_usu_id_promotor'] = intval($datos['fam_usu_id_promotor']);
        $datos['condicion'] = "fam_id = '" . $datos['fam_id'] . "' ";

        $res = $_obj_database->actualizarDatosTabla($datos, $campos);

        if (!$res) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }

        if($res == 1){
            return 'MFAM-02';
        } else {
            return 'MFAM-05';
        }
    }

}