
<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Includes/Interfaz.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("Familias.php");

$obj_usuarios = new Usuarios();
$obj_familias = new Familias();
$obj_interfaz = new Interfaz();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarJS("formulario.js");

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");
$_obj_interfaz->adicionarFooterJS("formulario.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
    //case 'fm_registrar'.$obj_usuarios->validarAcceso(array("A","P")):
    //case 'fm_registrar'.$obj_usuarios->validarAcceso(array()):7
    case 'fm_registrar'.validarAcceso(array(2)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];  
            $opciones['CADENA'] = 'El Alumne';           
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
		$contenido = $obj_familias->abrirFormularioRegistrarfamilias($datos);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'registrarfamilias'.validarAcceso(array(2)):
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['fam_usu_id_promotor'] = $usu_id;
        $resultado = $obj_familias->registrarfamilias($datos);
        if( $resultado=='MFAM-01' ) 
		{   
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=familias&accion=fm_registrar&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php      
			//header('Location: index.php?m=familias&accion=fm_registrar&tipo_gestion=1&msg=MFAM-01');    
			break;            
		}//Fin de if( $resultado==1 )      
		else {
            $opciones['CADENA'] = "El Alumne";      
			$opciones['ACCION'] = 1;//ingresar      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);  
			$contenido = $obj_familias->abrirFormularioRegistrarfamilias($datos);
			$_obj_interfaz->asignarContenido($contenido);
        }                
        break;    
    case 'listarfamilias'.validarAcceso(array(2)):
        if(isset($datos['msg'])){
            $opciones['ACCION'] = $datos['tipo_gestion'];
            $opciones['CADENA'] = 'El usuario';
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }
        $contenido = $obj_familias->listadofamilias($datos);
		$campos = array(
            "fam_id" => "Codi Família ",
			"fam_nombre_padre" => "Nombre Pare", 
			"fam_nombre_madre" => "Nombre Mare",
            "fam_numerosa" => "Família nombrosa",
            "fam_monoparental" => "Família monoparental",
            "fam_observaciones" => "Observacions",
		);  	
        
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        $opciones['actualizar'] = "usc_usu_id_promotor=$usu_id";
		$opciones['eliminar'] = "usc_usu_id_promotor=$usu_id";
        
		$datos['sql'] = "SELECT fam.fam_id, fam.fam_nombre_padre, fam.fam_nombre_madre, fam.fam_numerosa, 
        fam.fam_monoparental, fam.fam_observaciones
        FROM familia AS fam
        WHERE fam.fam_usu_id_promotor = ".$usu_id."
        ORDER BY fam.fam_id DESC;";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
		$contenido .= $_obj_interfaz->crearListadofamiliaspereclosa($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'fm_editar'.validarAcceso(array(2)):
        $valores = $obj_familias->obtenerDatosFamilia($datos['fam_id']);
        if( !is_array($valores))
		{
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
        $datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_familias->abrirFormularioEditarfamilias($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'editarfamilias'.validarAcceso(array(2)):
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }
        $datos['fam_usu_id_promotor'] = $usu_id;
        $resultado = $obj_familias->editarfamilias($datos);
        if( $resultado=='MFAM-02' ) 
        {         
            ?>
            <script type="text/javascript"> 
                window.location="index.php?m=familias&accion=listarfamilias&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
            </script> 
            <?php
            break;            
        }//Fin de if( $resultado==1 )
        break;
    case 'fm_eliminar'.validarAcceso(array(2)):
        $resultado = $obj_familias->Eliminarfamilias($datos);
        ?>
        <script type="text/javascript"> 
            window.location="index.php?m=familias&accion=listarfamilias&tipo_gestion=1&msg=<?php echo $resultado; ?>"; 
        </script> 
        <?php		
        break;
}

?>