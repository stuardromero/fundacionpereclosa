<?php

require_once($_PATH_SERVIDOR . "Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Includes/Interfaz.php");
require_once($_PATH_SERVIDOR . "Config.php");

require_once($_PATH_SERVIDOR . "Usuarios/Usuarios.php");
require_once("Centros.php");

$obj_usuarios = new Usuarios();
$obj_centros = new centros();
$obj_interfaz = new Interfaz();


$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
    //case 'fm_registrar'.$obj_usuarios->validarAcceso(array("A","P")):
    //case 'fm_registrar'.$obj_usuarios->validarAcceso(array()):7
    case 'fm_registrar'.validarAcceso(array(1)):
        if(isset($datos['msg']))                             
        {                         
            $opciones['ACCION'] = $datos['tipo_gestion'];  
            $opciones['CADENA'] = 'El Alumne';           
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }  
		$contenido = $obj_centros->abrirFormularioRegistrarcentros($datos);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'registrarcentros'.validarAcceso(array(1)):
        $resultado = $obj_centros->registrarcentros($datos);
        if( $resultado=='U-18' ) 
		{                 
			header('Location: index.php?m=centros&accion=fm_registrar&tipo_gestion=1&msg=U-18');    
			break;            
		}//Fin de if( $resultado==1 )      
		else {
            $opciones['CADENA'] = "El Centro";      
			$opciones['ACCION'] = 1;//ingresar      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);  
			$contenido = $obj_centros->abrirFormularioRegistrarcentros($datos);
			$_obj_interfaz->asignarContenido($contenido);
        }                
        //$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'obtenerSelectorTerritorios'.validarAcceso(array(1)):
        $resultado = $obj_centros->obteberSelectFormaTerritorios($datos);
        print_r($resultado);
        exit;
        break;    
    case 'listarcentros'.validarAcceso(array(1)):
        if(isset($datos['msg'])){
            $opciones['ACCION'] = $datos['tipo_gestion'];
            $opciones['CADENA'] = 'El Centro';
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }
        $contenido = $obj_centros->listadocentros($datos);
        $campos = array(
            "ces_id" => "ID",
            "ces_nombre" => "Nombre Centre",
            "ter_nombre" => "Territori", 
            "tce_nombre" => "Tipus de Centre",
            "ces_maxcomplejidad" => "Màxima Complexitat",
            "usu_nombre" => "Director",
            "ces_num_gitanos" => "Total D'Alumnes Susceptibles"
        );
        
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        $opciones['actualizar'] = "ces_usu_id_director=$usu_id&est_idalu=%est_idalu%";
		$opciones['eliminar'] = "ces_usu_id_director=$usu_id&est_idalu=%est_idalu%";
        
		$datos['sql'] = "SELECT ces.ces_id, ces.ces_nombre, ces.ces_direccion, ces.ces_num_gitanos,
        ces.ces_bar ,ter.ter_nombre, tce.tce_nombre, ces.ces_maxcomplejidad, usu.usu_nombre
        FROM centro_escolar AS ces
        INNER JOIN territorio AS ter
        ON ter.ter_id = ces.ces_ter_id
        INNER JOIN tipos_centro_escolar AS tce
        ON tce.tce_id = ces.ces_tce_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = ces.ces_usu_id_director
        WHERE 1 ORDER BY ces.ces_id DESC;";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

		$arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['CAMPOS_EXPORTAR'] = $campos_exportar;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
	
		$contenido .= $_obj_interfaz->crearListadocentros($arreglo_datos);

		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'fm_editar'.validarAcceso(array(1)):
        $valores = $obj_centros->obtenerDatosCentro($datos['ces_id']);
        if( !is_array($valores))
		{
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
        $datos['TIPO_ACCION'] = "editar";
		$contenido = $obj_centros->abrirFormularioEditarcentros($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
        break;
    case 'editarcentros'.validarAcceso(array(1)):
        $resultado = $obj_centros->editarcentros($datos);
        if( $resultado=='CNT-02' ) 
        {                 
            header('Location: index.php?m=centros&accion=listarcentros&tipo_gestion=1&msg='.$resultado);    
            break;            
        }//Fin de if( $resultado==1 )      
        
        break;
    case 'fm_eliminar'.validarAcceso(array(1)):
        $resultado = $obj_centros->Eliminarcentros($datos);
        
        $enlace = "index.php?m=centros&accion=listarcentros&tipo_gestion=3&msg=".$resultado."";
		header('Location: '.$enlace);
		
        break;
    case 'listar_centros_exportar'.validarAcceso(array(1)):
        if(isset($datos['msg'])){
            $opciones['ACCION'] = $datos['tipo_gestion'];
            $opciones['CADENA'] = 'El Centro';
            $datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
        }
        $contenido = $obj_centros->listadocentrosExportar($datos);
        $campos = array(
            "ces_id" => "ID",
            "ces_nombre" => "Nombre Centre",
            "ste_nombre" => "Servei Territorial",
            "ter_nombre" => "Territori", 
            "tce_nombre" => "Tipus de Centre",
            "ces_maxcomplejidad" => "Màxima Complexitat",
            "usu_nombre" => "Director",
            "ces_num_gitanos" => "Total D'Alumnes Susceptibles"
        );  	
        
        if(isset($_SESSION['usu_id'])){
            $usu_id = $_SESSION['usu_id'];
            $opciones['login'] = "usuario_id=%usu_id%";
        } else {
            $usu_id = 0;
        }

        $opciones['actualizar'] = "ces_usu_id_director=$usu_id&est_idalu=%est_idalu%";
        $opciones['eliminar'] = "ces_usu_id_director=$usu_id&est_idalu=%est_idalu%";
        
        $datos['sql'] = "SELECT ces.ces_id, ces.ces_nombre, ces.ces_direccion, ces.ces_num_gitanos,
        ces.ces_bar ,ter.ter_nombre, ste.ste_nombre, tce.tce_nombre, ces.ces_maxcomplejidad, usu.usu_nombre
        FROM centro_escolar AS ces
        INNER JOIN territorio AS ter
        ON ter.ter_id = ces.ces_ter_id
        INNER JOIN tipos_centro_escolar AS tce
        ON tce.tce_id = ces.ces_tce_id
        INNER JOIN usuarios AS usu
        ON usu.usu_id = ces.ces_usu_id_director
        INNER JOIN servicio_territorial AS ste
        ON ste.ste_id = ter.ter_ste_id
        WHERE 1
        ORDER BY ces.ces_id DESC;";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $arreglo_datos['CAMPOS'] = $campos;
        $arreglo_datos['CAMPOS_EXPORTAR'] = $campos_exportar;
        $arreglo_datos['VALORES'] = $resultado;
        $arreglo_datos['OPCIONES'] = $opciones;
        $arreglo_datos['PROMOTOR'] = $usu_id;
        $arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
        $arreglo_datos['ACCION_ELIMINAR']="fm_eliminar";
    
        $contenido .= $_obj_interfaz->crearListadocentrosExportar($arreglo_datos);

        $_obj_interfaz->asignarContenido($contenido);
        break;
}

?>