<?php

class Centros{

    var $_plantillas = "";

    function __construct() {
        global $_PATH_SERVIDOR;
        $this->_plantillas = $_PATH_SERVIDOR."/centros/Plantillas";
    }

    /** abrirFormularioRegistrarcentros
     * parametro: $datos
     * autor : BDL
     * descripcion: ABRE EL CONTENIDO DE REGISTRO DE CENTROS
    **/
    function abrirFormularioRegistrarcentros($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registrar.html");

        $label_inicio = "Centre Escolar";
        $msg_descripcion = "Registre de Centre Escolar";
        $breadcump_centros = "Centre Escolar";
        $input_centro_nombre = "Nombre Centre*";
        $input_centro_direccion = "ADREÇA*";
        $input_centro_codigo_barrio = "Barri*";
        $input_centro_territorio = "Territori";
        $input_centro_tipo_centro = "Tipus Centre Escolar";
        $input_centro_max_complejidad = "Màxima Complexitat*";
        $input_centro_director = "Director*";
        $input_centro_servicio_territorial = "Servei Territorial*";
        $input_centro_num_suceptibles = "TOTAL D'ALUMNES SUSCEPTIBLES DE PARTICIPAR EN EL PROJECTE";
        $input_centro_num_suceptibles_ad = "TOTAL D'ALUMNES SUSCEPTIBLES";

        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }

        $tipo_centros_escolares = $this->obteberSelectFormaTipoCentro($datos);
        $servicio_territorial = $this->obteberSelectServicioTerritorial($datos);
        //$territorio = $this->obteberSelectFormaTerritorios($datos);
        $director = $this->obteberSelectFormaDirector($datos);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_centros", $breadcump_centros, $contenido);

        Interfaz::asignarToken("input_centro_nombre", $input_centro_nombre, $contenido);
        Interfaz::asignarToken("input_centro_direccion", $input_centro_direccion, $contenido);
        Interfaz::asignarToken("input_centro_codigo_barrio", $input_centro_codigo_barrio, $contenido);
        Interfaz::asignarToken("input_centro_servicio_territorial", $input_centro_servicio_territorial, $contenido);
        Interfaz::asignarToken("input_centro_territorio", $input_centro_territorio, $contenido);
        Interfaz::asignarToken("input_centro_tipo_centro", $input_centro_tipo_centro, $contenido);
        Interfaz::asignarToken("input_centro_max_complejidad", $input_centro_max_complejidad, $contenido);
        Interfaz::asignarToken("input_centro_director", $input_centro_director, $contenido);
        Interfaz::asignarToken("select_centro_tipo_centro", $tipo_centros_escolares, $contenido);
        Interfaz::asignarToken("select_centro_servicio_territorial", $servicio_territorial, $contenido);
        Interfaz::asignarToken("input_centro_num_suceptibles", $input_centro_num_suceptibles, $contenido);
        Interfaz::asignarToken("input_centro_num_suceptibles_ad", $input_centro_num_suceptibles_ad, $contenido);
        //Interfaz::asignarToken("select_centro_territorio", $territorio, $contenido);
        Interfaz::asignarToken("select_centro_director", $director, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    function registrarcentros($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        // if(!isset($_SESSION))
        // {
        //     session_start();
        // }

        $datos = Herramientas::trimCamposFormulario($datos);

        //var_dump($datos);
        //exit();
        $campos = array(    
        "ces_nombre", 	
        "ces_direccion",
        "ces_bar",
        "ces_ter_id",
        "ces_tce_id", 
        "ces_maxcomplejidad",
        "ces_usu_id_director",
        "ces_num_gitanos",
        );        
        
        $datos['tabla'] = 'centro_escolar';
        $datos['ces_bar'] = $datos['ces_bar'];
        $datos['ces_ter_id'] = intval($datos['ces_ter_id']);
        $datos['ces_tce_id'] = intval($datos['ces_tce_id']);
        $datos['ces_usu_id_director'] = intval($datos['ces_usu_id_director']);
        //$datos['ces_num_gitanos'] = intval($datos['ces_num_gitanos']);

        if(isset($datos['ces_maxcomplejidad']) && $datos['ces_maxcomplejidad'] == "1"){
            $datos['ces_maxcomplejidad'] = 1;
        } else if(!isset($datos['ces_maxcomplejidad'])){
            $datos['ces_maxcomplejidad'] = 0;
        }

        $sql = $_obj_database->generarSQLInsertar($datos, $campos);

        $res = $_obj_database->ejecutarSql($sql);

        if($res == 1){
            return 'CNT-01';
        } else {
            return 'CNT-04';
        }

    }

    function obteberSelectFormaTipoCentro($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT tce_id, tce_nombre 
        FROM tipos_centro_escolar 
        WHERE 1"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $ces_tce_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['ces_tce_id'])){
                $ces_tce_id = $valores['ces_tce_id'];
            }
        }
        
        if (count($resultados) === 0) {
            $select = "<select class='form-select' name='ces_tce_id' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar tipus centre escolar</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' name='ces_tce_id' aria-label='Default select example' required>";
            $select .= "<option selected>Seleccionar tipus centre escolar</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['tce_id'] == $ces_tce_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['tce_id']."' ".$selected.">".$resultado['tce_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
        
    }

    function obteberSelectFormaTerritorios($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $servicio_territorial = $datos['ste_id'];

        // $est_cur_id = "";
        // if($datos['TIPO_ACCION'] == "editar"){
        //     if(isset($valores['est_cur_id'])){
        //         $etapa_educativa = $valores['est_edu_id'];
        //         $est_cur_id = $valores['est_cur_id'];
        //     }
        // }

        $sql = "SELECT DISTINCT(ter_id), ter_nombre
                FROM territorio 
                WHERE 1;"; 

        $select = "";
        $ces_ter_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['ces_ter_id'])){
                $ces_ter_id = $valores['ces_ter_id'];
                
                $sql_ste = "SELECT DISTINCT(ter_id), ter_ste_id
                FROM territorio 
                WHERE ter_id = ".$ces_ter_id.";";
                $res_ste = $_obj_database->obtenerRegistrosAsociativos($sql_ste);

                $servicio_territorial = $res_ste[0]['ter_ste_id'];
            }
        }

        $sql2 = "SELECT DISTINCT(ter_id), ter_nombre
                FROM territorio 
                WHERE ter_ste_id = ".$servicio_territorial.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql2);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='ces_ter_id' name='ces_ter_id' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar territori</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='ces_ter_id' name='ces_ter_id' aria-label='Default select example' required>";
            $select .= "<option value='0' selected>Seleccionar territori</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ter_id'] == $ces_ter_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ter_id']."' ".$selected.">".$resultado['ter_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }

    function obteberSelectServicioTerritorial($datos,$valores = NULL){
        global $_obj_database;

        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $sql = "SELECT DISTINCT(ste_id), ste_nombre
                FROM servicio_territorial 
                WHERE 1;"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        $select = "";
        $ces_ste_id = "";
        if($datos['TIPO_ACCION'] == "editar"){
            $ces_ter_id = $this->obtenerDatosCentro($datos['ces_id']);

            $territorio = $ces_ter_id[0]['ces_ter_id'];
            
            $sql_ste = "SELECT DISTINCT(ter_id), ter_ste_id
            FROM territorio 
            WHERE ter_id = ".$territorio.";"; 
            $res_ste = $_obj_database->obtenerRegistrosAsociativos($sql_ste);

            $ces_ste_id = $res_ste[0]['ter_ste_id'];
        }

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='ces_ste_id' name='' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar servei territorial</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='ces_ste_id' name='' aria-label='Default select example' required>";
            $select .= "<option value='0' selected>Seleccionar servei territorial</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['ste_id'] == $ces_ste_id){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['ste_id']."' ".$selected.">".$resultado['ste_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;
    }

    function obteberSelectFormaDirector($datos,$valores = NULL){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $etapa_educativa = $datos['edu_id'];

        $ces_usu_id_director = "";
        if($datos['TIPO_ACCION'] == "editar"){
            if(isset($valores['ces_usu_id_director'])){
                $etapa_educativa = $valores['est_edu_id'];
                $ces_usu_id_director = $valores['ces_usu_id_director'];
            }
        }

        $sql = "SELECT DISTINCT(usu_id), usu_nombre
                FROM usuarios 
                WHERE usu_per_id = 3;"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);

        if (count($resultados) === 0) {
            $select = "<select class='form-select' id='ces_usu_id_director' name='ces_usu_id_director' aria-label='Default select example' disabled>";
            $select .= "<option>Seleccionar Director</option>";
            $select .="</select>";
            return $select;
        }else {
            $select = "<select class='form-select' id='ces_usu_id_director' name='ces_usu_id_director' aria-label='Default select example' required>";
            $select .= "<option selected>Seleccionar Director</option>";
            foreach($resultados as $resultado){
                $selected = "";
                if($resultado['usu_id'] == $ces_usu_id_director){
                    $selected = "selected";
                }
                $select .= "<option value='".$resultado['usu_id']."' ".$selected.">".$resultado['usu_nombre']."</option>";
            }
            $select .="</select>";
        }

        return $select;        
    }

    function listadocentros($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_listar_centros.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
        $label_inicio = "Centre Educatiu";
        $msg_descripcion = "Llistat Centre Educatiu";
        $breadcump_centros = "Centre Educatiu";

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_centros_listar", $breadcump_centros, $contenido);

        return $contenido;
    }

    function Eliminarcentros($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        if (!$_obj_database->iniciarTransaccion()) {
            //problma al inicar la transaccion
            return -1003;
        }
        $ces_id = $datos["ces_id"];
        $sql = "DELETE FROM centro_escolar WHERE ces_id ='".$ces_id."';";

        if (!$_obj_database->ejecutarSql($sql)) {
            $_obj_database->cancelarTransaccion();
            return -1003;
        } else {
            $_obj_database->terminarTransaccion();
        }

        return 'CNT-03';
        
    }

    function abrirFormularioEditarcentros($datos,$valores){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_centros.html");

        $label_inicio = "Centre Escolar";
        $msg_descripcion = "Registre de Centre Escolar";
        $breadcump_centros = "Centre Escolar";
        $input_centro_nombre = "Nombre Centre*";
        $input_centro_direccion = "ADREÇA*";
        $input_centro_codigo_barrio = "Barri*";
        $input_centro_territorio = "Territori";
        $input_centro_tipo_centro = "Tipus Centre Escolar";
        $input_centro_max_complejidad = "Màxima Complexitat*";
        $input_centro_director = "Director*";
        $input_centro_servicio_territorial = "Servei Territorial*";
        $input_centro_num_suceptibles = "TOTAL D'ALUMNES SUSCEPTIBLES DE PARTICIPAR EN EL PROJECTE";
        $input_centro_num_suceptibles_ad = "TOTAL D'ALUMNES SUSCEPTIBLES";


        if(isset($datos['mensaje'])){
            $mensaje = $datos['mensaje'];
        }
        //<input class="form-check-input" name="ces_maxcomplejidad" type="checkbox" value="1" id="<!--input_centro_max_complejidad-->">
        $valores = $valores[0];

        $tipo_centros = $this->obteberSelectFormaTipoCentro($datos,$valores);
        $servicio_territorial = $this->obteberSelectServicioTerritorial($datos);
        $territorio = $this->obteberSelectFormaTerritorios($datos,$valores);
        $director = $this->obteberSelectFormaDirector($datos,$valores);
        $max_comlejidad = $this->obteberCheckFormaMaxComplejidad($datos);

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_centros", $breadcump_centros, $contenido);


        Interfaz::asignarToken("input_centro_nombre", $input_centro_nombre, $contenido);
        Interfaz::asignarToken("ces_centro_nombre",$valores["ces_nombre"], $contenido);
        Interfaz::asignarToken("input_centro_direccion", $input_centro_direccion, $contenido);
        Interfaz::asignarToken("ces_centro_direccion", $valores["ces_direccion"], $contenido);
        Interfaz::asignarToken("input_centro_servicio_territorial", $input_centro_servicio_territorial, $contenido);
        Interfaz::asignarToken("input_centro_territorio", $input_centro_territorio, $contenido);
        Interfaz::asignarToken("select_centro_territorio", $territorio, $contenido);
        Interfaz::asignarToken("input_centro_codigo_barrio", $input_centro_codigo_barrio, $contenido);
        Interfaz::asignarToken("ces_centro_barrio", $valores["ces_bar"], $contenido);
        Interfaz::asignarToken("input_centro_tipo_centro", $input_centro_tipo_centro, $contenido);
        Interfaz::asignarToken("select_centro_tipo_centro", $tipo_centros, $contenido);
        Interfaz::asignarToken("input_centro_director", $input_centro_director, $contenido);
        Interfaz::asignarToken("select_centro_director", $director, $contenido);
        Interfaz::asignarToken("check_centro_max_complejidad", $max_comlejidad, $contenido);
        Interfaz::asignarToken("input_centro_max_complejidad", $input_centro_max_complejidad, $contenido);
        Interfaz::asignarToken("ces_centro_id", $valores["ces_id"], $contenido);
        Interfaz::asignarToken("select_centro_servicio_territorial", $servicio_territorial, $contenido);
        Interfaz::asignarToken("input_centro_num_suceptibles", $input_centro_num_suceptibles, $contenido);
        Interfaz::asignarToken("input_centro_num_suceptibles_ad", $input_centro_num_suceptibles_ad, $contenido);
        Interfaz::asignarToken("ces_centro_num_suceptibles", $valores['ces_num_gitanos'], $contenido);
        //Interfaz::asignarToken("select_alumn_curso_etapa_educativa", $cursos, $contenido);
        Interfaz::asignarToken("mensaje", $mensaje, $contenido);

        return $contenido;
    }

    function obteberCheckFormaMaxComplejidad($datos){
        global $_obj_database;
        
        if(isset($_SESSION))
        {
            $usu_id = $_SESSION['usu_id'];
        }

        $datos = Herramientas::trimCamposFormulario($datos);

        $ces_id = $datos['ces_id'];

        $sql = "SELECT ces_maxcomplejidad
                FROM centro_escolar 
                WHERE ces_id = ".$ces_id.";"; 
        
        $resultados = $_obj_database->obtenerRegistrosAsociativos($sql);
        if (count($resultados) === 0) {
            $check = "<input class='form-check-input' name='ces_maxcomplejidad' type='checkbox' value='1'>";
            return $check;
        }else {
            $checked = "";
            if($resultados[0]['ces_maxcomplejidad'] === "1"){
                $checked = "checked";
            } else if($resultados[0]['ces_maxcomplejidad'] === "0") {
                $checked = "";
            }
            $check = "<input class='form-check-input' name='ces_maxcomplejidad' type='checkbox' value='1' $checked>";
        }

        return $check;
    }

    function obtenerDatosCentro($ces_id) {
        global $_obj_database;

        $sql = "SELECT * FROM centro_escolar
				WHERE ces_id=$ces_id";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    function editarcentros($datos){
        global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

        // if(!isset($_SESSION))
        // {
        //     session_start();
        // }

        $datos = Herramientas::trimCamposFormulario($datos);

        //var_dump($datos);
        //exit();
        $campos = array(    
            "ces_nombre", 	
            "ces_direccion",
            "ces_bar",
            "ces_ter_id",
            "ces_tce_id", 
            "ces_maxcomplejidad",
            "ces_usu_id_director",
            "ces_num_gitanos"
        );  
        
        $datos['tabla'] = 'centro_escolar';
        $datos['ces_bar'] = $datos['ces_bar'];
        $datos['ces_ter_id'] = intval($datos['ces_ter_id']);
        $datos['ces_tce_id'] = intval($datos['ces_tce_id']);
        $datos['ces_usu_id_director'] = intval($datos['ces_usu_id_director']);
        if(isset($datos['ces_maxcomplejidad']) && $datos['ces_maxcomplejidad'] == "1"){
            $datos['ces_maxcomplejidad'] = 1;
        } else if(!isset($datos['ces_maxcomplejidad'])){
            $datos['ces_maxcomplejidad'] = 0;
        }
        $datos['condicion'] = "ces_id = '" . $datos['ces_id'] . "' ";

        $res = $_obj_database->actualizarDatosTabla($datos, $campos);

        if (!$res) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }

        if($res == 1){
            return 'CNT-02';
        } else {
            return 'CNT-05';
        }
        
    }

    function listadocentrosExportar($datos){
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/listar_centros_exportar.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);	
        $label_inicio = "Centre Educatiu";
        $msg_descripcion = "Llistat Centre Educatiu";
        $breadcump_centros = "Centre Educatiu";

        Interfaz::asignarToken("label_msg_inicio", $label_inicio, $contenido);
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
        Interfaz::asignarToken("input_centros_listar", $breadcump_centros, $contenido);

        return $contenido;
    }

}
?>